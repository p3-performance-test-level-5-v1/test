#!/usr/bin/env bash
apt-get install -y jq

generate_post_data(){
  cat <<EOF
{
  "hook_info":{
    "type":"bitrise",
    "build_trigger_token":"${BITRISE_BUILD_TOKEN}"
  },
  "build_params":{
    "branch":"${BITBUCKET_BRANCH}",
    "workflow_id":"${WORKFLOW_ID}",
    "environments":[
      {
        "mapped_to":"API_ENV",
        "value":"${API_ENV}",
        "is_expand":true
      },
      {
        "mapped_to":"APP_NAME",
        "value":"${APP_NAME}",
        "is_expand":true
      },
      {
        "mapped_to":"API_ENV",
        "value":"${API_ENV}",
        "is_expand":true
      },
      {
        "mapped_to":"school",
        "value":"${school}",
        "is_expand":true
      },
      {
        "mapped_to":"VERSION_NUMBER",
        "value":"${VERSION_NUMBER}",
        "is_expand":true
      },
      {
        "mapped_to":"provision_profile",
        "value":"${provision_profile}",
        "is_expand":true
      },
      {
        "mapped_to":"Bundle_Identififer",
        "value":"${Bundle_Identififer}",
        "is_expand":true
      },
      {
        "mapped_to":"APP_ID",
        "value":"${APP_ID}",
        "is_expand":true
      },
      {
        "mapped_to":"App_Bundle_ID",
        "value":"${App_Bundle_ID}",
        "is_expand":true
      },
      {
        "mapped_to":"Icons",
        "value":"${Icons}",
        "is_expand":true
      },
      {
        "mapped_to":"default_provision_profile",
        "value":"${default_provision_profile}",
        "is_expand":true
      }
    ]
  },
  "triggered_by":"curl"
}
EOF
}

triggerBuild() {
  RESPONSE=$(curl -s "${BUILD_WORKFLOW}" --header 'Content-Type: application/json' -d "$(generate_post_data)")
  RES_BUILD_NUM=$( jq -r  '.build_number' <<< "${RESPONSE}" )
  RES_URL=$( jq -r  '.build_url' <<< "${RESPONSE}" )
  RES_STATUS=$( jq -r  '.status' <<< "${RESPONSE}" )
  RES_MSG=$( jq -r  '.message' <<< "${RESPONSE}" )
}


printErr() {
  echo "----------------------------------"
  echo "----------------------------------"
  echo "Build failed"
  echo "Build status: ${RES_STATUS}"
  echo "Message: ${RES_MSG}"
  echo "----------------------------------"
  echo "----------------------------------"
}

printSuccess() {
  echo "----------------------------------"
  echo "----------------------------------"
  echo "Build number: ${RES_BUILD_NUM}"
  echo "Build status: ${RES_STATUS}"
  echo "Message: ${RES_MSG}"
  echo "Click on Build URL : ${RES_URL}"
  echo "----------------------------------"
  echo "----------------------------------"
}

setSchoolBasedVars() {
  if [[ $1 == "1" ]]; then
    # mfs
    SCHOOL="mfs"
    SCHOOL_UPPERCASE="MFS"
    APP_NAME="MFS Kiosk"
    provision_profile="NTUC SkoolNet MFS Kiosk Dev Distribution"
    Bundle_Identififer="com.ntuc.skoolnet.mfs.kiosk"
    APP_ID="1491459012"
    App_Bundle_ID="com.ntuc.skoolnet.mfs.kiosk.dev"
    Icons="lsh/ic_launcher_dev"
    default_provision_profile="NTUC SkoolNet TCC Kiosk"
    API_ENV="dev"
    echo "===Data for MFS"
  elif [[ $1 == "2" ]]; then
    # lsh
    SCHOOL="lsh"
    SCHOOL_UPPERCASE="LSH"
    APP_NAME="LSH Kiosk"
    provision_profile="NTUC SkoolNet LSH Kiosk Dev Distribution"
    Bundle_Identififer="com.ntuc.skoolnet.lsh.kiosk.dev"
    APP_ID="1491459012"
    App_Bundle_ID="com.ntuc.skoolnet.lsh.kiosk.dev"
    Icons="lsh/ic_launcher_dev"
    default_provision_profile="NTUC SkoolNet TCC Kiosk"
    API_ENV="dev"
    echo "===Data for LSH"
  else
     echo "schoolID was not defined"
     exit 1
  fi
}


schoolID=$1
# API_ENV=$2
echo $schoolID

echo "====do.sh===="
echo $API_ENV

setSchoolBasedVars $1
triggerBuild

echo $RESPONSE

if [[ $RES_STATUS = 'error' ]]; then
    printErr
    exit 1
fi

printSuccess
