#!/usr/bin/env bash
# fail if any commands fails
set -ex

# set env variables from bitbucket to bitrise env variables. direct env override is not allowed
if [ ! -z "${APP_NAME}" ] ; then
  envman add --key APP_NAME --value "${APP_NAME}"
fi

if [ ! -z "${DEFAULT_APP_NAME}" ] ; then
  envman add --key DEFAULT_APP_NAME --value "${DEFAULT_APP_NAME}"
fi

if [ ! -z "${API_ENV}" ] ; then
  envman add --key API_ENV --value "${API_ENV}"
fi

if [ ! -z "${school}" ] ; then
  envman add --key school --value "${school}"
fi

if [ ! -z "${VERSION_NUMBER}" ] ; then
  envman add --key APP_ICON_FOLDER --value "${VERSION_NUMBER}"
fi

if [ ! -z "${provision_profile}" ] ; then
  envman add --key provision_profile --value "${provision_profile}"
fi

if [ ! -z "${Bundle_Identififer}" ] ; then
  envman add --key Bundle_Identififer --value "${Bundle_Identififer}"
fi

if [ ! -z "${APP_ID}" ] ; then
  envman add --key APP_ID --value "${APP_ID}"
fi

if [ ! -z "${App_Bundle_ID}" ] ; then
  envman add --key App_Bundle_ID --value "${App_Bundle_ID}"
fi

if [ ! -z "${Icons}" ] ; then
  envman add --key Icons --value "${Icons}"
fi

if [ ! -z "${default_provision_profile}" ] ; then
  envman add --key default_provision_profile --value "${default_provision_profile}"
fi



