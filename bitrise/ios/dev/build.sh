# set -e
# debug log
set -ex
bash ./bitrise/ios/dev/envman.sh

# Replace variables with input values
CUSTOM_APP_NAME=${APP_NAME}
echo ${APP_NAME}

CUSTOM_DEFAULT_APP_NAME=${DEFAULT_APP_NAME}
echo ${DEFAULT_APP_NAME}

CUSTOM_API_ENV=${API_ENV}
echo $API_ENV

CUSTOM_SCHOOL=${school}
echo ${SCHOOL}

CUSTOM_VERSION_NUMBER=${VERSION_NUMBER}
echo $VERSION_NUMBER

CUSTOM_PROVISION_PROFILE=${provision_profile}
echo $PROVISION_PROFILE

CUSTOM_BUNDLE_IDENTIFIER=${Bundle_Identififer}
echo $BUNDLE_IDENTIFIER

CUSTOM_APP_ID=${APP_ID}
echo $APP_ID

CUSTOM_APP_BUNDLE_ID=${App_Bundle_ID}
echo $APP_BUNDLE_ID

CUSTOM_ICONS=${Icons}
echo $ICONS

CUSTOM_DEFAULT_PROVISION_PROFILE=${default_provision_profile}
echo ${default_provision_profile}

if [[ -n "$CUSTOM_APP_NAME" ]]; then
  APP_NAME="${CUSTOM_APP_NAME}"
  echo "Replace APP_NAME Variable"
  echo $APP_NAME
fi

if [[ -n "$CUSTOM_DEFAULT_APP_NAME" ]]; then
  DEFAULT_APP_NAME="${CUSTOM_DEFAULT_APP_NAME}"
  echo "Replace DEFAULT_APP_NAME Variable"
  echo $DEFAULT_APP_NAME
fi

if [[ -n "$CUSTOM_API_ENV" ]]; then
  API_ENV="${CUSTOM_API_ENV}"
  echo "Replace API_ENV Variable"
  echo $API_ENV
fi

if [[ -n "$CUSTOM_SCHOOL" ]]; then
  SCHOOL="${CUSTOM_SCHOOL}"
  echo "Replace SCHOOL Variable"
  echo $SCHOOL
fi

if [[ -n "$CUSTOM_VERSION_NUMBER" ]]; then
  VERSION_NUMBER="${CUSTOM_VERSION_NUMBER}"
  echo "Replace VERSION_NUMBER Variable"
  echo $VERSION_NUMBER
fi

if [[ -n "$CUSTOM_PROVISION_PROFILE" ]]; then
  PROVISION_PROFILE="${CUSTOM_PROVISION_PROFILE}"
  echo "Replace PROVISION_PROFILE Variable"
  echo $PROVISION_PROFILE
fi

if [[ -n "$CUSTOM_BUNDLE_IDENTIFIER" ]]; then
  BUNDLE_IDENTIFIER="${CUSTOM_BUNDLE_IDENTIFIER}"
  echo "Replace BUNDLE_IDENTIFIER Variable"
  echo $BUNDLE_IDENTIFIER
fi

if [[ -n "$CUSTOM_APP_ID" ]]; then
  APP_ID="${CUSTOM_APP_ID}"
  echo "Replace APP_ID Variable"
  echo $APP_ID
fi

if [[ -n "$CUSTOM_APP_BUNDLE_ID" ]]; then
  APP_BUNDLE_ID="${CUSTOM_APP_BUNDLE_ID}"
  echo "Replace APP_BUNDLE_ID Variable"
  echo $APP_BUNDLE_ID
fi

if [[ -n "$CUSTOM_ICONS" ]]; then
  ICONS="${CUSTOM_ICONS}"
  echo "Replace ICONS Variable"
  echo $ICONS
fi

if [[ -n "$CUSTOM_DEFAULT_PROVISION_PROFILE" ]]; then
  DEFAULT_PROVISION_PROFILE="${CUSTOM_DEFAULT_PROVISION_PROFILE}"
  echo "Replace DEFAULT_PROVISION_PROFILE Variable"
  echo $DEFAULT_PROVISION_PROFILE
fi

# build for MFS of sn_kiosk app

if [[ "$SCHOOL" == "mfs" ]]; then
  SCHOOL_UPPERCASE="MFS"
  SCHOOL_ID=1
fi

if [[ "$SCHOOL" == "lsh" ]]; then
  SCHOOL_UPPERCASE="LSH"
  SCHOOL_ID=2
fi

echo "SCHOOL_ID"
echo ${SCHOOL_ID}

cp src/redux/config/MFS/index-dev.js src/redux/config/index.js

sed -i '' "s/'TCC'/'$SCHOOL_UPPERCASE'/g" src/redux/config/index.js
sed -i '' "s/SCHOOL_ID = 3/SCHOOL_ID = $SCHOOL_ID/g" src/config/config.js


cp ios/GoogleService-Info.${API_ENV}.plist ios/GoogleService-Info.plist
cp ./ios/GoogleService-Info.${API_ENV}.plist ./ios/GoogleService-Info.plist

sed -i '' "s/TCC Kiosk/$APP_NAME/g" ios/sn_kiosk/Info.plist


sed -i '' "s/${Bundle_Identififer}/${APP_BUNDLE_ID}/g" ios/sn_kiosk.xcodeproj/project.pbxproj
sed -i '' "s/${default_provision_profile}/${provision_profile}/g" ios/sn_kiosk.xcodeproj/project.pbxproj

if [[ "$SCHOOL" == "lsh" ]]; then
    cp src/images/icons/LSH/icon\ ${API_ENV}\ 1024x1024.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 1024x1024.png
    cp src/images/icons/LSH/icon\ ${API_ENV}\ 167x167.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 167x167.png
    cp src/images/icons/LSH/icon\ ${API_ENV}\ 152x152.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 152x152.png
    cp src/images/icons/LSH/icon\ ${API_ENV}\ 76x76.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 76x76.png
    cp src/images/icons/LSH/icon\ ${API_ENV}\ 40x40.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 40x40.png
fi

if [[ "$SCHOOL" == "mfs" ]]; then
    cp src/images/icons/MFS/icon\ ${API_ENV}\ 1024x1024.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 1024x1024.png
    cp src/images/icons/MFS/icon\ ${API_ENV}\ 167x167.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 167x167.png
    cp src/images/icons/MFS/icon\ ${API_ENV}\ 152x152.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 152x152.png
    cp src/images/icons/MFS/icon\ ${API_ENV}\ 76x76.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 76x76.png
    cp src/images/icons/MFS/icon\ ${API_ENV}\ 40x40.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 40x40.png
fi