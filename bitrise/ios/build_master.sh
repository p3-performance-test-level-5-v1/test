#!/usr/bin/env bash
set -ex

checkVars(){
    while read -r line || [ -n "$line" ]; do
        [ -z "${!line}"] && { echo "$line is empty or not set. Exiting..."; exit 1;}
    done
}

checkFiles(){
    while read -r line || [ -n "$line"]; do
        [ ! -f "${!line}"] && { echo "$line not found. Exiting..."; exit 1;}
    done
}

checkDirectory(){
    while read -r line || [ -n "$line" ]; do
        [ ! -d "${!line}"] && { echo "$line not found. Exiting..."; exit 1;}
    done
}

checkVars cat <<EOF | sh
    app_name
    deep_link_prefix
    school_id
    api_env
    ios_app_id
    ios_icon_launcher
EOF

if [[ "${school_id}" = "1" ]]; then
    school_lowercase="mfs"
    school_uppercase="MFS"
elif [[ "${school_id}" = "2"]]; then
    school_lowercase="lsh"
    school_uppercase="LSH"
else 
    echo "school_id: $school_id is not supported."
    exit 1
fi 

checkVars cat <<EOF | sh
    school_lowercase
    school_uppercase
EOF

if [[ ${api_env} == "dev" ]]; then
    CONFIG_FILE_PATH=src/config/${school_lowercase}/config-${api_env}.js
elif [[ ${api_env} == "stg" ]]; then
    CONFIG_FILE_PATH=src/config/${school_lowercase}/config-${api_env}.js
elif [[ ${api_env} == "prd" ]]; then
    CONFIG_FILE_PATH=src/config/${school_lowercase}/config-${api_env}.js
else
    CONFIG_FILE_PATH=src/config/feature/config-feature-${api_env}.js
fi

checkFiles cat <<EOF | sh
    CONFIG_FILE_PATH
EOF

# changing school name in config TODO copy first and then change config.js then copy it to shared repo
sed -i '' "s/SCHOOL = '[a-zA-Z]*'/SCHOOL = '${school_uppercase}'/g" "${CONFIG_FILE_PATH}"
echo "replaced : $(grep -rnw "$CONFIG_FILE_PATH" -e 'SCHOOL')"

# changing deep link on config.js
sed -i '' "s#DEEP_LINK_PREFIX = .*#DEEP_LINK_PREFIX = '${deep_link_prefix}://';#g" "${CONFIG_FILE_PATH}"
echo "replaced : $(grep -rnw "$CONFIG_FILE_PATH" -e 'DEEP_LINK_PREFIX')"

# set config
rm -f src/config/config
rm -f node_modules/sn-reactjs-mobile-component/src/config.js
cp "$CONFIG_FILE_PATH" src/config/config.js
cp "$CONFIG_FILE_PATH" node_modules/sn-reactjs-mobile-component/src/config.js

# copy google-info.plist file
cp ios/TCCParent/GoogleService-Info.plist.${api_env}.plist ios/TCCParent/GoogleService-Info.plist
cp ./ios/TCCParent/GoogleService-Info.plist.${api_env}.plist ./ios/TCCParent/GoogleService-Info.plist
