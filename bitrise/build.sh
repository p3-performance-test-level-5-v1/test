bash ./bitrise/ios/dev/envman.sh

# variables
APP_NAME = ${APP_NAME}
API_ENV = ${API_ENV}
SCHOOL = ${school}
VERSION_NUMBER = ${VERSION_NUMBER}
PROVISION_PROFILE = ${provision_profile}
BUNDLE_IDENTIFIER = ${Bundle_Identififer}
APP_ID = ${APP_ID}
APP_BUNDLE_ID = ${App_Bundle_ID}
ICONS = ${Icons}
DEFAULT_PROVISION_PROFILE = ${default_provision_profile}

# Replace variables with input values
SCHOOL_ID=1

if [[ -n "$CUSTOM_SCHOOL" ]]; then
  SCHOOL=${CUSTOM_SCHOOL}
  echo "Replace SCHOOL Variable"
  echo $SCHOOL
fi

if [[ -n "$CUSTOM_API_ENV" ]]; then
  API_ENV=${CUSTOM_API_ENV}
  echo "Replace API_ENV Variable"
  echo $API_ENV
fi

if [[ -n "$CUSTOM_APP_NAME" ]]; then
  APP_NAME="${CUSTOM_APP_NAME}"
  echo "Replace APP_NAME Variable"
  echo $APP_NAME
fi


# Check if it's devX env
if [[ "$API_ENV" == "dev1" || "$API_ENV" == "dev2" || "$API_ENV" == "dev3" || "$API_ENV" == "deva" || "$API_ENV" == "devb" || "$API_ENV" == "devc" || "$API_ENV" == "devw" || "$API_ENV" == "devy" || "$API_ENV" == "devz" ]]; then
  IS_DEVX_ENV=1
else
  IS_DEVX_ENV=0
fi


# Update the school uppercase
if [[ "$SCHOOL" == "tcc" ]]; then
  SCHOOL_UPPERCASE="TCC"
  SCHOOL_ID=3
fi

if [[ "$SCHOOL" == "lsh" ]]; then
  SCHOOL_UPPERCASE="LSH"
  SCHOOL_ID=2
fi

if [[ "$SCHOOL" == "mfs" ]]; then
  SCHOOL_UPPERCASE="MFS"
  SCHOOL_ID=1
fi

echo "SCHOOL_UPPERCASE"
echo $SCHOOL_UPPERCASE



if [[ $IS_DEVX_ENV == 1 ]]; then
  cp src/redux/config/feature/index-${API_ENV}.js src/redux/config/index.js
else
  cp src/redux/config/${SCHOOL_UPPERCASE}/index-${API_ENV}.js src/redux/config/index.js
fi

sed -i -e "s/'LSH'/'$SCHOOL_UPPERCASE'/g" src/redux/config/index.js
sed -i -e "s/SCHOOL_ID = 3/SCHOOL_ID = $SCHOOL_ID/g" src/config/config.js



cp ios/GoogleService-Info.dev.plist ios/GoogleService-Info.plist


if [[ "$API_ENV" == "dev" || "$API_ENV" == "stg" ]]; then
  cp ./ios/GoogleService-Info.${API_ENV}.plist ./ios/GoogleService-Info.plist
else
  if [[ "$SCHOOL" == "mfs" ]]; then
    cp ./ios/GoogleService-Info.${SCHOOL}.prod.plist ./ios/GoogleService-Info.plist
  fi

  if [[ "$SCHOOL" == "lsh" ]]; then
    cp ./ios/GoogleService-Info.${SCHOOL}.prod.plist ./ios/GoogleService-Info.plist
  fi
fi


sed -i -e "s/TCC Kiosk/$APP_NAME/g" ios/sn_kiosk/Info.plist

sed -i -e 's/com.ntuc.skoolnet.tcc.kiosk/com.ntuc.skoolnet.tcc.kiosk.dev/g' ios/sn_kiosk.xcodeproj/project.pbxproj
sed -i -e "s/${default_provision_profile}/${provision_profile}/g" ios/sn_kiosk.xcodeproj/project.pbxproj



if [[ "$API_ENV" == "dev1" || "$API_ENV" == "dev2" || "$API_ENV" == "dev3" || "$API_ENV" == "deva" || "$API_ENV" == "devb" || "$API_ENV" == "devc" || "$API_ENV" == "devw" || "$API_ENV" == "devy" || "$API_ENV" == "devz" ]]; then
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ dev\ 1024x1024.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 1024x1024.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ dev\ 167x167.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 167x167.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ dev\ 152x152.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 152x152.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ dev\ 76x76.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 76x76.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ dev\ 40x40.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 40x40.png
elif [[ "$API_ENV" == "stg" ]]; then
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ staging\ 1024x1024.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 1024x1024.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ staging\ 167x167.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 167x167.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ staging\ 152x152.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 152x152.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ staging\ 76x76.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 76x76.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ staging\ 40x40.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 40x40.png
else
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ ${API_ENV}\ 1024x1024.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 1024x1024.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ ${API_ENV}\ 167x167.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 167x167.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ ${API_ENV}\ 152x152.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 152x152.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ ${API_ENV}\ 76x76.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 76x76.png
  cp src/images/icons/${SCHOOL_UPPERCASE}/Icon\ ${API_ENV}\ 40x40.png ios/sn_kiosk/Images.xcassets/AppIcon.appiconset/Icon\ 40x40.png
fi
