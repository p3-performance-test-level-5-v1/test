#!/usr/bin/env bash
#set -ex
cp ./config.env.template.bk ./config.env.template

generate_post_data(){
  cat ./bitrise/payload.json
}

triggerBuild() {
  apt-get install -y jq
  RESPONSE=$(curl -s "${BUILD_WORKFLOW}" --header 'Content-Type: application/json' -d "$(generate_post_data)")
  RES_BUILD_NUM=$( jq -r  '.build_number' <<< "${RESPONSE}" )
  RES_URL=$( jq -r  '.build_url' <<< "${RESPONSE}" )
  RES_STATUS=$( jq -r  '.status' <<< "${RESPONSE}" )
  RES_MSG=$( jq -r  '.message' <<< "${RESPONSE}" )

  echo "RESPONSE==="
  echo "$RESPONSE"

  if [[ $RES_STATUS = 'error' ]]; then
      echo "----------------------------------"
      echo "----------------------------------"
      echo "Build failed"
      echo "Build status: ${RES_STATUS}"
      echo "Message: ${RES_MSG}"
      echo "----------------------------------"
      echo "----------------------------------"
      exit 1
  fi

  echo "----------------------------------"
  echo "----------------------------------"
  echo "Build number: ${RES_BUILD_NUM}"
  echo "Build status: ${RES_STATUS}"
  echo "Message: ${RES_MSG}"
  echo "Click on Build URL : ${RES_URL}"
  echo "----------------------------------"
  echo "----------------------------------"

  sleep 10
  echo "sleep for 10s"
}

checkVars() {
  while read -r line || [ -n "$line" ]; do
    [ -z "${!line}" ] && { echo "$line is empty or not set. Exiting.."; exit 1; }
  done
}
checkFiles() {
  while read -r line || [ -n "$line" ]; do
    [ ! -f "${!line}" ] && { echo "$line not found. Exiting.."; exit 1; }
  done
}

if [ -z "${OS}" ]; then
  OS="ios"
fi

checkVars cat <<EOF | sh
  SchoolID
  API_ENV
  PIPELINE_CONFIG_FILE
  OS
  BITRISE_BUILD_TOKEN
  BITBUCKET_BRANCH
  WORKFLOW_ID
  BUILD_WORKFLOW
  MFS_DEEP_LINK
  LSH_DEEP_LINK
  APP_ID
EOF

config_file=./bitrise/config/$PIPELINE_CONFIG_FILE
checkFiles cat <<EOF | sh
  config_file
EOF

cat $config_file > ./config.ini

cat ./config.ini

# Check for missing variables from template
(source ./config.ini && checkVars cat <<EOF | sh
  $(grep -oE "\[%[^%]+%\]" config.env.template | sort | uniq | sed -E "s/\[%([^%]+)%\]/\1/g")
EOF
)
cat ./config.ini

echo "BITRISE_BUILD_TOKEN = $BITRISE_BUILD_TOKEN"

sed -i "s/school_id=.*/school_id='${SchoolID}'/g" ./config.ini
sed -i "s/api_env=.*/api_env='${API_ENV}'/g" ./config.ini
sed -i "s/os=.*/os='${OS}'/g" ./config.ini

if [[ "${SchoolID}" = "1" ]]; then
  school_lowercase="mfs"
  sed -i "s/deep_link_prefix=.*/deep_link_prefix='${MFS_DEEP_LINK}'/g" ./config.ini
elif [[ "${SchoolID}" = "2" ]]; then
  sed -i "s/deep_link_prefix=.*/deep_link_prefix='${LSH_DEEP_LINK}'/g" ./config.ini
  school_lowercase="lsh"
else
  echo "school_id : $SchoolID is not supported."
  exit 1
fi

if [[ "${OS}" = "android" ]]; then
  WORKFLOW_ID=$WORKFLOW_ID
elif [[ "${OS}" = "ios" ]]; then
  WORKFLOW_ID=$WORKFLOW_ID
  echo "WORKFLOW_ID-====:$WORKFLOW_ID "
  iosAppIdJson="$( echo $(cat ./bitrise/config/ios_app_id.json))"
  echo "iosAppIdJson = $iosAppIdJson"

  appType=$( jq -r  '.appType' <<< "${iosAppIdJson}" )
  echo "appType = $appType"
  
  schoolType=$school_lowercase"_"$appType"_"$API_ENV
  echo "schoolType = $schoolType"
  
  dataSchoolType=$( jq -r  .$schoolType <<< "${iosAppIdJson}" )
  echo "dataSchoolType: $dataSchoolType"
  
  appID=$( jq -r  '.appID' <<< "${dataSchoolType}" )
  echo "appID = $appID"
  
  profileID=$( jq -r  '.profileID' <<< "${dataSchoolType}" )
  echo "profileID = $profileID"
  
else
  echo "OS : $OS is not supported."
  exit 1
fi

ios_app_id=$appID

{
  echo school_lowercase="'${school_lowercase}'"
  echo school_uppercase="'$(echo ${school_lowercase} | awk '{print toupper($0)}')'"
  echo api_env_uppercase="'$(echo ${API_ENV} | awk '{print toupper($0)}')'"
  echo ios_app_id="'${ios_app_id}'"
} >> ./config.ini

# initial substitution
(source ./config.ini && grep -oE "\[%[^%]+%\]" config.env.template | sort | uniq | sed -E "s/\[%([^%]+)%\]/\1/g" | while read line; do sed -i "s#\[%${line}%\]#'${!line}'#g" config.env.template; done;)
# secondary substitution, to support depth=2 templating
(source ./config.ini && grep -oE "\[%[^%]+%\]" config.env.template | sort | uniq | sed -E "s/\[%([^%]+)%\]/\1/g" | while read line; do \sed -i "s#\[%${line}%\]#${!line}#g" config.env.template; done;)

source ./config.env.template

# write payload
last_line=$(( $(wc -l < ./config.env.template) + 1))
current_line=0
jsonObjTemplate=$(cat <<'EOF'
{
  "mapped_to":"var_name",
  "value":"var_val",
  "is_expand":true
}
EOF
)

touch ./tmp.txt
jsonStr=""
while read line || [ -n "$line" ]; do
  current_line=$(($current_line + 1))
  obj=$jsonObjTemplate
  key=$(echo $line | cut -d'=' -f 1 | sed -E "s/'//g")
  val=$(echo $line | cut -d'=' -f 2 | sed -E "s/'//g")
  echo $obj > ./tmp.txt
  sed -i "s/var_name/$key/g" ./tmp.txt
  sed -i "s/var_val/$val/g" ./tmp.txt
  obj=$(cat ./tmp.txt)
  echo $obj
  if [[ $current_line -ne $last_line ]]; then
    jsonStr+="$obj,"
  else
    jsonStr+="$obj"
  fi
done < ./config.env.template

echo "Payload --"
echo "$jsonStr"
echo ""

rm ./tmp.txt

COMMIT_MESSAGE="Build for ${API_ENV} with school ID : ${SchoolID} with config: ${PIPELINE_CONFIG_FILE}"
sed -i 's#"${JSON_PAYLOAD}"#'"$jsonStr"'#g' ./bitrise/payload.json
sed -i 's#"${BITRISE_BUILD_TOKEN}"#"'"$BITRISE_BUILD_TOKEN"'"#g' ./bitrise/payload.json
sed -i 's#"${BITBUCKET_BRANCH}"#"'"$BITBUCKET_BRANCH"'"#g' ./bitrise/payload.json
sed -i 's#"${COMMIT_MESSAGE}"#"'"$COMMIT_MESSAGE"'"#g' ./bitrise/payload.json

echo "work-flow"


sed -i 's#"${WORKFLOW_ID}"#"'"$WORKFLOW_ID"'"#g' ./bitrise/payload.json
triggerBuild

rm ./config.ini ./config.env.template ./bitrise/payload.json
