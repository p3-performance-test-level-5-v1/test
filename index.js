/**
 * @format
 */
import {AppRegistry} from 'react-native';
import React from 'react';
import App from './src/App';
import { Provider } from 'react-redux';
import { store, persist } from "./src/store/store";
import {name as appName} from './app.json';

const RNRedux = () => (
    <Provider store={ store } persistor={ persist }>
      <App />
    </Provider>
  )

AppRegistry.registerComponent(appName, () => RNRedux);

