import { StyleSheet } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { scaledHeight, scaledFont } from '../../utils/Scale';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
  },
  contentContainerStyle: {
    flexGrow: 1,
    paddingHorizontal: 100,
    paddingBottom: baseStyles.padding50,
    alignItems: 'center',
  },
  avatarContainer: {
    marginTop: 90,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    marginBottom: baseStyles.margin20,
  },
  checkInTextStyle: {
    textAlign: 'center',
    marginTop: baseStyles.margin25,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
    fontSize: baseStyles.labelFontSize24,
  },
  checkInDescStyle: {
    textAlign: 'center',
    lineHeight: 30,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.textInputFont16,
    marginBottom: baseStyles.margin25,
  },
  addDescStyle: {
    alignSelf: 'flex-start',
    lineHeight: 30,
    color: baseStyles.avatharTextColor,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    marginTop: baseStyles.margin40,
    marginBottom: baseStyles.margin15,
  },
  checkInHealthStyle: {
    marginBottom: baseStyles.margin5,
  },
  listTemperature: {
    flexGrow: 0,
    flexShrink: 0,
  },
  separator: {
    height: scaledHeight(10),
  },
  textInputContainer: {
    borderBottomColor: '#dadada',
    paddingTop: baseStyles.padding5,
    height: scaledHeight(35),
    borderBottomWidth: 2,
  },
  pickerContainer: {
    flexDirection: 'row',
    marginTop: baseStyles.margin25,
  },
  errorMessageStyle: {
    marginTop: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkRed,
  },
  textInputStyle: {
    fontSize: baseStyles.textInputFont16,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  labelStyle: {
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  timePickerContainer: {
    flex: 1,
    marginRight: baseStyles.margin15,
  },
  datePickerContainer: {
    flex: 1,
    marginLeft: baseStyles.margin15,
  },
  get addHealthBtnStyle() {
    return {
      alignSelf: 'center',
      marginTop: baseStyles.margin50 * 3,
      backgroundColor: CustomConfig.Colors.primaryColor,
      ...buttonCommonStyle.butttonStyle,
      ...buttonCommonStyle.commonStyle,
    }
  },
  checkInTxtStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  textInputStyle: {
    fontSize: baseStyles.buttonFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  textAreaStyle: {
    width: '100%',
    marginTop: baseStyles.margin15,
    height: baseStyles.textareaViewHeight,
    borderColor: baseStyles.lightGrey,
    borderRadius: baseStyles.defaultBorderRadius,
    padding: baseStyles.padding10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1,
    fontSize: scaledFont(18)
  },
  textFieldContainer: {
    width: '100%',
    paddingHorizontal: 120,
    marginVertical: baseStyles.margin25
  },
  remarksText: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  remarksCountText: {
    marginTop: baseStyles.margin5,
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
    textAlign: 'right',
  },
});
