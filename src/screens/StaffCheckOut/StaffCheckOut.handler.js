import { useState } from 'react';
import Constant from '../../constants/Constant';
import { get } from 'lodash';
import { 
  errorResponse, 
  handleErrorDataFromApi, 
  retrieveAsyncStorage 
} from '../../utils';

export default Handler = (props) => {
  const {
    navigation,
    updateErrorMessage,
    staffCheckInOutV3,
    staffToken,
  } = props;

  let isApiRequested = false;
  const [isRequesting, setIsRequesting] = useState(false);
  const [remarks, setRemarks] = useState('');

  const checkOutAction = async () => {
    const userId = navigation.getParam('userId', '');
    const staffName = navigation.getParam('staffName', '');
    let centreId = await retrieveAsyncStorage('centreId');

    try {
      if (isApiRequested) return;
      isApiRequested = true;
      setIsRequesting(true);
      const result = await staffCheckInOutV3(
        {
          IDCentre: centreId,
          type: Constant.TYPE_CHECK_OUT,
          remarks,
          userId,
        },
        staffToken
      );
      isApiRequested = false;
      setIsRequesting(false);
  
      if (get(result, 'success', false)) {
        navigation.navigate('StaffSignInSuccess', { 
          staffName, 
          actionType: Constant.TYPE_CHECK_OUT 
        });
        return;
      }
  
      const message = handleErrorDataFromApi(result, errorResponse);
      message && updateErrorMessage(message, checkOutAction);
    } catch (error) {
      isApiRequested = false;
      setIsRequesting(false);
      throw error;
    }
  };

  return {
    setRemarks,
    checkOutAction,
    isRequesting,
    remarks,
  };
};
