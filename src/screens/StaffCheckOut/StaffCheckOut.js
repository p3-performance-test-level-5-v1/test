import React from 'react';
import { connect } from 'react-redux';
import { 
  View, 
  Text, 
  TouchableOpacity, 
  ScrollView, 
  TextInput 
} from 'react-native';
import { get } from 'lodash';
import AvatarNavigator from '../../components/AvatarNavigator';
import styles from './StaffCheckOut.style';
import Handler from './StaffCheckOut.handler';
import { t } from '../../utils/LocalizationUtils.js';
import Header from '../../components/Header';
import {
  updateErrorMessage,
  staffCheckInOutV3,
} from '../../redux/actions';
import { commonStyle } from '../../config/commonStyle';
import Constant from '../../constants/Constant';
import { baseStyles } from '../../config/baseStyles';
import withPreventDoubleClick from '../../components/TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

const StaffCheckOut = (props) => {
  const { navigation } = props;

  const {
    setRemarks,
    checkOutAction,
    isRequesting,
    remarks,
  } = Handler(props);

  const staffName = navigation.getParam('staffName', '');
  const staffImageKey = navigation.getParam('staffImageKey', '');

  return (
    <View style={styles.container}>
      <Header 
        backButton 
        navigation={navigation} 
        style={commonStyle.headerStyle} 
      />
      <View style={styles.contentContainer}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentContainerStyle}
        >
          <View style={styles.avatarContainer}>
            <AvatarNavigator
              imageUrl={staffImageKey}
              avatharText={staffName}
              customHeight={200}
              customWidth={200}
              customFontSize={80}
            />
          </View>
          <Text style={styles.checkInTextStyle}>
            {t('common.kioskWelcomeText', { staffName })}
          </Text>
          <Text style={styles.checkInDescStyle}>
            {t('common.checkoutRemarkDescription')}
          </Text>
          <View style={styles.textFieldContainer}>
            <Text style={styles.remarksText}>
              {t('common.remarkFieldTitle')}
            </Text>
            <TextInput
              multiline
              value={remarks}
              autoCorrect={false}
              autoCapitalize="none"
              placeholder={t('common.remarkPlaceholderText')}
              style={[
                styles.textInputStyle, 
                styles.textAreaStyle
              ]}
              maxLength={Constant.CHARACTER_LIMIT}
              onChangeText={value => setRemarks(value)}
            />
            <Text style={styles.remarksCountText}>
              {`${Constant.CHARACTER_LIMIT - remarks.length}/${Constant.CHARACTER_LIMIT}`}
            </Text>
          </View>

          <TouchableButton
            disabled={isRequesting}
            style={[
              styles.addHealthBtnStyle,
              { opacity: isRequesting ? baseStyles.disabledOpacity : 1 },
            ]}
            onPress={checkOutAction}
          >
            <Text style={styles.checkInTxtStyle}>{t('common.camelCheckOut')}</Text>
          </TouchableButton>
        </ScrollView>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  schoolConfig: get(state, 'schoolConfig', {}),
  staffHealthRecords: get(state, 'getStaffHealthRecord.data', []),
  staffToken: get(state, 'loginByPin.data'),
});

const mapDispatchToProps = {
  updateErrorMessage,
  staffCheckInOutV3,
};

export default connect(mapStateToProps, mapDispatchToProps)(StaffCheckOut);
