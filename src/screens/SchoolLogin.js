import React, { Component } from 'react';
import {
	View,
	Text,
	TouchableOpacity,
	ImageBackground,
	TextInput,
	Image,
	Dimensions,
	StyleSheet
} from 'react-native';
import {
	Dropdown
} from 'react-native-material-dropdown';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import {
	getCheckInOutToken,
	updateErrorMessage,
	listAllCentre, 
	centreLogin,
} from '../redux/actions';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import { errorResponse, getAppEnv, handleErrorDataFromApi } from '../utils';
import { baseStyles } from '../config/baseStyles';
import analytics from '@react-native-firebase/analytics';
const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import { getSchoolId } from '../utils/SchoolConfig';
import CustomConfig from '../utils/CustomConfig';

class SchoolLogin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			allCentres: [],
			centre: '',
			centreID: '',
			userName: '',
			password: '',
			loginClicked: false,
			secureTextEntry: true,
			centreFocus: false,
			pwdFocus: false, 
			userNameFocus: false
		}
	}
	async componentDidMount() {
		analytics().setCurrentScreen('schoollogin');
		this.getAllCentreList();
	}
	async getAllCentreList() {
		const schoolId = getSchoolId();
		const reqData = { IDSchool: schoolId };
		this.props.listAllCentre(reqData).then(data => {
			if (data.success) {
				const allCentres = _.get(data, 'data.findAllCentreForSchool.data', []);
				this.setState({ allCentres });
				return;
			}
			const message = handleErrorDataFromApi(data, errorResponse);
			message && this.props.updateErrorMessage(message, this.getAllCentreList.bind(this))
		});
	}
	onAccessoryPress() {
		this.setState(({ secureTextEntry }) => ({
			secureTextEntry: !secureTextEntry
		}));
	}
	onSignInAction() {
		const { centreID, userName, password } = this.state;
		if (centreID && userName.trim().length > 0 && password.trim().length > 0) {
			this.setState({ loginClicked: true });
			const reqData = {
				IDCentre: centreID,
				email: userName,
				password
			}

			const { navigation } = this.props;
			this.props.centreLogin(reqData, navigation).then(data => {
				this.setState({ loginClicked: false });
				if (data.success) {
					const { centreID } = this.state;
					this.props.navigation.navigate('Home');
					return;
				}
				const message = handleErrorDataFromApi(data, errorResponse);
				message && this.props.updateErrorMessage(message, this.onSignInAction.bind(this));
			});
		}
	}
	renderPasswordAccessory() {
		const { secureTextEntry } = this.state;
		const name = secureTextEntry ? "eye-off" : "eye";
		return (
			<Feather
				size={scaledFont(16)}
				name={name}
				color={"#dadada"}
				onPress={this.onAccessoryPress.bind(this)}
			/>
		);
	}
	onCentreChange(value, index, data){
		this.setState({
			centre: data[index].label,
			centreID: data[index].ID
		});
	}
	onTextFieldChange(fieldName, text) {
		const thisState = this.state;
		const newState = Object.assign({ ...thisState }, { [fieldName]: text });
		this.setState(newState);
	}
	onFocus(fieldName) {
		this.setState({ [fieldName]: true });
	}
	renderForm() {
		const {
			allCentres,
			secureTextEntry,
			centre,
			centreFocus,
			pwdFocus,
			userNameFocus,
			userName,
			password,
			centreID
		} = this.state;
		const {
			schoolConfig,
		} = this.props;

		const disableCheck = centreID && userName && userName.trim().length > 0 && password && password.trim().length > 0;
		const isDisabled = disableCheck ? false : true;
		

		return (
			<View
	        style={styles.formView}>
	          <View
	          style={{
	            height: baseStyles.inputViewHeight
	          }}>
	            <Text style={styles.labelTxt}>{t('common.kioskCentreText')}</Text>
	            <Dropdown
								affixTextStyle={styles.latoFont}
								itemTextStyle={styles.latoFont}
								labelTextStyle={styles.latoFont}
								style={styles.latoFont}
	              fontSize={baseStyles.textInputFont}
	              containerStyle={{
	                width: baseStyles.baseWidth
	              }}
	              value={centre}
	              textColor={baseStyles.labelColor}
	              labelHeight={5}
	              renderAccessory={() => {
	                return <MaterialIcons
	                  size={baseStyles.inputFontSize}
	                  name='keyboard-arrow-down'
	                  color="#4a4a4a"
	                />
	              }}
	              data={allCentres}
	              onChangeText={this.onCentreChange.bind(this)}
	              placeholder={t('common.kioskSelectOne')}
	              label=''
	            />
	          </View>
	          <View
	          style={{
	            height: baseStyles.inputViewHeight
	          }}>
	            <Text
	            style={styles.labelTxt}>{t('common.kioskUsername')}</Text>
	            <TextInput
	              placeholder={t('common.kioskPlaceHolderForEmail')}
	              placeholderTextColor='#dadada'
	              keyboardType='email-address'
	              autoCapitalize='none'
	              onFocus={this.onFocus.bind(this, 'userNameFocus')}
	              onEndEditing={() => this.setState({ userNameFocus: false })}
	              value={userName}
	              onChangeText={this.onTextFieldChange.bind(this, 'userName')}
	              style={[
	                styles.userNameField,
	                { borderBottomColor: userNameFocus ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada' }
	              ]}
	            />
	          </View>
	          <View
	          style={{
	            height: baseStyles.inputViewHeight
	          }}>
	            <Text
	            style={styles.labelTxt}>{t('common.password')}</Text>
	            <TextField
	              placeholder={t('common.password')}
	              label=""
	              tintColor={baseStyles.labelColor}
	              errorColor="#ec3535"
	              style={styles.textInputStyle}
	              value={password}
	              onFocus={this.onFocus.bind(this, 'pwdFocus')}
	              onChangeText={this.onTextFieldChange.bind(this, 'password')}
	              secureTextEntry={secureTextEntry}
	              onEndEditing={() => this.setState({ pwdFocus: false })}
	              renderAccessory={this.renderPasswordAccessory.bind(this)}
	              inputContainerStyle={[
	                styles.passwordField,
	                { borderBottomColor: pwdFocus ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada' }
	              ]}
	            />
	          </View>
	          <View
	          style={styles.buttonContainer}>
	            <TouchableOpacity 
	            disabled={isDisabled}
	            onPress={this.onSignInAction.bind(this)}
	            style={[
	              styles.signInBtn,
	              { backgroundColor: CustomConfig.Colors.btnPrimaryBgColor }
	            ]}>
	              <Text
	              style={styles.signInBtnTxt}>{t('common.kioskLoginText')}</Text>
	            </TouchableOpacity>
	          </View>
	        </View>
		);
	}

	_renderAppVersion() {
		const { mobileAppVersion } = this.props;
		const appEnv = getAppEnv();
		return (
			<Text style={styles.appVersionStyle}>Version {appEnv && appEnv !== '' ? `${mobileAppVersion} - ${appEnv}` : mobileAppVersion}</Text>
		)
	}

	render() {
		const {
			schoolConfig,
		} = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', '');

		if (school === 'TCC') {
			return <React.Fragment>
				<View style={styles.container}>
					<Image
						resizeMode='contain'
						style={styles.schoolLogo}
						source={require('../images/tcc-logo.png')}
					/>
					{this.renderForm()}
					{this._renderAppVersion()}
				</View>
			</React.Fragment>
		}
		return (
			<React.Fragment>
				<ImageBackground
				resizeMode='cover'
				style={styles.container}>
					{this.renderForm()}
					{this._renderAppVersion()}
				</ImageBackground>
			</React.Fragment>
    	);
	}
}

const mapStateToProps = state => ({
	allCentres: state.listAllCentre,
	loginData: state.centreLogin,
	schoolConfig: state.schoolConfig,
	mobileAppVersion: _.get(state, 'commonVariableValues.localAppVersion', ''),
});

const mapDispatchToProps = ({
	listAllCentre,
	getCheckInOutToken,
	updateErrorMessage,
	centreLogin,
});

export default connect(mapStateToProps, mapDispatchToProps)(SchoolLogin);

const styles = StyleSheet.create({
	container: {
		width: deviceWidth,
		height: deviceHeight,
		alignItems: 'center',
		justifyContent: 'center'
	},
	formView: {
		height: deviceHeight * 0.4,
		width: baseStyles.baseWidth,
		alignItems: 'center',
		justifyContent: 'center'
	},
	labelTxt: {
		color: baseStyles.labelColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoRegular
	},
	userNameField: {
		fontSize: baseStyles.textInputFont,
		borderBottomWidth: 1,
		color: baseStyles.labelColor,
		width: baseStyles.baseWidth,
		height: scaledHeight(38),
		fontFamily: baseStyles.latoRegular
	},
	passwordField: {
		width: baseStyles.baseWidth,
		paddingTop: scaledHeight(5),
		height: scaledHeight(38),
		borderBottomWidth: 1
	},
	signInBtn: {
		height: scaledHeight(54),
		width: baseStyles.baseWidth,
		alignItems: 'center',
		borderRadius: 2,
		justifyContent: 'center'
	},
	forgotBtn: {
		borderRadius: 2,
		paddingVertical: scaledHeight(15),
		paddingHorizontal: scaledWidth(20)
	},
	forgotTxt: {
		fontFamily: baseStyles.latoRegular,
		fontSize: baseStyles.buttonFontSize
	},
	signInBtnTxt: {
		color: '#ffffff',
		fontFamily: baseStyles.latoBold,
		fontSize: baseStyles.labelFontSize
	},
	buttonContainer: {
		flex: 0.3,
		alignItems: 'center',
		marginTop: scaledHeight(30),
		justifyContent: 'space-evenly'
	},
	textInputStyle: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.labelColor,
		fontFamily: baseStyles.latoRegular
	},
	schoolLogo: {
		height: scaledHeight(200),
		width: scaledWidth(230)
	},
	appVersionStyle: {
		justifyContent: 'flex-end',
		fontSize: baseStyles.buttonFontSize,
		textAlign: "center",
		position: 'absolute',
		bottom: 20
	},
	latoFont: {
		fontFamily: baseStyles.latoRegular
	}
});
