import React from 'react';
import { View, Text, ScrollView, FlatList, TextInput, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { last } from 'lodash';

import Header from '../../components/Header';
import { commonStyle } from '../../config/commonStyle';
import { baseStyles } from '../../config/baseStyles';
import DateRangePickerField from '../../components/RangeDatepicker';
import Checkbox from '../../components/Checkbox';
import DateTimePicker from '../../components/DateTimePicker';
import SNDropdown from '../../components/SNDropdown';

import MissingRecordHandler from './MissingRecord.handler';
import styles from './MissingRecord.style';
import { t } from '../../utils/LocalizationUtils.js';
import Constant, { TIME_FORMAT } from '../../constants/Constant';
import withPreventDoubleClick from '../../components/TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

const MissingRecord = (props) => {
  const { navigation } = props;
  const {
    endDate,
    startDate,
    toDate,
    availableDates,
    onDatePicked,
    onLeaveCheck,
    isLeave,
    selectedDates,
    onTimeChange,
    reason,
    reasons,
    setReason,
    remark,
    setRemark,
    addMissingRecords,
    staffCICORecords,
    isRequesting,
  } = MissingRecordHandler(props);

  const invalidTime = !!selectedDates.find((item) => !item.isValid);

  const markedAllCheckInOut = !isLeave && 
    !!selectedDates.find((item) => (
      (!item.checkInTime && !item.isRemoveCheckin) ||
      (!item.checkOutTime && !item.isRemoveCheckout)
    ));

  const isRequireReason = !isLeave && !reason?.ID;

  const isRequireRemark = isLeave || reason?.label === 'others';

  const isMoreMargin = isLeave || !toDate;

  const isDisableSubmit = !toDate || invalidTime || markedAllCheckInOut || (isRequireRemark && !remark.trim()) || isRequireReason;

  const lastRecord = last(staffCICORecords);
  const lastTimeFormat = lastRecord
    ? moment(lastRecord.time, TIME_FORMAT.DATA_TRANFERS).format(
      TIME_FORMAT.TIME_PICKER
    )
    : '';

  const renderLastTime = (time, isCheckIn) => (
    <View style={{ marginTop: baseStyles.margin10 }}>
      <Text
        style={[
          styles.txtCICO,
          { color: baseStyles.darkShadeGray },
        ]}
      >
        {isCheckIn ? t('common.lastCheckInTime') : t('common.lastCheckOutTime')}
      </Text>
      <Text
        style={{
          color: baseStyles.darkShadeGray,
          marginTop: baseStyles.margin10,
        }}
      >
        {time}
      </Text>
    </View>
  );

  const renderCheckInCheckOutItem = ({ item }) => {
    const { checkInTime, checkOutTime, isValid, value, isRemoveCheckout, isRemoveCheckin } = item;

    const baseColor = isValid ? baseStyles.lightGrey : baseStyles.darkRed;

    return (
      <View style={styles.CICOItemContainer}>
        <Text style={styles.txtCICODate}>{moment(value).format('DD MMM YYYY')}</Text>
        {(lastRecord && isRemoveCheckout) && renderLastTime(lastTimeFormat, false)}
        <View style={styles.CICOContainer}>
          <View style={styles.checkInContainer}>
            {isRemoveCheckin ? (
              renderLastTime(lastTimeFormat, true)
            ) : (
              <>
                <Text
                  style={[
                    styles.txtCICO,
                    { color: !checkInTime ? baseStyles.darkShadeGray : baseStyles.darkGrayTintColor },
                  ]}
                >
                  {t('common.checkInTime')}
                  <Text style={styles.requireMark}>*</Text>
                </Text>

                <DateTimePicker
                  onDateTimeChange={onTimeChange(item, true)}
                  pickerMode="time"
                  dateTime={checkInTime}
                  pickerFomat="hh:mm A"
                  iconColor={baseStyles.grayishBlue}
                  iconName="chevron-down"
                  iconSize={24}
                  inputContainerStyle={styles.dateTimeInput}
                  inputTextStyle={styles.txtDateTime}
                  baseColor={baseColor}
                />
              </>
            )}
          </View>

          <View style={styles.checkOutContainer}>
            {!isRemoveCheckout && (
              <>
                <Text
                  style={[
                    styles.txtCICO,
                    {
                      color: !checkOutTime ? baseStyles.darkShadeGray : baseStyles.darkGrayTintColor,
                    },
                  ]}
                >
                  {t('common.checkOutTime')}
                  <Text style={styles.requireMark}>*</Text>
                </Text>

                <DateTimePicker
                  onDateTimeChange={onTimeChange(item, false)}
                  pickerMode="time"
                  dateTime={checkOutTime}
                  pickerFomat="hh:mm A"
                  iconColor={baseStyles.grayishBlue}
                  iconName="chevron-down"
                  iconSize={24}
                  inputContainerStyle={styles.dateTimeInput}
                  inputTextStyle={styles.txtDateTime}
                  baseColor={baseColor}
                />
              </>
            )}
          </View>
        </View>

        {!isValid && <Text style={styles.errorMessageStyle}>{t('common.invalidCICOTime')}</Text>}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header backButton navigation={navigation} style={commonStyle.headerStyle} />
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.bodyContainer}>
          <Text style={styles.title}>{t('common.addMissingRecords')}</Text>
          <Text style={styles.description}>{t('common.missingRecordsDesc')}</Text>

          <View style={styles.formContainer}>
            <Text style={styles.txtDateRange}>{t('common.dateRange')}</Text>
            <View style={styles.rowDateRange}>
              <View style={styles.startDateContainer}>
                <Text style={styles.txtDate}>{moment(startDate).format('DD/MM/YYYY')}</Text>
              </View>
              <Text style={styles.txtDate}>to</Text>
              <DateRangePickerField
                placeHolderText={t('common.selectDate')}
                customPlaceholderTexColor={baseStyles.darkGrayTintColor}
                containerStyle={{ marginLeft: baseStyles.margin30, flex: 1 }}
                initialMonth={moment(startDate).format('YYYYMM')}
                maxMonth={moment(endDate).diff(moment(startDate), 'month') + 2}
                selectedTextColor={baseStyles.white}
                startDate={moment(startDate).format('YYYYMMDD')}
                untilDate={toDate && moment(toDate).format('YYYYMMDD')}
                availableDates={availableDates}
                onConfirm={onDatePicked}
                value={toDate ? moment(toDate).format('DD/MM/YYYY') : ''}
                isFixedStartDate
                baseColor={baseStyles.lightGrey}
              />
            </View>

            <View style={styles.leaveCheckContainer}>
              <Checkbox
                isChecked={isLeave}
                label={t('common.onLeaveDesc')}
                textStyle={styles.txtLeave}
                iconSize={baseStyles.buttonSize20}
                iconColor={baseStyles.grayishBlue}
                onChecked={onLeaveCheck}
              />
            </View>

            {!!toDate && !isLeave && (
              <>
                <FlatList
                  data={selectedDates}
                  keyExtractor={(item) => item.id}
                  renderItem={renderCheckInCheckOutItem}
                />

                <View style={styles.reasonContainer}>
                  <Text style={styles.txtLabel}>
                    {t('common.reason')}
                    <Text style={styles.requireMark}>*</Text>
                  </Text>
                  <SNDropdown
                    placeHolderasText
                    placeholder={t('common.selectReason')}
                    selectedOption={reason}
                    options={reasons}
                    handleChange={setReason}
                  />
                </View>
              </>
            )}

            <Text style={styles.txtLabelRemark}>
              {t('common.remarks')}
              {isRequireRemark && (
                <Text h10 style={styles.requireMark}>
                  *
                </Text>
              )}
            </Text>
            <View style={styles.textareaContainer}>
              <TextInput
                value={remark}
                onChangeText={setRemark}
                multiline
                numberOfLines={4}
                style={styles.txtField}
                placeholderTextColor={baseStyles.lightGrey}
                maxLength={Constant.CHARACTER_LIMIT}
              />
            </View>
            <Text style={styles.remarksCountText}>
              {`${Constant.CHARACTER_LIMIT - remark.length}/${Constant.CHARACTER_LIMIT}`}
            </Text>
          </View>

          <TouchableButton
            disabled={isDisableSubmit || isRequesting}
            style={[
              styles.submitBtnStyle,
              { opacity: (isDisableSubmit || isRequesting) ? baseStyles.disabledOpacity : 1 },
              isMoreMargin && { marginTop: 270 },
            ]}
            onPress={addMissingRecords}
          >
            <Text style={styles.txtSubmit}>{t('common.addRecord')}</Text>
          </TouchableButton>
        </ScrollView>
      </View>
    </View>
  );
};

export default MissingRecord;
