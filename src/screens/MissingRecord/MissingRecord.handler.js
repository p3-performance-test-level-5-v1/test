import { useState, useEffect } from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { get, isEmpty, first, last } from 'lodash';

import { getDatesInRange } from '../../utils/Common';
import { retrieveAsyncStorage, hasMarkedAllRecords } from '../../utils';
import Constant, { TIME_FORMAT } from '../../constants/Constant';
import { t } from '../../utils/LocalizationUtils';

import {
  getStaffMissingDatesRange,
  getStaffMissingRecordsReason,
  getStaffCICORecords,
  addStaffMissingRecords,
  showCustomAlert,
  checkStaffHasMissingRecord,
} from '../../redux/actions';

const { TIME_PICKER, DATA_TRANFERS } = TIME_FORMAT;

const MissingRecordHandler = (props) => {
  const { navigation } = props;

  let isApiRequested = false;
  const [isLeave, setIsLeave] = useState(false);
  const [toDate, setToDate] = useState(null);
  const [availableDates, setAvailableDates] = useState([]);
  const [selectedDates, setSelectedDates] = useState([]);
  const [remark, setRemark] = useState('');
  const [reason, setReason] = useState({});
  const [isRequesting, setIsRequesting] = useState(false);

  const startDate = useSelector((state) =>
    moment(get(state, 'getStaffMissingDateRange.data.from'), TIME_FORMAT.DATA_TRANFERS).startOf('d').valueOf()
  );
  const endDate = useSelector((state) =>
    moment(get(state, 'getStaffMissingDateRange.data.to'), TIME_FORMAT.DATA_TRANFERS).endOf('d').valueOf()
  );

  const { actionType: typeCheckInOut } = get(navigation, 'state.params', {});

  const isCheckOut = typeCheckInOut === Constant.TYPE_CHECK_OUT;
  const isCheckIn = typeCheckInOut === Constant.TYPE_CHECK_IN;

  const reasons = useSelector((state) => get(state, 'getMissingRecordReason.data.missingRecordReasons', []));

  const staffCICORecords = useSelector((state) => get(state, 'getStaffCheckInOutRecords.data', []));

  const centreHolidays = useSelector((state) => get(state, 'getCentreHolidays.data', []));

  const dispatch = useDispatch();

  useEffect(() => {
    getMissingDatesRange();
    getReasonList();
  }, []);

  useEffect(() => {
    if (toDate) {
      onToDateChange();
    }
  }, [toDate]);

  useEffect(() => {
    if (!!startDate && !!endDate) {
      const newAvailableDates = getDatesInRange(startDate, endDate, true, centreHolidays).map((item) =>
        moment(item).format('YYYYMMDD')
      );
      setAvailableDates(newAvailableDates);
    }
  }, [startDate, endDate]);

  const onDatePicked = (startDate, untilDate) => {
    if (untilDate) {
      setToDate(moment(untilDate).valueOf());
    }
  };

  const onLeaveCheck = () => {
    setIsLeave(!isLeave);
  };

  const onTimeChange = (selectedItem, isCheckIn) => (time) => {
    const newSelectedDates = selectedDates.map((item) => {
      if (item.id === selectedItem.id) {
        const newDateItem = {
          ...item,
          [isCheckIn ? 'checkInTime' : 'checkOutTime']: time,
        };

        const lastRecord = last(staffCICORecords);
        const mTime = moment(time, TIME_FORMAT.TIME_PICKER);
        const selectedDateTime = moment(selectedItem.value)
          .hour(mTime.hour())
          .minute(mTime.minute());
        
        if (lastRecord) {
          const lastTime = moment(lastRecord.time, TIME_FORMAT.DATA_TRANFERS);
          const isValid = moment(selectedDateTime).isAfter(lastTime);
          if (!isValid) {
            newDateItem.isValid = false;
            return newDateItem;
          }
        }

        const isValid = moment(selectedDateTime).isBefore(moment());
        if (!isValid) {
          newDateItem.isValid = false;
          return newDateItem;
        }

        if (newDateItem.checkInTime && newDateItem.checkOutTime) {
          newDateItem.isValid = moment(newDateItem.checkInTime, TIME_FORMAT.TIME_PICKER).isBefore(
            moment(newDateItem.checkOutTime, TIME_FORMAT.TIME_PICKER)
          );
          return newDateItem;
        }

        return ({
          ...newDateItem,
          isValid: true,
        });
      }

      return item;
    });

    setSelectedDates(newSelectedDates);
  };

  const onToDateChange = () => {
    const isFirstSelection = isEmpty(selectedDates);
    let dateCICOArr = getDatesInRange(startDate, toDate, true, centreHolidays).map((time) => {
      return {
        id: time.toString(),
        value: time,
        checkInTime: '',
        checkOutTime: '',
        isValid: true,
        isRemoveCheckout: false,
        isRemoveCheckin: false,
      };
    });

    if (dateCICOArr.length === 0) {
      setSelectedDates([]);
      return;
    }

    if (staffCICORecords.length > 0) {
      const currentDateCICO = first(dateCICOArr);
      const lastRecord = last(staffCICORecords);
      if (!moment(currentDateCICO.value).isSame(moment(), 'date') && lastRecord) {
        currentDateCICO.isRemoveCheckin = lastRecord.type === Constant.TYPE_CHECK_IN;
        currentDateCICO.isRemoveCheckout = lastRecord.type === Constant.TYPE_CHECK_OUT;
      }
    }

    if (moment(toDate).isSame(moment(), 'date')) {
      const currentDateCICO = last(dateCICOArr);
      if (isCheckOut) currentDateCICO.isRemoveCheckout = true;
      if (isCheckIn) currentDateCICO.isRemoveCheckin = true;
      if (isCheckIn && dateCICOArr.length > 1) dateCICOArr.pop();
    }

    if (!isFirstSelection) {
      dateCICOArr = dateCICOArr.map((item) => {
        const oldDateItem = selectedDates.find((el) => el.id === item.id);
        if (oldDateItem) return oldDateItem;
        return item;
      });
    }

    setSelectedDates(dateCICOArr);
  };

  const getReasonList = async () => {
    await dispatch(getStaffMissingRecordsReason());
  };

  const getMissingDatesRange = async () => {
    try {
      const { staffId } = get(navigation, 'state.params', {});

      let centreId = await retrieveAsyncStorage('centreId');
      const reqData = { staffId, centreId, checkInOutType: typeCheckInOut };

      const data = await dispatch(getStaffMissingDatesRange(reqData));
      if (data) {
        const from = moment(get(data, 'from'), TIME_FORMAT.DATA_TRANFERS)
          .startOf('d')
          .format(TIME_FORMAT.DATA_TRANFERS);
        const to = moment(get(data, 'from'), TIME_FORMAT.DATA_TRANFERS).endOf('d').format(TIME_FORMAT.DATA_TRANFERS);

        return await dispatch(getStaffCICORecords({ staffId, centreId, from, to }));
      }
      return [];
    } catch (error) {
      console.log(error);
      return [];
    }
  };

  const addMissingRecords = async () => {
    try {
      if (isApiRequested) return;
      isApiRequested = true;
      setIsRequesting(true);
      let centreId = await retrieveAsyncStorage('centreId');
      const { staffId } = get(navigation, 'state.params', {});

      const reqData = {
        staffId,
        centreId: Number(centreId),
        remarks: remark,
      };

      if (reason?.ID) {
        reqData.reasonId = reason.ID;
      }

      const missingRecords = [];
      selectedDates.forEach((date) => {
        const { checkInTime, checkOutTime, isRemoveCheckin, isRemoveCheckout } = date;
        const currentTime = moment().format(TIME_PICKER);
        
        if (!isRemoveCheckin) {
          const time = isLeave
            ? moment(currentTime, TIME_PICKER)
            : moment(checkInTime || currentTime, TIME_PICKER);
          const dateTime = moment(date.value)
            .hour(time.hour())
            .minute(time.minute());

          missingRecords.push({
            attendanceType: 'check_in',
            onLeave: isLeave,
            time: moment(dateTime).format(DATA_TRANFERS)
          });
        }
        if (!isRemoveCheckout) {
          const time = isLeave
            ? moment(currentTime, TIME_PICKER)
            : moment(checkOutTime || currentTime, TIME_PICKER);

          let dateTime = moment(date.value)
            .hour(time.hour())
            .minute(time.minute());
          if (moment(dateTime).isAfter(moment())) dateTime = moment();

          missingRecords.push({
            attendanceType: 'check_out',
            onLeave: isLeave,
            time: moment(dateTime).format(DATA_TRANFERS),
          });
        }
      });

      reqData.missingRecords = missingRecords;

      const res = await dispatch(addStaffMissingRecords(reqData));
      isApiRequested = false;
      setIsRequesting(false);

      if (res.error) {
        let title = 'Error';
        let description = get(res, 'error.0.message');
        
        if (typeof get(res, 'error.0', '') === 'string') {
          const message = get(res, 'error.0', '');
          const isNetworkIssue = !message || message.indexOf('timeout of') >= 0 || message.indexOf('Network Error') >= 0;
          
          title =  isNetworkIssue ? t('error.errorPopUpTitleNoInternet') : title;
          description = isNetworkIssue ? t('error.errorPopUpDescNoInternet') : message;
        }
        dispatch(
          showCustomAlert(true, {
            title,
            desc: description,
            primaryButtonText: 'OK',
          })
        );
      } else {
        const result = await dispatch(
          checkStaffHasMissingRecord({
            staffId,
            typeCheckInOut: typeCheckInOut,
          })
        );

        const hasMissingRecord = get(result, 'data.hasMissingRecord', false);

        dispatch(
          showCustomAlert(true, {
            title: 'Success',
            desc: 'Missing record succesfully added',
            primaryButtonText: 'OK',
            primaryButtonCallback: () => onSubmmitSuccess(hasMissingRecord),
          })
        );
      }
    } catch (error) {
      isApiRequested = false;
      setIsRequesting(false);
      dispatch(
        showCustomAlert(true, {
          title: 'Error',
          desc: get(error, 'message', 'Add missing records unsuccessful'),
          primaryButtonText: 'OK',
        })
      );
    }
  };

  const resetForm = () => {
    setIsLeave(false);
    setToDate(null);
    setSelectedDates([]);
    setRemark('');
    setReason({});
  };

  const onSubmmitSuccess = async (stillMissingRecord) => {
    resetForm();

    if (stillMissingRecord) {
      await getMissingDatesRange();
    } else {
      const currentDate = moment().format('DD/MM/YYYY');
      const lastDate = moment(toDate).format('DD/MM/YYYY');
      const isSameDate = moment(lastDate, 'DD/MM/YYYY').isSame(moment(currentDate, 'DD/MM/YYYY'), 'date');
      if (isSameDate && isLeave) {
        navigation.navigate('Home');
        return;
      }
      const { onNoMoreMissingRecord } = get(navigation, 'state.params', {});
      if (onNoMoreMissingRecord) onNoMoreMissingRecord();
    }
  };

  return {
    endDate,
    startDate,
    toDate,
    availableDates,
    onDatePicked,
    isLeave,
    onLeaveCheck,
    selectedDates,
    onTimeChange,
    reason,
    reasons,
    setReason,
    remark,
    setRemark,
    addMissingRecords,
    staffCICORecords,
    isRequesting,
  };
};

export default MissingRecordHandler;
