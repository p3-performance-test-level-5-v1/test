import { StyleSheet } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: baseStyles.bgColor,
  },
  bodyContainer: {
    color: baseStyles.bgColor,
    paddingHorizontal: 150,
    paddingTop: 100,
    paddingBottom: 100,
  },
  title: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize24,
    fontFamily: baseStyles.latoBold,
  },
  description: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize16,
    fontFamily: baseStyles.latoRegular,
    lineHeight: baseStyles.labelFontSize24,
    marginTop: baseStyles.margin10,
  },
  formContainer: {
    marginTop: 45,
    paddingHorizontal: 60,
  },
  txtDateRange: {
    color: baseStyles.darkGrayTintColor,
  },
  rowDateRange: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: baseStyles.margin15,
    marginTop: -baseStyles.margin5,
  },
  startDateContainer: {
    flex: 1,
  },
  txtDate: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize16,
    color: baseStyles.darkShadeGray,
  },
  toDateContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  leaveCheckContainer: {
    marginBottom: baseStyles.margin30,
  },
  txtLeave: {
    fontSize: baseStyles.labelFontSize16,
    color: baseStyles.darkShadeGray,
    marginLeft: baseStyles.margin5,
  },
  CICOItemContainer: {
    marginBottom: baseStyles.margin30,
  },
  CICOContainer: {
    marginTop: baseStyles.margin10,
    flexDirection: 'row',
  },
  checkInContainer: {
    flex: 1,
    marginRight: baseStyles.margin10,
  },
  checkOutContainer: {
    flex: 1,
    marginLeft: baseStyles.margin10,
  },
  txtCICODate: {
    fontFamily: baseStyles.latoBold,
    fontSize: baseStyles.labelFontSize16,
    color: baseStyles.darkShadeGray,
  },
  txtCICO: {
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  dateTimeInput: {
    height: baseStyles.buttonSize30,
  },
  txtDateTime: {
    fontSize: baseStyles.labelFontSize16,
    fontFamily: baseStyles.latoRegular,
  },
  errorMessageStyle: {
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkRed,
  },
  txtLabel: {
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkShadeGray,
  },
  reasonContainer: {
    marginBottom: baseStyles.margin25,
  },
  txtLabelRemark: {
    fontSize: baseStyles.labelSecondaryFontSize,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkShadeGray,
  },
  requireMark: {
    color: baseStyles.statusGrey,
  },
  textareaContainer: {
    borderRadius: baseStyles.borderRadius,
    borderWidth: 1,
    borderColor: baseStyles.lightGrey,
    padding: baseStyles.padding10,
    height: baseStyles.textareaViewHeight,
    marginTop: baseStyles.margin8,
  },
  txtField: {
    fontSize: baseStyles.labelFontSize16,
    color: baseStyles.veryDarkGray,
    paddingTop: 0,
    flex: 1,
  },
  get submitBtnStyle() {
    return {
      alignSelf: 'center',
      marginTop: 90,
      backgroundColor: CustomConfig.Colors.primaryColor,
      ...buttonCommonStyle.butttonStyle,
      ...buttonCommonStyle.commonStyle,
    }
  },
  txtSubmit: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  remarksCountText: {
    marginTop: baseStyles.margin5,
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
    textAlign: 'right',
  },
});

export default styles;
