import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	FlatList,
	TouchableWithoutFeedback,
	TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { baseStyles } from '../config/baseStyles';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { 
	updateErrorMessage,
	listAllBusEntries
} from '../redux/actions';
import { 
	scaledFont 
} from '../utils/Scale';
import { 
	errorResponse,
	retrieveAsyncStorage, 
	handleErrorDataFromApi 
} from '../utils';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import { getFullName } from '../utils/index';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth } = Dimensions.get('window');

class BusRouteList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			busEntries: [],
			selectedBus: {},
		};
	}
	componentDidMount() {
		this.findAllBusEntries();
	}
	findAllBusEntries = async () => {
		const accessToken = await retrieveAsyncStorage('accessToken');
		let fkCentre = await retrieveAsyncStorage('centreId');
		fkCentre = fkCentre ? parseInt(fkCentre) : 0;
		if (accessToken && fkCentre) {
			const { listAllBusEntries, navigation } = this.props;
			const passCode = navigation.getParam('passCode');
			const reqData = { centreID: fkCentre, passcode: passCode };
			const data = await listAllBusEntries(reqData, accessToken);
			if (!data.success) {
				const message = handleErrorDataFromApi(data, errorResponse);
				message && this.props.updateErrorMessage(message, this.findAllBusEntries.bind(this))
				return;
			}
			const busEntryLists = _.get(data, 'data.listAllBusEntries.data', [])
			let busEntries = busEntryLists.filter(entry => (_.get(entry,'busUserByFkBusAttendant.passcode') === passCode));

			// Bus driver can see all routes associated with his phone number
			const busDriverPhoneNumber = _.get(busEntries, '[0].busUserByFkBusDriver.mobilePhone', null);
			if (busDriverPhoneNumber) {
				busEntries = busEntryLists.filter(entry => (_.get(entry,'busUserByFkBusDriver.mobilePhone') === busDriverPhoneNumber));
			}

			busEntries.length > 0 && this.setState({ busEntries });
		}
	}
	goToChildListScreen = () => {
		const { selectedBus } = this.state;
		const { navigation } = this.props;
		const isSignIn = selectedBus.direction === 'arrival' ? true : false;
		navigation.navigate('BusAttendant', {
			selectedBus,
			isSignIn 
		});
	}
	renderBusEntry = ({ item }) => {
		const { selectedBus } = this.state;
		const { schoolConfig } = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', '');
		return (
			<TouchableWithoutFeedback
			onPress={() => this.setState({ selectedBus: item })}>
				<View
				style={{
					...styles.itemStyle,
					...(selectedBus.ID === item.ID && { backgroundColor: baseStyles.lightGreen })
				}}>
					<View 
					style={{
						...styles.itemContainerStyle,
						borderColor: selectedBus.ID === item.ID ? CustomConfig.Colors.btnPrimaryBgColor : baseStyles.darkGray,
					}}>
						<View style={{
							...styles.radioBtnStyle,
							backgroundColor: selectedBus.ID === item.ID ? CustomConfig.Colors.btnPrimaryBgColor : baseStyles.white
						}}/>
					</View>
					<Text style={styles.itemTextStyle}>
						{`${_.get(item, 'bus.plateNumber', '')} - ${_.get(item, 'label', '')} (${item.direction === 'arrival' ? 'To School' : 'From School'})`}
					</Text>
				</View>
			</TouchableWithoutFeedback>
		);
	}
	render() {
		const { navigation } = this.props;
		const { busEntries, selectedBus } = this.state;

		const passCode = navigation.getParam('passCode');
		const busEntry = busEntries.find(bus => (
			_.get(bus, 'busUserByFkBusDriver.passcode') === passCode ||
			_.get(bus, 'busUserByFkBusAttendant.passcode') === passCode
		));
		const firstName = _.get(busEntry, 'busUserByFkBusAttendant.firstname') || '';
		const lastName = _.get(busEntry, 'busUserByFkBusAttendant.lastname') || '';
		const driverName = getFullName(firstName, lastName);

		return (
			<View 
			style={styles.container}>
				<Header
					backButton 
					navigation={navigation}
					style={commonStyle.headerStyle}
				/>
				<View 
				style={styles.container}>
					<View style={styles.topView}>
						<Text style={styles.busAttendantTxt}>
							{t('common.kioskDriverName', { driverName })}
						</Text>
						<Text 
						style={styles.busAttendantTxt}>{t('common.kioskSelectBusText')}</Text>
					</View>
					<View style={styles.flatListView}>
						<FlatList
							data={busEntries} 
							extraData={this.state} 
							style={styles.flatList}
							removeClippedSubviews={false}  
							showsVerticalScrollIndicator={false} 
							keyExtractor={item => item.ID.toString()}
							renderItem={this.renderBusEntry}
						/>
					</View>
					<TouchableOpacity 
					disabled={selectedBus.ID ? false : true}
					onPress={this.goToChildListScreen}
					style={[
						styles.nextBtn,
						styles.primaryColor,
						{ opacity: selectedBus.ID ? 1 : baseStyles.disabledOpacity }
					]}>
						<Text 
						style={styles.nextBtnTxt}>{t('common.kioskNextBtnTxt')}</Text>
					</TouchableOpacity>
				</View>
				<Footer 
				style={styles.footerStyle}/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	centreLoginToken: state && state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	updateErrorMessage,
	listAllBusEntries
});

export default connect(mapStateToProps, mapDispatchToProps)(BusRouteList);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	topView: {
		height: '10%',
		alignItems: 'center',
		width: deviceWidth * 0.65,
		justifyContent: 'space-between'
	},
	busAttendantTxt: {
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold,
		color: baseStyles.eclipseColor,
		textTransform: 'capitalize',
	},
	flatListView: {
		alignItems: 'center',
		maxHeight: baseStyles.listContainerMedium,
		marginTop: baseStyles.margin30,
		width: deviceWidth * 0.5
	},
	flatList: {
		flexGrow: 0,
		width: '100%'
	},
	nextBtn: {
		marginTop: baseStyles.margin50,
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle,
	},
	nextBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle
	},
	itemStyle: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: baseStyles.margin10,
		paddingHorizontal: baseStyles.padding30,
		paddingVertical: baseStyles.padding10,
	},
	itemContainerStyle: {
		borderWidth: 1,
		height: baseStyles.buttonSize30,
		width: baseStyles.buttonSize30,
		borderRadius: (baseStyles.buttonSize30/2),
		justifyContent: 'center',
		alignItems: 'center',
	},
	radioBtnStyle: {
		height: baseStyles.buttonSize20,
		width: baseStyles.buttonSize20,
		borderRadius: (baseStyles.buttonSize20/2),
	},
	itemTextStyle: {
		flex: 1,
		marginLeft: baseStyles.margin20,
		color: baseStyles.greyColor,
		fontSize: baseStyles.h3FontSize,
		fontFamily: baseStyles.latoBlack
	},
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor 
		}
	}
});