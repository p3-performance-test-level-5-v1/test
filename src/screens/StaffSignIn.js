import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Keyboard,
  Image,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../components/Header';
import { connect } from 'react-redux';
import { baseStyles } from '../config/baseStyles';
import {
  findAllStaff,
  updateErrorMessage,
  fetchCheckInCheckOutToken,
  staffCheckInOutV2,
  loginByPin,
} from '../redux/actions';
import { scaledHeight, scaledFont } from '../utils/Scale';
import { errorResponse, handleErrorDataFromApi, retrieveAsyncStorage } from '../utils';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import Constant from '../constants/Constant';
import CustomConfig from '../utils/CustomConfig';
import { getFullName } from '../utils/index';
import { visitorHeaderImage } from '../config/config';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const STAFF_DATA = {
  staffName: '',
  queryText: '',
  userId: null,
  staffId: null,
};
class StaffSignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      staffData: '',
      staffList: [],
      staffImageKey: '',
      ...STAFF_DATA,
      pin: '',
      isHidePin: true,
    };
    this.getStaffList = this.getStaffList.bind(this);
  }
  componentDidMount() {
    this.getAllStaffList();
  }
  async getAllStaffList() {
    const accessToken = await retrieveAsyncStorage('accessToken');
    let centreId = await retrieveAsyncStorage('centreId');
    centreId = centreId ? parseInt(centreId) : 0;

    this.props.findAllStaff(accessToken, centreId).then((result) => {
      if (result.success) {
        const staffList = _.get(result, 'data.findAllStaff.data', []);

        this.setState({ staffList });
        return;
      }
      const message = handleErrorDataFromApi(result, errorResponse);
      message && this.props.updateErrorMessage(message, this.getAllStaffList.bind(this));
    });
  }
  getStaffList() {
    let staffList = this.state.staffList;
    return staffList.map((obj) => {
      return {
        value: _.get(obj, 'userByFkUser.email', ''),
        name: getFullName(_.get(obj, 'userByFkUser.firstname', ''), _.get(obj, 'userByFkUser.lastname', '')),
        userId: _.get(obj, 'userByFkUser.ID', null),
        staffId: _.get(obj, 'ID', null),
        imageKey: _.get(obj, 'imageKey'),
      };
    });
  }

  handleSignIn = async () => {
    const { navigation, loginByPin } = this.props;
    const { staffName, staffImageKey, userId, staffId, pin, email } = this.state;

    const data = { staffName, staffImageKey, userId, staffId };
    const result = await loginByPin({ email, pin });

    const token = _.get(result, 'data.loginByPin');
    if (!token) {
      const errMsg = _.get(result, 'error[0].message', '');
      if (errMsg === Constant.INCORRECT_PASSWORD_ERROR) {
        alert(t('common.kioskIncorrectPin'));
      } else {
        alert(errMsg);
      }
    } else {
      navigation.navigate('CheckInCheckOut', data);
    }
  };

  handleChange(item) {
    if (!item.value || !item.name) {
      return;
    }

    this.setState(
      {
        email: item.value,
        userId: item.userId,
        staffId: item.staffId,
        staffName: item.name,
        staffData: `${item.value} ${item.name}`,
      },
      () => Keyboard.dismiss()
    );
  }
  render() {
    const { navigation, schoolConfig } = this.props;
    const school = _.get(schoolConfig, 'data.data[0].school.code', '');

    const { staffName, queryText, pin, isHidePin } = this.state;
    const actionType = navigation.getParam('actionType');
    const isSignIn = actionType !== Constant.TYPE_CHECK_OUT;

    const validString = Constant.STAFF_SEARCH_REGEX.test(queryText);
    const staffLists = this.getStaffList();
    const filterOption = validString ? staffLists.filter(
      (opt) => opt.value.toLowerCase().includes(queryText.toLowerCase())
    ) : [];

    const isDisabled = !staffName || !pin.trim();

    return (
      <View style={styles.container}>
        <Header 
          backButton 
          navigation={navigation} 
          style={[commonStyle.headerStyle, styles.zIndex10]} 
        />
        <KeyboardAvoidingView 
          behavior="position" 
          style={styles.keyboardAvoidingViewStyle}
        >
          <View style={styles.innerContainer}>
            <View style={styles.logoContainer}>
              <Image resizeMode="contain" source={visitorHeaderImage[school]} style={styles.logoStyle} />
            </View>
            <View style={styles.contentView}>
              <View style={styles.signInView}>
                <Text style={styles.signInTxt}>{t('common.kioskStaffSignIn')}</Text>
                <Text style={styles.description}>{t('common.kioskStaffSignInDesc')}</Text>
              </View>
              <View style={styles.formView}>
                <Text style={styles.labelStyle}>{t('common.emailId')}</Text>
                <View style={styles.textInputContainerStyle}>
                  <TextInput
                    value={queryText || staffName}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholder={t('common.emailIdPlaceHolder')}
                    style={[styles.textInputStyle]}
                    onEndEditing={() => this.setState({ queryText: '' })}
                    onFocus={() => this.setState(STAFF_DATA)}
                    onChangeText={(queryText) => this.setState({ queryText })}
                    keyboardType="twitter"
                  />
                </View>
                {validString && (
                  <View style={[styles.shadowStyle, styles.listContainer]}>
                    <ScrollView
                      bounces={false}
                      keyboardShouldPersistTaps="handled"
                      contentContainerStyle={styles.scrollContainer}
                    >
                      {filterOption.length > 0 ? (
                        filterOption.map((option, index) => (
                          <TouchableOpacity
                            onPress={() => this.handleChange(option)}
                            key={`option_${index}`}
                            style={styles.itemContainer}
                          >
                            <Text style={styles.listItemStyle}>{`${option.value} ${option.name || ''}`}</Text>
                          </TouchableOpacity>
                        ))
                      ) : (
                        <View style={styles.itemContainer}>
                          <Text style={styles.listItemStyle}>
                            {t('common.emptyList')}
                          </Text>
                        </View>
                      )}
                    </ScrollView>
                  </View>
                )}
                <Text style={[styles.labelStyle, styles.txtPin]}>{t('common.pin')}</Text>
                <View style={styles.textInputContainerStyle}>
                  <TextInput
                    value={pin}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholder={t('common.pinPlaceHolder')}
                    style={[styles.textInputStyle]}
                    onChangeText={(pin) => this.setState({ pin })}
                    secureTextEntry={isHidePin}
                  />
                  <TouchableOpacity onPress={() => this.setState({ isHidePin: !isHidePin })}>
                    <Icon size={20} color={baseStyles.darkShadeGray} name={isHidePin ? 'eye-off' : 'eye'} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.nextBtnView}>
                <TouchableOpacity
                  disabled={isDisabled}
                  onPress={this.handleSignIn}
                  style={[styles.nextBtn, styles.themeColor, { opacity: !isDisabled ? 1 : baseStyles.disabledOpacity }]}
                >
                  <Text style={styles.nextBtnTxt}>{t('common.kioskStaffSignInText', {defaultValue: 'Sign-In'})}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  loginData: state.centreLogin,
  schoolConfig: state.schoolConfig,
});

const mapDispatchToProps = {
  findAllStaff,
  updateErrorMessage,
  fetchCheckInCheckOutToken,
  staffCheckInOutV2,
  loginByPin,
};

export default connect(mapStateToProps, mapDispatchToProps)(StaffSignIn);

const centreAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  contentView: {
    flex: 0.65,
    width: deviceWidth * 0.5,
    alignItems: 'center',
  },
  signInView: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  signInTxt: {
    fontSize: scaledFont(36),
    color: baseStyles.eclipseColor,
    fontFamily: baseStyles.latoBold,
    marginBottom: baseStyles.margin15,
  },
  description: {
    fontSize: baseStyles.textInputFont,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    marginBottom: baseStyles.margin30,
  },
  labelStyle: {
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  formView: {
    minHeight: deviceHeight * 0.17,
    maxHeight: deviceHeight * 0.35,
    width: '100%',
    justifyContent: 'center',
  },
  nextBtnView: {
    width: '80%',
    justifyContent: 'center',
    marginTop: 200,
  },
  nextBtn: {
    ...buttonCommonStyle.butttonStyle,
    ...buttonCommonStyle.commonStyle,
  },
  nextBtnTxt: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  logoContainer: {
    flex: 0.35,
    width: '100%',
    ...centreAlignStyle,
  },
  logoStyle: {
    height: scaledHeight(160),
    width: '100%',
  },
  textInputContainerStyle: {
    fontSize: baseStyles.buttonFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    borderBottomWidth: 1,
    borderBottomColor: baseStyles.lightGrey,
    paddingTop: baseStyles.padding5,
    paddingBottom: baseStyles.padding8,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  get themeColor() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  textInputStyle: {
    fontSize: baseStyles.textInputFont,
    color: baseStyles.titleColor,
    fontFamily: baseStyles.latoRegular,
    flex: 1,
  },
  listContainer: {
    maxHeight: deviceHeight * 0.17,
    position: 'absolute',
    top: 80,
    backgroundColor: baseStyles.bgColor,
    zIndex: 10,
  },
  shadowStyle: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
  },
  scrollContainer: {
    width: '100%',
    backgroundColor: baseStyles.bgColor,
  },
  keyboardAvoidingViewStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  listItemStyle: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  txtPin: {
    marginTop: baseStyles.margin30,
  },
  itemContainer: {
    minWidth: 400,
    padding: baseStyles.padding10,
  },
  innerContainer: { 
    flex: 1, 
    alignItems: 'center' 
  },
  zIndex10: { 
    zIndex: 10 
  },
});
