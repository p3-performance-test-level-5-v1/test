import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	FlatList,
	Dimensions,
	StyleSheet,
	ImageBackground,
	TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { connect } from 'react-redux';
import _ from 'lodash';
import { baseStyles } from '../config/baseStyles';
import { t } from '../utils/LocalizationUtils.js';
import CachedImage from '../components/CachedImage';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class SignedInSuccess extends Component {
	constructor(props) {
		super(props);
		this.state = {
			childDetails: [],
			selected: []
		};
	}
	componentWillMount() {
		const { navigation } = this.props;
		const { state } = navigation;
		const { params = {} } = state;
		const { selected = [], childDetails = [] } = params;
		this.setState({ 
			selected, 
			childDetails
		});
	}
	renderChildDetails = (heading, textValue) => {
		return <View 
		style={{
			paddingHorizontal: scaledWidth(15),
			paddingBottom: scaledHeight(15)
		}}>
			<Text 
			style={styles.detailHead}>{heading}</Text>
			<Text 
			numberOfLines={1} 
			style={styles.detailTxt}>{textValue}</Text>
		</View>
	}
	renderChild = ({ item, index }) => {
		const { selected } = this.state;
		if (selected.indexOf(item.id) < 0) {
			return false;
		}
		const length = selected.length - 1;
		return <View 
		style={[
			{ borderBottomWidth: ( index == length ) ? 0 : 1 },
			styles.childItemView
		]}>
			<CachedImage
	            style={styles.childImage}
	            source={item.childImage}
	        />
			<View 
			style={{
				width: '80%'
			}}>
				{ this.renderChildDetails(t('common.name'), item.name) }
				<View 
				style={styles.rowView}>
					{ this.renderChildDetails(t('common.kioskTodaysDate'), moment().format('DD/MM/YYYY')) }
				</View>
			</View>
		</View>
	}
	renderSuccessData() {
		const { navigation } = this.props;
		const isSignIn = navigation.getParam('isSignIn', false);
		const signedInOutTxt = isSignIn ? t('common.kioskCheckedInTxt') : t('common.kioskCheckedOutTxt');
		const { childDetails = [], selected = [] } = this.state;
		const selectedNames = childDetails.filter(child => selected.indexOf(child.id) >= 0).map(chld => chld.name);
		let childNames = '';
		if (selectedNames.length > 1) {
			childNames = selectedNames.slice(0, selectedNames.length - 1).join();
		} 
		nameToBeAppend = selectedNames.splice(selectedNames.length - 1, 1);
		childNames += childNames.length > 0 ? (' and ' + nameToBeAppend) : nameToBeAppend;

		return <View 
		style={[
			{ width: deviceWidth * 0.4 },
			styles.container
		]}>
			<Text 
			style={styles.successTxt}>{childNames}</Text>
			<Text 
			style={styles.successTxt}>{t('common.kioskSuccessMessageTxt', { checkInOutTxt: signedInOutTxt })}</Text>
			<View 
			style={styles.flatListView}>
				<FlatList
					data={childDetails} 
					extraData={this.state} 
					removeClippedSubviews={false}  
					showsHorizontalScrollIndicator={false} 
					showsVerticalScrollIndicator={false} 
					keyExtractor={item => item.id.toString()}
					renderItem={this.renderChild}
				/>
			</View>
			<TouchableOpacity 
			onPress={() => navigation.navigate('Home')}
			style={[styles.doneBtn, styles.primaryColor]}>
				<Text 
				style={styles.doneBtnTxt}>{t('common.done')}</Text>
			</TouchableOpacity>
		</View>
	}
	render() {
		const { schoolConfig, navigation } = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', '');
		if (school === 'TCC') {
			return <View style={styles.container}>
				{this.renderSuccessData()}	
			</View>
		}
		return <ImageBackground 
		resizeMode='cover' 
		style={styles.container}
		source={require('../images/background-image.png')}>
			{this.renderSuccessData()}	
		</ImageBackground>
	}
}

const mapStateToProps = state => ({
	schoolConfig: state.schoolConfig
});

export default connect(mapStateToProps, {})(SignedInSuccess);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	successTxt: {
		textAlign: 'center',
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold,
		color: baseStyles.eclipseColor
	},
	doneBtn: {
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle
	},
	doneBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle,
	},
	flatListView: {
		maxHeight: deviceHeight * 0.4,
		marginVertical: scaledHeight(30),
		alignItems: 'center',
		width: '100%'
	},
	detailTxt: {
		width: '100%',
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(18),
		fontFamily: baseStyles.latoBold
	},
	detailHead: {
		color: baseStyles.eclipseColor,
		marginBottom: scaledHeight(5),
		fontSize: scaledFont(14),
		fontFamily: baseStyles.latoSemibold
	},
	detailView: {
		height: '15%',
		justifyContent: 'space-between'
	},
	childItemView: {
		width: '100%',
		flexDirection: 'row',
		paddingVertical: scaledHeight(20),
		borderBottomColor: '#d5d5d5'
	},
	childImage: {
		height: scaledFont(60),
		width: scaledFont(60),
		borderRadius: scaledFont(60)/2
	},
	rowView: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor 
		}
	}
});