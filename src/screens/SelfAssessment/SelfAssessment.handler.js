import { useState, useEffect } from 'react';
import { get } from 'lodash';
import {
	errorResponse,
	retrieveAsyncStorage,
	handleErrorDataFromApi ,
} from '../../utils';

export default Handler = (props) => {
  const { 
    fetchSelfAssessmentForm, 
    saveAssessmentOfVisitor, 
    navigation, 
    updateErrorMessage, 
  } = props;

  const [selfAssessments, setSelfAssessments] = useState([]);
  const [disableBtn, setDisableBtn] = useState(false);

  useEffect(() => {
    onComponentMount();
  }, []);

  const onComponentMount = async () => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    const fkSchool = await retrieveAsyncStorage('schoolId');
    if (!fkSchool || !accessToken) {
      return;
    }

    const result = await fetchSelfAssessmentForm({ fkSchool }, accessToken);
    const data = get(result, 'data.fetchSelfAssessmentForm.data', []);
    setSelfAssessments(data);
  };

  const onRadioButtonChange = (value, isChecked = false) => {
    const data = selfAssessments.map(dt => {
      if (value === dt.ID) {
        Object.assign(dt, { isChecked }); 
      }
      return dt;
    });
    setSelfAssessments(data.concat([]));
  };

  const onCheckInAction = async () => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    const fkSchool = await retrieveAsyncStorage('schoolId');
    const fkVisitorLog = navigation.getParam('logID', '');
    const data = selfAssessments.map(dt => ({ FkCode: dt.ID,Value: dt.isChecked }));
    if ((data.length !== selfAssessments.length) || !accessToken || !fkSchool || !fkVisitorLog) return;

    const reqData = { fkSchool, fkVisitorLog, data: { assessments: data } };
    setDisableBtn(true);
    const result = await saveAssessmentOfVisitor(reqData, accessToken);
    setDisableBtn(false);
    const status = get(result, 'data.saveAssessmentOfVisitor', false);
    if (result.success && status) {
      navigation.navigate('CheckInOutSuccess', { isCheckIn: true });
      return;
    }
    const message = handleErrorDataFromApi(result, errorResponse);
		message && updateErrorMessage(message, onCheckInAction);
  };

  return ({
    selfAssessments,
    onRadioButtonChange,
    onCheckInAction,
    disableBtn,
  });
}