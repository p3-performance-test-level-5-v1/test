import React from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import styles from './SelfAssessment.style';
import Handler from './SelfAssessment.handler';
import Header from '../../components/Header';
import { t } from '../../utils/LocalizationUtils.js';
import {
  fetchSelfAssessmentForm,
  saveAssessmentOfVisitor,
  updateErrorMessage,
} from '../../redux/actions';
import { connect } from 'react-redux';
import { get } from 'lodash';
import { commonStyle } from '../../config/commonStyle';

const Options = (props) => {
  const { optionName, fkCode, callback, isChecked } = props;
  return (
    <View style={styles.optionContainer}>
      <TouchableOpacity
        onPress={() => callback(fkCode)}
        style={{
          borderColor: isChecked ? '#38576B' : baseStyles.darkGrayTintColor,
          ...styles.radioButtonStyle,
        }}
      >
        {isChecked && (
          <View style={styles.checkedIconStyle}/>
        )}
      </TouchableOpacity>
      <Text style={styles.optionNameStyle}>{optionName}</Text>
    </View>
  );
}

const SelfAssessment = (props) => {
  const { navigation } = props;

  const {
    selfAssessments,
    onRadioButtonChange,
    onCheckInAction,
    disableBtn,
  } = Handler(props);

  const renderItem = item => {
    return (
      <View style={styles.itemContainer}>
        <Text style={styles.listItemStyle}>{item.description}</Text>
        <View style={styles.yesContainer}>
          <Options
            item={item}
            optionName='Yes'
            isChecked={item.isChecked ? true : false}
            callback={(code) => onRadioButtonChange(code, true)}
            fkCode={item.ID} 
          />
        </View>
        <View style={styles.noContainer}>
          <Options
            item={item}
            optionName='No'
            isChecked={item.isChecked === false ? true : false}
            callback={(code) => onRadioButtonChange(code)}
            fkCode={item.ID}
          />
        </View>
      </View>
    );
  }

  const checkedCount = selfAssessments.filter(data => (data.isChecked || data.isChecked === false));
  const isDisabled = (checkedCount.length === selfAssessments.length) && !disableBtn;

  return (
    <View style={styles.container}>
      <Header
        backButton 
        navigation={navigation}
        style={commonStyle.headerStyle}
      />
      <View style={styles.contentContainer}>
        <View style={styles.descContainer}>
          <Text style={styles.assessmentTextStyle}>{t('common.selfAssessmentVisitor')}</Text>
          <Text style={styles.assessmentDescStyle}>{t('common.fillFormForCheckInVisitor')}</Text>
        </View>
        <View style={styles.listContainer}>
          <FlatList
            data={selfAssessments}
            style={styles.flatListStyle}
            ItemSeparatorComponent={() => <View style={styles.separatorStyle}/>}
            renderItem={({ item }) => renderItem(item)}
            keyExtractor={item => item.ID.toString()}
          />
        </View>
        <View style={styles.checkInBtnContainer}>
          <TouchableOpacity 
            disabled={isDisabled ? false : true}
            style={[
              styles.checkInBtnStyle,
              { opacity: isDisabled ? 1 : baseStyles.disabledOpacity }
            ]}
            onPress={onCheckInAction}
          >
            <Text style={styles.checkInTxtStyle}>{t('common.checkInVisitor')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  schoolConfig: get(state, 'schoolConfig', {}),
});

const mapDispatchToProps = {
  fetchSelfAssessmentForm,
  saveAssessmentOfVisitor,
  updateErrorMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelfAssessment);