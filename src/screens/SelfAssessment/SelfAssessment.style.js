import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { scaledFont } from '../../utils/Scale';
const { width: deviceWidth } = Dimensions.get('window');

const centerAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    ...centerAlignStyle,
  },
  contentContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: baseStyles.white,
    ...centerAlignStyle,
  },
  descContainer: {
    flex: 0.17,
    ...centerAlignStyle,
  },
  assessmentTextStyle: {
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
    fontSize: baseStyles.labelFontSize24,
  },
  assessmentDescStyle: {
    color: baseStyles.darkShadeGray,
    marginTop: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.buttonFontSize,
  },
  listContainer: {
    flex: 0.63,
    alignItems: 'center',
    width: deviceWidth * 0.6,
  },
  checkInBtnContainer: {
    flex: 0.2,
    width: deviceWidth * 0.4,
    marginTop: baseStyles.margin30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  get checkInBtnStyle() {
    return {
      backgroundColor: CustomConfig.Colors.primaryColor,
      ...buttonCommonStyle.butttonStyle,
      ...buttonCommonStyle.commonStyle,
    }
  },
  checkInTxtStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  listItemStyle: { 
    flex: 1,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.buttonFontSize
  },
  yesContainer: { 
    marginLeft: baseStyles.margin30, 
  },
  noContainer: { 
    marginLeft: baseStyles.margin50, 
  },
  radioButtonStyle: {
    borderWidth: 1,
    height: scaledFont(20),
    width: scaledFont(20),
    borderRadius: scaledFont(20)/2,
    ...centerAlignStyle,
  },
  checkedIconStyle: {
    height: scaledFont(10),
    width: scaledFont(10),
    borderRadius: scaledFont(10)/2,
    backgroundColor: '#38576B',
  },
  optionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  optionNameStyle: { 
    marginLeft: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.buttonFontSize
  },
  flatListStyle: { 
    width: '100%', 
    marginVertical: baseStyles.margin20, 
  },
  separatorStyle: { 
    marginBottom: baseStyles.margin30,
  },
});