import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { connect } from 'react-redux';
import { 
	findParentByIC,
	updateErrorMessage,
	getCheckInOutToken,
	childCheckInOutV2,
	showCustomAlert, 
} from '../redux/actions';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import { 
	TextField 
} from 'react-native-material-textfield';
import { 
	errorResponse, 
	handleErrorDataFromApi,
	retrieveAsyncStorage,
	checkIfSingleChild
} from '../utils';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class ChildSignIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			focusTxt: false,
			nricOrContactNo: '',
			secureTextEntry: true,
			canUserByPhone: false,
		};
	}
	async componentDidMount() {
		const canFindUserByPhone = await retrieveAsyncStorage('canFindUserByPhone');
		if (canFindUserByPhone) {
			this.setState({ canUserByPhone: true });
		}
	}
	async childSignIn() {
		const { nricOrContactNo, canUserByPhone } = this.state;
		const accessToken = await retrieveAsyncStorage('accessToken');
		if (!accessToken || nricOrContactNo.trim().length === 0) {
			return;
		}

		const data = { ic: nricOrContactNo };
		const { findParentByIC, loginData } = this.props;

		findParentByIC(data, accessToken).then(data => {
			if (data.success) {
				const { navigation } = this.props;
				const isSignIn = navigation.getParam('isSignIn', false);
				const parentUserId = _.get(data, 'data.findParentByIC.data[0].ID', null);
				const findParentByICData = _.get(data, 'data.findParentByIC.data[0].childRelations.data', []);
				const findParentByIC = findParentByICData.filter(child => {
					const fkCentre = _.get(child, 'child.currentLevel.fkCentre', 0);
					const fkLevel = _.get(child, 'child.currentLevel.fkLevel', 0);
					if ((fkCentre === loginData.centreId) && fkLevel > 0) {
						return child;
					}
				});

				const thisProps = this.props;
				const childDetails = checkIfSingleChild(findParentByIC, thisProps, parentUserId);
				if (childDetails.length === 0) {
					alert(t('common.kioskNoChildForGuardianAlert'));
				} else if(childDetails.length > 1) {
					navigation.navigate('ChildAttendance', { 
						findParentByIC: childDetails,
						nricNo: nricOrContactNo,
						isSignIn,
						parentUserId: parentUserId,
					});
				}
				return;
			}
			let errMessage = handleErrorDataFromApi(data, errorResponse);
			let message = errMessage.indexOf('/Mobile no') >= 0 ? errMessage.replace('/Mobile no', '').trim() : errMessage;
			if (canUserByPhone) {
				message = t('error.noUserFoundForNricOrMobileNo');
			}
			message && this.props.updateErrorMessage(message, this.childSignIn.bind(this))
		});
	}

  	onAccessoryPress = () => {
		this.setState(({ secureTextEntry }) => ({
			secureTextEntry: !secureTextEntry
		}));
	}
  
  	renderPasswordAccessory = () => {
		const { secureTextEntry } = this.state;
		const name = secureTextEntry ? "eye-off" : "eye";
		return (
			<Feather
				size={scaledFont(16)}
				name={name}
				color={"#dadada"}
				onPress={this.onAccessoryPress}
			/>
		);
  	}
  
	render() {
		const { navigation } = this.props;
		const { nricOrContactNo, focusTxt, secureTextEntry, canUserByPhone } = this.state;
		const isSignIn = navigation.getParam('isSignIn', false);
		const isDisabled = nricOrContactNo && nricOrContactNo.trim().length > 0;

		return <View 
		style={styles.container}>
			<Header
				backButton 
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			<View 
			style={styles.container}>
				<View 
				style={styles.contentView}>
					<View 
					style={styles.signInView}>
						<Text 
						style={styles.signInTxt}>{isSignIn ? t('common.kioskChildCheckInTxt') : t('common.kioskChildCheckOutTxt')}</Text>
						<Text 
						style={styles.relationTextStyle}>{t('common.kioskRelationshipWithChildTxt')}</Text>
					</View>
					<View style={{
						flex: 0.5
					}}>
						<View style={styles.radioBtnView}>
							<View style={styles.radioBtn}>
								<View style={styles.radioBtnInnerView}/>
							</View>
							<Text style={styles.radioBtnOptions}>
								{t('common.kioskParentOrGuardianTxt')}
							</Text>
						</View>
						<View style={{ justifyContent: 'center' }}>
							<TextField 
								tintColor={baseStyles.darkGrayTintColor}
								label={canUserByPhone ? t('common.kioskNricOrFinOrPassportOrPhoneNumber') : t('common.kioskNricOrFinOrPassport')}
								fontSize={baseStyles.inputFontSize} 
								labelFontSize={baseStyles.labelFontSize} 
								value={nricOrContactNo} 
								style={styles.textInputStyle}
								inputContainerStyle={{
									borderBottomWidth: 2,
									borderBottomColor: focusTxt ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada'
								}}
								onBlur={() => this.setState({ focusTxt: false })}
								onFocus={() => this.setState({ focusTxt: true })}
								onChangeText={value => this.setState({ nricOrContactNo: value })}
								renderAccessory={this.renderPasswordAccessory}
								secureTextEntry={secureTextEntry}
							/> 
						</View>
					</View>
					<TouchableOpacity 
					disabled={isDisabled ? false : true}
					onPress={this.childSignIn.bind(this)}
					style={[
						styles.nextBtn, 
						styles.primaryColor,
						{ opacity: isDisabled ? 1 : baseStyles.disabledOpacity }
					]}>
						<Text 
						style={styles.nextBtnText}>{t('common.kioskNextBtnTxt')}</Text>
					</TouchableOpacity>
				</View>
			</View>
			<Footer 
			style={styles.footerStyle}/>
		</View>
	}
}

const mapStateToProps = state => ({
	loginData: state && state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	findParentByIC,
	updateErrorMessage,
	getCheckInOutToken,
	childCheckInOutV2,
	showCustomAlert, 
});

export default connect(mapStateToProps, mapDispatchToProps)(ChildSignIn)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	contentView: {
		height: deviceHeight * 0.4,
		width: deviceWidth * 0.4,
		justifyContent: 'center'
	},
	signInView: {
		flex: 0.25,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'space-evenly',
		marginBottom: scaledHeight(20)
	},
	signInTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold
	},
	radioBtnView: {
		width: '100%',
		flexDirection: 'row'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	radioBtn: {
		height: scaledHeight(25),
		width: scaledHeight(25),
		borderRadius: scaledHeight(25)/2,
		borderWidth: 2,
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: '#38576B'
	},
	radioBtnInnerView: {
		height: scaledHeight(15),
		width: scaledHeight(15),
		borderRadius: scaledHeight(15)/2,
		backgroundColor: '#38576B',
	},
	radioBtnOptions: {
		marginLeft: scaledWidth(10),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		fontSize: scaledFont(18)
	},
	nextBtn: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle,
	},
	get primaryColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		}
	},
	nextBtnText: {
		color: '#ffffff',
		...buttonCommonStyle.buttonTextStyle,
	},
	textInputStyle: {
		fontFamily: baseStyles.latoRegular,
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize,
	},
	relationTextStyle: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(16),
		fontFamily: baseStyles.latoSemibold
	}
});