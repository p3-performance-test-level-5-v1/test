import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
  Image,
} from 'react-native';
import Header from '../../components/Header';
import { connect } from 'react-redux';
import {
  authoriseGuardian,
  updateErrorMessage,
  parentOrGuardianSignIn,
} from '../../redux/actions';
import { t } from '../../utils/LocalizationUtils.js';
import { visitorHeaderImage } from '../../config/config';
import { baseStyles } from '../../config/baseStyles';
import { commonStyle } from '../../config/commonStyle';
import { Handler } from './Guardian.handler';
import { styles } from './Guardian.style';
import _ from 'lodash';

const Guardian = (props) => {
  const { navigation, schoolConfig } = props;
  const school = _.get(schoolConfig, 'data.data[0].school.code', '');

  const { 
    nricOrFin,
    onChangeNricOrFin,
    onSubmitAction,
  } = Handler(props);
  const isDisabled = !nricOrFin.trim();

  return (
    <View style={styles.container}>
      <Header 
        backButton 
        navigation={navigation} 
        style={[commonStyle.headerStyle, styles.zIndex10]} 
      />
      <KeyboardAvoidingView 
        behavior="position" 
        style={styles.keyboardAvoidingViewStyle}
      >
        <View style={styles.scrollableContainer}>
          <View style={styles.logoContainer}>
            <Image resizeMode="contain" source={visitorHeaderImage[school]} style={styles.logoStyle} />
          </View>
          <View style={styles.contentView}>
            <View style={styles.signInView}>
              <Text style={styles.signInTxt}>{t('common.kioskGuardianSignIn')}</Text>
              <Text style={styles.description}>{t('common.signInDescription')}</Text>
            </View>
            <View style={styles.formView}>
              <View style={styles.nricFinContainer}>
                <Text style={styles.labelStyle}>{t('common.nricOrFinOrPassportText')}</Text>
                <View style={styles.textInputContainerStyle}>
                  <TextInput
                    value={nricOrFin}
                    autoCorrect={false}
                    autoCapitalize="none"
                    autoCapitalize='characters'
                    placeholder={t('common.nricOrFinOrPassportText')}
                    style={[styles.textInputStyle]}
                    onChangeText={(nric) => onChangeNricOrFin(nric)}
                  />
                </View>
              </View>
            </View>
            <View style={styles.nextBtnView}>
              <TouchableOpacity
                disabled={isDisabled}
                onPress={onSubmitAction}
                style={[styles.nextBtn, styles.themeColor, { 
                  opacity: !isDisabled ? 1 : baseStyles.disabledOpacity 
                }]}
              >
                <Text style={styles.nextBtnTxt}>{t('common.proceed')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
}

const mapStateToProps = (state) => ({
  loginData: state.centreLogin,
  schoolConfig: state.schoolConfig,
});
const mapDispatchToProps = {
  authoriseGuardian,
  updateErrorMessage,
  parentOrGuardianSignIn,
};

export default connect(mapStateToProps, mapDispatchToProps)(Guardian);