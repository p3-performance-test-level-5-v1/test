import { get } from 'lodash';
import { useState } from 'react';
import { 
  errorResponse, 
  handleErrorDataFromApi, 
  retrieveAsyncStorage 
} from '../../utils';

export const Handler = (props) => {
  const { 
    navigation, 
    authoriseGuardian, 
    updateErrorMessage,
    parentOrGuardianSignIn,
  } = props;
  const [nricOrFin, setNricOrFin] = useState('');

  const onChangeNricOrFin = (value) => {
    setNricOrFin(value.trim().toUpperCase());
  }

  const onSubmitAction = async () => {
    const centreId = await retrieveAsyncStorage('centreId');
    const accessToken = await retrieveAsyncStorage('accessToken');
    const reqData = { identificationNo: nricOrFin, IDCentre: centreId };
    const result = await authoriseGuardian(reqData, accessToken);
    const authToken = get(result, 'data.authoriseGuardian', '');
    if(result.success && authToken) {
      parentOrGuardianSignIn(true);
      navigation.navigate('ChildCheckInOut', { identificationNo: nricOrFin });
      return;
    }
    const message = handleErrorDataFromApi(result, errorResponse);
		message && updateErrorMessage(message);
  }

  return ({
    nricOrFin,
    onChangeNricOrFin,
    onSubmitAction,
  });
}