import { StyleSheet } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { scaledFont, scaledHeight } from '../../utils/Scale';
const centerAlignStyle = {
	alignItems: 'center',
	justifyContent: 'center'
};

export default styles = StyleSheet.create({
	container: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
		backgroundColor: baseStyles.white
	},
	imageContainer: {
		flex: 0.35,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'flex-end',
		marginBottom: baseStyles.margin30
	},
	schoolLogoStyle: {
		height: scaledHeight(160),
		width: '100%'
	},
	descContainer: {
		flex: 0.2,
		...centerAlignStyle
	},
	checkInTitleStyle: {
		fontFamily: baseStyles.latoBold,
		fontSize: scaledFont(36),
		color: baseStyles.darkShadeGray
	},
	descriptionStyle: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.darkShadeGray,
		fontFamily: baseStyles.latoRegular,
		marginTop: baseStyles.margin5
	},
	buttonViewStyle: {
		flex: 0.3,
		flexDirection: 'column'
	}
});
