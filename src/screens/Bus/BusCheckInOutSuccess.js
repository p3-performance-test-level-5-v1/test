import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	FlatList,
	Image,
	Dimensions
} from 'react-native';
import { useDispatch } from 'react-redux';
import { get } from 'lodash';
import { baseStyles } from '../../config';
import CheckedIcon from 'react-native-vector-icons/MaterialIcons';
import { scaledWidth, scaledFont } from '../../utils/Scale';
import { t } from '../../utils/LocalizationUtils';
import { getFullName } from '../../utils';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { CDN_URL } from '../../redux/config';
import {
	BUS_ENTRY_BY_PASSCODE,
	LIST_ALL_BUS_CHILDREN
} from '../../redux/actions/actionTypes';
const { width: deviceWidth } = Dimensions.get('window');

const BusCheckInOutSuccess = ({ navigation }) => {
	const dispatch = useDispatch();
	const children = navigation.getParam('children', []);
	const shoolBusNo = navigation.getParam('shoolBusNo', '-');
	const isCheckIn = navigation.getParam('isCheckIn', true);

	const onDone = () => {
		// Reset store after checkin/out done
		dispatch({
			type: BUS_ENTRY_BY_PASSCODE,
			value: null
		});
		dispatch({
			type: LIST_ALL_BUS_CHILDREN,
			value: null
		});
		navigation.navigate('Home');
	};

	const renderChild = ({ item }) => {
		const childImage = get(item, 'child.imageKey')
			? `${CDN_URL}/${get(item, 'child.imageKey')}`
			: '';

		return (
			<View style={styles.childListView}>
				<Image
					style={styles.childImageStyle}
					source={{ uri: childImage }}
				/>
				<Text style={styles.boldTextStyle}>
					{getFullName(
						get(item, 'child.firstname'),
						get(item, 'child.lastname', '-')
					)}
				</Text>
			</View>
		);
	};

	return (
		<View style={styles.container}>
			<View style={styles.successWrapper}>
				<CheckedIcon
					name='check-circle'
					size={130}
					color={baseStyles.parrotGreen}
				/>
				<Text style={styles.textTitle}>
					{isCheckIn
						? t('bus.checkInSuccess', {
								defaultValue: 'Check-In Successful'
						  })
						: t('bus.checkOutSuccess', {
								defaultValue: 'Check-Out Successful'
						  })}
				</Text>
				<Text style={styles.textDescription}>
					{t('bus.busNumber', {
						busNo: shoolBusNo,
						defaultValue: `School Bus No. ${shoolBusNo}`
					})}
				</Text>
			</View>
			<View style={styles.flatListView}>
				<FlatList
					data={children}
					style={styles.flatList}
					removeClippedSubviews={false}
					showsVerticalScrollIndicator={false}
					keyExtractor={(item) => get(item, 'ID', '').toString()}
					renderItem={renderChild}
				/>
			</View>
			<View style={styles.buttonView}>
				<TouchableOpacity onPress={onDone} style={styles.button}>
					<Text style={styles.btnText}>
						{t('common.done', { defaultValue: 'Done' })}
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default BusCheckInOutSuccess;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: baseStyles.white
	},
	successWrapper: {
		flex: 0.5,
		justifyContent: 'center',
		alignItems: 'center'
	},
	textTitle: {
		fontSize: scaledFont(36),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		marginTop: 24,
		marginBottom: baseStyles.margin15
	},
	textDescription: {
		fontSize: scaledFont(24),
		color: baseStyles.darkShadeGray
	},
	buttonView: {
		flex: 0.2,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	get button() {
		return {
			...buttonCommonStyle.butttonStyle,
			...buttonCommonStyle.commonStyle,
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		};
	},
	btnText: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle
	},
	flatListView: {
		flex: 0.5,
		alignItems: 'center',
		width: deviceWidth * 0.65
	},
	flatList: {
		width: '100%'
	},
	childListView: {
		flexDirection: 'row',
		paddingVertical: baseStyles.padding10,
		alignItems: 'center'
	},
	childImageStyle: {
		backgroundColor: baseStyles.lightGreyColor,
		marginRight: scaledWidth(20),
		height: scaledFont(40),
		width: scaledFont(40),
		borderRadius: scaledFont(40) / 2
	},
	boldTextStyle: {
		fontSize: scaledFont(16),
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.textInputFont
	}
});
