import React, { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TouchableOpacity,
	KeyboardAvoidingView,
	Image
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { TextField } from 'react-native-material-textfield';
import Feather from 'react-native-vector-icons/Feather';
import Header from '../../components/Header';
import { commonStyle, baseStyles } from '../../config';
import { visitorHeaderImage } from '../../config/config';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { t } from '../../utils/LocalizationUtils';
import {
	errorResponse,
	retrieveAsyncStorage,
	handleErrorDataFromApi
} from '../../utils';
import {
	busLogin,
	updateErrorMessage,
	busEntryByPassCode
} from '../../redux/actions';
import { scaledHeight, scaledFont } from '../../utils/Scale';
import { get } from 'lodash';

const { width: deviceWidth } = Dimensions.get('window');

const BusSignIn = ({ navigation }) => {
	const dispatch = useDispatch();
	const schoolConfig = useSelector((state) => get(state, 'schoolConfig'));
	const loginLoading = useSelector((state) =>
		get(state, 'driverLogin.data.loading', false)
	);
	const school = get(schoolConfig, 'data.data[0].school.code', '');
	const [pwdFocus, setPwdFocus] = useState(false);
	const [passCode, setPasscode] = useState('');
	const [secureTextEntry, setSecureTextEntry] = useState(true);
	const isDisabled = loginLoading || !passCode;

	const toggleAccessoty = useCallback(
		() => setSecureTextEntry(!secureTextEntry),
		[secureTextEntry]
	);

	const onScreenWillFocus = () => {
		setPasscode('');
		setPwdFocus(false);
		setSecureTextEntry(true);
	};

	const renderPasswordAccessory = () => (
		<Feather
			size={scaledFont(16)}
			name={secureTextEntry ? 'eye-off' : 'eye'}
			color={baseStyles.placeHolderColor}
			onPress={toggleAccessoty}
		/>
	);

	const onBusSignIn = async () => {
		const fkCentre = await retrieveAsyncStorage('centreId');
		if (passCode.trim().length === 0 || !fkCentre) return; // Check valid required params
		const reqData = {
			centreID: parseInt(fkCentre),
			passCode
		};

		const onSignSuccessCallback = (result) => {
			if (get(result, 'success', false)) {
				dispatch(
					busEntryByPassCode({
						passcode: passCode,
						IDCentre: parseInt(fkCentre)
					})
				);
				navigation.navigate('BusCheckInCheckOut', { passCode });
			}

			// handle message error
			const message = handleErrorDataFromApi(result, errorResponse);
			if (message) {
				let messageText = message;
				if (message.includes('no rows in result set')) {
					messageText = t('common.kioskNoBusRoute');
				} else if (message.includes('Invalid Bus Attendant Passcode')) {
					messageText = t('common.kioskInvalidBusAttendantCode');
				}
				dispatch(updateErrorMessage(messageText, onBusSignIn));
			}
		};

		dispatch(busLogin({ reqData, onSignSuccessCallback }));
	};

	return (
		<View style={styles.container}>
			<NavigationEvents onWillFocus={onScreenWillFocus} />
			<Header
				backButton
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			<KeyboardAvoidingView
				behavior='position'
				style={styles.keyboardAvoidingViewStyle}
			>
				<View style={styles.innerContainer}>
					<View style={styles.logoContainer}>
						<Image
							resizeMode='contain'
							source={visitorHeaderImage[school]}
							style={styles.logoStyle}
						/>
					</View>
				</View>
				<View style={styles.contentView}>
					<View style={centerAlignStyle}>
						<Text style={styles.signInTxt}>
							{t('bus.busSignIn', {
								defaultValue: 'School Bus Sign-In'
							})}
						</Text>
						<Text style={styles.description}>
							{t('bus.kioskLoginDescription', {
								defaultValue:
									'Please key in your details to sign in.'
							})}
						</Text>
					</View>
					<View style={styles.formView}>
						<Text style={styles.labelStyle}>
							{t('common.kioskPasscode', {
								defaultValue: 'Passcode'
							})}
						</Text>
						<TextField
							placeholder={t('bus.kioskPasscodePlaceholder', {
								defaultValue: 'Input Passcode'
							})}
							label=''
							tintColor={baseStyles.labelColor}
							errorColor={baseStyles.errorColor}
							style={styles.fieldStyle}
							value={passCode}
							secureTextEntry={secureTextEntry}
							renderAccessory={renderPasswordAccessory}
							onEndEditing={() => setPwdFocus(false)}
							onFocus={() => setPwdFocus(true)}
							onChangeText={(passCode) => setPasscode(passCode)}
							inputContainerStyle={[
								styles.textFieldStyle,
								{
									borderBottomColor: pwdFocus
										? CustomConfig.Colors.btnPrimaryBgColor
										: baseStyles.placeHolderColor
								}
							]}
						/>
					</View>
					<View style={styles.nextBtnView}>
						<TouchableOpacity
							disabled={isDisabled}
							onPress={onBusSignIn}
							style={[
								styles.nextBtn,
								styles.primaryColor,
								{
									opacity: isDisabled
										? baseStyles.disabledOpacity
										: 1
								}
							]}
						>
							<Text style={styles.nextBtnTxt}>
								{t('common.login')}
							</Text>
						</TouchableOpacity>
					</View>
				</View>
			</KeyboardAvoidingView>
		</View>
	);
};

export default BusSignIn;

const centerAlignStyle = {
	alignItems: 'center',
	justifyContent: 'center'
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		...centerAlignStyle
	},
	textFieldStyle: {
		width: '100%',
		paddingTop: scaledHeight(5),
		height: scaledHeight(35),
		borderBottomWidth: 2
	},
	contentView: {
		flex: 1.5,
		width: deviceWidth * 0.45,
		...centerAlignStyle
	},
	signInTxt: {
		fontSize: scaledFont(36),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		marginBottom: baseStyles.margin15
	},
	labelStyle: {
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoSemibold
	},
	formView: {
		flex: 1,
		marginTop: scaledHeight(40),
		width: '100%'
	},
	fieldStyle: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold
	},
	nextBtnView: {
		flex: 1,
		width: '100%',
		...centerAlignStyle
	},
	nextBtn: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle
	},
	nextBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle
	},
	get primaryColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		};
	},
	keyboardAvoidingViewStyle: {
		flex: 1,
		width: '100%',
		alignItems: 'center'
	},
	innerContainer: {
		flex: 1,
		...centerAlignStyle
	},
	logoContainer: {
		flex: 0.5,
		width: '100%',
		...centerAlignStyle
	},
	logoStyle: {
		height: scaledHeight(170),
		width: '100%'
	},
	description: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.darkShadeGray,
		fontFamily: baseStyles.latoRegular,
		marginBottom: baseStyles.margin30
	}
});
