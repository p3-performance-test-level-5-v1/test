import React, { useState, useEffect } from 'react';
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	Dimensions,
	TouchableOpacity,
	Image,
	ActivityIndicator
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { get, isEmpty } from 'lodash';
import CheckBox from 'react-native-check-box';
import moment from 'moment';
import Header from '../../components/Header';
import { commonStyle, baseStyles } from '../../config';
import { scaledHeight, scaledFont, scaledWidth } from '../../utils/Scale';
import CustomConfig from '../../utils/CustomConfig';
import { buttonCommonStyle } from '../../config/buttonStyle';
import { t } from '../../utils/LocalizationUtils';
import { retrieveAsyncStorage, getFullName } from '../../utils';
import { isDisableChild } from '../../utils/CheckInCheckOutUtils';
import { listAllBusChildren, busAddChildCheckInOut } from '../../redux/actions';
import { CDN_URL } from '../../redux/config';
import Constant from '../../constants/Constant';

const { width: deviceWidth } = Dimensions.get('window');

const BusCheckInCheckOut = ({ navigation }) => {
	const dispatch = useDispatch();
	const [selectedChildIds, setSelectedChildIds] = useState([]);
	const isDisabled = selectedChildIds.length <= 0;
	const busChildren = useSelector((state) =>
		get(state, 'listAllBusChildren.data', {})
	);
	const busChildrenLoading = get(busChildren, 'loading', false);
	const busEntry = useSelector((s) =>
		get(s, 'busEntry.data.data.getBusEntryByPassCode', {})
	);
	const busID = get(busEntry, 'bus.ID');
	const isCheckInMode =
		get(busEntry, 'direction', Constant.ARRIVAL) === Constant.ARRIVAL;

	const listBusChildren = get(
		busChildren,
		'data.listAllBusChildren.data',
		[]
	).filter(
		(childData) =>
			!isDisableChild(
				get(childData, 'child.checkInOuts.data', []),
				isCheckInMode
			)
	);

	const onToggleSelectChild = (childId) => {
		const selectedIds = [...selectedChildIds];
		if (!selectedChildIds.includes(childId)) selectedIds.push(childId);
		else selectedIds.splice(selectedIds.indexOf(childId), 1);
		setSelectedChildIds(selectedIds);
	};

	const onCheckInOutChildren = async () => {
		const idCentre = await retrieveAsyncStorage('centreId');
		const shoolBusNo = get(busEntry, 'bus.plateNumber', '-');
		const busUserID = get(busEntry, 'fkBusAttendant', 0);
		const children = listBusChildren.filter((child) =>
			selectedChildIds.includes(parseInt(get(child, 'child.ID', 0)))
		);
		const reqData = {
			dto: {
				idChildren: selectedChildIds,
				idCentre,
				checkInOutType: isCheckInMode
					? Constant.TYPE_CHECK_IN
					: Constant.TYPE_CHECK_OUT,
				at: moment().format('YYYY-MM-DD HH:mm:ss'),
				source: Constant.KIOSK,
				busUserID
			}
		};

		dispatch(
			busAddChildCheckInOut(reqData, () => {
				navigation.navigate('BusCheckInOutSuccess', {
					children,
					shoolBusNo,
					isCheckIn: isCheckInMode
				});
			})
		);
	};

	const getAllBusChildren = async (busID) => {
		const centreID = await retrieveAsyncStorage('centreId');
		const accessToken = await retrieveAsyncStorage('accessToken');

		if (busID)
			dispatch(
				listAllBusChildren(
					{
						centreID,
						busID
					},
					accessToken
				)
			);
	};

	useEffect(() => {
		getAllBusChildren(busID);
	}, [busID]);

	const renderTitleAndBusInfo = () => (
		<View style={styles.titleContainer}>
			<View style={styles.titleWrapper}>
				<Text style={styles.textTitle}>
					{isCheckInMode
						? t('bus.busCheckIn', {
								defaultValue: 'School Bus Check-In'
						  })
						: t('bus.busCheckOut', {
								defaultValue: 'School Bus Check-Out'
						  })}
				</Text>
			</View>

			<View style={styles.busInfoWrapper}>
				<View>
					<Text style={styles.textLabel}>
						{t('bus.busLicenseNo', {
							defaultValue: 'Bus License Plate no.'
						})}
					</Text>
					<Text style={styles.busInfoText}>
						{get(busEntry, 'bus.plateNumber', '-')}
					</Text>
				</View>
				<View>
					<Text style={styles.textLabel}>
						{t('bus.busNumOfChildren', {
							defaultValue: 'No. of children in School Bus.'
						})}
					</Text>
					<Text style={styles.busInfoText}>
						{get(listBusChildren, 'length', 0)}
					</Text>
				</View>
			</View>
		</View>
	);

	const renderChild = ({ item }) => {
		const childID = get(item, 'child.ID');
		const isChecked = selectedChildIds.includes(childID);
		const childImage = get(item, 'child.imageKey')
			? `${CDN_URL}/${get(item, 'child.imageKey')}`
			: '';

		return (
			<View style={styles.childListView}>
				<CheckBox
					style={styles.checkBoxStyle}
					onClick={() => onToggleSelectChild(childID)}
					checkBoxColor={baseStyles.grayishBlue}
					isChecked={isChecked}
				/>
				<Image
					style={styles.childImageStyle}
					source={{ uri: childImage }}
				/>
				<Text style={styles.boldTextStyle}>
					{getFullName(
						get(item, 'child.firstname'),
						get(item, 'child.lastname', '-')
					)}
				</Text>
			</View>
		);
	};

	const renderEmptyChild = () => (
		<View style={styles.emptyChild}>
			{busChildrenLoading ? (
				<ActivityIndicator />
			) : (
				<Text style={styles.emptyChildText}>
					{t('bus.busNoChildren', {
						defaultValue: 'No children found.'
					})}
				</Text>
			)}
		</View>
	);

	return (
		<View style={styles.container}>
			<Header
				backButton
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			{renderTitleAndBusInfo()}
			<View style={styles.flatListView}>
				<FlatList
					data={listBusChildren}
					extraData={selectedChildIds}
					style={styles.flatList}
					removeClippedSubviews={false}
					showsVerticalScrollIndicator={false}
					keyExtractor={(item) => get(item, 'ID', '').toString()}
					renderItem={renderChild}
					ListEmptyComponent={renderEmptyChild}
					contentContainerStyle={
						isEmpty(listBusChildren)
							? styles.emptyFlatlistContainer
							: null
					}
				/>
			</View>

			<View style={styles.buttonView}>
				<TouchableOpacity
					disabled={isDisabled}
					onPress={onCheckInOutChildren}
					style={[
						styles.button,
						styles.primaryColor,
						{
							opacity: isDisabled ? baseStyles.disabledOpacity : 1
						}
					]}
				>
					<Text style={styles.btnText}>
						{isCheckInMode
							? t('bus.checkIn', {
									defaultValue: 'Check-In'
							  })
							: t('bus.checkOut', {
									defaultValue: 'Check-Out'
							  })}
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default BusCheckInCheckOut;
const centerStyle = {
	alignItems: 'center',
	justifyContent: 'center'
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		width: '100%',
		alignItems: 'center',
		backgroundColor: baseStyles.white
	},
	titleContainer: { alignItems: 'center', width: '100%' },
	titleWrapper: {
		...centerStyle,
		marginTop: scaledHeight(90)
	},
	textTitle: {
		fontSize: scaledFont(36),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		marginBottom: baseStyles.margin15
	},
	busInfoWrapper: {
		width: deviceWidth * 0.65,
		marginVertical: baseStyles.margin15,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	textLabel: {
		fontSize: scaledFont(16),
		color: baseStyles.darkGrayTintColor,
		fontFamily: baseStyles.latoRegular,
		marginBottom: baseStyles.margin10
	},
	busInfoText: {
		fontSize: scaledFont(18),
		color: baseStyles.darkShadeGray,
		marginBottom: baseStyles.margin10
	},
	flatListView: {
		flex: 0.7,
		alignItems: 'center',
		width: deviceWidth * 0.65
	},
	flatList: {
		width: '100%'
	},
	buttonView: {
		flex: 0.3,
		width: '100%',
		...centerStyle
	},
	button: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle
	},
	btnText: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle
	},
	get primaryColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		};
	},
	childListView: {
		flexDirection: 'row',
		paddingVertical: baseStyles.padding10,
		alignItems: 'center'
	},
	childImageStyle: {
		backgroundColor: baseStyles.lightGreyColor,
		marginRight: scaledWidth(20),
		height: scaledFont(31),
		width: scaledFont(31),
		borderRadius: scaledFont(40) / 2
	},
	boldTextStyle: {
		fontSize: scaledFont(16),
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.textInputFont
	},
	checkBoxStyle: {
		marginRight: scaledWidth(20)
	},
	emptyChild: {
		flex: 1,
		height: '100%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	emptyFlatlistContainer: { flex: 1, justifyContent: 'center' },
	emptyChildText: {
		fontSize: scaledFont(20),
		color: baseStyles.darkGrayTintColor
	}
});
