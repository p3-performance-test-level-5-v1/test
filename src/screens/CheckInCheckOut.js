import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native';
import { connect } from 'react-redux';

import { scaledFont } from '../utils/Scale';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { baseStyles } from '../config/baseStyles';
import _, { get } from 'lodash';
import Constant from '../constants/Constant';
import { t } from '../utils/LocalizationUtils.js';
import { commonStyle } from '../config/commonStyle';
import { visitorHeaderImage } from '../config/config';
import { scaledHeight } from '../utils/Scale';
import CommonCheckInOutButton from '../components/CommonCheckInOutButton';

import {
  updateErrorMessage,
  addStaffCheckInCheckOut,
  checkStaffHasMissingRecord,
} from '../redux/actions';

const { width: deviceWidth } = Dimensions.get('window');

class CheckInCheckOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRequesting: false,
    }
    this.isApiRequested = false;
  }

  handleStaffCheckIn = async () => {
    const { navigation, checkStaffHasMissingRecord } = this.props;
    const params = _.get(navigation, 'state.params', {});
    const { staffId } = params;

    if (this.isApiRequested) return;
    this.isApiRequested = true;
    this.setState({ isRequesting: true });

    const result = await checkStaffHasMissingRecord({
      staffId,
      typeCheckInOut: Constant.TYPE_CHECK_IN,
    });

    this.isApiRequested = false;
    this.setState({ isRequesting: false });

    const hasMissingRecord = get(result, 'data.hasMissingRecord', false);

    const navParams = {
      actionType: Constant.TYPE_CHECK_IN,
      ...params,
    };
    const onNoMoreMissingRecord = () => navigation.navigate('StaffCheckIn', navParams);

    navigation.navigate(hasMissingRecord ? 'MissingRecord' : 'StaffCheckIn', { ...navParams, onNoMoreMissingRecord });
  };

  handleAddTemperature = () => {
    const { navigation } = this.props;
    const params = _.get(navigation, 'state.params', {});

    navigation.navigate('StaffCheckIn', {
      actionType: Constant.TYPE_ADD_TEMPERATURE,
      ...params,
    });
  };

  handleCheckOut = async () => {
    const { navigation, checkStaffHasMissingRecord } = this.props;
    const params = _.get(navigation, 'state.params', {});
    const { staffId } = params;

    if (this.isApiRequested) return;
    this.isApiRequested = true;
    this.setState({ isRequesting: true });

    const result = await checkStaffHasMissingRecord({
      staffId,
      typeCheckInOut: Constant.TYPE_CHECK_OUT,
    });
    this.isApiRequested = false;
    this.setState({ isRequesting: false });

    const hasMissingRecord = get(result, 'data.hasMissingRecord', false);

    if (hasMissingRecord) {
      navigation.navigate('MissingRecord', {
        ...params,
        actionType: Constant.TYPE_CHECK_OUT,
        onNoMoreMissingRecord: this.goToCheckOutScreen,
      });
    } else {
      this.goToCheckOutScreen();
    }
  };

  goToCheckOutScreen = () => {
    const { navigation } = this.props;
    const params = _.get(navigation, 'state.params', {});
    navigation.navigate('StaffCheckOut', {
      ...params,
      actionType: Constant.TYPE_CHECK_OUT,
    });
  };

  render() {
    const { isRequesting } = this.state;
    const { navigation, schoolConfig } = this.props;
    const school = _.get(schoolConfig, 'data.data[0].school.code', '');

    return (
      <View style={styles.container}>
        <Header backButton navigation={navigation} style={commonStyle.headerStyle} />
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Image resizeMode="contain" source={visitorHeaderImage[school]} style={styles.logoStyle} />
          </View>
          <View style={styles.descContainer}>
            <Text style={styles.welcomeTxt}>{t('common.kioskWelcome')}</Text>
            <Text style={styles.selectTxt}>{t('common.selectOptionTextVisitor')}</Text>
          </View>
          <View style={styles.columnView}>
            <CommonCheckInOutButton
              navigateAction={this.handleStaffCheckIn}
              secondaryLabel='登入'
              label={t('common.snKioskCheckIn')}
              image={require('../images/checkInIcon.png')}
              buttonStyle={styles.width100P}
              disabled={isRequesting}
            />
            <CommonCheckInOutButton
              navigateAction={this.handleCheckOut}
              secondaryLabel='退出'
              label={t('common.snKioskCheckOut')}
              image={require('../images/checkOutIcon.png')}
              buttonStyle={styles.width100P}
              disabled={isRequesting}
            />
            <CommonCheckInOutButton
              navigateAction={this.handleAddTemperature}
              label={t('common.addTemperature')}
              buttonStyle={styles.width100P}
            />
          </View>
        </View>
        <Footer style={styles.footerStyle} />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  schoolConfig: state.schoolConfig,
  staffToken: get(state, 'loginByPin.data', ''),
});

const mapDispatchToProps = {
  updateErrorMessage,
  addStaffCheckInCheckOut,
  checkStaffHasMissingRecord,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckInCheckOut);

const centreAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerStyle: {
    flex: 0.1,
    width: deviceWidth,
  },
  logoContainer: {
    flex: 0.5,
    width: '100%',
    ...centreAlignStyle,
  },
  logoStyle: {
    height: scaledHeight(160),
  },
  descContainer: {
    flex: 0.15,
  },
  welcomeTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(24),
    fontFamily: baseStyles.latoBold,
  },
  selectTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(16),
    fontFamily: baseStyles.latoRegular,
  },
  columnView: {
    flex: 1,
    width: deviceWidth * 0.43,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  width100P: {
    width: '100%'
  },
});
