import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity
} from 'react-native';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Feather from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import {
	authorisedPersonalByPasscode,
	updateErrorMessage,
	childCheckInOutV2,
	showCustomAlert, 
	getCheckInOutToken
} from '../redux/actions';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import {
	TextField
} from 'react-native-material-textfield';
import {
	errorResponse,
	handleErrorDataFromApi,
	retrieveAsyncStorage,
	checkIfSingleChild
} from '../utils';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class AuthorisedPerson extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected: 0,
			focusTxt: false,
			password: '',
			secureTextEntry: true,
		};
	}
	onAccessoryPress() {
		this.setState(({ secureTextEntry }) => ({
			secureTextEntry: !secureTextEntry
		}));
	}
	renderPasswordAccessory() {
		const { secureTextEntry } = this.state;
		const name = secureTextEntry ? "eye-off" : "eye";
		return (
			<Feather
				size={scaledFont(14)}
				name={name}
				color={"#dadada"}
				onPress={this.onAccessoryPress.bind(this)}
			/>
		);
	}
	async childSignIn() {
		const { password } = this.state;
		const accessToken = await retrieveAsyncStorage('accessToken');
		if (!accessToken || password.trim().length === 0) {
			return;
		}

		const { authorisedPersonalByPasscode, loginData } = this.props;
		const passcode = { passcode: password };
		authorisedPersonalByPasscode(passcode, accessToken).then(data => {
			if (data.success) {
				const { navigation } = this.props;
				const isSignIn = navigation.getParam('isSignIn', false);


				const childAuthorisedPersonals = _.get(data, 'data.authorisedPersonalByPasscode.childAuthorisedPersonals.data', []);
				const findParentByIC = childAuthorisedPersonals.filter(child => {
					const fkCentre = _.get(child, 'child.currentLevel.fkCentre', 0);
					const fkLevel = _.get(child, 'child.currentLevel.fkLevel', 0);
					if ((fkCentre === loginData.centreId) && fkLevel > 0) {
						return child;
					}
				});

				const parentUserId = _.get(data, 'data.authorisedPersonalByPasscode.fkUser', null);
				const thisProps = this.props;
				const childDetails = checkIfSingleChild(findParentByIC, thisProps, parentUserId, password);
				if (findParentByIC.length === 0) {
					alert(t('common.kioskNoChildForAuthorisedPersonAlert'));
				} else if (findParentByIC.length > 1) {
					navigation.navigate('ChildAttendance', {
						findParentByIC: childDetails,
						isSignIn,
						nricNo: password,
						parentUserId: parentUserId,
						passcode: password,
					});
				}
				return;
			}

			if (_.get(data, 'error[0].message', '') === 'error in AuthorisedPersonalRepository: sql: no rows in result set') {
				alert(t('common.kioskWrongInvalidPasscodeAlert'));
				return;
			}

			const message = handleErrorDataFromApi(data, errorResponse);
			message && this.props.updateErrorMessage(message, this.childSignIn.bind(this))
		});
	}
	render() {
		const { navigation } = this.props;
		const { selected, focusTxt, password, secureTextEntry } = this.state;
		const isSignIn = navigation.getParam('isSignIn', false);

		return <View
		style={styles.container}>
			<Header
				backButton
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			<View
			style={styles.container}>
				<View
				style={styles.contentView}>
					<View
					style={styles.signInView}>
						<Text
						style={styles.signInTxt}>{isSignIn ? t('common.kioskChildCheckInTxt') : t('common.kioskChildCheckOutTxt')}</Text>
						<Text
						style={styles.relationTextStyle}>{t('common.kioskRelationshipWithChildTxt')}</Text>
					</View>
					<View style={{
						flex: 0.5
					}}>
						<View style={styles.radioBtnView}>
							<View style={styles.radioBtn}>
								<View style={styles.radioBtnInnerView}/>
							</View>
							<Text
							style={styles.radioBtnOptions}>{t('common.kioskOneTimeAuthorisationTxt')}</Text>
						</View>
						<View
						style={{ justifyContent: 'center' }}>
							<TextField
								tintColor={baseStyles.darkGrayTintColor}
								label={t('common.kioskGuardianPasscodePlaceholderTxt')}
								fontSize={baseStyles.inputFontSize}
								labelFontSize={baseStyles.labelFontSize}
								style={styles.textInputStyle}
								inputContainerStyle={{
									borderBottomWidth: 2,
									borderBottomColor: focusTxt ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada'
								}}
								value={password}
								secureTextEntry={true}
								secureTextEntry={secureTextEntry}
								renderAccessory={this.renderPasswordAccessory.bind(this)}
								onBlur={() => this.setState({ focusTxt: false })}
								onFocus={() => this.setState({ focusTxt: true })}
								onChangeText={value => this.setState({ password: value })}
								keyboardType={'numeric'}
							/>
							<Text
							style={styles.descriptionTxt}>{t('common.kioskContactGuardianMessageTxt')}</Text>
						</View>
					</View>
					<TouchableOpacity
					disabled={password && password.trim().length > 0 ? false : true}
					onPress={this.childSignIn.bind(this)}
					style={[
						styles.nextBtn,
						styles.primaryColor,
						{ opacity: password && password.trim().length > 0 ? 1 : baseStyles.disabledOpacity }
					]}>
						<Text
						style={styles.nextBtnText}>{t('common.kioskNextBtnTxt')}</Text>
					</TouchableOpacity>
				</View>
			</View>
			<Footer
			style={styles.footerStyle}/>
		</View>
	}
}

const mapStateToProps = state => ({
	loginData: state && state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	authorisedPersonalByPasscode,
	updateErrorMessage,
	childCheckInOutV2,
	showCustomAlert,
	getCheckInOutToken
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthorisedPerson)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	contentView: {
		height: deviceHeight * 0.4,
		width: baseStyles.baseWidth,
		justifyContent: 'center'
	},
	signInView: {
		flex: 0.25,
		width: '100%',
		alignItems: 'center',
		justifyContent: 'space-evenly',
		marginBottom: scaledHeight(20)
	},
	signInTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold
	},
	radioBtnView: {
		width: '100%',
		flexDirection: 'row'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	radioBtn: {
		height: scaledHeight(25),
		width: scaledHeight(25),
		borderRadius: scaledHeight(25)/2,
		borderWidth: 2,
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: '#38576B',
	},
	radioBtnInnerView: {
		height: scaledHeight(15),
		width: scaledHeight(15),
		borderRadius: scaledHeight(15)/2,
		backgroundColor: '#38576B',
	},
	radioBtnOptions: {
		marginLeft: scaledWidth(10),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		fontSize: scaledFont(18)
	},
	nextBtn: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle,
	},
	nextBtnText: {
		color: '#ffffff',
		...buttonCommonStyle.buttonTextStyle,
	},
	textInputStyle: {
		fontFamily: baseStyles.latoBold,
		color: baseStyles.eclipseColor
	},
	relationTextStyle: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(16),
		fontFamily: baseStyles.latoSemibold
	},
	descriptionTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(12),
		fontFamily: baseStyles.latoSemibold
	},
	get primaryColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		}
	},
});
