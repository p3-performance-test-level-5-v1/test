import { useState, useEffect } from 'react';
import Constant from '../../constants/Constant';
import { 
  errorResponse, 
  handleErrorDataFromApi, 
  retrieveAsyncStorage 
} from '../../utils';
import moment from 'moment';
import _ from 'lodash';

export const Handler = (props) => {
  const { 
    allChild, 
    addChildCheckInCheckOut, 
    guardianAddChildCheckInCheckOut,
    navigation, 
    updateErrorMessage,
    isGuardian,
  } = props;

  let isApiRequested = false;
  const [isRequesting, setIsRequesting] = useState(false);
  const identificationNo = navigation.getParam('identificationNo', '');
  const [childDetails, setChildDetails] = useState([]);
  
  useEffect(() => {
    getChildDetails();
  }, [allChild]);

  const getSignedInParent = () => {
    let signedInParent = {};
    for (let i = 0; i < allChild.length; i++) {
      if (isGuardian) {
        const guardianChildRelations = _.get(allChild[i], 'guardianChildRelations.data', []);
        const selectedGuardian = guardianChildRelations.find(cR =>(
          _.get(cR, 'guardian.identificationNo', '').toUpperCase() === identificationNo
        ));
        signedInParent = _.get(selectedGuardian, 'guardian');
      } else {
        const childRelations = _.get(allChild[i], 'childRelations.data', []);
        const selectedParent = childRelations.find(cR =>(
          _.get(cR, 'parent.identificationNo', '').toUpperCase() === identificationNo
        ));
        signedInParent = _.get(selectedParent, 'parent');
      }
    }
    return signedInParent;
  }

  const getChildDetails = () => {
    const childLists = allChild.map(ch => ({ ...ch, ...{ isSelected: allChild.length === 1 } }));
    setChildDetails(childLists.concat([]));
  }

  const onToggleChildCard = (childID) => {
    let selected = childDetails.map(ch => {
      if (ch.ID === childID) {
        ch.isSelected = !ch.isSelected;
      }
      return ch;
    });
    setChildDetails(selected.concat([]));
  }

  checkInButtonStatus = (checkInOutData = []) => {
    const checkInList = checkInOutData.filter(
      ch => ch.type === Constant.TYPE_CHECK_IN && ch.status === Constant.VERIFIED
    );
    const latestCheckIn = _.last(checkInList);
    const latestCheckInTime = _.get(latestCheckIn, 'time');
    if (!latestCheckInTime) {
      return false;
    }

    const checkOutList = checkInOutData.filter(
      ch => ch.type === Constant.TYPE_CHECK_OUT && ch.status === Constant.VERIFIED
    );
    const latestCheckOut = _.last(checkOutList);
    const latestCheckOutTime = _.get(latestCheckOut, 'time');

    return (
      !latestCheckOutTime ||
      moment(latestCheckInTime).isAfter(latestCheckOutTime)
    );
  };

  const addCheckInOut = async (idChildren = [], shouldNavigate = true) => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    const fkCentre = await retrieveAsyncStorage('centreId');
    const isCheckIn = navigation.getParam('isCheckIn');
    const dropOffBy = getSignedInParent();

    const common = {
      idChildren,
      idCentre: fkCentre,
      checkInOutType: isCheckIn ? Constant.TYPE_CHECK_IN : Constant.TYPE_CHECK_OUT,
      at: moment().format('YYYY-MM-DD HH:mm:ss'),
      source: Constant.KIOSK ,
    };

    try {
      if (isApiRequested) return;
      isApiRequested = true;
      setIsRequesting(true);
      let status, result;
      if (isGuardian) {
        const reqData = {dto: { ...common, guardianID: _.get(dropOffBy, 'ID') }};
        result = await guardianAddChildCheckInCheckOut(reqData, accessToken);
        status = _.get(result, 'data.guardianAddChildCheckInCheckOut');
      } else {
        const reqData = { ...common, droppedBy: _.get(dropOffBy, 'ID') };
        result = await addChildCheckInCheckOut(reqData, accessToken);
        status = _.get(result, 'data.addChildCheckInCheckOut');
      }
      setIsRequesting(false);
      isApiRequested = false;
      
      if (status && shouldNavigate) {
        const checkedChild = childDetails.filter(ch => idChildren.some(id => id === ch.ID));
        navigation.navigate('ChildCheckInOutSuccess', {
          childDetails: checkedChild,
          identificationNo,
          isCheckIn,
        });
        return;
      } else if(!status) {
        const message = handleErrorDataFromApi(result, errorResponse);
        message && updateErrorMessage(message);
      }
    } catch (error) {
      setIsRequesting(false);
      isApiRequested = false;
      throw error;
    }
  }

  const onCheckInOutAction = async () => {
    const idChildren = childDetails.filter(ch => ch.isSelected);
    const isCheckIn = navigation.getParam('isCheckIn');
    if (idChildren.length === 0) {
      return;
    }

    const missingIds = [];
    let IDChilds = [];
    if (isCheckIn) {
      IDChilds = idChildren.map(ch => ch.ID);
    } else {
      idChildren.forEach(ch => {
        const checkInOuts = _.get(ch, 'checkInOuts.data', []);
        const checkInData = checkInOuts.filter(ch => (
          ch.type === Constant.TYPE_CHECK_IN && ch.status === Constant.VERIFIED
        ));
        const checkOutData = checkInOuts.filter(ch => (
          ch.type === Constant.TYPE_CHECK_OUT && ch.status !== Constant.DECLINED
        ));
        const lastCheckIn = _.last(checkInData);
        const lastCheckOut = _.last(checkOutData);
        
        let hasCheckIn = true;
        if (!lastCheckIn) {
          hasCheckIn = false;
        } else if (lastCheckIn && lastCheckOut) {
          const checkOutDate = _.get(lastCheckOut, 'time');
          const checkInDate = _.get(lastCheckIn, 'time');
          hasCheckIn = moment(checkInDate).isAfter(checkOutDate);
        }

        if (hasCheckIn) {
          IDChilds.push(ch.ID);
        } else {
          missingIds.push(ch.ID);
        }
      });
    }
    if (IDChilds.length > 0) {
      await addCheckInOut(IDChilds, missingIds.length === 0);
    }
    if (missingIds.length > 0) {
      navigation.navigate('ChildMissingRecord', { missingRecords: missingIds, identificationNo });
    }
  }

  return ({
    isRequesting,
    onToggleChildCard,
    checkInButtonStatus,
    onCheckInOutAction,
    childDetails,
  });
}