import React from 'react';
import {
	View,
	Text,
  TouchableOpacity,
	FlatList,
} from 'react-native';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { connect } from 'react-redux';
import { baseStyles } from '../../config/baseStyles';
import { 
  updateErrorMessage,
	getCheckInOutToken,
  addChildCheckInCheckOut,
  guardianAddChildCheckInCheckOut,
	showCustomAlert,
} from '../../redux/actions';
import { styles } from './ChildCheckInOut.style';
import { Handler } from './ChildCheckInOut.handler';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import { t } from '../../utils/LocalizationUtils.js';
import { commonStyle } from '../../config/commonStyle';
import AvatarNavigator from '../../components/AvatarNavigator';
import { S3_URL } from '../../redux/config';
import Constant from '../../constants/Constant';
import { getChildName, maskBirthCertificate } from '../../utils';
import _ from 'lodash';
import withPreventDoubleClick from '../../components/TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

const ChildCheckInOut = (props) => {
  const { navigation } = props;

  const { 
    isRequesting,
    onToggleChildCard,
    checkInButtonStatus,
    onCheckInOutAction,
    childDetails,
  } = Handler(props);
  const isCheckIn = navigation.getParam('isCheckIn');
  const checkInOutTxt = isCheckIn ? t('common.kioskCheckInTxt') : t('common.kioskCheckOutTxt');
  const selectedChild = childDetails.filter(ch => ch.isSelected);

  renderChild = ({ item }) => {
    const checkInOuts = _.get(item, 'checkInOuts.data', []);

    let isDisabled = true;
    if (isCheckIn) {
      isDisabled = checkInButtonStatus(checkInOuts);
    } else {
      const checkOutList = checkInOuts.filter(ch => (
        ch.type === Constant.TYPE_CHECK_OUT && ch.status === Constant.PENDING
      ));
      const latestCheckOut = _.last(checkOutList)
      isDisabled = !_.isEmpty(latestCheckOut);
    }

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        disabled={isDisabled}
        style={[
          styles.childViewStyle,
          {
            borderWidth: item.isSelected ? 2 : 1,
            borderColor: item.isSelected ? baseStyles.grayishBlue : baseStyles.lightGrey,
            opacity: isDisabled ? baseStyles.disabledOpacity : 1,
          },
        ]}
        onPress={() => onToggleChildCard(item.ID)}
      >
        <View style={styles.childImageContainerStyle}>
          <AvatarNavigator
            imageUrl={
              _.get(item, 'imageKey')
                ? `${S3_URL}/${_.get(item, 'imageKey', '')}`
                : null
            }
            avatharText={getChildName(item)}
            customHeight={68}
            customWidth={68}
            customFontSize={20}
          />
        </View>
        <View style={styles.childContainerStyle}>
          <Text style={styles.childNameStyle}>
            {getChildName(item)}
          </Text>
          <Text style={styles.classLabelText}>
            {_.get(item, 'childClassAt.class.label', '')}
          </Text>
          <Text style={styles.childBCText}>
            {maskBirthCertificate(_.get(item, 'birthCertificate', ''), 4)}
          </Text>
        </View>
        <View style={styles.checkIconView}>
          <MaterialIcons
            size={22}
            name={item.isSelected ? 'check-circle' : 'radio-button-unchecked'}
            color={
              item.isSelected ? baseStyles.grayishBlue : baseStyles.darkGrayTintColor
            }
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header 
        backButton 
        navigation={navigation}
        style={commonStyle.headerStyle}
      />
      <View style={styles.topHeaderView}>
        <Text style={styles.signInTxt}>
          {isCheckIn ? t('common.childCheckInText') : t('common.childCheckOutText')}
        </Text>
        <Text style={styles.signInTxtSmall}>
          {t('common.kioskSelectChildForCheckInOutTxt', { checkInOutTxt })}
        </Text>
      </View>
      <View style={styles.contentView}>
        <View style={styles.flatListView}>
          <FlatList 
            data={childDetails} 
            removeClippedSubviews={false} 
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.ID.toString()}
            style={styles.width100p}
            renderItem={renderChild}
          />
        </View>
        <TouchableButton 
        disabled={selectedChild.length === 0 || isRequesting}
        onPress={() => onCheckInOutAction()}
        style={[
          styles.signInBtn,
          styles.primaryColor,
          { opacity: selectedChild.length === 0 || isRequesting ? baseStyles.disabledOpacity : 1 }
        ]}>
          <Text style={styles.signInBtnTxt}>
            {isCheckIn ? t('common.snKioskCheckIn') : t('common.snKioskCheckOut')}
          </Text>
        </TouchableButton>
      </View>
      <Footer style={styles.footerStyle}/>
    </View>
  );
}

const mapStateToProps = state => ({
  isGuardian: _.get(state, 'isGuardianSignIn.data'),
  allChild: _.get(state, 'findAllChildByParent.data.data.data', []),
	schoolConfig: state.schoolConfig,
});

const mapDispatchToProps = ({
  updateErrorMessage,
	getCheckInOutToken,
  addChildCheckInCheckOut,
  guardianAddChildCheckInCheckOut,
	showCustomAlert,
});

export default connect(mapStateToProps, mapDispatchToProps)(ChildCheckInOut);