import { 
  StyleSheet, 
  Dimensions,
} from 'react-native';
import { buttonCommonStyle } from '../../config/buttonStyle';
import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';
import { scaledHeight, scaledFont } from '../../utils/Scale';
const { width: deviceWidth } = Dimensions.get('window');

const centerAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};
const buttonTextStyle = {
  fontSize: baseStyles.buttonFontSize,
  fontFamily: baseStyles.latoRegular,
};

export const styles = StyleSheet.create({
	container: {
		flex: 1,
    width: '100%',
		...centerAlignStyle,
	},
	footerStyle: {
		flex: 0.08,
    width: '100%',
	},
	contentView: {
		flex: 1,
    width: '40%',
		alignItems: 'center'
	},
	topHeaderView: {
		flex: 0.25,
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	signInTxt: {
		color: baseStyles.eclipseColor,
		marginBottom: scaledHeight(10),
		fontSize: scaledFont(36),
		fontFamily: baseStyles.latoBold
	},
	signInTxtSmall: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(18),
		fontFamily: baseStyles.latoRegular
	},
	signInBtn: {
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle,
	},
	signInBtnTxt: {
		color: '#ffffff',
		...buttonCommonStyle.buttonTextStyle,
	},
	flatListView: {
		flex: 0.7,
    width: '100%',
    alignItems: 'center',
		marginVertical: scaledHeight(40)
	},
  childContainerStyle: {
    flex: 0.7,
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginLeft: baseStyles.margin30,
  },
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
		}
	},
  childViewStyle: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: baseStyles.padding20,
    marginTop: baseStyles.margin20,
    borderRadius: baseStyles.borderRadius,
  },
  childImageContainerStyle: {
    flex: 0.2,
    ...centerAlignStyle,
  },
  childNameStyle: {
    lineHeight: baseStyles.lineHeightMedium,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
    fontSize: scaledFont(18),
  },
  classLabelText: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkGrayTintColor,
    lineHeight: baseStyles.lineHeightMedium,
  },
  childBCText: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkGrayTintColor,
    lineHeight: baseStyles.lineHeightMedium,
  },
  checkIconView: {
    position: 'absolute',
    right: 10,
    top: 10
  },
  headerStyle: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: baseStyles.padding15,
    borderBottomColor: baseStyles.platinumColor,
    borderBottomWidth: 1,
  },
  successScreenContainer: {
    flex: 1,
    backgroundColor: baseStyles.white,
    ...centerAlignStyle,
  },
  contentContainer: {
    flex: 0.8,
    ...centerAlignStyle,
  },
  checkInTitleStyle: {
    fontFamily: baseStyles.latoBold,
    fontSize: scaledFont(36),
    color: baseStyles.darkShadeGray,
  },
  descriptionStyle: {
    marginTop: baseStyles.margin10,
    color: baseStyles.darkShadeGray,
    ...buttonTextStyle,
    fontSize: scaledFont(24)
  },
  doneContainer: {
    flex: 0.2,
    alignItems: 'center'
  },
  doneButtonStyle: {
    ...buttonCommonStyle.butttonStyle,
    ...buttonCommonStyle.commonStyle,
  },
  doneButtonTextStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  marginTop50: {
    marginTop: baseStyles.margin50,
  },
  width100p: {
    width: '100%',
  },
  infoTextStyle: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.h3FontSize,
    color: baseStyles.avatharTextColor
  }
});