import React from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { t } from '../../utils/LocalizationUtils.js';
import { styles } from './ChildCheckInOut.style';
import { getChildName } from '../../utils';
import { get } from 'lodash';

const ChildCheckInOutSuccess = props => {
  const { navigation } = props;
  const isCheckIn = navigation.getParam('isCheckIn');
  const childDetails = navigation.getParam('childDetails', []);
  const checkInOutText = isCheckIn ? t('common.snKioskCheckIn') : t('common.snKioskCheckOut');

  return (
    <View style={styles.successScreenContainer}>
      <View style={styles.contentContainer}>
        <MaterialIcons
          size={120}
          name='check-circle'
          color='#12B886'
        />
        <Text style={[styles.checkInTitleStyle, styles.marginTop50]}>
          {t('common.successMessageVisitor', { checkInOutText })}
        </Text>
        {childDetails.map(ch => (
          <Text 
            key={`child_${ch.ID}`}
            style={styles.descriptionStyle}
          >{getChildName(ch)}</Text>
        ))}
      </View>
      <View style={styles.doneContainer}>
        <TouchableOpacity 
          onPress={() => navigation.navigate('Home')}
          style={[styles.primaryColor, styles.doneButtonStyle]}>
            <Text style={styles.doneButtonTextStyle}>{t('common.done')}</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
}

const mapStateToProps = state => ({
	schoolConfig: get(state, 'schoolConfig', {}),
});

export default connect(mapStateToProps, {})(ChildCheckInOutSuccess);