import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	FlatList,
	Dimensions,
	StyleSheet,
	ImageBackground,
	TouchableOpacity,
} from 'react-native';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import _ from 'lodash';
import { connect } from 'react-redux';
import CachedImage from '../components/CachedImage';
import Constant from '../constants/Constant';
import moment from 'moment';
import { getChildName } from '../utils';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class BusAttendantSuccess extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	renderChild = ({ item }) => {
		return <View 
		style={styles.eachChild}>
			<CachedImage
	            style={styles.childImage}
	            source={item.imageKey}
	        />
			<Text 
			style={styles.childName}>{getChildName(item)}</Text>
		</View>
	}
	renderChildDetails() {
		const { navigation } = this.props;
		const plateNumber = navigation.getParam('plateNumber', '');
		const isSignIn = navigation.getParam('isSignIn', false);
		const checkedList = navigation.getParam('checkedList', []);
		const childList = navigation.getParam('childList', []);
		const checkedChild = childList.filter(child => checkedList.indexOf(child.ID) >= 0);
		const isSignInTxt = isSignIn ? Constant.CHECKED_IN : Constant.CHECKED_OUT;

		return <View 
		style={{
			...styles.container,
			paddingHorizontal: baseStyles.padding50
		}}>
			<Text style={styles.successTxt}>
				{t('common.kioskBusNumber', { plateNumber })}
			</Text>
			<Text style={{
				...styles.successTxt,
				marginTop: baseStyles.margin5
			}}>
				{t('common.kioskBusSuccessMessageTxt', {
					time: moment().format('hh:mm A'),
					isSignInTxt, 
				})}
			</Text>
			<View style={{ width: deviceWidth * 0.4 }}>
				<View 
				style={styles.flatListView}>
					<FlatList
						data={checkedChild} 
						extraData={this.state} 
						style={styles.flatList} 
						removeClippedSubviews={false}  
						showsVerticalScrollIndicator={false} 
						keyExtractor={item => item.ID.toString()}
						renderItem={this.renderChild}
					/>
				</View>
				<TouchableOpacity 
				onPress={() => navigation.navigate('Home')}
				style={[
					styles.doneBtn,
					{ backgroundColor: CustomConfig.Colors.btnPrimaryBgColor }
				]}>
					<Text 
					style={styles.doneBtnTxt}>{t('common.kioskDoneBtnTxt')}</Text>
				</TouchableOpacity>
			</View>
		</View>
	}
	render() {
		const { schoolConfig } = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', '');
		
    if (school === 'TCC') {
			return <View style={styles.container}>
				{this.renderChildDetails()}
			</View>
		}

		return <ImageBackground 
		resizeMode='cover' 
		style={styles.container}
		source={require('../images/background-image.png')}>
			{this.renderChildDetails()}
		</ImageBackground>
	}
}

const mapStateToProps = state => ({
	schoolConfig: state.schoolConfig
});

export default connect(mapStateToProps, {})(BusAttendantSuccess);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	successTxt: {
		textAlign: 'center',
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoRobotoMedium,
		color: baseStyles.eclipseColor
	},
	doneBtn: {
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle,
	},
	doneBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle,
	},
	flatListView: {
		maxHeight: deviceHeight * 0.4,
		marginVertical: scaledHeight(30),
		alignItems: 'center',
		width: '100%',
	},
	flatList: {
		width: '100%',
		borderTopWidth: 1,
		borderTopColor: '#dadada'
	},
	eachChild: {
		flexDirection: 'row',
		paddingVertical: 10,
		alignItems: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#dadada'
	},
	childImage: {
		marginRight: scaledWidth(20),
		height: scaledFont(40),
		width: scaledFont(40),
		borderRadius: scaledFont(40)/2,
		backgroundColor: baseStyles.lightGreyColor,
	},
	childName: {
		fontFamily: baseStyles.latoRobotoMedium,
		fontSize: scaledFont(18),
		color: baseStyles.eclipseColor
	}
});