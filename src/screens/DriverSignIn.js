import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity
} from 'react-native';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { connect } from 'react-redux';
import { baseStyles } from '../config/baseStyles';
import { 
	TextField 
} from 'react-native-material-textfield';
import { 
	busDriverLogin,
	updateErrorMessage
} from '../redux/actions';
import { 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { 
	errorResponse,
	retrieveAsyncStorage, 
	handleErrorDataFromApi  
} from '../utils';
import _ from 'lodash';
import Feather from 'react-native-vector-icons/Feather';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth } = Dimensions.get('window');

class DriverSignIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pwdFocus: false,
			password: '',
			staffList: null,
			secureTextEntry: true,
		};
	}
	async onBusDriverSignIn() {
		const { password } = this.state;
		const fkCentre = await retrieveAsyncStorage('centreId');
		if (password.trim().length === 0 || !fkCentre) {
			return;
		}
		const { navigation, busDriverLogin } = this.props;
		const reqData = { 
			centreID: parseInt(fkCentre),
			passCode: password 
		};
		busDriverLogin(reqData).then(data => {
			if (data.success) {
				navigation.navigate('BusRouteList', {
					passCode: password
				});
				return;
			}
			const message = handleErrorDataFromApi(data, errorResponse);
			let messageText = message ? message : t('error.defaultError');
			if (message.indexOf('no rows in result set') >= 0) {
				messageText = t('common.kioskNoBusRoute');
			} else if (message.indexOf('Invalid Bus Attendant Passcode') >= 0) {
				messageText = t('common.kioskInvalidBusAttendantCode');
			}
			message && this.props.updateErrorMessage(messageText, this.onBusDriverSignIn.bind(this))
		});
	}
	renderPasswordAccessory() {
		const { secureTextEntry } = this.state;
		const name = secureTextEntry ? "eye-off" : "eye";
		return (
			<Feather
				size={scaledFont(16)}
				name={name}
				color={"#dadada"}
				onPress={this.onAccessoryPress.bind(this)}
			/>
		);
	}
	onAccessoryPress() {
		this.setState(({ secureTextEntry }) => ({
			secureTextEntry: !secureTextEntry
		}));
	}
	render() {
		const { navigation } = this.props;
		const { pwdFocus, password, secureTextEntry } = this.state;

		const isDisabled = password && password.trim().length > 0 ? false : true;

		return <View 
		style={styles.container}>
			<Header
				backButton 
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			<View 
			style={{
				...styles.container,
				paddingHorizontal: baseStyles.padding30
			}}>
				<View
				style={styles.signInView}>
					<Text 
					style={styles.signInTxt}>{t('common.kioskBusAttendantText')}</Text>
				</View>
				<View style={styles.contentView}>
					<View 
					style={styles.formView}>
						<Text 
						style={styles.labelStyle}>{t('common.kioskPasscode')}</Text>
						<TextField 
							placeholder={t('common.kioskPasscode')} 
							label="" 
							tintColor={baseStyles.labelColor}
							errorColor="#ec3535" 
							style={styles.fieldStyle} 
							value={password} 
							secureTextEntry={secureTextEntry} 
							renderAccessory={this.renderPasswordAccessory.bind(this)}
							onEndEditing={() => this.setState({ pwdFocus: false })} 
							onFocus={() => this.setState({ pwdFocus: true })} 
							onChangeText={password => this.setState({ password })}
							inputContainerStyle={[
								styles.textFieldStyle,
								{ borderBottomColor: pwdFocus ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada' }
							]} 
						/>
					</View>
					<View 
					style={styles.nextBtnView}>
						<TouchableOpacity 
						disabled={isDisabled}
						onPress={this.onBusDriverSignIn.bind(this)}
						style={[
							styles.nextBtn,
							styles.primaryColor,
							{ opacity: isDisabled ? baseStyles.disabledOpacity : 1 }
						]}>
							<Text 
							style={styles.nextBtnTxt}>{t('common.kioskNextBtnTxt')}</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
			<Footer 
			style={styles.footerStyle}/>
		</View>
	}
}


const mapStateToProps = state => ({
	loginData: state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	busDriverLogin,
	updateErrorMessage
});

export default connect(mapStateToProps, mapDispatchToProps)(DriverSignIn);

const centerAlignStyle = {
	alignItems: 'center',
	justifyContent: 'center'
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		...centerAlignStyle,
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	textFieldStyle: {
		width: '100%',
		paddingTop: scaledHeight(5),
		height: scaledHeight(35),
		borderBottomWidth: 2
	},
	contentView: {
		flex: 0.33,
		width: deviceWidth * 0.45,
		...centerAlignStyle,
	},
	signInView: {
		flex: 0.1,
		...centerAlignStyle,
	},
	signInTxt: {
		fontSize: scaledFont(24),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		textTransform: 'capitalize',
		textAlign: 'center',
	},
	labelStyle: {
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoSemibold
	},
	formView: {
		flex: 1,
		justifyContent: 'center',
		marginTop: scaledHeight(40),
		width: '100%',
	},
	fieldStyle: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold
	},
	nextBtnView: {
		flex: 1,
		width: '100%',
		...centerAlignStyle,
	},
	nextBtn: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle,
	},
	nextBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle,
	},
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor 
		}
	}
});