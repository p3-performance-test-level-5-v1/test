import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { scaledHeight, scaledFont } from '../../utils/Scale';
const { width: deviceWidth } = Dimensions.get('window');

const centerAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

export default styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
  },
  contentContainerStyle: {
    flexGrow: 1,
    paddingHorizontal: 200,
    paddingVertical: baseStyles.padding50,
    justifyContent: 'center'
  },
  extraPadding: {
    paddingBottom: 200,
  },
  avatarContainer: {
    alignSelf: 'center',
    justifyContent: 'flex-end',
    marginBottom: baseStyles.margin20,
  },
  checkInTextStyle: {
    textAlign: 'center',
    marginTop: baseStyles.margin25,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
    fontSize: baseStyles.labelFontSize24,
  },
  checkInDescStyle: {
    textAlign: 'center',
    lineHeight: 30,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.textInputFont16,
    marginBottom: baseStyles.margin40,
  },
  addDescStyle: {
    alignSelf: 'flex-start',
    lineHeight: 30,
    color: baseStyles.avatharTextColor,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    marginTop: baseStyles.margin40,
    marginBottom: baseStyles.margin15,
  },
  checkInHealthStyle: {
    marginBottom: baseStyles.margin5,
  },
  listTemperature: {
    flexGrow: 0,
    flexShrink: 0,
  },
  separator: {
    height: scaledHeight(10),
  },
  textInputContainer: {
    borderBottomColor: '#dadada',
    paddingTop: baseStyles.padding5,
    height: scaledHeight(35),
    borderBottomWidth: 2,
  },
  pickerContainer: {
    marginTop: baseStyles.margin25,
  },
  errorMessageStyle: {
    marginTop: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkRed,
  },
  textInputStyle: {
    fontSize: baseStyles.textInputFont16,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  labelStyle: {
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  timePickerContainer: {
    flex: 1,
  },
  datePickerContainer: {
    flex: 1,
    marginTop: baseStyles.margin25,
  },
  get addHealthBtnStyle() {
    return {
      alignSelf: 'center',
      marginTop: baseStyles.margin50,
      backgroundColor: CustomConfig.Colors.primaryColor,
      ...buttonCommonStyle.butttonStyle,
      ...buttonCommonStyle.commonStyle,
    }
  },
  checkInTxtStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  arrowStyle: {
    backgroundColor: 'transparent',
    height: 0,
  },
  popoverOutsite: {
    backgroundColor: 'transparent',
  },
  popover: {
    borderRadius: 0,
  },
  extraMargin: {
    marginTop: baseStyles.margin50,
  },
  errorField: { 
    borderBottomColor: baseStyles.darkRed
  },
  tempPlaceholder: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkGrayTintColor,
  },
  tempTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: baseStyles.padding10,
    borderBottomColor: baseStyles.darkGrayTintColor,
    borderBottomWidth: 1,
  },
  tempValue: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.textInputFont16,
    color: baseStyles.darkShadeGray,
  },
  textInputStyle: {
    fontSize: baseStyles.buttonFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  textAreaStyle: {
    width: '100%',
    marginTop: baseStyles.margin15,
    height: baseStyles.textareaViewHeight,
    borderColor: baseStyles.lightGrey,
    borderRadius: baseStyles.defaultBorderRadius,
    padding: baseStyles.padding10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1,
    fontSize: scaledFont(18)
  },
  remarksText: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  remarksCountText: {
    marginTop: baseStyles.margin5,
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
    textAlign: 'right',
  },
  verticalMargin: {
    marginVertical: baseStyles.margin25
  },
});
