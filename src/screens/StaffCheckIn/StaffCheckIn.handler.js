import { useState, useEffect, useRef } from 'react';
import Constant from '../../constants/Constant.js';
import { t } from '../../utils/LocalizationUtils.js';
import * as SchoolConfig from '../../utils/SchoolConfig';
import { get } from 'lodash';
import { errorResponse, handleErrorDataFromApi, retrieveAsyncStorage } from '../../utils';
import moment from 'moment';

export default Handler = (props) => {
  const {
    navigation,
    updateErrorMessage,
    staffCheckInOutV3,
    getStaffHealthRecords,
    addStaffHealthRecord,
    staffToken,
    showCustomAlert,
  } = props;

  let isApiRequested = false;
  const [isRequesting, setIsRequesting] = useState(false);
  const [tempValue, setTempValue] = useState('36.5');
  const [tempError, setTempError] = useState('');
  const [temperatureThreshold, setTemperatureThreshold] = useState(37.5);
  const [isFocusTempInput, setIsFocusTempInput] = useState(false);
  const [showPopover, setShowPopover] = useState(false);

  const [time, setTime] = useState(moment().format('hh:mm A'));
  const [date, setDate] = useState(moment().format('DD/MM/YYYY'));
  const [remarks, setRemarks] = useState('');

  const scrollVieWRef = useRef(null);
  const inputRef = useRef(null);

  useEffect(() => {
    getTodayTemperatureHistory();
    getTemperatureThreshold();
  }, []);

  useEffect(() => {
    !showPopover && tempValidation(tempValue);
  }, [showPopover]);

  const onTimeChange = (time) => {
    setTime(time);
  };

  const onDateChange = (date) => {
    setDate(date);
  };

  const onTempChange = (value) => {
    setTempValue(value.toString());
  };

  const onTempInputStart = () => {
    setTempError('');
    setIsFocusTempInput(true);
    setTimeout(() => {
      scrollVieWRef.current.scrollToEnd({ animated: true });
    }, 100);
    setTimeout(() => {
      setShowPopover(true);
    }, 300);
  };

  const onCloseKeyboard = () => {
    setIsFocusTempInput(false);
    setShowPopover(false);
  };

  const addHealthRecordAction = async () => {
    const staffId = navigation.getParam('staffId', '');
    const staffName = navigation.getParam('staffName', '');
    const actionType = navigation.getParam('actionType');
    let centreId = await retrieveAsyncStorage('centreId');
    if (!staffId || !centreId || isApiRequested) {
      return;
    }

    try {
      isApiRequested = true;
      setIsRequesting(true);
      const reqData = {
        staffHealthRecordDtos: [
          {
            StaffId: staffId,
            HealthRecords: [
              {
                FkStaff: staffId,
                Value: tempValue,
                HealthTypes: 'check',
                Time: moment(`${date} ${time}`, 'DD/MM/YYYY hh:mm A').format('YYYY-MM-DD HH:mm:ss'),
              },
            ],
          },
        ],
        at: moment().format('YYYY-MM-DD HH:mm:ss'),
      };
  
      const result = await addStaffHealthRecord(reqData);
      const status = get(result, 'success', false);
      isApiRequested = false;
      setIsRequesting(false);

      if (status) {
        navigation.navigate('StaffSignInSuccess', { 
          staffName, 
          actionType, 
          temperature: tempValue, 
        });
        return;
      }
      const message = handleErrorDataFromApi(result, errorResponse);
      message && updateErrorMessage(message, addHealthRecordAction);
    } catch (error) {
      isApiRequested = false;
      setIsRequesting(false);
      throw error;
    }
  };

  const checkInAction = async () => {
    const userId = navigation.getParam('userId', '');
    const staffName = navigation.getParam('staffName', '');
    const actionType = navigation.getParam('actionType');
    let centreId = await retrieveAsyncStorage('centreId');

    try {
      if (isApiRequested) return;
      isApiRequested = true;
      setIsRequesting(true);
      const result = await staffCheckInOutV3(
        {
          IDCentre: centreId,
          type: Constant.TYPE_CHECK_IN,
          temperature: Number(tempValue),
          remarks,
          userId,
        },
        staffToken
      );
      isApiRequested = false;
      setIsRequesting(false);

      if (get(result, 'success', false)) {
        navigation.navigate('StaffSignInSuccess', { staffName, actionType });
        return;
      }
      const errorCode = get(result, 'error.0.extensions.errorCode');
      const message = handleErrorDataFromApi(result, errorResponse);

      if (errorCode === 'ERR_CHECKIN_ON_LEAVE_DAY') {
        showCustomAlert(true, {
          title: 'Error',
          desc: t('error.checkInOnLeaveError'),
          primaryButtonText: 'OK',
          primaryButtonCallback: () => navigation.navigate('StaffSignIn'),
        })
        return;
      }
      message && updateErrorMessage(message, checkInAction);
    } catch (error) {
      isApiRequested = false;
      setIsRequesting(false);
      throw error;
    }
  };

  const getTemperatureThreshold = async () => {
    const value = await SchoolConfig.getValueByKey('body_temperature_threshold');
    setTemperatureThreshold(parseFloat(value));
  };

  const tempValidation = (tempValue) => {
    if (!tempValue) {
      error = t('common.temperatureRequired');
      setTempError(error);
      return;
    }

    const value = Number(tempValue);

    if (tempValue >= temperatureThreshold) {
      error = t('common.temperatureInvalid');
      setTempError(error);
      return;
    }

    if (value < 36) {
      error = t('common.temperatureWrong');
      setTempError(error);
      return;
    }

    setTempError('');
  };

  const getTodayTemperatureHistory = async () => {
    let centreId = await retrieveAsyncStorage('centreId');
    const staffId = navigation.getParam('staffId', '');

    getStaffHealthRecords({
      centre: centreId,
      fromDate: moment().format('YYYY-MM-DD'),
      toDate: moment().format('YYYY-MM-DD'),
      filter: {
        fkStaff: staffId,
      },
      pagination: {
        sort: 'time',
      },
    });
  };

  const isInvalidTime = moment(`${date} ${time}`, 'DD/MM/YYYY hh:mm A').isAfter(moment());

  return {
    tempValue,
    onTempChange,
    tempError,
    time,
    onTimeChange,
    date,
    onDateChange,
    addStaffHealthRecord,
    isInvalidTime,
    addHealthRecordAction,
    checkInAction,
    isFocusTempInput,
    scrollVieWRef,
    tempValidation,
    inputRef,
    showPopover,
    setShowPopover,
    onTempInputStart,
    onCloseKeyboard,
    isRequesting,
    setRemarks,
    remarks,
  };
};
