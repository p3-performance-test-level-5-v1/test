import React from 'react';
import { connect } from 'react-redux';
import { 
  View, 
  Text, 
  TouchableOpacity, 
  FlatList, 
  ScrollView, 
  TextInput 
} from 'react-native';
import { get } from 'lodash';
import moment from 'moment';
import Popover from 'react-native-popover-view';
import { baseStyles } from '../../config/baseStyles';
import AvatarNavigator from '../../components/AvatarNavigator';
import styles from './StaffCheckIn.style';
import Handler from './StaffCheckIn.handler';
import { t } from '../../utils/LocalizationUtils.js';
import Header from '../../components/Header';
import DateTimePicker from '../../components/DateTimePicker';
import {
  updateErrorMessage,
  fetchCheckInCheckOutToken,
  staffCheckInOutV3,
  getStaffHealthRecords,
  addStaffHealthRecord,
  addStaffCheckInCheckOut,
  showCustomAlert,
} from '../../redux/actions';
import { commonStyle } from '../../config/commonStyle';
import TemperatureItem, { EmptyRecords } from '../../components/TemperatureItem';
import Constant from '../../constants/Constant';
import TemperaturePicker from '../../components/TemperaturePicker';
import withPreventDoubleClick from '../../components/TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

const StaffCheckIn = (props) => {
  const { navigation, staffHealthRecords } = props;

  const isCheckIn = navigation.getParam('actionType') === Constant.TYPE_CHECK_IN;

  const {
    tempValue,
    onTempChange,
    tempError,
    time,
    onTimeChange,
    date,
    onDateChange,
    isInvalidTime,
    addHealthRecordAction,
    checkInAction,
    isFocusTempInput,
    scrollVieWRef,
    onTempInputStart,
    inputRef,
    showPopover,
    onCloseKeyboard,
    isRequesting,
    setRemarks,
    remarks,
  } = Handler(props);

  const staffName = navigation.getParam('staffName', '');
  const staffImageKey = navigation.getParam('staffImageKey', '');
  const isTempError = tempError.trim().length > 0;
  const isDisable = isTempError || isInvalidTime;

  return (
    <View style={styles.container}>
      <Header backButton navigation={navigation} style={commonStyle.headerStyle} />
      <View style={styles.contentContainer}>
        <ScrollView
          ref={scrollVieWRef}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={[styles.contentContainerStyle, isFocusTempInput && !isCheckIn && styles.extraPadding]}
        >
          <View style={styles.avatarContainer}>
            <AvatarNavigator
              imageUrl={staffImageKey}
              avatharText={staffName}
              customHeight={200}
              customWidth={200}
              customFontSize={80}
            />
          </View>

          <Text style={styles.checkInTextStyle}>{t('common.kioskWelcomeText', { staffName })}</Text>

          {isCheckIn && <Text style={styles.checkInDescStyle}>{t('common.checkinRemarkDescription')}</Text>}

          {!isCheckIn && (
            <>
              <Text style={[styles.addDescStyle, styles.checkInHealthStyle]}>
                {t('common.kioskTemperatureHistory')}
              </Text>
              <FlatList
                style={styles.listTemperature}
                bounces={false}
                numColumns={3}
                data={staffHealthRecords}
                renderItem={({ item }) => <TemperatureItem data={item} />}
                ListEmptyComponent={EmptyRecords}
                ItemSeparatorComponent={() => <View style={styles.separator} />}
              />

              <Text style={styles.addDescStyle}>{t('common.kioskTempAddDesc')}</Text>
            </>
          )}

          <Text style={styles.labelStyle}>{t('common.kioskTempText')}</Text>
          <TouchableOpacity ref={inputRef} activeOpacity={1} onPress={onTempInputStart}>
            <View style={styles.tempTextContainer}>
              <Text style={styles.tempValue}>{tempValue}</Text>
              <Text style={styles.tempValue}>°C</Text>
            </View>
          </TouchableOpacity>
          {isTempError && <Text style={styles.errorMessageStyle}>{tempError}</Text>}
          <Popover
            from={inputRef}
            isVisible={showPopover}
            onRequestClose={onCloseKeyboard}
            placement={'bottom'}
            arrowStyle={styles.arrowStyle}
            backgroundStyle={styles.popoverOutsite}
            popoverStyle={[styles.popover, isTempError && styles.extraMargin]}
          >
            <TemperaturePicker
              handleTemperature={onTempChange}
              marginBetween={isTempError ? 10 : 0}
              showKeyboard
              temperature={tempValue}
              setTemperatureMarked={onCloseKeyboard}
            />
          </Popover>

          {!isCheckIn ? (
            <>
              <View style={styles.pickerContainer}>
                <Text style={styles.labelStyle}>{t('common.time')}</Text>
                <DateTimePicker
                  baseColor={baseStyles.lightGray}
                  iconColor={baseStyles.grayishBlue}
                  onDateTimeChange={onTimeChange}
                  pickerMode="time"
                  dateTime={time}
                  pickerFomat="hh:mm A"
                  iconName="clock-outline"
                />
                <View style={styles.datePickerContainer}>
                  <Text style={styles.labelStyle}>{t('common.date')}</Text>
                  <DateTimePicker
                    baseColor={baseStyles.lightGray}
                    iconColor={baseStyles.grayishBlue}
                    onDateTimeChange={onDateChange}
                    pickerMode="date"
                    dateTime={date}
                    pickerFomat="DD/MM/YYYY"
                    maxDate={moment().format('DD/MM/YYYY')}
                  />
                </View>
              </View>
              {isInvalidTime && <Text style={styles.errorMessageStyle}>{t('common.invalidTime')}</Text>}
            </>
          ) : (
            <View style={styles.verticalMargin}>
              <Text style={styles.remarksText}>
                {t('common.remarkFieldTitle')}
              </Text>
              <TextInput
                multiline
                value={remarks}
                autoCorrect={false}
                autoCapitalize="none"
                placeholder={t('common.remarkPlaceholderText')}
                style={[
                  styles.textInputStyle, 
                  styles.textAreaStyle
                ]}
                onChangeText={value => setRemarks(value)}
                maxLength={Constant.CHARACTER_LIMIT}
              />
              <Text style={styles.remarksCountText}>
                {`${Constant.CHARACTER_LIMIT - remarks.length}/${Constant.CHARACTER_LIMIT}`}
              </Text>
            </View>
          )}

          <TouchableButton
            disabled={isDisable || isRequesting}
            style={[
              styles.addHealthBtnStyle,
              { opacity: isDisable || isRequesting ? baseStyles.disabledOpacity : 1 },
            ]}
            onPress={isCheckIn ? checkInAction : addHealthRecordAction}
          >
            <Text style={styles.checkInTxtStyle}>{isCheckIn ? t('common.kioskCheckIn') : t('common.addRecord')}</Text>
          </TouchableButton>
        </ScrollView>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  schoolConfig: get(state, 'schoolConfig', {}),
  staffHealthRecords: get(state, 'getStaffHealthRecord.data', []),
  staffToken: get(state, 'loginByPin.data')
});

const mapDispatchToProps = {
  updateErrorMessage,
  fetchCheckInCheckOutToken,
  staffCheckInOutV3,
  getStaffHealthRecords,
  addStaffHealthRecord,
  addStaffCheckInCheckOut,
  showCustomAlert,
};

export default connect(mapStateToProps, mapDispatchToProps)(StaffCheckIn);
