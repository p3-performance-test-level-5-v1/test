import { useState } from 'react';
import { t } from '../../utils/LocalizationUtils.js';
import { 
  errorResponse, 
  handleErrorDataFromApi, 
  retrieveAsyncStorage 
} from '../../utils';
import { get, isEmpty, uniqBy } from 'lodash';
import moment from 'moment';

export default Handler = (props) => {
  const { 
    findVisitorLog, 
    updateErrorMessage, 
    checkoutVisitor,
    navigation,
  } = props;

  const [nricPhoneFinPassport, setNricPhoneFinPassport] = useState('');
  const [visitorLogs, setVisitorLogs] = useState([]);
  const [disableBtn, setDisableBtn] = useState(false);

  const onNricOrPhoneOrPassportChange = (value) => {
    setNricPhoneFinPassport(value);
  };

  const resetForm = () => {
    setVisitorLogs([]);
    setNricPhoneFinPassport('');
  }

  const onVisitorLogSearch = async () => {
    const reqData = {};
    const accessToken = await retrieveAsyncStorage('accessToken');
    if (nricPhoneFinPassport.trim().length > 0) {
      Object.assign(reqData, { nric: nricPhoneFinPassport, countryCode: '+65' });
    }
    
    if (isEmpty(reqData) || !accessToken) {
      return
    }

    const result = await findVisitorLog(reqData, accessToken);
    if (result.success) {
      const logsForNric = get(result, 'data.getVisitorLog1.data', []);
      const logsForPhone = get(result, 'data.getVisitorLog2.data', []);
      const logs = uniqBy([...logsForNric, ...logsForPhone ], 'ID');
      const today = moment().format('YYYY-MM-DD');

      const checkOutData = logs.filter(log => {
        const checkInTime = moment(get(log, 'checkInTime')).format('YYYY-MM-DD');
        if(!get(log, 'checkOutTime') && moment(today).diff(moment(checkInTime), 'days') === 0) {
          return log;
        }
        return;
      });
      setVisitorLogs(checkOutData.map(dt => ({ ...dt, isChecked: true })));
      if (checkOutData.length === 0) {
        updateErrorMessage(t('common.noVisitorLogWarning'));
      }
      return;
    }
    const message = handleErrorDataFromApi(result, errorResponse);
		message && updateErrorMessage(message, onVisitorLogSearch);
  }

  const visitorCheckOut = async () => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    let centreId = await retrieveAsyncStorage('centreId');
    const checkedLogs = visitorLogs.filter(log => get(log, 'isChecked'));

    if (!accessToken || !centreId || checkedLogs.length === 0) {
      return;
    }
    
    const reqData = {
      fkCentre: parseInt(centreId),
      checkOutTime: moment().format('YYYY-MM-DD HH:mm:ss'),
      visitorIDs: checkedLogs.map(log => (log.ID)),
    };

    setDisableBtn(true);
    const result = await checkoutVisitor(reqData, accessToken);
    setDisableBtn(false);
    if (get(result, 'data.checkoutVisitor', false) && get(result, 'success', false)) {
      navigation.navigate('CheckInOutSuccess', { isCheckIn: false });
      return;
    }
    const message = handleErrorDataFromApi(result, errorResponse);
		message && updateErrorMessage(message, onVisitorLogSearch);
  }

  const onVisitorLogChecked = (visitorID = '') => {
    if(!visitorID) {
      return;
    }

    const position = visitorLogs.findIndex(log => (visitorID === log.ID));
    if (position >= 0) {
      const visitorLog = visitorLogs[position];
      visitorLog.isChecked = visitorLog.isChecked ? false : true;
      visitorLogs[position] = visitorLog;
    }
    setVisitorLogs(visitorLogs.concat([]));
  }

  return ({
    resetForm,
    visitorLogs,
    onVisitorLogSearch,
    nricPhoneFinPassport,
    onNricOrPhoneOrPassportChange,
    onVisitorLogChecked,
    visitorCheckOut,
    disableBtn,
  });
}