import { StyleSheet, Dimensions } from 'react-native';
import CustomConfig from '../../utils/CustomConfig';
import { baseStyles } from '../../config/baseStyles';
import { scaledHeight, scaledFont } from '../../utils/Scale';
import { buttonCommonStyle } from '../../config/buttonStyle';
const { width: deviceWidth } = Dimensions.get('window');

const centerAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    ...centerAlignStyle,
  },
  contentContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    backgroundColor: baseStyles.white,
  },
  descContainer: {
    flex: 0.25,
    ...centerAlignStyle,
  },
  checkOutTextStyle: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize24,
    fontFamily: baseStyles.latoBold,
  },
  descriptionStyle: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.buttonFontSize,
    marginTop: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
  },
  formContainer: {
    width: deviceWidth * 0.4,
  },
  labelStyle: {
    color: baseStyles.eclipseColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  textFieldStyle: {
    fontSize: baseStyles.textInputFont,
    color: baseStyles.eclipseColor,
    fontFamily: baseStyles.latoBold
  },
  textFieldContainerStyle: { 
    width: '100%',
    borderBottomColor: '#dadada',
    paddingTop: baseStyles.padding5,
    height: scaledHeight(35),
    borderBottomWidth: 2
  },
  orTextContainer: {
    width: '100%',
    alignItems: 'center',
  },
  orTextStyle: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.buttonFontSize,
    fontFamily: baseStyles.latoSemibold,
  },
  searchContainer: {
    width: deviceWidth * 0.4,
    marginTop: baseStyles.margin30,
  },
  get searchButtonStyle() {
    return {
      width: '100%',
      borderWidth: 1,
      height: baseStyles.btnHeight,
      borderColor: CustomConfig.Colors.primaryColor,
      borderRadius: baseStyles.borderRadius,
      ...centerAlignStyle,
    }
  },
  get searchButtonTextStyle() {
    return {
      color: CustomConfig.Colors.primaryColor,
      ...buttonCommonStyle.buttonTextStyle,
    }
  },
  visitorListContainer: {
    flex: 0.6,
    justifyContent: 'center',
    paddingHorizontal: baseStyles.padding20,
    marginTop: baseStyles.margin30,
    width: deviceWidth * 0.4
  },
  checkOutBtnContainer: {
    width: deviceWidth * 0.4,
    marginTop: baseStyles.margin30,
  },
  get checkOutBtnStyle() {
    return {
      width: '100%',
      height: baseStyles.btnHeight,
      backgroundColor: CustomConfig.Colors.primaryColor,
      borderRadius: baseStyles.borderRadius,
      ...centerAlignStyle,
    }
  },
  checkOutBtnTxtStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  itemContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  visitorDetailContainer: {
    flex: 1,
    marginLeft: baseStyles.margin10,
  },
  visitorNameStyle: {
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoSemibold,
    fontSize: baseStyles.textInputFont,
  },
  checkInTimeStyle: {
    color: '#6C6C6C',
    marginTop: baseStyles.margin5,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.buttonFontSize,
  },
  marginBottom: {
    marginBottom: baseStyles.margin20,
  },
  mandatoryIconStyle: {
    color: '#DE7B39',
    fontSize: baseStyles.buttonFontSize,
  },
  checkboxStyle: { 
    padding: scaledFont(10) 
  },
});
