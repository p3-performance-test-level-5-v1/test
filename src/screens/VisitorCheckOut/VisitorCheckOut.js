import React from 'react';
import { View, TouchableOpacity, Text, FlatList } from "react-native";
import { connect } from 'react-redux';
import { TextField } from 'react-native-material-textfield';
import { baseStyles } from '../../config/baseStyles';
import Header from '../../components/Header';
import { t } from '../../utils/LocalizationUtils.js';
import CheckBox from 'react-native-check-box';
import Handler from './VisitorCheckOut.handler';
import styles from './VisitorCheckOut.style';
import moment from 'moment';
import { 
  findVisitorLog, 
  updateErrorMessage,
  checkoutVisitor,
} from '../../redux/actions';
import { get } from 'lodash';
import { commonStyle } from '../../config/commonStyle';

const VisitorCheckOut = props => {
  const { navigation, schoolConfig } = props;
  const {
    resetForm,
    visitorLogs,
    onVisitorLogSearch,
    nricPhoneFinPassport,
    onNricOrPhoneOrPassportChange,
    onVisitorLogChecked,
    visitorCheckOut,
    disableBtn,
  } = Handler(props);
  const checkedLogs = visitorLogs.filter(log => (log.isChecked));

  const school = get(schoolConfig, 'data.data[0].school.code', '');

  const renderVisitorLog = (item) => {
    const visitorName = `${get(item, 'name', '')}`;
    return (
      <View style={styles.itemContainer}>
        <CheckBox
          style={styles.checkboxStyle}
			    onClick={()=> onVisitorLogChecked(item.ID)} 
			    checkBoxColor={baseStyles.grayishBlue} 
			    isChecked={item.isChecked || false}
        />
        <View style={styles.visitorDetailContainer}>
          <Text style={styles.visitorNameStyle}>{visitorName}</Text>
          {(item.checkInTime) && (
            <Text style={styles.checkInTimeStyle}>
              {t('common.checkInTimeVisitor', { 
                checkInTime: moment(item.checkInTime).format('HH:mm A') 
              })}
            </Text>
          )}
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        backButton 
        navigation={navigation}
        style={commonStyle.headerStyle}
      />
      <View style={styles.contentContainer}>
        <View style={styles.descContainer}>
          <Text style={styles.checkOutTextStyle}>{t('common.visitorCheckOut')}</Text>
          <Text style={styles.descriptionStyle}>{t('common.fillFormForCheckOutVisitor')}</Text>
        </View>
        <View style={styles.formContainer}>
          <Text style={styles.labelStyle}>
            {t('common.mobileNricFinPassportNumberVisitor')} 
            <Text style={styles.mandatoryIconStyle}>{' *'}</Text>
          </Text>
          <TextField
            label=""
            placeholder={t('common.mobileNricFinPassportNumberVisitor')}
            tintColor={baseStyles.eclipseColor}
            errorColor="#ec3535"
            style={styles.textFieldStyle}
            defaultValue={nricPhoneFinPassport}
            value={nricPhoneFinPassport}
            onFocus={() => resetForm()}
            onChangeText={(value) => onNricOrPhoneOrPassportChange(value)}
            inputContainerStyle={styles.textFieldContainerStyle}
            autoCapitalize='characters'
          />
        </View>
        <View style={styles.searchContainer}>
          <TouchableOpacity 
          onPress={onVisitorLogSearch}
          style={styles.searchButtonStyle}>
            <Text style={styles.searchButtonTextStyle}>{t('common.search')}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.visitorListContainer}>
          <FlatList
            data={visitorLogs}
            renderItem={({ item }) => renderVisitorLog(item)}
            keyExtractor={(item) => item.ID.toString()}
            ItemSeparatorComponent={() => <View style={styles.marginBottom}/>}
          />
        </View>
        <View style={styles.checkOutBtnContainer}>
          <TouchableOpacity 
          onPress={visitorCheckOut}
          disabled={(checkedLogs.length === 0 || disableBtn) ? true : false}
          style={[
            styles.checkOutBtnStyle,
            { opacity: (checkedLogs.length === 0 || disableBtn) ? baseStyles.disabledOpacity : 1 }
          ]}>
            <Text style={styles.checkOutBtnTxtStyle}>{t('common.checkOutVisitor')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const mapStateToProps = state => ({
	schoolConfig: get(state, 'schoolConfig', {}),
});

const mapDispatchToProps = {
  findVisitorLog,
  updateErrorMessage,
  checkoutVisitor,
};

export default connect(mapStateToProps, mapDispatchToProps)(VisitorCheckOut);