import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { scaledFont } from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { connect } from 'react-redux';
import _ from 'lodash';
import { buttonCommonStyle } from '../config/buttonStyle';
import Constant from '../constants/Constant';

class StaffSignInSuccess extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const { navigation } = this.props;
		const staffName = navigation.getParam('staffName', 'TestName');
		const actionType = navigation.getParam('actionType');
		const temperature = navigation.getParam('temperature');

		let caption = t('common.kioskSuccessCheckIn');

		switch (actionType) {
			case Constant.TYPE_CHECK_OUT:
				caption = t('common.kioskSuccessCheckOut');
				break;

			case Constant.TYPE_ADD_TEMPERATURE:
				caption = t('common.kioskTemperatureAdded');
				break;
		}

		return (
			<View style={styles.container}>
				<View style={styles.headings}>
					<MaterialIcons
						size={120}
						name='check-circle'
						color='#12B886'
					/>
					<Text style={styles.successTxt}>{caption}</Text>
					{temperature && (
						<Text style={styles.staffNameStyle}>{`${temperature}°C`}</Text>
					)}
					<Text style={styles.staffNameStyle}>{staffName}</Text>
				</View>
				<TouchableOpacity
					onPress={() => navigation.navigate('Home')}
					style={[styles.doneBtn, styles.themeColor]}
				>
					<Text style={styles.doneBtnTxt}>{t('common.done')}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	schoolConfig: state.schoolConfig,
});

export default connect(mapStateToProps, {})(StaffSignInSuccess);

const centreAlignStyle = {
	alignItems: 'center',
	justifyContent: 'center'
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: baseStyles.padding50,
		...centreAlignStyle
	},
	contentView: {
		flex: 0.5,
		...centreAlignStyle
	},
	staffNameStyle: {
		textAlign: 'center',
		marginTop: baseStyles.margin20,
		fontSize: baseStyles.labelFontSize24,
		fontFamily: baseStyles.latoRegular,
		color: baseStyles.darkShadeGray
	},
	doneBtn: {
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle
	},
	doneBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle
	},
	headings: {
		flex: 0.7,
		marginBottom: baseStyles.margin50,
		...centreAlignStyle
	},
	successTxt: {
		textAlign: 'center',
		marginTop: baseStyles.margin30,
		fontSize: scaledFont(36),
		fontFamily: baseStyles.latoBold,
		color: baseStyles.darkShadeGray
	},
	get themeColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor
		}
	}
});
