import React from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { t } from '../../utils/LocalizationUtils.js';
import styles from './Visitor.style';
import { get } from 'lodash';

const CheckInOutSuccess = props => {
  const { navigation } = props;
  const isCheckIn = navigation.getParam('isCheckIn');
  const checkInOutText = isCheckIn ? t('common.kioskCheckedInTxt') : t('common.kioskCheckedOutTxt');
  const description = isCheckIn ? t('common.successDescCheckInVisitor') : t('common.successDescCheckOutVisitor');

  return (
    <View style={styles.successScreenContainer}>
      <View style={styles.contentContainer}>
        <MaterialIcons
          size={80}
          name='check-circle'
          color='#12B886'
        />
        <Text style={[styles.checkInTitleStyle, styles.marginTop50]}>{t('common.successMessageVisitor', { checkInOutText })}</Text>
        <Text style={styles.descriptionStyle}>{description}</Text>
      </View>
      <View style={styles.doneContainer}>
        <TouchableOpacity 
          onPress={() => navigation.navigate('Home')}
          style={[styles.checkInBtnStyle, styles.doneButtonStyle]}>
            <Text style={styles.doneButtonTextStyle}>{t('common.done')}</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
}

const mapStateToProps = state => ({
	schoolConfig: get(state, 'schoolConfig', {}),
});

export default connect(mapStateToProps, {})(CheckInOutSuccess);