import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { scaledFont, scaledHeight } from '../../utils/Scale';
const { width: deviceWidth } = Dimensions.get('window');

const centerAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

const buttonTextStyle = {
  fontSize: baseStyles.buttonFontSize,
  fontFamily: baseStyles.latoRegular,
};

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    backgroundColor: baseStyles.white,
  },
  buttonViewStyle: {
    flex: .3,
    flexDirection: 'column'
  },
  imageContainer: {
    flex: 0.35,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: baseStyles.margin30,
  },
  schoolLogoStyle: {
    height: scaledHeight(160),
    width: '100%',
  },
  descContainer: {
    flex: 0.2,
    ...centerAlignStyle,
  },
  checkInTitleStyle: {
    fontFamily: baseStyles.latoBold,
    fontSize: scaledFont(36),
    color: baseStyles.darkShadeGray,
  },
  descriptionStyle: {
    fontWeight: '600',
    marginTop: baseStyles.margin10,
    color: baseStyles.darkShadeGray,
    ...buttonTextStyle,
  },
  buttonStyle: {
    ...buttonCommonStyle.commonStyle,
    ...buttonCommonStyle.butttonStyle,
  },
  buttonCheckInOutTxt: {
    color: baseStyles.white,
    textAlign: 'center',
    margin: baseStyles.margin10,
    fontSize: scaledFont(32),
    fontFamily: baseStyles.latoBold,
  },
  buttonContainerText: {
    flexDirection: "row"
  },
  buttonContainerIcon: {
    flex: .4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTextContainer: {
     flex: 0.6
  },
  columnFlex: {
    flexDirection: 'column',
  },
  checkInOutLogo: {
    margin: baseStyles.margin20
  },
  buttonStyle: {
    ...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle,
    flex: 0.8,
  },
  get primaryButton() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
      borderColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  get checkInBtnStyle() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor, 
    }
  },
  get checkOutBtnStyle() {
    return {
      borderWidth: 1,
      borderColor: CustomConfig.Colors.btnPrimaryBgColor
    }
  },
  checkInTextStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  get checkOutTextStyle() {
    return {
      color: CustomConfig.Colors.btnPrimaryBgColor,
      ...buttonCommonStyle.buttonTextStyle,
    }
  },
  successScreenContainer: {
    flex: 1,
    backgroundColor: baseStyles.white,
    ...centerAlignStyle,
  },
  contentContainer: {
    flex: 0.8,
    ...centerAlignStyle,
  },
  doneContainer: {
    flex: 0.2,
    width: deviceWidth * 0.4,
  },
  doneButtonStyle: {
    ...buttonCommonStyle.butttonStyle,
    ...buttonCommonStyle.commonStyle,
  },
  doneButtonTextStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  marginTop50: {
    marginTop: baseStyles.margin50,
  },
});