import React from 'react';
import { View, Text, TouchableOpacity, Image } from "react-native";
import { connect } from 'react-redux';
import Header from '../../components/Header';
import { t } from '../../utils/LocalizationUtils.js';
import { visitorHeaderImage } from '../../config/config';
import styles from './Visitor.style';
import { get } from 'lodash';
import { commonStyle } from '../../config/commonStyle';
import CommonCheckInOutButton from '../../components/CommonCheckInOutButton';

const Visitor = props => {
  const { navigation, schoolConfig } = props;
  const school = get(schoolConfig, 'data.data[0].school.code', '');

  return (
    <View style={styles.container}>
      <Header
        backButton 
        navigation={navigation}
        style={commonStyle.headerStyle}
      />
      <View style={styles.imageContainer}>
        <Image
          resizeMode='contain'
          source={visitorHeaderImage[school]}
          style={styles.schoolLogoStyle}
        />
      </View>
      <View style={styles.descContainer}>
        <Text style={styles.checkInTitleStyle}>{t('common.welcomeVisitor')}</Text>
        <Text style={styles.descriptionStyle}>{t('common.selectOptionTextVisitor')}</Text>
      </View>
      <View style={styles.buttonViewStyle}>
         <CommonCheckInOutButton
            navigateAction={() => navigation.navigate('VisitorCheckIn')}
            label={t('common.checkInVisitor')}
            secondaryLabel='登入'
            image={require('../../images/checkInIcon.png')}
          />
          <CommonCheckInOutButton
            navigateAction={() => navigation.navigate('VisitorCheckOut')}
            label={t('common.checkOutVisitor')}
            secondaryLabel='退出'
            image={require('../../images/checkOutIcon.png')}
          />
      </View>
    </View>
  );
}

const mapStateToProps = state => ({
	schoolConfig: get(state, 'schoolConfig', {}),
});

export default connect(mapStateToProps, {})(Visitor);