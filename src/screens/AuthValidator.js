import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../redux/actions';

class AuthValidator extends Component {
  componentWillMount() {
    const { redirectByAuthStatus, navigation } = this.props;
    redirectByAuthStatus(navigation);
  }

  render() {
    return <React.Fragment />;
  }
}

const mapDispatchToProps = {
  redirectByAuthStatus: actions.redirectByAuthStatus,
};

export default connect(
  null,
  mapDispatchToProps
)(AuthValidator);
