import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  Dimensions, 
  Image 
} from 'react-native';
import { connect } from 'react-redux';
import { scaledFont } from '../utils/Scale';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { baseStyles } from '../config/baseStyles';
import Constant from '../constants/Constant';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
import { visitorHeaderImage } from '../config/config';
import { scaledHeight } from '../utils/Scale';
import { get, last, isEmpty } from 'lodash';
import CommonCheckInOutButton from '../components/CommonCheckInOutButton';
import { NavigationEvents } from 'react-navigation';
import moment from 'moment';
import {
  addStaffCheckInCheckOut,
  findAllChildrenByParent,
} from '../redux/actions';

const { width: deviceWidth } = Dimensions.get('window');

class ChildCheckInOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkInStatus: true,
      checkOutStatus: true,
    };
  }
  onComponentDidMount = async () => {
    const { findAllChildrenByParent, authToken } = this.props;
    const respFilter = {
      childLevelAt: { time: moment().format('YYYY-MM-DD') },
      childClassAt: { time: moment().format('YYYY-MM-DD') },
      checkInOuts: { filter: { date: moment().format('YYYY-MM-DD') } },
      classAttendances: { 
        filter: { date: moment().format('YYYY-MM-DD'), status: Constant.PRESENT } 
      },
    };
    await findAllChildrenByParent(respFilter, authToken);
  }
  componentDidUpdate(prevProps) {
    if (this.props.allChildren && prevProps.allChildren !== this.props.allChildren) {
      const childLists = this.props.allChildren;
      const checkInLists = this.checkInButtonStatus(childLists);
      const checkOutLists = this.checkOutButtonStatus(childLists);
      this.setState({
        checkInStatus: checkInLists.length === 0,
        checkOutStatus: checkOutLists.length === 0
      });
    }
  }
  checkInButtonStatus = (allChildByParent = []) => {
    return allChildByParent.filter(ch => ch.childLevelAt)
      .map(child => {
        const checkInOutData = get(child, 'checkInOuts.data', []);
        const checkInList = checkInOutData.filter(
          ch => ch.type === Constant.TYPE_CHECK_IN && ch.status === Constant.VERIFIED
        );
        const latestCheckIn = last(checkInList);
        const latestCheckInTime = get(latestCheckIn, 'time');
        if (!latestCheckInTime) {
          return false;
        }

        const checkOutList = checkInOutData.filter(
          ch =>
            ch.type === Constant.TYPE_CHECK_OUT && ch.status === Constant.VERIFIED
        );
        const latestCheckOut = last(checkOutList);
        const latestCheckOutTime = get(latestCheckOut, 'time');

        return (
          !latestCheckOutTime ||
          moment(latestCheckInTime).isAfter(latestCheckOutTime)
        );
      })
      .filter(status => !status);
  };
  checkOutButtonStatus = (allChildByParent = []) => {
    return allChildByParent.filter(ch => ch.childLevelAt)
      .map(child => {
        const checkInOutData = get(child, 'checkInOuts.data', []);
        const checkOutList = checkInOutData.filter(
          ch => ch.type === Constant.TYPE_CHECK_OUT && ch.status === Constant.PENDING
        );
        const latestCheckOut = last(checkOutList);
        return !isEmpty(latestCheckOut);
      })
      .filter(status => !status);
  };
  render() {
    const { navigation, schoolConfig } = this.props;
    const school = get(schoolConfig, 'data.data[0].school.code', '');
    const identificationNo = navigation.getParam('identificationNo', '');
    const { checkInStatus, checkOutStatus } = this.state;

    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={this.onComponentDidMount.bind(this)}/>
        <Header backButton navigation={navigation} style={commonStyle.headerStyle} />
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Image resizeMode="contain" source={visitorHeaderImage[school]} style={styles.logoStyle} />
          </View>
          <View style={styles.descContainer}>
            <Text style={styles.welcomeTxt}>{t('common.kioskWelcome')}</Text>
            <Text style={styles.selectTxt}>{t('common.selectOptionTextVisitor')}</Text>
          </View>
          <View style={styles.columnView}>
            <CommonCheckInOutButton
               navigateAction={() => navigation.navigate('ChildCheckInCheckOut', { isCheckIn: true, identificationNo })}
               disabled={checkInStatus}
               label={t('common.snKioskCheckIn')}
               secondaryLabel='登入'
               image={require('../images/checkInIcon.png')}
            />
             <CommonCheckInOutButton
               navigateAction={() => navigation.navigate('ChildCheckInCheckOut', { isCheckIn: false, identificationNo })}
               disabled={checkOutStatus}
               label={t('common.snKioskCheckOut')}
               secondaryLabel='退出'
               image={require('../images/checkOutIcon.png')}
            />
          </View>
        </View>
        <Footer style={styles.footerStyle} />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  schoolConfig: state.schoolConfig,
  staffToken: get(state, 'loginByPin.data', ''),
  allChildren: get(state, 'findAllChildByParent.data.data.data', null),
  authToken: get(state, 'authTokenParentOrGuardian.data.data.token', null)
});

const mapDispatchToProps = {
  addStaffCheckInCheckOut,
  findAllChildrenByParent,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChildCheckInOut);

const centreAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...centreAlignStyle,
  },
  textStyle: {
    textAlign: 'center',
    fontSize: scaledFont(32),
    fontFamily: baseStyles.latoBold,
    color: baseStyles.white,
  },
  footerStyle: {
    flex: 0.15,
    width: deviceWidth,
  },
  logoContainer: {
    flex: 0.3,
    width: '100%',
    alignItems: 'center',
    paddingTop: baseStyles.padding20,
  },
  logoStyle: {
    height: scaledHeight(160),
  },
  descContainer: {
    flex: 0.1,
  },
  welcomeTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(36),
    fontFamily: baseStyles.latoBold,
  },
  selectTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(18),
    fontFamily: baseStyles.latoRegular,
  },
  columnView: {
    flex: 0.4,
    flexDirection: 'column',
    ...centreAlignStyle,
    marginTop: baseStyles.margin30
  },
  buttonCheckInOutTxt: {
    color: baseStyles.white,
    textAlign: 'center',
    margin: baseStyles.margin10,
    fontSize: scaledFont(32),
    fontFamily: baseStyles.latoBold,
  },
  buttonStyle: {
    ...buttonCommonStyle.commonStyle,
    ...buttonCommonStyle.butttonStyle,
    flex: 0.8,
    flexDirection: 'column'
  },
  buttonCheckInTxt: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
    textAlign: 'center',
    margin: baseStyles.margin10
  },
  buttonContainerText: {
    flexDirection: "row"
  },
  buttonContainerIcon: {
    flex: .4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTextContainer: {
     flex: 0.6
  },
  columnFlex: {
    flexDirection: 'column',
  },
  checkInOutLogo: {
    margin: baseStyles.margin20
  },
  get buttonCheckOutTxt() {
    return {
      color: CustomConfig.Colors.btnSecondaryBgColor,
      ...buttonCommonStyle.buttonTextStyle,
      textAlign: 'center'
    };
  },
  get primaryButton() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
      borderColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  get secondaryButton() {
    return {
      borderWidth: 1,
      borderColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  get primaryColor() {
    return {
      backgroundColor: CustomConfig.Colors.primaryColor,
    };
  },
});
