import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	FlatList,
	Image,
	TouchableWithoutFeedback,
	TouchableOpacity
} from 'react-native';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { connect } from 'react-redux';
import { baseStyles } from '../config/baseStyles';
import { checkImage } from '../config/config';
import { 
	getChildrenStates,
	updateErrorMessage,
	getCheckInOutToken,
	childCheckInOutV2,
	showCustomAlert,
} from '../redux/actions';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { 
	retrieveAsyncStorage,
	onSignInSignOut
} from '../utils';
import _ from 'lodash';
import moment from 'moment';
import { t } from '../utils/LocalizationUtils.js';
import CachedImage from '../components/CachedImage';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth } = Dimensions.get('window');

class ChildAttendance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			childDetails: [],
			isSignIn: false,
			selected: [],
			childStates: [],
			parentUserId: null,
			passcode: null,
		};
	}
	componentWillMount() {
    	const { navigation } = this.props;
    	const childDetails = _.get(navigation, 'state.params.findParentByIC', []);
		const isSignIn = _.get(navigation, 'state.params.isSignIn', false);
		const parentUserId = _.get(navigation, 'state.params.parentUserId', null);
		const passcode = _.get(navigation, 'state.params.passcode', null);
		this.setState({ childDetails, isSignIn, parentUserId, passcode });
	}
	async componentDidMount() {
		const accessToken = await retrieveAsyncStorage('accessToken');
		const { childDetails } = this.state;
		const childIds = childDetails.map(child => child.id);
		const reqData = {
			childIds,
			date: moment().format('YYYY-MM-DD')
		};
		this.props.getChildrenStates(reqData, accessToken).then(resp => {
			const childStates = _.get(resp, 'data.getChildrenStates.childStates', []);
			if (childStates.length > 0) {
				this.setState({ childStates });
			}
		});
	}
	selectOrDeselectChild = (childId, index) => {
		const { selected, childDetails } = this.state;
		const position = selected.indexOf(childId);
		if (position >= 0) {
			selected.splice(position, 1);
		} else {
			selected.push(childId);
		}

		this.setState({
			selected: selected.concat([]),
			childDetails: childDetails.concat([])
		});
  	}

	alertDisabled = (child) => {
		const childName = _.get(child, 'name', t('common.kioskDefaultChildNameTxt'));
		alert(t('common.kioskCheckOutWithoutCheckInAlert', { childName }));
	}
  
	renderChild = ({ item, index }) => {
		const { selected, childStates, isSignIn } = this.state;
		const { schoolConfig } = this.props;
		const position = selected.indexOf(item.id);
		const school = _.get(schoolConfig, 'data.data[0].school.code', '');

	    const isDisabled = (isSignIn || (childStates.findIndex(chld => (chld.childId === item.id) && chld.checkInTime) >= 0)) ? false : true;
	    return <TouchableWithoutFeedback
	    onPress={() => isDisabled ? this.alertDisabled(item) : this.selectOrDeselectChild(item.id, index)}>
	      <View 
	      style={[
	        styles.cardView,
	        {
	          borderColor: position >= 0 ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada',
	          borderWidth: position >= 0 ? 2 : 1,
	          backgroundColor: isDisabled ? baseStyles.lightGreyColor : '#ffffff'
	        }
	      ]}>
	        <View 
	        style={styles.childListView}>
		      {
		      	item.isImageFromURI ? <CachedImage
			        style={styles.childImage}
			        source={item.childImage}
			    /> : <Image 
		            source={item.childImage} 
		            style={styles.childImage} 
		            resizeMethod='resize'
		        />
		      }
	          <View 
	          style={styles.childDetailView}>
	            <Text 
	            style={styles.childNameTxt}>{item.name}</Text>
	          </View>
	        </View>
	        <View 
	        style={styles.checkImageView}>
	          {
	            position >= 0 ? <Image 
	              source={checkImage[school]} 
	              style={styles.checkImage} 
	              resizeMethod='resize'
	            /> : null
	          }
	        </View>
	      </View>
	    </TouchableWithoutFeedback>
  	}
  
	render() {
		const thisProps = this.props;
		const { childDetails, isSignIn, selected, parentUserId, passcode } = this.state;
		const { navigation } = thisProps;
		const checkInOutTxt = isSignIn ? t('common.kioskCheckInTxt') : t('common.kioskCheckOutTxt');

		return <View 
		style={styles.container}>
			<Header 
				backButton 
				navigation={navigation}
				style={commonStyle.headerStyle}
			/>
			<View 
			style={styles.contentView}>
				<View 
				style={styles.topHeaderView}>
					<Text 
					style={styles.signInTxt}>{isSignIn ? t('common.kioskChildCheckInTxt') : t('common.kioskChildCheckOutTxt')}</Text>
					<Text 
					style={styles.signInTxtSmall}>{t('common.kioskSelectChildForCheckInOutTxt', { checkInOutTxt })}</Text>
				</View>
				<View 
				style={styles.flatListView}>
					<FlatList 
						data={childDetails} 
						extraData={this.state} 
						removeClippedSubviews={false} 
						showsVerticalScrollIndicator={false}
						keyExtractor={item => item.id.toString()}
						renderItem={this.renderChild}
					/>
				</View>
				<TouchableOpacity 
				disabled={selected.length > 0 ? false : true}
				onPress={() => onSignInSignOut(childDetails, thisProps, selected, parentUserId, passcode)}
				style={[
					styles.signInBtn,
					styles.primaryColor,
					{ opacity: selected.length > 0 ? 1 : baseStyles.disabledOpacity }
				]}>
					<Text style={styles.signInBtnTxt}>
						{isSignIn ? t('common.snKioskCheckIn') : t('common.snKioskCheckOut')}
					</Text>
				</TouchableOpacity>
			</View>
			<Footer 
			style={styles.footerStyle}/>
		</View>
	}
}

const mapStateToProps = state => ({
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	getChildrenStates,
	updateErrorMessage,
	getCheckInOutToken,
	childCheckInOutV2,
	showCustomAlert,
});

export default connect(mapStateToProps, mapDispatchToProps)(ChildAttendance);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	contentView: {
		flex: 1,
		alignItems: 'center'
	},
	topHeaderView: {
		flex: 0.3,
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	signInTxt: {
		color: baseStyles.eclipseColor,
		marginBottom: scaledHeight(10),
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold
	},
	signInTxtSmall: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(16),
		fontFamily: baseStyles.latoRegular
	},
	signInBtn: {
		...buttonCommonStyle.commonStyle,
		...buttonCommonStyle.butttonStyle,
	},
	signInBtnTxt: {
		color: '#ffffff',
		...buttonCommonStyle.buttonTextStyle,
	},
	cardView: {
		width: deviceWidth * 0.33,
		marginBottom: scaledWidth(10),
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		borderRadius: scaledFont(3)
	},
	childDetailView: {
		width: '60%',
		paddingLeft: scaledWidth(15),
		justifyContent: 'center'
	},
	childNameTxt: {
		flexWrap: 'wrap',
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		marginBottom: scaledHeight(5),
		fontSize: scaledFont(16)
	},
	contactNoTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(14),
		fontFamily: baseStyles.latoSemibold
	},
	childImage: {
		height: scaledFont(60),
		width: scaledFont(60),
		borderRadius: scaledFont(60)/2
	},
	flatListView: {
		flex: 0.35,
		marginVertical: scaledHeight(40)
	},
	childListView: {
		width: '80%',
		padding: scaledFont(15),
		flexDirection: 'row',
		alignItems: 'center'
	},
	checkImageView: {
		height: '100%',
		width: '20%',
		alignItems: 'flex-end',
		padding: scaledFont(5)
	},
	checkImage: {
		height: scaledFont(20),
		width: scaledFont(20),
		borderRadius: scaledFont(20)/2
	},
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
		}
	}
});