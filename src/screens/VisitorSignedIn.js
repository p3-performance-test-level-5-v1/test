import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity
} from 'react-native';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import { connect } from 'react-redux';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class VisitorSignedIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: {}
		};
	}
	componentWillMount() {
		const { navigation } = this.props;
		const data = _.get(navigation, 'state.params.data', {});
		if(Object.keys(data).length > 0) {
			this.setState({ data });
		}
	}
	render() {
		const { navigation } = this.props;
		const { data } = this.state;

		return <View
		style={styles.container}>
			<View
			style={styles.signInView}>
				<Text
				style={styles.signInText}>{t('common.kioskVisitorCheckInSuccessTxt')}</Text>
				<Text
				style={styles.description}>{t('common.kioskVisitorCheckInDescriptionTxt')}</Text>
			</View>
			<View
			style={styles.detailContainerView}>
				<View
				style={styles.detailView}>
					<Text
					style={styles.textFontStyle}>{t('common.kioskVisitorsNameTxt')}</Text>
					<Text
					style={styles.detailTxt}>{data.visitorsName}</Text>
				</View>
				<View
				style={styles.detailView}>
					<Text
					style={styles.textFontStyle}>{t('common.kioskVisitorsContactNoTxt')}</Text>
					<Text
					style={styles.detailTxt}>{data.contactNo}</Text>
				</View>
				<View
				style={styles.detailView}>
					<Text
					style={styles.textFontStyle}>{t('common.kioskPurposeOfVisitTxt')}</Text>
					<Text
					style={styles.detailTxt}>{data.purposeOfVisitLabel}</Text>
				</View>
				{ data.meetingWith !== '' &&
					<View
					style={styles.detailView}>
						<Text
						style={styles.textFontStyle}>{t('common.kioskVisitorMeetingWithTxt')}</Text>
						<Text
						style={styles.detailTxt}>{data.meetingWith}</Text>
					</View>
				}
				{ data.otherReason && data.otherReason !== '' &&
				<View
					style={styles.detailView}>
					<Text
						style={styles.textFontStyle}>{t('common.kioskOtherReasonTitle')}</Text>
					<Text
						style={styles.detailTxt}>{data.otherReason}</Text>
				</View>
				}
				{ data.organization && data.organization !== '' &&
				<View
					style={styles.detailView}>
					<Text
						style={styles.textFontStyle}>{t('common.kioskOrganization')}</Text>
					<Text
						style={styles.detailTxt}>{data.organization}</Text>
				</View>
				}
			</View>
			<View
			style={styles.doneBtnView}>
				<TouchableOpacity
				onPress={() => navigation.navigate('Home')}
				style={[
					styles.doneBtn,
					{ backgroundColor: CustomConfig.Colors.btnPrimaryBgColor }
				]}>
					<Text
					style={styles.doneBtnTxt}>{t('common.done')}</Text>
				</TouchableOpacity>
			</View>
		</View>
	}
}

const mapStateToProps = state => ({
	schoolConfig: state.schoolConfig
});

export default connect(mapStateToProps, {})(VisitorSignedIn);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	signInView: {
		flex: 0.12,
		width: deviceWidth,
		paddingHorizontal: scaledWidth(30),
		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	signInText: {
		fontSize: scaledFont(24),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold
	},
	description: {
		textAlign: 'center',
		fontSize: scaledFont(16),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoSemibold
	},
	detailContainerView: {
		flex: 0.35,
		width: deviceWidth * 0.4,
		alignItems: 'center',
		justifyContent: 'space-evenly',
		paddingHorizontal: scaledWidth(40)
	},
	textFontStyle: {
		textAlign: 'center',
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoSemibold
	},
	detailTxt: {
		textAlign: 'center',
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold,
		fontSize: baseStyles.textInputFont
	},
	detailView: {
		height: '20%',
		alignItems: 'center',
		justifyContent: 'space-evenly'
	},
	doneBtnView: {
		flex: 0.1,
		width: deviceWidth * 0.4,
		justifyContent: 'center',
		alignItems: 'center'
	},
	doneBtn: {
		width: '100%',
		borderRadius: 3,
		height: scaledHeight(60),
		justifyContent: 'center',
		alignItems: 'center'
	},
	doneBtnTxt: {
		color: '#ffffff',
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoRegular,
		textTransform: 'uppercase'
	}
});
