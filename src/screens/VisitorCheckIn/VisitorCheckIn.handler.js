import { useState, useEffect } from 'react';
import { get, isEmpty } from 'lodash';
import { t } from '../../utils/LocalizationUtils.js';
import moment from 'moment';
import {
	errorResponse,
	retrieveAsyncStorage,
	handleErrorDataFromApi,
} from '../../utils';

export default Handler = (props) => {
  const { 
    findAllConfigByCategory, 
    insertVisitorLog, 
    navigation, 
    updateErrorMessage 
  } = props;
  
  const [visitorName, setVisitorName] = useState('');
  const [nricNumber, setNricNumber] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [organization, setOrganization] = useState('');
  const [temperature, setTemperature] = useState('');
  const [purposeOfVisit, setPurposeOfVisit] = useState('');
  const [fkCode, setFkCode] = useState(null);
  const [remarks, setRemarks] = useState('');
  const [purpose, setPurpose] = useState([]);
  const [tempError, setTempError] = useState('');
  const [mobNumberError, setMobNumberError] = useState('');
  const [disableBtn, setDisableBtn] = useState(false);
  const [tempErrorStatus, setTempErrorStatus] = useState(false);
  const [mobErrorStatus, setMobErrorStatus] = useState(false);
  const [temperatureThreshold, setTemperatureThreshold] = useState(37.5);
  const [minTemperatureThreshold, setMinTemperatureThreshold] = useState(36);

  useEffect(() => {
    onComponentMount();
    getTemperatureThreshold();
  }, []);

  const onComponentMount = async () => {
    const fkSchool = await retrieveAsyncStorage('schoolId');
    if (!fkSchool) {
      return;
    }

    const reqData = { searchCategory: "Purpose_Of_Visit", filter: { fkSchool } };
    const result = await findAllConfigByCategory(reqData);
    const data = get(result, 'data.findAllConfigByCategory.data', []);
    const pData = data.map(dt => ({
      ID: dt.ID,
      category: dt.label,
      value: dt.description
    }))
    setPurpose(pData);
  };

  const onFormFieldChange = (fieldValue, fieldName) => {
    if (fieldName === 'visitorName') {
      setVisitorName(fieldValue);
    } else if (fieldName === 'nricNumber') {
      setNricNumber(fieldValue);
    } else if (fieldName === 'phoneNumber') {
      setPhoneNumber(fieldValue);
    } else if (fieldName === 'organization') {
      setOrganization(fieldValue);
    } else if (fieldName === 'temperature') {
      setTemperature(fieldValue);
      tempValidation(fieldValue);
    } else if (fieldName === 'remarks') {
      setRemarks(fieldValue);
    }
  }

  const onPurposeChange = (selected = {}) => {
    if (isEmpty(selected)) {
      return;
    }
    setFkCode(selected);
    setPurposeOfVisit(selected.value);
  }

  const onProceedAction = async () => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    let fkCentre = await retrieveAsyncStorage('centreId');
    if (!accessToken || !fkCentre) {
      return;
    }

    const reqData = {
      fkCentre,
      isAutoFilled: false,
      fkCode: get(fkCode, 'ID'),
      name: visitorName,
      countryCode: "+65",
      phone: phoneNumber,
      nric: nricNumber,
      temperature,
      checkInTime: moment().format('YYYY-MM-DD HH:mm:ss'),
      organization,
      remarks,
    };

    setDisableBtn(true);
    const result = await insertVisitorLog({ data: reqData }, accessToken);
    setDisableBtn(false);
    const visitorLogId = get(result, 'data.insertVisitorLog.ID', '');
    if (visitorLogId && result.success) {
      navigation.navigate('CheckInOutSuccess', { isCheckIn: true });
      return;
    }
    const message = handleErrorDataFromApi(result, errorResponse);
		message && updateErrorMessage(message, onProceedAction);
  }

  const getTemperatureThreshold = async () => {
    const value = await SchoolConfig.getValueByKey('body_temperature_threshold');
    setTemperatureThreshold(parseFloat(value));
  };

  const tempValidation = (tempValue) => {
    const regEx = /^\d{2}.{1}\d{1}$/;
    let error = '';
    setTempError(error);
    setTempErrorStatus(false);
    if(!regEx.test(tempValue)) {
      error = t('common.temperatureWarning');
      setTempError(error);
      setTempErrorStatus(true);
      return;
    }
    if (tempValue > temperatureThreshold) {
      error = t('common.invalidTempVisitor');
      setTempError(error);
      setTempErrorStatus(true);
      return
    }

    if (tempValue < minTemperatureThreshold) {
      error = t('error.minimumTempValidation');
      setTempError(error);
      setTempErrorStatus(true);
      return
    }
    setTempErrorStatus(false);
  }

  const phoneNumberValidation = (phoneNo) => {
    const regEx = /^[0-9]+$/;
    let error = '';
    if(!regEx.test(phoneNo)) {
      error = t('accountAndSettings.invalidPhoneNumber');
      setMobNumberError(error);
      setMobErrorStatus(true);
      return;
    }
    setMobErrorStatus(false);
  }

  return ({
    onPurposeChange,
    onFormFieldChange,
    onProceedAction,
    visitorName,
    nricNumber,
    phoneNumber,
    organization,
    temperature,
    purposeOfVisit,
    tempValidation,
    tempErrorStatus,
    mobErrorStatus,
    phoneNumberValidation,
    setMobNumberError,
    setTempError,
    tempError,
    disableBtn,
    mobNumberError,
    remarks,
    purpose,
    fkCode,
  });
}