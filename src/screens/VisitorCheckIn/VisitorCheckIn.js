import React from 'react';
import {  
  Text, 
  TouchableOpacity, 
  KeyboardAvoidingView,
  ScrollView,
  View,
} from "react-native";
import { TextField } from 'react-native-material-textfield';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { baseStyles } from '../../config/baseStyles';
import Header from '../../components/Header';
import { t } from '../../utils/LocalizationUtils.js';
import { scaledFont } from '../../utils/Scale';
import Handler from './VisitorCheckIn.handler';
import styles from './VisitorCheckIn.style';
import {
  insertVisitorLog,
  findAllConfigByCategory,
  updateErrorMessage,
} from '../../redux/actions';
import { get } from 'lodash';
import { commonStyle } from '../../config/commonStyle';

const VisitorCheckIn = props => {
  const { navigation } = props;

  const {
    onPurposeChange,
    onFormFieldChange,
    onProceedAction,
    visitorName,
    nricNumber,
    phoneNumber,
    organization,
    temperature,
    purposeOfVisit,
    tempValidation,
    tempErrorStatus,
    mobErrorStatus,
    setTempError,
    phoneNumberValidation,
    setMobNumberError,
    tempError,
    disableBtn,
    mobNumberError,
    remarks,
    purpose,
    fkCode
  } = Handler(props);

  const renderMandatory = () => (<Text style={styles.mandatoryIconStyle}>*</Text>)

  const remarkStatus = (remarks.trim().length > 0 || get(fkCode, 'category', '') !== 'others');
  const isDisabled = () => {
    return !(
      nricNumber.trim().length > 0 && 
      visitorName.trim().length > 0 && 
      phoneNumber.trim().length > 0 && 
      temperature.trim().length > 0 && 
      purposeOfVisit.trim().length > 0 &&
      !(disableBtn) &&
      !(tempErrorStatus) && 
      !(mobErrorStatus) &&
      remarkStatus
    );
  };

  return (
    <View style={styles.container}>
      <Header
        backButton 
        navigation={navigation}
        style={[styles.headerStyle]}
      />
      <KeyboardAvoidingView 
        behavior={'padding'}
        style={styles.container}
      >
        <ScrollView 
          keyboardShouldPersistTaps={'handled'}
          contentContainerStyle={styles.scrollableView}
        >
          <View style={styles.contentContainer}>
            <View style={styles.descContainer}>
              <Text style={styles.checkInTextStyle}>{t('common.visitorCheckIn')}</Text>
              <Text style={styles.checkInDescStyle}>{t('common.fillFormForCheckInVisitor')}</Text>
            </View>
            <View style={styles.formContainer}>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.kioskVisitorsNameTxt')}{renderMandatory()}</Text>
                <TextField
                  placeholder={t('common.kioskVisitorsNameTxt')}
                  label=""
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={visitorName || ''}
                  onChangeText={(value) => onFormFieldChange(value, 'visitorName')}
                  inputContainerStyle={styles.textFieldStyle}
                />
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.mobileNumberVisitor')}{renderMandatory()}</Text>
                <TextField
                  placeholder={t('common.mobileNumberVisitor')}
                  label=""
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={phoneNumber || ''}
                  onEndEditing={() => phoneNumberValidation(phoneNumber)}
                  onFocus={() => setMobNumberError('')}
                  onChangeText={(value) => onFormFieldChange(value, 'phoneNumber')}
                  inputContainerStyle={[
                    styles.textFieldStyle,
                    mobNumberError.trim().length > 0 && { borderBottomColor: baseStyles.darkRed }
                  ]}
                  keyboardType={'phone-pad'}
                />
                {mobNumberError.trim().length > 0 && (
                  <Text style={styles.errorMessageStyle}>{mobNumberError}</Text>
                )}
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.nricFinPassportNumberVisitor')}{renderMandatory()}</Text>
                <TextField
                  label=""
                  placeholder={t('common.nricFinPassportNumberVisitor')}
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={nricNumber || ''}
                  autoCapitalize='characters'
                  onChangeText={(value) => onFormFieldChange(value, 'nricNumber')}
                  inputContainerStyle={styles.textFieldStyle}
                />
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.kioskOrganization')}</Text>
                <TextField
                  placeholder={t('common.kioskOrganization')}
                  label=""
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={organization || ''}
                  onChangeText={(value) => onFormFieldChange(value, 'organization')}
                  inputContainerStyle={styles.textFieldStyle}
                />
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.temperatureVisitor')}{renderMandatory()}</Text>
                <TextField
                  placeholder={t('common.temperatureVisitor')}
                  label=""
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={temperature || ''}
                  onEndEditing={() => tempValidation(temperature)}
                  onFocus={() => temperature ? tempValidation(temperature) : setTempError('')}
                  onChangeText={(value) => onFormFieldChange(value, 'temperature')}
                  inputContainerStyle={[
                    styles.textFieldStyle,
                    tempError.trim().length > 0 && { borderBottomColor: baseStyles.darkRed }
                  ]}
                  keyboardType={'number-pad'}
                />
                {tempError.trim().length > 0 && (
                  <Text style={styles.errorMessageStyle}>{tempError}</Text>
                )}
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>{t('common.kioskPurposeOfVisitTxt')}{renderMandatory()}</Text>
                <Dropdown
                  data={purpose}
                  fontSize={baseStyles.inputFontSize}
                  containerStyle={styles.width100p}
                  defaultValue={purposeOfVisit || ''}
                  textColor={baseStyles.eclipseColor}
                  itemTextStyle={styles.dropdownItemTextStyle}
                  labelHeight={5}
                  renderAccessory={() => {
                    return <MaterialIcons
                      size={scaledFont(30)}
                      name='keyboard-arrow-down'
                      color={baseStyles.dropdownColor}
                    />
                  }}
                  style={styles.textFontStyle}
                  dropdownOffset={styles.dropdownOffsetStyle}
                  onChangeText={(value, index, data) => onPurposeChange(data[index])}
                  placeholder={t('common.kioskPurposeOfVisitTxt')}
                  inputContainerStyle={styles.textFieldStyle}
                  pickerStyle={styles.pickerStyle}
                  label=''
                />
              </View>
              <View style={styles.fieldContainer}>
                <Text style={styles.labelStyle}>
                  {t('common.remarks')}{!remarkStatus && renderMandatory()}
                </Text>
                <TextField
                  placeholder={t('common.remarks')}
                  label=""
                  tintColor={baseStyles.eclipseColor}
                  errorColor="#ec3535"
                  style={styles.textFontStyle}
                  defaultValue={remarks || ''}
                  onChangeText={(value) => onFormFieldChange(value, 'remarks')}
                  inputContainerStyle={styles.textFieldStyle}
                />
              </View>
              <View style={styles.proceedBtnContainer}>
                <TouchableOpacity 
                  disabled={isDisabled()}
                  style={[
                    styles.proceedBtnStyle,
                    { opacity: isDisabled() ? baseStyles.disabledOpacity : 1 }
                  ]}
                  onPress={onProceedAction}
                >
                  <Text style={styles.proceedTxtStyle}>{t('common.proceed')}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
}

const mapStateToProps = state => ({
	schoolConfig: get(state, 'schoolConfig', {}),
});

const mapDispatchToProps = {
  findAllConfigByCategory,
  insertVisitorLog,
  updateErrorMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(VisitorCheckIn);