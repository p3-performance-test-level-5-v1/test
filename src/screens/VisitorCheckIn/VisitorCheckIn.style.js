import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { buttonCommonStyle } from '../../config/buttonStyle';
import CustomConfig from '../../utils/CustomConfig';
import { scaledHeight } from '../../utils/Scale';
const { width: deviceWidth, height: dHeight } = Dimensions.get('window');

const regularFontStyle = {
  fontSize: baseStyles.labelFontSize,
  fontFamily: baseStyles.latoRegular,
};

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth * 0.5,
    backgroundColor: baseStyles.white,
  },
  descContainer: {
    flex: 0.15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkInTextStyle: {
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
    fontSize: baseStyles.labelFontSize24,
  },
  checkInDescStyle: {
    color: baseStyles.darkShadeGray,
    marginTop: baseStyles.margin10,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.buttonFontSize,
  },
  formContainer: {
    flex: 0.85, 
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fieldContainer: { 
    flex: 0.14, 
    justifyContent: 'center', 
    width: '100%', 
  },
  textFieldStyle: { 
    width: '100%',
    paddingTop: baseStyles.padding5,
    height: scaledHeight(35),
    borderBottomWidth: 2,
    borderBottomColor: '#dadada' 
  },
  labelStyle: {
    color: baseStyles.eclipseColor,
    ...regularFontStyle,
  },
  textFontStyle: {
    fontSize: baseStyles.buttonFontSize,
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoRegular,
  },
  proceedBtnContainer: {
    flex: 0.15,
    width: '100%',
    paddingTop: baseStyles.padding30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  get proceedBtnStyle() {
    return {
      width: '80%',
      height: baseStyles.btnHeight,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: CustomConfig.Colors.primaryColor,
      borderRadius: baseStyles.borderRadius,
    }
  },
  proceedTxtStyle: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  width100p: {
    width: '100%',
  },
  mandatoryIconStyle: {
    color: '#DE7B39',
    fontSize: baseStyles.buttonFontSize,
  },
  errorMessageStyle: {
    color: baseStyles.darkRed,
    ...regularFontStyle,
  },
  dropdownItemTextStyle: {
    color: baseStyles.darkShadeGray,
    ...regularFontStyle,
  },
  get headerStyle() {
    return { 
      width: deviceWidth,
      height: dHeight * 0.08,
      backgroundColor: CustomConfig.Colors.primaryColor,
    }
  },
  scrollableView: { 
    flexGrow: 1, 
    height: dHeight * 0.9,
  },
  pickerStyle: {
    height: 260,
  },
  dropdownOffsetStyle: { 
    top: 0, 
    left: 0 
  },
});