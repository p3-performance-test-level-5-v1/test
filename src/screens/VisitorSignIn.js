import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	TouchableOpacity
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Header from '../components/Header';
import Footer from '../components/Footer';
import {
	TextField
} from 'react-native-material-textfield';
import {
	KeyboardAwareScrollView
} from 'react-native-keyboard-aware-scroll-view';
import {
	Dropdown
} from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import {
	findAllConfigByCategory,
	findAllStaff,
	updateErrorMessage,
	insertVisitorLog
} from '../redux/actions';
import { baseStyles } from '../config/baseStyles';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import {
	errorResponse,
	retrieveAsyncStorage,
	handleErrorDataFromApi ,
	mobileGoogleValidation
} from '../utils';
import _ from 'lodash';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { getFullName } from '../utils/index';
import { commonStyle } from '../config/commonStyle';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class VisitorSignIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			contactNo: '',
			purpose: [],
			purposeId: '',
			purposeOfVisitLabel: '',
			staff: [],
			staffId: '',
			cntctNo: false,
			visitorsName: '',
			visName: false,
			purposeOfVisit: '',
			meetingWith: '',
			phoneNumberStatus: false,
			showOtherReasonInput: false,
			showOrganizationInput: false,
			otherReason: '',
			organization: '',
			phoneNumberErrorMsg: '',
			schoolId: null
		};
	}
	async componentDidMount() {
		const schoolId = await retrieveAsyncStorage('schoolId');
		this.setState({
			schoolId: schoolId ? parseInt(schoolId) : null
		}, () => {
			this.getAllStaffAndVisitCategory();
		})
	}
	async getAllStaffAndVisitCategory() {
		const { findAllConfigByCategory, findAllStaff } = this.props;
		const { schoolId } = this.state;
		const accessToken = await retrieveAsyncStorage('accessToken');
		const centreStaffOnly = true;

		findAllConfigByCategory({ searchCategory: "Purpose_Of_Visit", filter: {fkSchool: schoolId}}).then(data => {
			if (data.success) {
				const purposeData = _.get(data, 'data.findAllConfigByCategory.data', []);
				const purpose = purposeData.map(category => ({
					ID: category.ID,
				    category: category.label,
				    value: category.description
				}));
				this.setState({ purpose });
				return;
			}
			const message = handleErrorDataFromApi(data, errorResponse);
			message && this.props.updateErrorMessage(message, this.getAllStaffAndVisitCategory.bind(this));
		}).catch(error => {});

		if (accessToken) {
			findAllStaff(accessToken, this.props.token.centreId,centreStaffOnly).then(data => {  
				if (data.success) {
					const staffData = _.get(data, 'data.findAllStaff.data', []);
					const staff = staffData.map(st => ({
						ID: st.userByFkUser.ID,
						value: getFullName(st.userByFkUser.firstname, st.userByFkUser.lastname)
					}));
					staff.length > 0 && this.setState({ staff });
					return;
				}
				const message = handleErrorDataFromApi(data, errorResponse);
				message && this.props.updateErrorMessage(message, this.getAllStaffAndVisitCategory.bind(this));
			}).catch(error => {});
		}
	}
	onFieldChange(fieldName, text) {
		const thisState = this.state;
		const newState = Object.assign({ ...thisState }, { [fieldName]: text });
		this.setState(newState);
	}
	onContactNumberChange(fieldName, text){
		this.onFieldChange(fieldName, text);
		this.phoneNumberValidation(text);
	}
	onPurposeChange(index, data){

		if (data[index].ID) {

			const organizationInputOptions = ['maintenance', 'delivery','enrichment','practicum','other'];
			let showOtherReasonInput = false;
			let showOrganizationInput = false;

			if (data[index].category === 'other') {
				showOtherReasonInput = true;
			}

			if (organizationInputOptions.includes(data[index].category)) {
				showOrganizationInput = true;
			}

			this.setState({
				purposeId: data[index].ID,
				purposeOfVisit:  data[index].category,
				purposeOfVisitLabel:data[index].value,
				showOtherReasonInput,
				showOrganizationInput,
			});
		}
	}
	onStaffChange(index, data){
		if (data[index].ID) {
			this.setState({
				staffId: data[index].ID,
				meetingWith:  data[index].value
			});
		}
	}
	async onVisitorSignIn() {
		const { navigation, insertVisitorLog } = this.props;
		let fkCentre = await retrieveAsyncStorage('centreId');
		const accessToken = await retrieveAsyncStorage('accessToken');
		fkCentre = fkCentre ? parseInt(fkCentre) : 0;
		if (!fkCentre || !accessToken) {
			return;
		}

		const {
			contactNo,
			staffId,
			visitorsName,
			purposeId,
			meetingWith,
			purposeOfVisitLabel,
			otherReason,
			organization,
			showOtherReasonInput,
			showOrganizationInput
		} = this.state;

		if (contactNo && visitorsName && purposeId && accessToken ) {
			const reqData = {
				fkCentre,
				fkCode: purposeId,
				name: visitorsName,
				countryCode: "+65",
				phone: contactNo
			};

			const visitorData = {
				visitorsName,
				meetingWith,
				contactNo,
				purposeOfVisitLabel
			};

			// fkUser is nullable
			if (staffId !== '') {
				reqData.fkUser = staffId;
			}

			if (showOtherReasonInput && otherReason === '') {
				alert(t('common.kioskOtherReasonError'));
				return;
			}

			if (showOrganizationInput && organization === '') {
				alert(t('common.kioskOrganizationError'));
				return;
			}

			// otherReason is nullable
			if (showOtherReasonInput && otherReason !== '') {
				reqData.otherReason = otherReason;
				visitorData.otherReason = otherReason;
			}

			// organization is nullable
			if (showOrganizationInput && organization !== '') {
				reqData.organization = organization;
				visitorData.organization = organization;
			}

			insertVisitorLog({data: reqData}, accessToken).then(data => {
				if (data.success) {
					navigation.navigate('VisitorSignedIn', {
						data: visitorData
					});
					return;
				}
				const message = handleErrorDataFromApi(data, errorResponse);
				message && this.props.updateErrorMessage(message, this.onVisitorSignIn.bind(this));
			});
		}
	}
	phoneNumberValidation(value) {
        const status = mobileGoogleValidation(value, 'SG');
        let errorMessage = '';
        if (status.valid) {
        	phoneNumberStatus = true;
        } else {
        	errorMessage = _.get(status, 'errors.mobile');
        	phoneNumberStatus = false;
        }
        this.setState({
	        phoneNumberStatus,
	        phoneNumberErrorMsg: errorMessage
        });
	}

	validateForm(phoneNumberStatus, contactNo, staffId, visitorsName, purposeId) {
		let valid = false;
		const { schoolId } = this.state;
		switch (schoolId) {
			case 2:
				valid =  purposeId && phoneNumberStatus && (contactNo && contactNo.trim().length > 0 ) && (visitorsName && visitorsName.trim().length > 0);
				break;
			default:
				valid = purposeId && staffId && phoneNumberStatus && (contactNo && contactNo.trim().length > 0) && (visitorsName && visitorsName.trim().length > 0);
				break;
		}

		return valid;
	}

	renderStaffList () {
		const { meetingWith, staff, schoolId } = this.state;

		if (schoolId === 2) {
			return;
		}

		return (
			<View style={{ height: '25%' }}>
				<Text
					style={styles.labelStyle}>{t('common.kioskVisitorMeetingWithTxt')}</Text>
				<Dropdown
					fontSize={baseStyles.inputFontSize}
					containerStyle={{ width: deviceWidth * 0.4 }}
					value={meetingWith}
					textColor={baseStyles.eclipseColor}
					labelHeight={5}
					renderAccessory={() => {
						return <MaterialIcons
							size={scaledFont(18)}
							name='keyboard-arrow-down'
							color={baseStyles.dropdownColor}
						/>
					}}
					style={{ fontFamily: baseStyles.latoBold }}
					data={staff}
					onChangeText={(value, index, data) =>
						this.onStaffChange(index, data)
					}
					placeholder={t('common.kioskVisitorMeetingWithTxt')}
					label=''
				/>
			</View>
		)
	}
	render() {
		const {
			contactNo,
			purposeOfVisit,
			phoneNumberStatus,
			phoneNumberErrorMsg,
			visitorsName,
			meetingWith,
			cntctNo,
			staffId,
			purposeId,
			visName,
			purpose,
			staff,
			showOtherReasonInput,
			showOrganizationInput,
			otherReason,
			organization,
		} = this.state;
		const { navigation } = this.props;

		const isDisabled =!this.validateForm(phoneNumberStatus, contactNo, staffId, visitorsName, purposeId);

		return <KeyboardAwareScrollView
		keyboardShouldPersistTaps="handled"
		resetScrollToCoords={{ x: 0, y: 0 }}
		contentContainerStyle={{
			height: deviceHeight
		}}
		scrollEnabled={false}>
			<View
			style={styles.container}>
				<Header
					backButton
					navigation={navigation}
					style={commonStyle.headerStyle}
				/>
				<View
				style={styles.container}>
					<View
					style={styles.viewContainer}>
						<View
						style={styles.signInView}>
							<Text
							style={styles.signInTxt}>{t('common.kioskVisitorCheckInTxt')}</Text>
							<Text
							style={styles.description}>{t('common.kioskVisitorsDescriptionTxt')}</Text>
						</View>
						<View
						style={styles.formContainer}>
							<View style={{ height: '25%' }}>
								<Text
								style={styles.labelStyle}>{t('common.kioskVisitorsNameTxt')}</Text>
								<TextField
									placeholder={t('common.kioskVisitorsNameTxt')}
									label=""
									tintColor={baseStyles.eclipseColor}
									errorColor="#ec3535"
									style={styles.textFontStyle}
									value={visitorsName}
									onEndEditing={() => this.setState({ visName: false })}
									onFocus={this.onFieldChange.bind(this, 'visName')}
									onChangeText={this.onFieldChange.bind(this, 'visitorsName')}
									inputContainerStyle={[
										styles.textFieldStyle,
										{ borderBottomColor: visName ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada' }
									]}
								/>
							</View>
							<View style={{ height: '25%' }}>
								<Text
								style={styles.labelStyle}>{t('common.kioskVisitorsContactNoTxt')}</Text>
								<TextField
									error={phoneNumberErrorMsg}
									placeholder={t('common.kioskVisitorsContactNoTxt')}
									label=""
									tintColor={baseStyles.eclipseColor}
									errorColor="#ec3535"
									style={styles.textFontStyle}
									value={contactNo}
									onBlur={() => this.phoneNumberValidation(contactNo)}
									onFocus={this.onFieldChange.bind(this, 'cntctNo')}
									onEndEditing={() => {this.setState({ cntctNo: false })}}
									onChangeText={this.onContactNumberChange.bind(this, 'contactNo')}
									inputContainerStyle={[
										styles.textFieldStyle,
										{ borderBottomColor: cntctNo ? CustomConfig.Colors.btnPrimaryBgColor : '#dadada' }
									]}
								/>
							</View>
							<View style={{ height: '25%' }}>
								<Text
								style={styles.labelStyle}>{t('common.kioskPurposeOfVisitTxt')}</Text>
								<Dropdown
									fontSize={baseStyles.inputFontSize}
									containerStyle={{
										width: deviceWidth * 0.4
									}}
									value={purposeOfVisit}
									textColor={baseStyles.eclipseColor}
									labelHeight={5}
									renderAccessory={() => {
										return <MaterialIcons
											size={scaledFont(18)}
											name='keyboard-arrow-down'
											color={baseStyles.dropdownColor}
										/>
									}}
									style={{ fontFamily: baseStyles.latoBold }}
									data={purpose}
									onChangeText={(value, index, data) =>
										this.onPurposeChange(index, data)
									}
								    placeholder={t('common.kioskPurposeOfVisitTxt')}
									label=''
								/>
							</View>
							{showOtherReasonInput &&
								<View style={{ height: '25%' }}>
									<Text
										style={styles.labelStyle}>{t('common.kioskOtherReasonTitle')}</Text>
									<TextField
										error={otherReason==='' ? t('common.kioskOtherReasonError'): ''}
										placeholder={t('common.kioskOtherReasonTitle')}
										label=""
										tintColor={baseStyles.eclipseColor}
										errorColor="#ec3535"
										style={styles.textFontStyle}
										value={otherReason}
										onChangeText={this.onFieldChange.bind(this, 'otherReason')}
										inputContainerStyle={[
											styles.textFieldStyle,
											{ borderBottomColor:'#dadada' }
										]}
									/>
								</View>
							}
							{showOrganizationInput &&
							<View style={{ height: '25%' }}>
								<Text
									style={styles.labelStyle}>{t('common.kioskOrganization')}</Text>
								<TextField
									error={organization==='' ? t('common.kioskOrganizationError'): ''}
									placeholder={t('common.kioskOrganization')}
									label=""
									tintColor={baseStyles.eclipseColor}
									errorColor="#ec3535"
									style={styles.textFontStyle}
									value={organization}
									onChangeText={this.onFieldChange.bind(this, 'organization')}
									inputContainerStyle={[
										styles.textFieldStyle,
										{ borderBottomColor:'#dadada' }
									]}
								/>
							</View>
							}
							{this.renderStaffList()}
							<View style={{ height: '25%' }}>
								<TouchableOpacity
									onPress={() => this.onVisitorSignIn()}
									disabled={isDisabled}
									style={[
										styles.signInBtn,
										{ backgroundColor: isDisabled? '#808080' : CustomConfig.Colors.btnPrimaryBgColor }
									]}>
									<Text
										style={styles.nextBtnText}>Next</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</View>
				<Footer
				style={styles.footerStyle}/>
			</View>
		</KeyboardAwareScrollView>
	}
}

const mapStateToProps = state => ({
	token: state && state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	findAllConfigByCategory,
	findAllStaff,
	updateErrorMessage,
	insertVisitorLog
});

export default connect(mapStateToProps, mapDispatchToProps)(VisitorSignIn);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	viewContainer: {
		height: deviceHeight * 0.55,
		width: deviceWidth * 0.55,
		alignItems: 'center'
	},
	signInView: {
		height: '20%',
		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	signInTxt: {
		fontSize: scaledFont(24),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold
	},
	description: {
		textAlign: 'center',
		fontSize: scaledFont(16),
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoSemibold
	},
	textFieldStyle: {
		width: deviceWidth * 0.4,
		paddingTop: scaledHeight(5),
		height: scaledHeight(35),
		borderBottomWidth: 2
	},
	textFontStyle: {
		fontSize: baseStyles.textInputFont,
		color: baseStyles.eclipseColor,
		fontFamily: baseStyles.latoBold
	},
	signInBtn: {
		height: scaledHeight(60),
		width: deviceWidth * 0.4,
		borderRadius: 3,
		justifyContent: 'center',
		alignItems: 'center'
	},
	labelStyle: {
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize
	},
	formContainer: {
		height: '70%',
		padding: scaledFont(30)
	},
	nextBtnText: {
		color: '#ffffff',
		fontFamily: baseStyles.latoRegular,
		textTransform: 'uppercase',
		fontSize: baseStyles.labelFontSize
	},
});
