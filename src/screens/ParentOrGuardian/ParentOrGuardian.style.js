import { scaledHeight, scaledFont } from '../../utils/Scale';
import CustomConfig from '../../utils/CustomConfig';
import { Dimensions, StyleSheet } from 'react-native';
import { buttonCommonStyle } from '../../config/buttonStyle';
import { baseStyles } from '../../config/baseStyles';
const { width: deviceWidth } = Dimensions.get('window');

const centreAlignStyle = {
  alignItems: 'center',
  justifyContent: 'center',
};

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    ...centreAlignStyle,
  },
  contentView: {
    flex: 0.65,
    width: deviceWidth * 0.5,
    alignItems: 'center',
  },
  signInView: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  signInTxt: {
    fontSize: scaledFont(36),
    color: baseStyles.eclipseColor,
    fontFamily: baseStyles.latoBold,
    marginBottom: baseStyles.margin15,
  },
  description: {
    fontSize: scaledFont(18),
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  labelStyle: {
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  formView: {
    flex: 0.7,
    width: '85%',
  },
  nextBtnView: {
    width: '80%',
    justifyContent: 'center',
    marginTop: 200,
  },
  nextBtn: {
    ...buttonCommonStyle.butttonStyle,
    ...buttonCommonStyle.commonStyle,
  },
  nextBtnTxt: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  logoContainer: {
    flex: 0.35,
    width: '100%',
    ...centreAlignStyle,
  },
  logoStyle: {
    height: scaledHeight(160),
    width: '100%',
  },
  textInputContainerStyle: {
    fontSize: baseStyles.buttonFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    borderBottomWidth: 1,
    borderBottomColor: baseStyles.lightGrey,
    paddingTop: baseStyles.padding5,
    paddingBottom: baseStyles.padding8,
    flexDirection: 'row',
    alignSelf: 'center',
  },
  get themeColor() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  textInputStyle: {
    fontSize: baseStyles.textInputFont,
    color: baseStyles.titleColor,
    fontFamily: baseStyles.latoRegular,
    flex: 1,
  },
  keyboardAvoidingViewStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  nricFinContainer: { 
    flex: 0.7, 
    justifyContent: 'center' 
  },
  scrollableContainer: { 
    flex: 1, 
    alignItems: 'center'
  },
  zIndex10: {
    zIndex: 10,
  },
});