import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';
import { buttonCommonStyle } from '../../config/buttonStyle';
import { scaledFont } from '../../utils/Scale';
const { width: deviceWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: deviceWidth,
    alignItems: 'center',
  },
  footerStyle: {
    flex: 0.15,
    width: deviceWidth,
  },
  descContainer: {
    flex: 0.2,
    width: '70%',
    justifyContent: 'center',
    marginTop: baseStyles.margin30,
  },
  welcomeTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(36),
    fontFamily: baseStyles.latoBold,
  },
  selectTxt: {
    textAlign: 'center',
    color: baseStyles.darkShadeGray,
    fontSize: scaledFont(18),
    fontFamily: baseStyles.latoRegular,
    marginTop: baseStyles.margin15,
  },
  columnView: {
    flex: 0.8,
    width: '40%',
    flexDirection: 'column',
  },
  get primaryButton() {
    return {
      backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
      borderColor: CustomConfig.Colors.btnPrimaryBgColor,
    };
  },
  get primaryColor() {
    return {
      backgroundColor: CustomConfig.Colors.primaryColor,
    };
  },
  buttonStyle: {
    ...buttonCommonStyle.commonStyle,
    ...buttonCommonStyle.butttonStyle,
  },
  buttonCheckInTxt: {
    color: baseStyles.white,
    ...buttonCommonStyle.buttonTextStyle,
  },
  textInputStyle: {
    fontSize: baseStyles.buttonFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
  },
  textAreaStyle: {
    width: '100%',
    marginTop: baseStyles.margin15,
    height: baseStyles.textareaViewHeight,
    borderColor: baseStyles.lightGrey,
    borderRadius: baseStyles.defaultBorderRadius,
    padding: baseStyles.padding10,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1,
    fontSize: scaledFont(18)
  },
  labelStyle: {
    color: baseStyles.darkGrayTintColor,
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
  },
  textFieldContainer: {
    flex: 0.15,
    justifyContent: 'center',
  },
  txtLabel: {
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkShadeGray,
  },
  txtInput: {
    fontSize: baseStyles.inputFontSize,
    fontFamily: baseStyles.latoBold,
    color: baseStyles.black,
  },
  optionStyle: {
    flexDirection: 'row',
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.textInputFont16,
  },
  placeholderColor: {
    color: baseStyles.darkGrayTintColor,
  },
  containerStyle: {
    height: 50,
    borderBottomWidth: 1,
    borderColor: baseStyles.lightGrey,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  placeholderStyle: {
    fontSize: baseStyles.textInputFont16,
    fontFamily: baseStyles.latoRegular,
    color: baseStyles.darkGrayTintColor
  },
  remarksContainer: {
    flex: 0.25,
    justifyContent: 'center',
  },
  warningText: {
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.inputFontSizeSmall,
    color: baseStyles.darkRed,
  },
});