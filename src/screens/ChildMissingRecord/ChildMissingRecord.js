import React from 'react';
import { 
  View, 
  TextInput,
  TouchableOpacity, 
  Text,
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import { t } from '../../utils/LocalizationUtils.js';
import { commonStyle } from '../../config/commonStyle';
import { baseStyles } from '../../config/baseStyles'
import { Handler } from './ChildMissingRecord.handler';
import { styles } from './ChildMissingRecord.style';
import DateTimePicker from '../../components/DateTimePicker';
import SNDropdown from '../../components/SNDropdown';
import { isEmpty } from 'lodash';
import _ from 'lodash';
import {
  updateErrorMessage,
  findAllConfigByCategory,
  addChildCheckInCheckOut,
  guardianAddChildCheckInCheckOut,
	showCustomAlert,
} from '../../redux/actions';
import withPreventDoubleClick from '../../components/TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

const ChildMissingRecord = (props) => {
  const { navigation, allChild } = props;
  
  const {
    remarks,
    dropOffBy,
    onTimeChange,
    addMissingRecords,
    onRemarksChange,
    onSelectChange,
    checkInTime,
    isRequesting,
    relations,
    childIDs,
  } = Handler(props);
  const currentChild = allChild.find(ch => _.get(ch, 'ID') === _.last(childIDs));
  const classAttendance = _.get(currentChild, 'classAttendances.data[0]', null);
  const attendanceTime = classAttendance ? moment(classAttendance.time) : moment();
  const isAfterAttendance = moment(moment(checkInTime, 'hh:mm A')).isAfter(attendanceTime);
  const isTimeInValid = classAttendance && isAfterAttendance;
  const isBtnEnabled = dropOffBy && checkInTime && !(isTimeInValid || isAfterAttendance);

  return (
    <View style={styles.container}>
      <Header backButton navigation={navigation} style={commonStyle.headerStyle} />
      <View style={styles.container}>
        <View style={styles.descContainer}>
          <Text style={styles.welcomeTxt}>{t('common.addMissingCheckInText')}</Text>
          <Text style={styles.selectTxt}>
          {t('common.missingRecordsDescForChild')}
          </Text>
        </View>
        <View style={styles.columnView}>
        <View style={styles.textFieldContainer}>
            <Text style={styles.txtLabel}>
              {t('common.attendanceTakenLabel')}
            </Text>
            <Text style={styles.txtInput}>
              {classAttendance ? attendanceTime.format('DD MMMM YYYY, hh:mm A') : '-'}
            </Text>
          </View>
          <View style={styles.textFieldContainer}>
            <Text style={styles.txtLabel}>
              {t('common.dropOffByText')}
            </Text>
            <SNDropdown
              options={relations}
              placeHolderasText
              placeholder={t('common.selectParentOrGuardian')}
              placeHolderStyle={styles.placeholderStyle}
              selectedOptionStyle={{
                ...styles.optionStyle,
                ...(isEmpty(dropOffBy) && styles.placeholderColor)
              }}
              touchableContainerStyle={styles.containerStyle}
              handleChange={onSelectChange}
              selectedOption={dropOffBy}
            />
          </View>
          <View style={styles.textFieldContainer}>
            <Text style={styles.txtLabel}>{t('common.dropOffTimeText')}</Text>
            <DateTimePicker
              pickerMode="time"
              onDateTimeChange={onTimeChange}
              maxDate={moment().format('HH:mm:ss')}
              baseColor={isTimeInValid && baseStyles.darkRed}
              dateTime={checkInTime}
              pickerFomat="hh:mm A"
              iconName="clock-outline"
            />
            {isTimeInValid && (
              <Text style={styles.warningText}>{t('common.dropOffTimeAlert')}</Text>
            )}
          </View>
          <View style={styles.remarksContainer}>
            <Text style={styles.txtLabel}>
              {t('common.remarks')}
            </Text>
            <TextInput
              multiline
              value={remarks}
              autoCorrect={false}
              autoCapitalize="none"
              placeholder={t('common.missingCIRemarksPlaceholder')}
              style={[
                styles.textInputStyle, 
                styles.textAreaStyle
              ]}
              onChangeText={onRemarksChange}
            />
          </View>
        </View>
        <TouchableButton 
          disabled={!isBtnEnabled || isRequesting}
          onPress={addMissingRecords} 
          style={[
            styles.buttonStyle, 
            styles.primaryButton,
            { opacity: !isBtnEnabled || isRequesting ? baseStyles.disabledOpacity : 1 }
          ]}
        >
          <Text style={styles.buttonCheckInTxt}>{t('common.addRecord')}</Text>
        </TouchableButton>
      </View>
      <Footer style={styles.footerStyle} />
    </View>
  );
}

const mapStateToProps = (state) => ({
  isGuardian: _.get(state, 'isGuardianSignIn.data'),
  allChild: _.get(state, 'findAllChildByParent.data.data.data', []),
  schoolConfig: state.schoolConfig,
});

const mapDispatchToProps = {
  updateErrorMessage,
  findAllConfigByCategory,
  addChildCheckInCheckOut,
  guardianAddChildCheckInCheckOut,
	showCustomAlert,
};

export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(ChildMissingRecord);
