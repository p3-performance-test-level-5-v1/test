import { useState, useEffect } from 'react';
import Constant from '../../constants/Constant';
import { getFullName } from '../../utils/index';
import moment from 'moment';
import _ from 'lodash';
import { 
  errorResponse, 
  handleErrorDataFromApi, 
  retrieveAsyncStorage 
} from '../../utils';
let allRelations = [];

export const Handler = (props) => {
  const { 
    allChild, 
    isGuardian,
    addChildCheckInCheckOut, 
    guardianAddChildCheckInCheckOut,
    findAllConfigByCategory,
    updateErrorMessage,
    navigation,
  } = props;
  const identificationNo = navigation.getParam('identificationNo', '');
  const missingRecords = navigation.getParam('missingRecords', []);

  let isApiRequested = false;
  const [isRequesting, setIsRequesting] = useState(false);
  const [remarks, setRemarks] = useState('');
  const [dropOffBy, setDropOffBy] = useState(null);
  const [childIDs, setChildIDs] = useState(missingRecords);
  const [relations, setAllChildRelations] = useState([]);
  const [checkInTime, setCheckInTime] = useState();

  useEffect(() => {
    getAllRelationshipConfig();
  }, []);

  useEffect(() => {
    resetForm();
    getChildRelations();
    updateCheckInTime();
  }, [childIDs]);

  const updateCheckInTime = () => {
    const currentChild = allChild.find(ch => _.get(ch, 'ID') === _.last(childIDs));
    const classAttendance = _.get(currentChild, 'classAttendances.data.0', null);
    const attendanceTime = classAttendance ? moment(classAttendance.time) : moment();
    setCheckInTime(attendanceTime.format('hh:mm a'));
  }

  const resetForm = () => {
    setRemarks('');
    setDropOffBy(null);
    setCheckInTime();
  }

  const getAllRelationshipConfig = async () => {
    const schoolId = await retrieveAsyncStorage('schoolId');

    const nonParentRelationReqData = {
      searchCategory: Constant.NON_PARENT_RELATIONSHIP,
      filter: {
        fkSchool: schoolId,
      },
    };
    const npRelationChild = await findAllConfigByCategory(
      nonParentRelationReqData
    );
    const npRelations = _.get(
      npRelationChild,
      'data.findAllConfigByCategory.data',
      []
    );

    const relationChildReqData = {
      searchCategory: Constant.RELATION_CHILD,
      filter: {
        fkSchool: schoolId,
      },
    };
    const relationChild = await findAllConfigByCategory(relationChildReqData);
    const childRelations = _.get(
      relationChild,
      'data.findAllConfigByCategory.data',
      []
    );

    allRelations = [...npRelations, ...childRelations];
    getChildRelations();
  };

  const getChildRelations = () => {
    const currentChild = allChild.find(ch => _.get(ch, 'ID') === _.last(childIDs));
    const currentChildRelations = _.get(currentChild, 'childRelations.data', []);
    const guardianChildRelations = _.get(currentChild, 'guardianChildRelations.data', []);
    const guardianList = guardianChildRelations.map(gCR => {
      const firstname = _.get(gCR, 'guardian.firstname');
      const lastname = _.get(gCR, 'guardian.lastname');
      const relationship = allRelations.find(aR => aR.ID === _.get(gCR, 'fkRelation'));
      const guardianName = `${getFullName(firstname, lastname)} (${_.get(relationship, 'description', '')})`
      return {
        isGuardian: true,
        ID: _.get(gCR, 'guardian.ID'),
        name: guardianName,
      }
    });
    const parentList = currentChildRelations.map(cR => {
      const firstname = _.get(cR, 'parent.firstname');
      const lastname = _.get(cR, 'parent.lastname');
      const relationship = allRelations.find(aR => aR.ID === _.get(cR, 'fkRelation'));
      const guardianName = `${getFullName(firstname, lastname)} (${_.get(relationship, 'description', '')})`
      return {
        isGuardian: false,
        ID: _.get(cR, 'parent.ID'),
        name: guardianName,
      }
    });
    setAllChildRelations([...parentList, ...guardianList]);
  }

  const getSignedInParent = () => {
    let signedInParent = {};
    for (let i = 0; i < allChild.length; i++) {
      if (isGuardian) {
        const guardianChildRelations = _.get(allChild[i], 'guardianChildRelations.data', []);
        const selectedGuardian = guardianChildRelations.find(cR =>(
          _.get(cR, 'guardian.identificationNo', '').toUpperCase() === identificationNo
        ));
        signedInParent = { ..._.get(selectedGuardian, 'guardian'), isGuardian: true };
      } else {
        const childRelations = _.get(allChild[i], 'childRelations.data', []);
        const selectedParent = childRelations.find(cR =>(
          _.get(cR, 'parent.identificationNo', '').toUpperCase() === identificationNo
        ));
        signedInParent = { ..._.get(selectedParent, 'parent'), isGuardian: false };
      }
    }
    return signedInParent;
  }

  const onTimeChange = (time) => {
    setCheckInTime(time);
  }

  const onRemarksChange = (value) => {
    setRemarks(value);
  }

  const onSelectChange = (item) => {
    setDropOffBy(item);
  }

  const addChildCheckInOut = async ({ idChildren = [], droppedBy = {}, time, checkInOutType }) => {
    const accessToken = await retrieveAsyncStorage('accessToken');
    const fkCentre = await retrieveAsyncStorage('centreId');

    const common = {
      remarks,
      idChildren,
      checkInOutType,
      source: Constant.KIOSK,
      idCentre: fkCentre,
      at: time,
    };

    let status, result;
    try {
      if (isApiRequested) return;
      isApiRequested = true;
      setIsRequesting(true);
      if (isGuardian || _.get(droppedBy, 'isGuardian')) {
        let guardianParentIDObj = { parentID: _.get(droppedBy, 'ID') };
        if (_.get(droppedBy, 'isGuardian')) {
          guardianParentIDObj = { guardianID: _.get(droppedBy, 'ID') };
        }
        const reqData = {dto: { ...common,  ...guardianParentIDObj }};
        result = await guardianAddChildCheckInCheckOut(reqData, accessToken);
        status = _.get(result, 'data.guardianAddChildCheckInCheckOut', false);
      } else {
        const reqData = { ...common, droppedBy: _.get(droppedBy, 'ID') };
        result = await addChildCheckInCheckOut(reqData, accessToken);
        status = _.get(result, 'data.addChildCheckInCheckOut', false);
      }
      isApiRequested = false;
      setIsRequesting(false);
      
      if (!_.get(result, 'success')) {
        const message = handleErrorDataFromApi(result, errorResponse);
        message && updateErrorMessage(message);
        return false;
      } 
    } catch (error) {
      isApiRequested = false;
      setIsRequesting(false);
      throw error;
    }
    return status;
  }

  const addMissingRecords = async () => {
    const today = moment().format('YYYY-MM-DD');
    const droppedBy = getSignedInParent();
    
    const data = {
      idChildren: [_.last(childIDs)], 
      droppedBy: dropOffBy, 
      time: moment(today + ' ' + checkInTime, 'YYYY-MM-DD hh:mm A').format('YYYY-MM-DD HH:mm:ss'),
      checkInOutType: Constant.TYPE_CHECK_IN
    };
    const checkInStatus = await addChildCheckInOut(data);

    if (checkInStatus) {
      const data = {
        idChildren: [_.last(childIDs)], 
        time: moment().format('YYYY-MM-DD HH:mm:ss'),
        checkInOutType: Constant.TYPE_CHECK_OUT,
        droppedBy,
      };
      const checkOutStatus = await addChildCheckInOut(data);
      
      const updatedChildIDs = childIDs.filter(id => id !== _.last(childIDs));
      if (checkOutStatus && updatedChildIDs.length === 0) {
        const checkedChild = allChild.filter(ch => missingRecords.some(id => id === ch.ID));
        navigation.navigate('ChildCheckInOutSuccess', {
          childDetails: checkedChild,
          identificationNo,
          isCheckIn: false,
        });
      }
      setChildIDs(updatedChildIDs);
    }
  }

  return ({
    remarks,
    dropOffBy,
    onTimeChange,
    addMissingRecords,
    onRemarksChange,
    onSelectChange,
    checkInTime,
    isRequesting,
    relations,
    childIDs,
  });
}