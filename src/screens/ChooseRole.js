import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { scaledWidth, scaledHeight, scaledFont } from '../utils/Scale';
import Header from '../components/Header';
import { baseStyles } from '../config/baseStyles';
import _ from 'lodash';
import { connect } from 'react-redux';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth } = Dimensions.get('window');
import ButtonTextBox from '../components/ButtonTextBox';
import Constant from '../constants/Constant';

class ChooseRole extends Component {
	render() {
		const { navigation, schoolConfig } = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', Constant.LSH) || Constant.LSH;

		return (
			<View style={styles.container}>
				<Header
					backButton
					navigation={navigation}
					style={commonStyle.headerStyle}
				/>
				<View style={styles.container}>
					<Text style={styles.signInSignOutTxt}>
						{t('common.kioskCheckInCheckOutTxt')}
					</Text>
					<Text style={styles.chooseRoleTxt}>
						{t('common.kioskRoleSelectionTxt')}
					</Text>
					<View style={styles.roleOptionView}>
						<ButtonTextBox
							navigatePath={() =>
								navigation.navigate('ParentOrGuardian')
							}
							mainText={t('common.kioskParentsTxt')}
							secondaryText={t('common.kioskParentsTxtZh')}
						/>
						<ButtonTextBox
							navigatePath={() => navigation.navigate('Guardian')}
							mainText={t('common.kioskGuardiansTxt')}
							secondaryText={t('common.kioskGuardiansTxtZh')}
						/>
						<ButtonTextBox
							navigatePath={() =>
								navigation.navigate('StaffSignIn')
							}
							mainText={t('common.kioskCentreStaffTxt')}
							secondaryText={t('common.kioskCentreStaffTxtZh')}
						/>
						<ButtonTextBox
							navigatePath={() => navigation.navigate('Visitor')}
							mainText={t('common.kioskVisitor')}
							secondaryText={t('common.kioskVisitorZh')}
						/>
						{school === Constant.LSH && (
							<ButtonTextBox
								navigatePath={() =>
									navigation.navigate('BusSignIn')
								}
								mainText={t('bus.schoolBus', {
									defaultValue: 'School Bus'
								})}
								secondaryText={t('bus.schoolBusZh', {
									defaultValue: '校车'
								})}
							/>
						)}
					</View>
				</View>
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	schoolConfig: state.schoolConfig
});

export default connect(mapStateToProps, {})(ChooseRole);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	signInSignOutTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(36),
		fontFamily: baseStyles.latoBold
	},
	chooseRoleTxt: {
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(18),
		fontFamily: baseStyles.latoSemibold,
		marginTop: baseStyles.margin5
	},
	roleOptionView: {
		width: deviceWidth,
		flexDirection: 'column',
		alignItems: 'center'
	},
	roleBtn: {
		height: scaledHeight(130),
		width: scaledWidth(320),
		alignItems: 'center',
		justifyContent: 'space-evenly',
		borderWidth: 1,
		flexDirection: 'row'
	},
	roleTxt: {
		color: baseStyles.white,
		textAlign: 'center',
		fontSize: scaledFont(32),
		fontFamily: baseStyles.latoBold
	},
	childActivated: {
		height: scaledHeight(60),
		width: scaledWidth(88),
		left: 19,
		bottom: 0,
		position: 'absolute'
	},
	authorisedPerson: {
		height: scaledHeight(65),
		width: scaledWidth(50),
		left: 19,
		bottom: 0,
		position: 'absolute'
	},
	centreStaffOuter: {
		height: scaledHeight(52),
		width: scaledWidth(77),
		left: 17,
		bottom: 0,
		position: 'absolute'
	},
	busDriverActivated: {
		height: scaledHeight(47),
		width: scaledWidth(96),
		position: 'absolute',
		left: 6,
		bottom: 0
	},
	rowStyle: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	get primaryFontColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
			borderColor: CustomConfig.Colors.btnPrimaryBgColor
		};
	}
});
