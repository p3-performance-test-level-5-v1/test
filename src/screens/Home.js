import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
	AppState,
	Image
} from 'react-native';
import { connect } from 'react-redux';
import QRCode from 'react-native-qrcode-svg';
import { get } from 'lodash';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import moment from 'moment';
import { visitorHeaderImage } from '../config/config';
import { baseStyles } from '../config/baseStyles';
import {
	errorResponse,
	retrieveAsyncStorage,
	handleErrorDataFromApi,
	getAppEnv
} from '../utils';
import _ from 'lodash';
import { NavigationEvents } from 'react-navigation';
import * as actions from "../redux/actions";
import { t } from '../utils/LocalizationUtils.js';
import ModalCard from '../components/ModalCard/ModalCard.view';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

const {
	width: deviceWidth,
	height: deviceHeight
} = Dimensions.get('window');

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			qrcodeToken: "Not empty value", // Cannot accept "" for QRCode library
			appState: AppState.currentState,
		};
		this.timer = null;
	}

	async componentDidMount() {
		AppState.addEventListener('change', e => {
			this.handleAppStateChange(e);
		});
		const { getCentreHolidaysOfYear } = this.props;
		getCentreHolidaysOfYear()
	}

	componentWillUnmount() {
		AppState.removeEventListener('change', e => {
			this.handleAppStateChange(e);
		});
	}

	handleAppStateChange = nextAppState => {
		const { fetchUserDetails, navigation } = this.props;
		if (
			this.state.appState.match(/inactive|background/) &&
			nextAppState === 'active'
		) {
			fetchUserDetails(navigation);
			this.getCheckInCheckOutToken();
		}
		this.setState({ appState: nextAppState });
	};

	unmountComponent() {
		if (this.timer) {
			clearTimeout(this.timer);
			this.timer = null;
		}
	}
	async getCheckInCheckOutToken() {
		const { navigation } = this.props;
		const accessToken = await retrieveAsyncStorage('accessToken');
		let centreId = await retrieveAsyncStorage('centreId');
		centreId = centreId ? parseInt(centreId) : 0;

		let message = '';
		const result = await this.props.fetchCheckInCheckOutToken({IDCentre: centreId,}, accessToken);
		if (result.success) {
			const qrcodeToken = _.get(result, 'data.getCheckInOutToken', null); // Cannot accept "" for QRCode library
			if (qrcodeToken) {
				this.setState({
					qrcodeToken,
				});
			}
		} else {
			message = handleErrorDataFromApi(result, errorResponse);
		}
		this.props.updateErrorMessage(message, this.getCheckInCheckOutToken.bind(this));

		this.timer = setTimeout(() => {
			if (this.timer) {
				this.getCheckInCheckOutToken();
			}
		}, 60000);
	}
	render() {
		const {
			navigation,
			allCentres,
			centreLoginData,
			schoolConfig,
			showConfirmation,
			signOutConfirmation,
			mobileAppVersion,
		} = this.props;
		let centerObj = allCentres.filter(obj => obj.ID === centreLoginData.centreId);
		centerObj = centerObj ? centerObj[0] : null;
		const appEnv = getAppEnv();
		const school = get(schoolConfig, 'data.data[0].school.code', '');
		const schoolName = get(schoolConfig, 'data.data[0].school.name', '')

		return <View
		style={styles.container}>
			<View style={styles.headerContainer}>
				<TouchableOpacity
				onPress={() => signOutConfirmation(showConfirmation)}
				style={styles.signOutBtn}>
					<Text style={styles.signOutBtnText}>
						{t('common.kioskLogoutText')}
					</Text>
					<Icons
						name='logout'
						size={scaledHeight(20)}
						color={CustomConfig.Colors.primaryColor}
						style={styles.signOutIconMargin}
					/>
				</TouchableOpacity>
			</View>
			<View
			style={styles.qrContainer}>
				<View style={styles.logoContainer}>
					<Image resizeMode="contain" source={visitorHeaderImage[school]} style={styles.logoStyle} />
				</View>
				<View
				style={styles.timeCentreView}>
					<View style={{ flex: 0.3 }}>
						<Text style={styles.timeCentreHead}>{t('common.date')}</Text>
						<Text
						style={styles.timeCentreTxt}>{moment().format('ddd, DD/MM/YYYY')}</Text>
					</View>
					<View style={{ flex: 0.3 }}>
						<Text style={styles.timeCentreHead}>{t('common.time')}</Text>
						<Text
						style={styles.timeCentreTxt}>{moment().format('LT')}</Text>
					</View>
					<View style={{ flex: 0.4 }}>
						<Text style={styles.timeCentreHead}>{t('common.kioskCentreText')}</Text>
						<Text
						style={styles.timeCentreTxt}>{centerObj ? centerObj.label : ''}</Text>
					</View>
				</View>
				<View
				style={styles.thinLine}/>
				<View
				style={[
					styles.innerContainer,
					styles.centerItem
				]}>
					<View styles={styles.qrCodeDesc}>
						<Text
						style={styles.description}>{t('common.kioskCheckInText')}</Text>
						<Text
						style={styles.description}>{t('common.kioskCentreQRText', { schoolName})}</Text>
					</View>
					<QRCode
						value={this.state.qrcodeToken}
						size={300}
					/>
				</View>
				<View style={[
					styles.btnContainerStyle,
					styles.centerItem
				]}>
					<TouchableOpacity
					onPress={() => navigation.navigate('ChooseRole')}
					style={[styles.btnStyle, buttonCommonStyle.butttonStyle]}>
						<Text style={[styles.btnTxtColor, buttonCommonStyle.buttonTextStyle]}>
							{t('common.kioskManualCheckInCheckOutTxt')}
						</Text>
					</TouchableOpacity>
				</View>
			</View>
			<NavigationEvents
				onDidFocus={payload => this.getCheckInCheckOutToken()}
				onWillBlur={payload => this.unmountComponent()}
			/>
			<ModalCard
				isVisible={showConfirmation}
				title={t('common.kioskLogoutText')}
				desc={t('common.kioskLogoutConfirmText')}
				onCancel={() => signOutConfirmation(showConfirmation)}
				onConfirm={() => {
					signOutConfirmation(showConfirmation)
					actions.signOut(navigation);
				}}
				onClickContainer={() => signOutConfirmation(showConfirmation)}
			/>
			<View style={styles.versionContainer}>
				<Text style={styles.appVersionStyle}>
					Version {appEnv && appEnv !== '' ? `${mobileAppVersion} - ${appEnv}` : mobileAppVersion}
				</Text>
			</View>
		</View>;
	}
}

const mapStateToProps = state => ({
	signInOutQRCode: state.fetchCheckInCheckOutToken,
	centreLoginData: state.centreLogin,
	allCentres: get(state.listAllCentre, 'data.data.data.findAllCentreForSchool.data', []),
	schoolConfig: state.schoolConfig,
	showConfirmation: get(state, 'showConfirmation.data', false),
	mobileAppVersion: get(state, 'commonVariableValues.localAppVersion', ''),
});

const mapDispatchToProps = ({
	fetchCheckInCheckOutToken: actions.fetchCheckInCheckOutToken,
	updateErrorMessage: actions.updateErrorMessage,
	fetchUserDetails: actions.fetchUserDetails,
	signOutConfirmation: actions.showSignOutConfirmation,
	getCentreHolidaysOfYear: actions.getCentreHolidaysOfYear
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const centreAlignStyle = {
	alignItems: 'center',
	justifyContent: 'center',
  };

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	qrContainer: {
		flex: 1,
		width: deviceWidth * 0.75,
		alignItems: 'center',
		justifyContent: 'center'
	},
	timeCentreView: {
		flex: 0.15,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		alignItems: 'center'
	},
	timeCentreHead: {
		marginBottom: baseStyles.margin5,
		color: baseStyles.darkGrayTintColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoSemibold
	},
	timeCentreTxt: {
		color: baseStyles.darkShadeGray,
		fontSize: baseStyles.h3FontSize,
		fontFamily: baseStyles.latoRegular
	},
	logoContainer: {
		flex: 0.3,
		width: '100%',
		...centreAlignStyle,
	},
	description: {
		textAlign: 'center',
		color: baseStyles.eclipseColor,
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoRegular
	},
	thinLine: {
		width: deviceWidth * 0.75,
		borderColor: '#e5e5e5',
		borderWidth: 0.5
	},
	get btnStyle() {
		return {
			height: deviceHeight * 0.05,
			width: deviceWidth * 0.4,
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
			borderRadius: baseStyles.borderRadius,
			justifyContent: 'center',
			alignItems: 'center'
		}
	},
	btnTxtColor: {
		color: baseStyles.white,
	},
	get visitorBtnTxtColor() {
		return {
			color: CustomConfig.Colors.btnPrimaryBgColor,
		}
	},
	qrImage: {
		width: scaledWidth(200),
		height: scaledHeight(200)
	},
	centerItem: {
		alignItems: 'center',
		justifyContent: 'space-evenly'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	linkStyle: {
		paddingVertical: scaledHeight(10),
		paddingHorizontal: scaledWidth(15),
		justifyContent: 'center',
		alignItems: 'center'
	},
	qrCodeDesc: {
		width: deviceWidth * 0.6,
		marginBottom: scaledHeight(15)
	},
	appVersionStyle: {
		fontSize: baseStyles.buttonFontSize,
		textAlign: "center"
	},
	btnContainerStyle: {
		flex: 0.2,
	},
	headerContainer: {
		flex: 0.1,
		width: '100%',
		justifyContent: 'flex-end',
		paddingHorizontal: baseStyles.padding20,
		alignItems: 'flex-end'
	},
	signOutBtn: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: baseStyles.padding10,
		paddingHorizontal: baseStyles.padding20,
	},
	get signOutBtnText() {
		return ({
			fontSize: scaledFont(15),
			fontFamily: baseStyles.latoBold,
			color: CustomConfig.Colors.primaryColor,
		})
	},
	signOutIconMargin: {
		marginLeft: baseStyles.margin5,
	},
	innerContainer: { 
		flex: 0.6 
	},
	versionContainer: {
		flex: 0.1,
	},
});
