import React, { Component } from 'react';
import {
	View,
	Text,
	Dimensions,
	StyleSheet,
	FlatList,
	TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { baseStyles } from '../config/baseStyles';
import CheckBox from 'react-native-check-box';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { 
	listAllBusChildren,
	getCheckInOutToken,
	updateErrorMessage,
	childCheckIn,
	childCheckOut
} from '../redux/actions';
import { 
	scaledWidth, 
	scaledFont 
} from '../utils/Scale';
import { 
	getChildName,
	errorResponse,
	retrieveAsyncStorage, 
	handleErrorDataFromApi 
} from '../utils';
import _ from 'lodash';
import moment from 'moment';
import CachedImage from '../components/CachedImage';
import { t } from '../utils/LocalizationUtils.js';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';
import { commonStyle } from '../config/commonStyle';
const { width: deviceWidth } = Dimensions.get('window');

class BusAttendant extends Component {
	constructor(props) {
		super(props);
		this.state = {
			childList: [],
			checkedList: [],
		};
	}
	componentDidMount() {
		this.findAllBusChildren();	
	}
	async findAllBusChildren() {
		const { listAllBusChildren, navigation } = this.props;
		const accessToken = await retrieveAsyncStorage('accessToken');
		let fkCentre = await retrieveAsyncStorage('centreId');
		fkCentre = fkCentre ? parseInt(fkCentre) : 0;
		const selectedBus = navigation.getParam('selectedBus', {});
		const busID = _.get(selectedBus, 'bus.ID');
		if (accessToken && fkCentre && busID) {
			const reqData = { busID, centreID: fkCentre };
			const data = await listAllBusChildren(reqData, accessToken);
			if (!data.success) {
				const message = handleErrorDataFromApi(data, errorResponse);
				message && this.props.updateErrorMessage(message, this.findAllBusChildren.bind(this))
				return;
			}
			const childList = _.get(data, 'data.listAllBusChildren.data', []);
			const today = moment().format('YYYY-MM-DD');
			const lists = childList.filter(ch => {
				const serviceStart = moment(ch.serviceStartDate).format('YYYY-MM-DD');
				const serviceEnd = moment(ch.serviceEndDate).format('YYYY-MM-DD');
				const status = moment(today).isBetween(serviceStart, serviceEnd, null, '[]');
				if (status) {
					return ch;
				}
				return ;
			});
			lists.length > 0 && this.setState({ childList: lists });
		}
	}
	getAuthToken(checkInOutToken) {
		return _.get(checkInOutToken, 'data.getCheckInOutToken', null);
	}
	checkInOut(childId) {
		const { checkedList } = this.state;
		const indexOf = checkedList.indexOf(childId);
		const isChecked = indexOf >= 0 ? true : false;
		if (isChecked) {
			checkedList.splice(indexOf, 1);
		} else {
			checkedList.push(childId);
		}
		this.setState({
			checkedList: checkedList.concat([])
		});
	}
	checkInOutNavigation(data) {
		const { navigation } = this.props;
		const isSignIn = navigation.getParam('isSignIn', false);
		const selectedBus = navigation.getParam('selectedBus', {});
		const plateNumber = _.get(selectedBus, 'bus.plateNumber', '');
		const { checkedList, childList } = this.state;
		const childData = childList.map(ch => ch.child);
		if (data.success) {
			navigation.navigate('BusAttendantSuccess', { 
				childList: childData,
				plateNumber,
				checkedList, 
				isSignIn,
			});
			return;
		}
		const message = handleErrorDataFromApi(data, errorResponse);
		message && this.props.updateErrorMessage(message, this.childCheckInOut.bind(this));
	}
	async childCheckInOut() {
		const { 
			navigation, 
			childCheckOut, 
			childCheckIn,
			getCheckInOutToken
		} = this.props;
		const isSignIn = navigation.getParam('isSignIn', false);
		const accessToken = await retrieveAsyncStorage('accessToken');
		let fkCentre = await retrieveAsyncStorage('centreId');
		fkCentre = fkCentre ? parseInt(fkCentre) : 0;
		const { checkedList } = this.state;
		const checkInOutToken = await getCheckInOutToken({ IDCentre: fkCentre }, accessToken);
	    const authToken = this.getAuthToken(checkInOutToken);
	    const currentDatetime = moment().format('YYYY-MM-DD HH:mm:ss');

		if (!accessToken || !authToken || checkedList.length == 0) {
			return;
		}
		const reqData = { 
			IDChilds: checkedList, 
			token: authToken, 
			date: currentDatetime 
		};
		if (isSignIn) {
			childCheckIn(reqData, accessToken).then(checkInData => {
				this.checkInOutNavigation(checkInData);
			});
			return;
		}
		childCheckOut(reqData, accessToken).then(checkOutData => {
			this.checkInOutNavigation(checkOutData);
		});
	}
	renderChild = ({ item }) => {
		const { checkedList } = this.state;
		const childID = _.get(item, 'child.ID');
		const indexOf = checkedList.indexOf(childID);
		const isChecked = indexOf >= 0 ? true : false;

		return <View 
		style={styles.childListView}>
			<CheckBox 
				style={styles.checkBoxStyle}
			    onClick={()=> this.checkInOut(childID)} 
			    checkBoxColor={CustomConfig.Colors.btnPrimaryBgColor} 
			    isChecked={isChecked}
			/>
			<CachedImage
	            style={styles.childImageStyle}
	            source={_.get(item, 'child.imageKey')}
	        />
			<Text 
			style={styles.boldTextStyle}>{getChildName(item.child)}</Text>
		</View>
	}
	render() {
		const { navigation } = this.props;
		const selectedBus = navigation.getParam('selectedBus', {});
		const { childList, checkedList } = this.state;

		return (
			<View 
			style={styles.container}>
				<Header
					backButton 
					navigation={navigation}
					style={commonStyle.headerStyle}
				/>
				<View 
				style={styles.container}>
					<View 
					style={styles.topView}>
						<Text 
						style={styles.busAttendantTxt}>{`${_.get(selectedBus, 'label', '')} - ${selectedBus.direction === 'arrival' ? t('common.kioskToSchool') : t('common.kioskFromSchool')}`}</Text>
						<View style={styles.contentContainer}>
							<View 
							style={styles.plateNumberStyle}>
								<Text 
								style={styles.basicFont}>{t('common.kioskLicensePlateNumber')}</Text>
								<Text 
								style={{
									...styles.boldTextStyle,
									marginTop: baseStyles.margin5,
								}}>{_.get(selectedBus, 'bus.plateNumber', '')}</Text>
							</View>
							<View 
							style={styles.description}>
								<Text 
								style={styles.basicFont}>{t('common.kioskBusChildCount')}</Text>
								<Text 
								style={{
									...styles.boldTextStyle,
									marginTop: baseStyles.margin5,
								}}>{childList.length}</Text>
							</View>
						</View>
					</View>
					<View 
					style={styles.flatListView}>
						<FlatList
							data={childList} 
							extraData={this.state} 
							style={styles.flatList}
							removeClippedSubviews={false}  
							showsVerticalScrollIndicator={false} 
							keyExtractor={item => item.ID.toString()}
							renderItem={this.renderChild}
						/>
					</View>
					<View 
					style={styles.nextBtnView}>
						<TouchableOpacity 
						disabled={checkedList.length > 0 ? false : true}
						onPress={this.childCheckInOut.bind(this)}
						style={[
							styles.nextBtn,
							styles.primaryColor,
							{ opacity: checkedList.length > 0 ? 1 : baseStyles.disabledOpacity }
						]}>
							<Text 
							style={styles.nextBtnTxt}>{t('common.kioskSubmitBtnTxt')}</Text>
						</TouchableOpacity>
					</View>
				</View>
				<Footer 
				style={styles.footerStyle}/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	centreLoginToken: state && state.centreLogin,
	schoolConfig: state.schoolConfig
});

const mapDispatchToProps = ({
	listAllBusChildren,
	getCheckInOutToken,
	updateErrorMessage,
	childCheckIn,
	childCheckOut
});

export default connect(mapStateToProps, mapDispatchToProps)(BusAttendant);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	footerStyle: {
		flex: 0.08,
		width: deviceWidth
	},
	topView: {
		flex: 0.25,
		width: deviceWidth * 0.65,
		alignItems: 'center',
		justifyContent: 'space-evenly'
	},
	busAttendantTxt: {
		fontSize: scaledFont(24),
		fontFamily: baseStyles.latoBold,
		color: baseStyles.eclipseColor
	},
	basicFont: {
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.labelFontSize,
		fontFamily: baseStyles.latoSemibold
	},
	flatListView: {
		flex: 0.5,
		alignItems: 'center',
		width: deviceWidth * 0.65
	},
	flatList: {
		width: '100%',
		borderTopWidth: 1,
		borderTopColor: '#dadada'
	},
	nextBtnView: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	nextBtn: {
		...buttonCommonStyle.butttonStyle,
		...buttonCommonStyle.commonStyle,
	},
	nextBtnTxt: {
		color: baseStyles.white,
		...buttonCommonStyle.buttonTextStyle,
	},
	childListView: {
		flexDirection: 'row',
		paddingVertical: 10,
		alignItems: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#dadada'
	},
	childImageStyle: {
		backgroundColor: baseStyles.lightGreyColor,
		marginRight: scaledWidth(20),
		height: scaledFont(40),
		width: scaledFont(40),
		borderRadius: scaledFont(40)/2
	},
	boldTextStyle: {
		color: baseStyles.eclipseColor,
		fontSize: baseStyles.textInputFont,
		fontFamily: baseStyles.latoBold
	},
	checkBoxStyle: {
		padding: scaledFont(5),
		marginRight: scaledWidth(20),
	},
	plateNumberStyle: {
		flex: 0.5,
	},
	description: {
		flex: 0.5
	},
	contentContainer: { 
		flexDirection: 'row',
		alignItems: 'center',
		width: deviceWidth * 0.65 
	},
	get primaryColor() {
		return { 
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor 
		}
	}
});