import {
  Dimensions
} from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const guidelineBaseWidth = 768;
const guidelineBaseHeight = 1024;

const scale = size => deviceWidth / guidelineBaseWidth * size;

export const scaledWidth = wPx => {
  const x = wPx / guidelineBaseWidth;
  return deviceWidth * x;
};

export const scaledHeight = wPy => {
  const y = wPy / guidelineBaseHeight;
  return deviceHeight * y;
};

export const scaledFont = (size, factor = 0.5) => {
	return size + ( scale(size) - size ) * factor;
}
