import { I18nManager } from 'react-native';
import * as RNLocalize from 'react-native-localize';
import  { memoize, get } from 'lodash';
import DeviceInfo from 'react-native-device-info';
import i18n from 'i18n-js';
import SentryHandler from '../sentry';
import * as Sentry from '@sentry/react-native';
import { DEFAULT_LANGUAGE } from '../config/config';
import { APP_ENV, REMOTE_LOCALISATION_URL } from '../redux/config/';
import { APP_ENVIRONMENTS } from '../constants/Constant';
import { getSchoolId } from './SchoolConfig';

/**
 * LocalConfig Object -
 * Add the language and the local config here,
 * Remote localisation fetch uses this object's keys to determine the languages supported.
 * **/
const LocalConfig = {
  en: () => get(require('../translations/en'), 'default', {}),
};

export const t = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export class LocalisationUtils {
  constructor() {
    LocalisationUtils.init();
  }

  static init() {
    LocalisationUtils.initLocalisation();
    LocalisationUtils.setRemoteLocalisationConfig();
    LocalisationUtils.RemoteLocalisationaData = {};
  }

  static async fetchSchoolID() {
    /* We don't have school ID on login page */
    return getSchoolId();
  }

  static async setRemoteLocalisationConfig() {
    try {
      // This is an option to disable remote localisation fetch for certain environments.
      // Currently restricted on local env.

      const remoteLocalisationDisabledEnvs = [APP_ENVIRONMENTS.local];
      if (remoteLocalisationDisabledEnvs.includes(APP_ENV)) {
        return;
      }

      const schoolID = await this.fetchSchoolID();
      const languages = Object.keys(LocalConfig);
      languages.forEach(async language => {
        const response = await fetch(
          REMOTE_LOCALISATION_URL.replace('SCHOOL_ID', schoolID).replace(
            'LANGUAGE',
            language
          )
        );

        if (!response.ok) {
          const loggerMsg = `
          API Failure for fetching remote localisation config for language: ${language} with schoolID: ${schoolID},
          returned http status: ${response.status} with statusText: ${response.statusText}. Falling back to local config.`;

          SentryHandler.log(loggerMsg, Sentry.Severity.Fatal);
          return;
        }

        const data = await response.json();

        if (!data.Content) {
          SentryHandler.log(
            'Remote localisation fetch did not return data.content'
          );
        }

        LocalisationUtils.RemoteLocalisationaData[language] = () =>
          data.Content;
        LocalisationUtils.setI18nConfig(
          LocalisationUtils.RemoteLocalisationaData
        );
        I18nManager.forceRTL(false);
      });
    } catch (e) {
      SentryHandler.log(e.message, Sentry.Severity.Fatal);
    }
  }

  static initLocalisation() {
    LocalisationUtils.setI18nConfig(LocalConfig);
  }

  static setI18nConfig(config) {
    // fallback if no available language fits
    const fallback = { languageTag: DEFAULT_LANGUAGE, isRTL: false };

    const { languageTag, isRTL } =
      RNLocalize.findBestAvailableLanguage(Object.keys(config)) || fallback;

    // clear translation cache
    t.cache.clear();
    // update layout direction
    I18nManager.forceRTL(isRTL);

    i18n.translations = { [languageTag]: config[languageTag]() };
    i18n.locale = languageTag;
  }
}
