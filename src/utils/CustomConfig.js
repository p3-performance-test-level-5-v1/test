import { baseStyles } from '../config/baseStyles';
import { getValueByKey } from './SchoolConfig';
import { SCHOOL } from '../redux/config';

class CustomConfig {
  constructor() {
    /* Default color for each app */
    if (SCHOOL === 'TCC') {
      this.Colors = {
        primaryColor: '#f9a350',
        secondaryColor: '#a1a1a1',
        btnSecondaryBgColor: 'white',
        btnPrimaryBgColor: '#f9a350',
        primaryTextColor: '#f9a350',
        secondaryTextColor: 'white',
      };
    } else if (SCHOOL === 'LSH') {
      this.Colors = {
        primaryColor: '#56c4c5',
        secondaryColor: '#a1a1a1',
        btnSecondaryBgColor: 'white',
        btnPrimaryBgColor: '#56c4c5',
        primaryTextColor: '#56c4c5',
        secondaryTextColor: 'white',
      };
    } else if (SCHOOL === 'MFS') {
      this.Colors = {
        primaryColor: '#ec6608',
        secondaryColor: '#a1a1a1',
        btnSecondaryBgColor: 'white',
        btnPrimaryBgColor: '#ec6608',
        primaryTextColor: '#ec6608',
        secondaryTextColor: 'white',
      };
    } else {
      this.Colors = {
        primaryColor: baseStyles.primaryColor,
        secondaryColor: baseStyles.secondaryColor,
        btnSecondaryBgColor: baseStyles.btnSecondaryBgColor,
        btnPrimaryBgColor: baseStyles.btnPrimaryBgColor,
        primaryTextColor: baseStyles.primaryTextColor,
        secondaryTextColor: baseStyles.secondaryTextColor,
      };
    }
  }

  fetchTheme = async () => {
    /* Change color based on AsyncStorage, which come from school config API */
    const [primaryColor, secondaryColor] = await Promise.all([
      getValueByKey('header_color'),
      getValueByKey('secondary_color'),
    ]);
    /* Use primaryColor since we only had primary_color and secondary_color from DB */
    this.Colors.primaryColor = primaryColor;
    this.Colors.secondaryColor = secondaryColor;
    this.Colors.btnPrimaryBgColor = primaryColor;
    this.Colors.primaryTextColor = primaryColor;
    return this.Colors;
  };
}

export default new CustomConfig();