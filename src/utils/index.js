import {
    AsyncStorage,
    Platform
} from 'react-native';
import _ from 'lodash';
import DeviceInfo from "react-native-device-info";
import { t } from '../utils/LocalizationUtils.js';
import moment from 'moment';
import { CLOUD_FRONT_URL, APP_ENV, API_ENDPOINT, APP_NAME } from '../redux/config';
import SentryHandler from "../sentry";
import Constant, { APP_ENVIRONMENTS } from '../constants/Constant.js';
import analytics from '@react-native-firebase/analytics';

const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

export const handleResponse = (resp) => {
    const retResp = {
        success: false,
        data: null,
        error: [t('error.defaultError')]
    };

    if (_.get(resp, 'data.errors')) {
        retResp.error = _.get(resp, 'data.errors');
    } else if (_.get(resp, 'data.data.error')) {
        retResp.error = _.get(resp, 'data.data.error');
    } else {
        retResp.success = true;
        retResp.data = _.get(resp, 'data.data');
        retResp.error = null;
    }

    return retResp;
};

export const tokenString = resp => {
    if (!resp) {
        return;
    }

    const paramList = resp.split('?')[1].split('&');
    const myObj = {};
    paramList.map((obj) => {
        const splitObj = obj.split('=');
        myObj[splitObj[0]] = splitObj[1];
    });
    return myObj;
};

export const tokenData = token => {
    if (!token) {
        return;
    }
    const splitStr = token.split('.');

    if (splitStr.length && splitStr[1]) {
        const decodeStr = splitStr[1];
        return checkIfJson(atob(decodeStr));
    }
};

export const updateDataForObj = (srcObj, tarObj) => {
    if (
        typeof srcObj !== 'object'
        || typeof srcObj.length === 'number'
        || (typeof tarObj !== 'object' || typeof tarObj.length === 'number')
    ) {
        SentryHandler.log('Needs a valid JSON');
    }

    Object.keys(tarObj).forEach((eachKey) => {
        if (typeof tarObj[eachKey] !== 'undefined') {
            if (typeof tarObj[eachKey] === 'object' && typeof tarObj[eachKey] !== 'number') {
                tarObj[eachKey] = updateDataForObj(srcObj[eachKey], tarObj[eachKey]);
            } else {
                tarObj[eachKey] = srcObj[eachKey];
            }
        }
    });

    return tarObj;
};

// TODO: Add in more check for more variants.
export const FormatAddress = (blockNo, floorNo, unitNo, streetName, postalCode) => `Blk ${blockNo} ${streetName} ${floorNo} - ${unitNo} Singapore ${postalCode}`;

export const errorResponse = (data = {}) => {
    if (!data.success) {
        const error = data.error;
        const errResp = error && error[0] ? error[0] : '';
        SentryHandler.log(errResp);
        return errResp;
    }
}

export const handleErrorDataFromApi = (data, errorResponse) => {
    const errorMsg = _.get(data, 'error[0].message') || _.get(data, 'error[0]');
    const code = _.get(data, 'error[0].extensions.errorCode', '') || _.get(data, 'error[0].extensions.code', '');
    let translatedErr = '';
    let sentryLog = errorMsg;
    // Match error for translation. Supports match by code / match by label if code is not present
    if (code !== '') {
        translatedErr = t(`error.errorMessage_${code}`, { defaultValue: false });
        sentryLog = `error.errorMessage_${code}: ${errorMsg}` // log error with error code
    } else {
        // create error label needed to be mapped with translation file.
        const errorLabel = `GraphQL error: ${errorMsg}`;
        sentryLog = `error.errorMessage: ${errorLabel}`; // log error with error label
        translatedErr = TranslateAndLogErrorByLabel(errorLabel);
    }
    //  log the error
    SentryHandler.log(sentryLog);
    if (translatedErr) {
        const errData = Object.assign({}, data, { error: [translatedErr] });
        return errorResponse(errData);
    }

    if (errorMsg) {
        const errData = Object.assign({}, data, { error: [data.error[0].message] });
        return errorResponse(errData);
    } else {
        return errorResponse(data);
    }
};

const TranslateAndLogErrorByLabel = (error = '') => {
    const translatedErr = t(`error.${error}`, { defaultValue: false });
    if (!translatedErr) {
        return _.upperFirst(_.toLower(error.replace(/GraphQL error./, '').trim()));
    }
    return translatedErr;
};

export const storeAsyncStorage = async (key, value) => {
    try {
        const jsonValue = JSON.stringify(value)
        await AsyncStorage.setItem(key, jsonValue);

        return null;
    } catch (error) {
        // Error saving data

        return null;
    }
};

export const retrieveAsyncStorage = async (key) => {
    try {
        let jsonValue = await AsyncStorage.getItem(key);
        return jsonValue != null ? checkIfJson(jsonValue) : null;
    } catch (error) {
        // Error retrieving data
        return null;
    }
};

export const removeItemValue = async (key) => {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch (exception) {
        return false;
    }
};

/* Throw everything so sentry can retrieve it */
const makeMessage = message => `${APP_ENV} - ${message}`;

/*
  Clear All will clear all keys and if we need some keys to be restored, please refer to the logic on parent app and staff app.
*/
export const clearAll = async () => {
    try {
        /** Clear async storage safely.
         * Fix for https://sentry.io/organizations/ntuc-enterprise/issues/1754993344
         * When AsyncStorage is empty AsyncStorage.clear() throws
         * "Failed to delete storage directory... " exception on iOS
         * at the same time
         * AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove) throws exception on Android but not on iOS.
         *So to avoid errors from both platforms we use the below approach.
         */

        const asyncStorageKeys = await AsyncStorage.getAllKeys();
        if (asyncStorageKeys.length > 0) {
            if (Platform.OS === 'ios') {
                await AsyncStorage.multiRemove(asyncStorageKeys);
            } else {
                await AsyncStorage.clear();
            }
        }
    } catch (e) {
        SentryHandler.log(e.message);
    }
};


export const setUserAgent = async () => {
    const userAgent = await DeviceInfo.getUserAgent();
    await storeAsyncStorage('userAgent', userAgent);

};

export const getUserAgent = async () => await retrieveAsyncStorage('userAgent');

export const mobileGoogleValidation = (number, countryCode) => {
    let valid = true;
    const errors = {};
    const mobileNumberCheck = /^[0-9]+$/;
    const MIN_LENGTH_FOR_NUMBER = 2;
    if (number.length < MIN_LENGTH_FOR_NUMBER) {
        valid = false;
        errors.mobile = t('error.kioskMobileNoValidationError');
        return { valid, errors };
    }
    if (number && countryCode) {
        if (!mobileNumberCheck.test(number)) {
            valid = false;
            errors.mobile = t('error.kioskMobileNoValidationError');
            return { valid, errors };
        }
        const validateNumber = phoneUtil.parseAndKeepRawInput(number, countryCode);
        const validStatus = phoneUtil.isValidNumberForRegion(
            validateNumber,
            countryCode
        );
        if (!validStatus) {
            valid = false;
            errors.mobile = t('error.kioskMobileNoValidationError');
        }
    }
    return { valid, errors };
};

export const checkIfSingleChild = (childDatas = [], thisProps, userId, passcode) => {
    const childDetails = childDatas.map(childData => {
        const { child = {} } = childData;
        return {
            id: child.ID,
            name: getFullName(child.firstname, child.lastname),
            childImage: child.imageKey ? `${CLOUD_FRONT_URL}/${child.imageKey}` : require('../images/default_profile.png'),
            isImageFromURI: child.imageKey ? true : false,
            nricNo: child.birthCertificate,
            dateOfBirth: child.dateOfBirth ? moment(child.dateOfBirth).format('DD/MM/YYYY') : ''
        };
    });
    if (childDatas.length === 1) {
        const selected = _.get(childDetails, '[0].id');
        onSignInSignOut(childDetails, thisProps, [selected], userId, passcode);
    }
    return childDetails;
}

export const onSignInSignOut = async (childDetails, thisProps, selected = [], userId, passcode) => {
    const { navigation, childCheckInOutV2, getCheckInOutToken, showCustomAlert } = thisProps;
    const isSignIn = navigation.getParam('isSignIn', false);
    const accessToken = await retrieveAsyncStorage('accessToken');
    let fkCentre = await retrieveAsyncStorage('centreId');
    fkCentre = fkCentre ? parseInt(fkCentre) : 0;
    if (selected.length == 0 || !accessToken || !fkCentre || !thisProps) {
        return;
    }
    const checkInOutToken = await getCheckInOutToken({ IDCentre: fkCentre }, accessToken);
    const authToken = _.get(checkInOutToken, 'data.getCheckInOutToken', null);

    const type = isSignIn ? 'check_in' : 'check_out'
    const reqData = { type, childIds: selected, token: authToken };
    if (passcode) {
        reqData['passCode'] = passcode;
    } else {
        reqData['userId'] = userId;
    }

    childCheckInOutV2(reqData, accessToken).then(data => {
        if (data.success) {
            const successes = _.get(data, 'data.childCheckInOutV2.successes', []);
            const failures = _.get(data, 'data.childCheckInOutV2.failures', []);
            if (failures.length > 0) {
                const retryIds = failures.map(fail => fail.childId);
                showCustomAlert(true, {
                    title: isSignIn ? t('error.unsuccessfulCheckin') : t('error.unsuccessfulCheckout'),
                    desc: t('common.recordUpdateFailedMessage'),
                    cancelButtonText: t('giro.tryAgain'),
                    primaryButtonText: t('common.gotIt'),
                    cancelButtonCallback: () => onSignInSignOut(childDetails, thisProps, retryIds, userId, passcode)
                });
                return;
            }
            navigation.navigate('SignedInSuccess', {
                isSignIn,
                childDetails,
                selected: successes
            });
            return;
        }
        const message = handleErrorDataFromApi(data, errorResponse);
        message && thisProps.updateErrorMessage(message, checkIfSingleChild())
    });
}

export const getChildName = (child) => {
    return _.get(child, 'firstname') ?
        `${_.get(child, 'firstname')} ${_.get(child, 'lastname') || ''}` :
        _.get(child, 'lastname', '');
}

export const getFullName = (firstname = '', lastname = '') => {
    if (firstname === '' || null) {
        return lastname;
    }

    return `${firstname} ${lastname}`;
}

export const getAppEnv = () => {
    let appEnv = '';
    if (APP_ENV === APP_ENVIRONMENTS.dev) {
        const devX = API_ENDPOINT.substring(
            API_ENDPOINT.lastIndexOf("//") + 2,
            API_ENDPOINT.lastIndexOf(".a")
        );
        appEnv = devX;
    }
    if (APP_ENV === APP_ENVIRONMENTS.stg) {
        appEnv = APP_ENV;
    }
    return appEnv;
};

export const hasMarkedAllRecords = (startData, endDate, records = []) => {
    let start = startData;
    let isCompleted = true;
    while (moment(start).isSameOrBefore(endDate)) {
        const checkInData = records.find(record => {
            const date = moment(_.get(record, 'date')).format('YYYY-MM-DD');
            if (moment(date).isSame(start) && record.type === Constant.TYPE_CHECK_IN) {
                return record;
            }
            return;
        });
        const checkOutData = records.find(record => {
            const date = moment(_.get(record, 'date')).format('YYYY-MM-DD');
            if (moment(date).isSame(start) && record.type === Constant.TYPE_CHECK_OUT) {
                return record;
            }
            return;
        });
        const day = moment(start).format('ddd');
        start = moment(start).add(1, 'days').format('YYYY-MM-DD');
        if ((!checkInData || !checkOutData) && day !== 'Sun') {
            isCompleted = false;
            break;
        }
    }
    return isCompleted;
};

export const childrenByCentreAndWithdrawn = async (childLists = []) => {
    const fkCentre = await retrieveAsyncStorage('centreId');
    return childLists.filter(ch => _.get(ch, 'childLevelAt.centre.ID') === fkCentre);
}

export const maskBirthCertificate = (childBC, stringOffset) => {
    if (!childBC || childBC.length <= stringOffset) return childBC;
    return (
        new Array(childBC.length - (stringOffset + 1)).join('•') +
        childBC.slice(-stringOffset)
    );
};

export const checkIfJson = string => {
    try {
        const parsedData = JSON.parse(string);
        return parsedData;
    } catch (e) {
        return false;
    }
};

/**
 * 
 * @param {*} string 
 * @param {*} limit 
 */
export const checkIfLimitExceeds = (string = '', limit = 50) => {
    return string.length >= limit;
}

export const firebaseLogEvent = async (status, userId, error = "") => {
    const deveiceName = await DeviceInfo.getDeviceName();
    const appVersion = DeviceInfo.getVersion();
    await analytics().logEvent('auto_login', {
        status,
        appName: APP_NAME,
        userId,
        deveiceName,
        appVersion,
        error,
    });
}