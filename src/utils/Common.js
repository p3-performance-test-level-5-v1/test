import moment from 'moment';
import { TIME_FORMAT } from '../constants/Constant';

export const getDatesInRange = (start, end, isExceptSunday = false, holidays = []) => {
  const dates = [];
  const startDate = moment(start);
  const endDate = moment(end);
  while (!startDate.isAfter(endDate)) {
    let hasAddDateInList = true;

    hasAddDateInList = !(isExceptSunday && startDate.day() === 0);

    let isInHoliday = false;
    for (const holiday of holidays) {
      const from = moment(get(holiday, 'from'), TIME_FORMAT.DATA_TRANFERS);
      const to = moment(get(holiday, 'to'), TIME_FORMAT.DATA_TRANFERS);

      const date = moment(startDate.valueOf()).add(1, 's');

      isInHoliday = date.isBetween(from, to);
      if (isInHoliday) {
        break;
      }
    }

    if (hasAddDateInList && !isInHoliday) dates.push(startDate.valueOf());

    startDate.add(1, 'd');
  }

  return dates;
};
