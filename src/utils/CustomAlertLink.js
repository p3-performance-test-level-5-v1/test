import { connect } from 'react-redux';
import CustomAlert from '../components/CustomAlert/CustomAlert';
import { showCustomAlert } from '../redux/actions/CustomAlertAction';

const mapStateToProps = state => ({
  isVisible: state.customAlert.isVisible,
  customAlertParam: state.customAlert.customAlertParam,
});

const mapDispatchToProps = dispatch => ({
  showCustomAlert: (isVisible, customAlertParam) => {
    dispatch(showCustomAlert(isVisible, customAlertParam));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomAlert);
