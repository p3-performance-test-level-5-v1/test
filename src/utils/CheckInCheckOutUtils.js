import { get, isEmpty, last } from 'lodash';
import moment from 'moment';
import Constant from '../constants/Constant';

export const isDisableChild = (checkInOuts = [], isCheckIn = true) => {
	const filteredData = checkInOuts.filter((d) =>
		moment().isSame(moment(get(d, 'date')), 'd')
	);

	if (isEmpty(filteredData)) return false;
	const checkInList = filteredData.filter(
		(ch) => ch.type === Constant.CHECKIN && ch.status === Constant.VERIFIED
	);
	const checkOutList = filteredData.filter(
		(ch) => ch.type === Constant.CHECKOUT && ch.status === Constant.VERIFIED
	);
	const checkedInTime = get(last(checkInList), 'time', '');
	const checkedOutTime = get(last(checkOutList), 'time', '');

	if (!isCheckIn) return !isEmpty(checkedOutTime);
	return !isEmpty(checkedInTime);
};
