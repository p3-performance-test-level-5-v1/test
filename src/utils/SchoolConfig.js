import { get, isEqual } from 'lodash';
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
import Constant from '../constants/Constant';
import SentryHandler from '../sentry';
import { retrieveAsyncStorage, storeAsyncStorage } from '../utils';
import { t } from '../utils/LocalizationUtils';
import { SCHOOL } from '../redux/config/index';

export const getSchoolId = () => {
  /* Value based on `school` table. Hardcoded so we don't need to fetch twice in login page */
  if (SCHOOL.includes(Constant.MFS)) return 1;
  else if (SCHOOL.includes(Constant.LSH)) return 2;
  return 3; /* TCC */
};

export const getValueByKey = async key => {
  const schoolConfig = await retrieveAsyncStorage('schoolConfig');
  const schoolConfigData = get(schoolConfig, 'data', []);
  if (schoolConfigData.length === 0) {
    SentryHandler.log('App tried accessing school config but found empty.');
    return '';
  }
  const valueObj = get(schoolConfig, 'data', []).find(config => key === config.key);
  if (!valueObj) {
    SentryHandler.log(`App tried accessing key: ${key} and could not find`);
    return '';
  }
  return valueObj.value;
};

export const storeSchoolConfig = async getSchoolConfig => {
  try {
    /* We don't have school ID on login page */
    let schoolId = await retrieveAsyncStorage('schoolId');
    if (!schoolId) {
      schoolId = getSchoolId();
    }

    const reqData = {
      filter: {
        fkSchool: parseInt(schoolId)
      }
    };
    const schoolConfig = await getSchoolConfig(reqData);
    if (get(schoolConfig, 'success')) {
      const schoolConfigData = get(schoolConfig, 'data.findAllSchoolConfig', {});
      const canFindUserByPhone = get(schoolConfigData, 'data', []).find(school => (
        school.key === Constant.SHOULD_FIND_USER_BY_PHONE
      )) || false;
      await storeAsyncStorage('schoolConfig', schoolConfigData);
      await storeAsyncStorage('canFindUserByPhone', canFindUserByPhone);
    }
  } catch (e) {
    SentryHandler.log(e.message);
  }
};
