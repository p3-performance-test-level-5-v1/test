import { StyleSheet, Dimensions } from 'react-native';
import CustomConfig from '../utils/CustomConfig';
const { width: deviceWidth } = Dimensions.get('window');

export const commonStyle = StyleSheet.create({
  get headerStyle() {
    return { 
      flex: 0.07,
      width: deviceWidth,
      backgroundColor: CustomConfig.Colors.primaryColor,
    }
  },
});