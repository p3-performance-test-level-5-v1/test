import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from './baseStyles';
const { width: deviceWidth } = Dimensions.get('window');

export const buttonCommonStyle = StyleSheet.create({
  butttonStyle: {
    borderRadius: baseStyles.borderRadius,
    marginBottom: baseStyles.margin20,
  },
  buttonTextStyle: {
    fontSize: baseStyles.buttonFontSize,
		fontFamily: baseStyles.latoBold,
  },
  commonStyle: {
    height: baseStyles.btnHeight,
    width: deviceWidth * 0.4,
		alignItems: 'center',
		justifyContent: 'center',
  }
});