import { Dimensions } from 'react-native';
import { scaledWidth, scaledHeight, scaledFont } from '../utils/Scale';
import { SCHOOL } from '../config/config';
const { height: deviceHeight } = Dimensions.get('window');

let primaryColor = '#f15a22';
let secondaryColor = '#e8e8e8';
let btnSecondaryBgColor = 'white';
let btnPrimaryBgColor = '#f15a22';
let btnPrimaryBgColorDisabled = 'rgba(241, 90, 34, 0.5)';
let primaryTextColor = '#f15a22';
let secondaryTextColor = 'white';

if (SCHOOL === 'TCC') {
    primaryColor = '#f9a350';
    btnPrimaryBgColor = '#f9a350';
    primaryTextColor = '#f9a350';
} else if (SCHOOL === 'LSH') {
    primaryColor = '#56c4c5';
    btnPrimaryBgColor = '#56c4c5';
    primaryTextColor = '#56c4c5';
} else if (SCHOOL === 'MFS') {
    primaryColor = '#ec6608';
    btnPrimaryBgColor = '#ec6608';
    primaryTextColor = '#ec6608';
}

export const appTheme = {
    primaryColor: primaryColor,
    secondaryColor: secondaryColor,
    btnPrimaryBgColor: btnPrimaryBgColor,
    btnPrimaryBgColorDisabled: btnPrimaryBgColorDisabled,
    btnSecondaryBgColor: btnSecondaryBgColor,
    primaryTextColor: primaryTextColor,
    secondaryTextColor: secondaryTextColor,
};

export const styles = {
    primaryColor: primaryColor,
    secondaryColor: secondaryColor,
    btnPrimaryBgColorDisabled: btnPrimaryBgColorDisabled,
    primaryTextColor: primaryTextColor,
    secondaryTextColor: secondaryTextColor,
    btnPrimaryBgColor: btnPrimaryBgColor,
    btnSecondaryBgColor: btnSecondaryBgColor,

    baseWidth: scaledWidth(320),

    forgetViewHeight: scaledHeight(60),
    inputViewHeight: scaledHeight(80),
    textareaViewHeight: scaledHeight(90),
    buttonViewHeight: scaledHeight(100),

    labelColor: '#717171',

    labelFontSize: scaledFont(16),
    inputFontSize: scaledFont(18),
    buttonFontSize: scaledFont(20),
    buttonLargeFontSize: scaledFont(26),
    labelFontSize24: scaledFont(24),
    labelFontSize22: scaledFont(22),
    labelFontSize20: scaledFont(20),
    labelFontSize16: scaledFont(16),
    latoRegular: 'Lato-Regular',
    latoSemibold: 'Lato-Semibold',
    latoRobotoMedium: 'Roboto-Medium',
    latoRobotoRegular: 'Roboto-Regular',
    latoBlack: 'Lato-Black',
    latoBold: 'Lato-Bold',
    eclipseColor: '#363636',
    textInputFont: scaledFont(18),
    textInputFont16: scaledFont(16),
    dropdownColor: '#4a4a4a',
    lightGreyColor: '#cccccc',
    defaultBorderRadius: 5,

    //padding
    padding8: 8,
    padding15: 15,
    padding10: 10,
    padding20: 20,
    padding25: 25,
    padding30: 30,
    padding50: 50,
    padding5: 5,
    padding25: 25,
    defaultPaddingWidth: 16,

    //margin
    margin5: 5,
    margin8: 8,
    margin10: 10,
    margin15: 15,
    margin20: 20,
    margin25: 25,
    margin30: 30,
    margin40: 40,
    margin50: 50,

    borderColor: '#d8d8d8',
    titleColor: 'rgb(19, 19, 21)',
    inputFontSizeSmall: scaledFont(14),
    titleSecondaryColor: 'rgba(19, 19, 21, 0.5)',
    bgColor: 'white',
    descriptionColor: 'rgba(0, 0, 0, 0.5)',
    labelSecondaryFontSize: scaledFont(12),
    inputLabelColor: 'rgba(0, 0, 0, 0.6)',
    inputLabelHeight: 20,
    inputPlaceHeight: 60,
    lineHeightMedium: 24,

    dimGray: '#666666',
    textGreyColor: 'grey',
    black: 'black',
    darkShadeGray: '#303030',
    darkGrayTintColor: '#A8A8A8',
    grayishBlue: '#38576B',
    avatharTextColor: '#6c6c6c',
    parrotGreen: '#12B886',
    lightGrey: '#cfcfcf',
    lightGray: '#bababa',
    darkCharcoalBlack: '#334d5e',
    platinumColor: '#e4e4e4',

    //Bus Attendance
    greyColor: '#828282',
    h3FontSize: 18,
    lightGreen: 'rgba(117, 192, 68, 0.2)',
    white: '#ffffff',
    darkGray: '#acacac',
    buttonSize30: 30,
    buttonSize20: 20,
    listContainerMedium: deviceHeight * 0.4,
    listContainerLarge: deviceHeight * 0.6,
    btnHeight: 60,
    darkRed: '#c80000',
    statusGrey: '#e7ac08',
    disabledOpacity: 0.4,

    //Border Radius 
    borderRadius: 6,

    // Bus Management
    placeHolderColor: '#dadada',
    errorColor: '#ec3535'
};

export const baseStyles = Object.assign({}, styles, appTheme);
