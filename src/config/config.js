export const headerConfig = {
	MFS: {
		color: '#ffffff',
		image: require('../images/icon-close.png')
	},
	LSH: {
		color: '#ffffff',
		image: require('../images/icon-close-lsh-white.png')
	},
	TCC: {
		color: '#ffffff',
		image: require('../images/icon-close-tcc.png')
	}
}
export const layoutConfig = {
	statusBar: {
		height: 20
	}
}
export const radioButtonColor = {
	MFS: '#f15a22',
	LSH: '#6db33f',
	TCC: '#75c044'
}

export const checkImage = {
	MFS: require('../images/check.png'),
	TCC: require('../images/icon-check-tcc.png'),
	LSH: require('../images/check.png'),
}

export const DEFAULT_LANGUAGE = 'en';

export const AVATAR_COLORS = [
	'#00AA55',
	'#009FD4',
	'#B381B3',
	'#939393',
	'#E3BC00',
	'#D47500',
	'#DC2A2A',
];

export const visitorHeaderImage = {
	MFS: require('../images/mfs-visitor-logo.png'),
	TCC: require('../images/tcc-visitor-Logo.png'),
	LSH: require('../images/lsh-visitor-Logo.png'),
};