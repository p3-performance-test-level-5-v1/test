import common from "./common.json";
import event from "./event.json";
import message from "./message.json";
import photoAlbum from "./photoAlbum.json";
import accountAndSettings from "./accountAndSettings.json";
import statementOfAccounts from "./statementOfAccounts.json";
import health from "./health.json";
import leave from "./leave.json";
import transferAndWithdraw from "./transferAndWithdraw.json";
import error from "./error.json";
import giro from "./giro.json";

export default {
  ...common,
  ...event,
  ...message,
  ...photoAlbum,
  ...accountAndSettings,
  ...statementOfAccounts,
  ...health,
  ...leave,
  ...transferAndWithdraw,
  ...error,
  ...giro,
};
