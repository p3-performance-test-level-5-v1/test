import React from "react";
import {
  createAppContainer,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import axios from 'axios';
import sha1 from 'sha1';
import * as Sentry from '@sentry/react-native';
import Routes from "./navigations/Routes";
import analytics from '@react-native-firebase/analytics';
import ErrorAlert from './components/ErrorAlert';
import ForceUpdateModal from "./components/ForceUpdateModal/ForceUpdateModal.view";
import {setUserAgent} from "./utils";
import CustomAlert from "./utils/CustomAlertLink";
import DeviceInfo from 'react-native-device-info';
import * as RNLocalize from 'react-native-localize';
import { LocalisationUtils } from './utils/LocalizationUtils';
import moment from "moment";
import { ApolloStore } from './redux/api/BaseRequest';
import { APP_NAME, HEARTBIT_URL } from './redux/config/index';
import SentryHandler from './sentry';
import { connect } from 'react-redux';
import { getSchoolConfig } from './redux/actions';
import CustomConfig from "./utils/CustomConfig";
import { storeSchoolConfig } from './utils/SchoolConfig';

if(__DEV__) {
  import('./config/ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

console.disableYellowBox = true;
/* Set additional headers needed for the application here. */
ApolloStore.setAdditionalHeader('User-Agent', DeviceInfo.getUserAgent());
ApolloStore.setAdditionalHeader('X-Source', APP_NAME);
ApolloStore.setAdditionalHeader('X-App', DeviceInfo.getBundleId());

const getActiveRouteName = navigationState => {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    if (route.routes) {
        return getActiveRouteName(route);
    }
    return route.routeName;
};

const kioskAppRoutes = createStackNavigator(
  Routes,
  {
    initialRouteName: 'AuthValidatorRoute',
    headerMode: "screen",
    mode: "card",
    navigationOptions: {
      cardStack: {
        gesturesEnabled: true
      },
      gesturesEnabled: true
    },
    gesturesEnabled: true,
    defaultNavigationOptions: () => ({
      cardStyle: {
          backgroundColor: 'white',
      },
    }),
  }
);

const KioskAppContainer = createAppContainer(kioskAppRoutes);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.localisationUtils = new LocalisationUtils();
    this.sendDeviceStatus = this.sendDeviceStatus.bind(this)
    SentryHandler.init();
  }
  async componentWillMount () {
    const { getSchoolConfig } = this.props;
    await storeSchoolConfig(getSchoolConfig);
		await CustomConfig.fetchTheme();
    await setUserAgent();
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
    this.triggerheartBeats();
  }

  async triggerheartBeats() {
    const appName = await DeviceInfo.getApplicationName();
    const buildId = await DeviceInfo.getBuildId();
    const buildNumber = await DeviceInfo.getBuildNumber();
    const deviceId = await DeviceInfo.getDeviceId();
    const deviceType = await DeviceInfo.getDeviceType();
    const deviceName = await DeviceInfo.getDeviceName();
    const model = await DeviceInfo.getModel();
    const systemName = await DeviceInfo.getSystemName();
    const systemVersion  = await DeviceInfo.getSystemVersion();
    const version  = await DeviceInfo.getVersion();
    //Sending events on every 5 seconds
    setInterval(() => {
      analytics().logEvent('heartbeat',{
        appName,
        buildId,
        deviceId,
        buildNumber,
        deviceType,
        deviceName,
        model,
        systemName,
        systemVersion,
        sentAt:moment().format('YY-MM-DD HH:mm:ss'),
        version
      });
    },5000);
    //Sending status to heartbit API every 1 minute
    setInterval(() => {
     this.sendDeviceStatus(deviceName, model, systemVersion, version);
    }, 60000)
  }

  async sendDeviceStatus(deviceName, model, systemVersion, version) {
    const config = {
      method: 'POST',
      url: HEARTBIT_URL, // will be updated once the backend PR is merged and deployed
      params: {
        device_name: deviceName,
        device_model: model,
        last_sync_time: moment().format('YY-MM-DD HH:mm:ss'),
        app_version: version,
        device_version: systemVersion,
        key: sha1("heartbit-device-update")
      }
    };
    try {
      await axios(config);
    } catch (error) {
      console.error(error)
      SentryHandler.log(error, Sentry.Severity.Error);
    }
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    this.localisationUtils.init();
    this.forceUpdate();
  };
  render() {
    return (
      <React.Fragment>
        <KioskAppContainer
          onNavigationStateChange={(prevState, currentState, action) => {
            const currentScreen = getActiveRouteName(currentState);
            const prevScreen = getActiveRouteName(prevState);
            if (prevScreen !== currentScreen) {
              analytics().setCurrentScreen(currentScreen.toLowerCase());
              SentryHandler.setAdditionalTags(
                { currentScreen: currentScreen.toLowerCase() }
              );
            }
          }}
        />
        <CustomAlert/>
        <ForceUpdateModal/>
        <ErrorAlert/>
      </React.Fragment>
    )
  }
}

const mapDispatchToProps = { getSchoolConfig };
export default connect(null, mapDispatchToProps)(App);
