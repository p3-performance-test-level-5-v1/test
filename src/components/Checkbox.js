import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { baseStyles } from '../config/baseStyles';

/**
 * ### ExampleStart ###
 *
 *  <Checkbox
 *    label="test"
 *    iconSize={18}
 *    isChecked={true}
 *    textStyle={{}}
 *    iconColor={"blue"}
 *  />
 *
 * ### ExampleEnd ###
 */
export default class Checkbox extends React.Component {
  handleOnPress() {
    const { onChecked, item } = this.props;

    if (onChecked) {
      onChecked(item);
    }
  }

  render() {
    const { isChecked, label, textStyle, iconSize, iconColor } = this.props;
    return (
      <View>
        <TouchableWithoutFeedback onPress={() => this.handleOnPress()}>
          <View style={styles.buttonContainer}>
            <View style={[styles.iconWrapperStyle, { minHeight: iconSize, minWidth: iconSize }]}>
              <Icon
                name={isChecked ? 'md-checkbox' : 'ios-square-outline'}
                size={iconSize}
                color={isChecked ? iconColor : baseStyles.darkGrayTintColor}
              />
            </View>
            <View style={[styles.textWrapperStyle, { minHeight: iconSize }]}>
              <Text style={{ ...textStyle, width: '100%' }}>{label}</Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

Checkbox.propTypes = {
  isChecked: PropTypes.bool,
  label: PropTypes.string,
  textStyle: PropTypes.object,
  iconSize: PropTypes.number,
  onChecked: PropTypes.func,
};

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
  },
  iconWrapperStyle: {
    flexDirection: 'row',
  },
  textWrapperStyle: {
    flex: 1,
    flexWrap: 'wrap',
    marginLeft: baseStyles.margin5,
    justifyContent: 'center',
  },
});
