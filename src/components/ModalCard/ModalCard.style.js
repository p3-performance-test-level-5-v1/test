import {
  StyleSheet,
  Dimensions
} from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';

const {
  height: deviceHeight
} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
	  backgroundColor: 'rgba(128,128,128, 0.9)'
  },
  modalContent: {
    width: 400,
    height: 170,
    backgroundColor: 'white',
    paddingLeft: 25,
    paddingRight: 25,
    borderWidth: 1,
    justifyContent: 'center',
    borderColor: baseStyles.lightGreyColor
  },
  modalDescription: {
    marginVertical: 20
  },
  actionButton: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 10
  },
  cancelText: {
    color: '#a1a1a1'
  },
  get confirmText() {
    return {
      color: CustomConfig.Colors.btnPrimaryBgColor
    };
  }
});
