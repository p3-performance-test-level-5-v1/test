import React from 'react';
import {
    View,
    Text,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import styles from './ModalCard.style';
import { t } from '../../utils/LocalizationUtils.js';

class ModalCard extends React.Component {
  render() {
    const {
        isVisible,
        title,
        desc,
        onConfirm,
        hideCancelButton,
        onClickContainer,
        onCancel
    } = this.props;
    return (
        <Modal
          animationType="fade"
          transparent={true}
          visible={isVisible} 
          onRequestClose={() => { onCancel() }}
        >
          <TouchableWithoutFeedback
            onPress={() => { onClickContainer() }}
          >
            <View style={styles.container}>
              <TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                  <Text h3>{title}</Text>
                  <Text numberOfLines={3} style={styles.modalDescription}>{desc}</Text>
                  <View style={styles.actionButton}>
                    { !hideCancelButton && (
                      <TouchableOpacity
                        onPress={() => { onCancel() }}
                      >
                        <Text style={styles.cancelText}>{t('common.cancel')}</Text>
                      </TouchableOpacity>
                    )}
                    <TouchableOpacity
                      onPress={() => onConfirm()}
                      style={{ marginLeft: 30 }}
                    >
                      <Text style={styles.confirmText}>{t('common.confirmation')}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
    )
  }
}



export default ModalCard;
