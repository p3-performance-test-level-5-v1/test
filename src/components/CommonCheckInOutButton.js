import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { 
	scaledFont, scaledHeight 
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import { buttonCommonStyle } from '../config/buttonStyle';
import CustomConfig from '../utils/CustomConfig';
import withPreventDoubleClick from './TouchableButton';
const TouchableButton = withPreventDoubleClick(TouchableOpacity);

export default CommonCheckInOutButton = ({
    navigateAction = () => {},
    buttonStyle = {},
    disabled = false,
    label = '',
    secondaryLabel = '',
    image
}) => (
    <TouchableButton 
        onPress={navigateAction}
        disabled={disabled} 
        style={{
            opacity: disabled ? baseStyles.disabledOpacity : 1,
            ...styles.buttonStyle, 
            ...styles.primaryButton,
            ...buttonStyle
        }}
    >
        <View style={styles.buttonContainerText}>
            {image && (
                <View style={styles.buttonContainerIcon}>
                    <Image
                        resizeMode='contain'
                        style={styles.checkInOutLogo}
                        source={image}
                    />
                </View>
            )}
            <View style={styles.buttonTextContainer}>
                <View style={styles.columnFlex}>
                    <Text style={styles.buttonCheckInOutTxt}>{label}</Text>
                    {secondaryLabel ? (
                        <Text style={styles.buttonCheckInOutTxt}>
                            {secondaryLabel}
                        </Text>
                    ) : null}
                </View>
            </View>
        </View>
  </TouchableButton>
);


const styles = StyleSheet.create({
    buttonStyle: {
        ...buttonCommonStyle.commonStyle,
        ...buttonCommonStyle.butttonStyle,
        borderRadius: 10,
        height: scaledHeight(145),
        paddingHorizontal: baseStyles.padding15
    },
    get primaryButton() {
        return {
          backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
          borderColor: CustomConfig.Colors.btnPrimaryBgColor,
        };
    },
    buttonContainerText: {
        flexDirection: "row"
    },
    buttonContainerIcon: {
        flex: .5,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: baseStyles.padding5
    },
    checkInOutLogo: {
        margin: baseStyles.margin20
    },
    buttonTextContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    columnFlex: {
        flexDirection: 'column',
        justifyContent: 'center'
    },
    buttonCheckInOutTxt: {
        color: baseStyles.white,
        textAlign: 'center',
        fontSize: scaledFont(32),
        fontFamily: baseStyles.latoBold,
    },
  });