import React from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Text } from 'react-native';
import moment from 'moment';

import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';

const DAY_WIDTH = `${100 / 7}%`;
const DAY_HEIGHT = 36;

export default class Day extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { day } = this.props;
    if (nextProps.day.type === day.type) return false;

    return true;
  }

  render() {
    const { day, onSelectDate, isSameDate } = this.props;
    let dayStyle = {
      width: DAY_WIDTH,
    };
    let dayLiningStyle = {};
    let textDayStyle = styles.txtDayNormal;
    let isDisable = false;

    switch (day.type) {
      case 'single':
        textDayStyle = styles.txtDayActive;
        break;
      case 'first':
        dayLiningStyle = styles.dayLiningFirst;
        textDayStyle = styles.txtDayActive;
        break;
      case 'last':
        dayLiningStyle = isSameDate ? {} : styles.dayLiningLast;
        textDayStyle = styles.txtDayActive;
        break;
      case 'between':
        dayStyle = styles.dayBetween;
        break;
      case 'disabled':
        isDisable = true;
        textDayStyle = styles.txtDayDisabled;
        break;
      case 'blockout':
        isDisable = true;
        textDayStyle = styles.txtDayDisabled;
        break;
      case 'between_blockout':
        isDisable = true;
        dayStyle = styles.dayBetween;
        textDayStyle = styles.txtDayDisabled;
        break;

      default:
        break;
    }

    const isNull = !day.date;
    const isCurrent = day.date === moment().format('YYYYMMDD');

    return (
      <TouchableWithoutFeedback
        activeOpacity={1}
        style={dayStyle}
        disabled={isDisable || isNull}
        onPress={() => {
          if (!isNull) onSelectDate(moment(day.date, 'YYYYMMDD'));
        }}
      >
        <View style={[dayStyle, styles.day]}>
          <View style={[dayLiningStyle]} />
          <Text style={[textDayStyle, styles.txtDay, isNull && styles.hideText, isCurrent && styles.currentDate]}>
            {moment(day.date, 'YYYYMMDD').date()}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  hideText: {
    backgroundColor: 'transparent',
    color: 'transparent',
  },
  day: {
    height: DAY_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    width: DAY_WIDTH,
  },
  get dayBetween() {
    return {
      backgroundColor: `${CustomConfig.Colors.primaryColor}33`,
      width: DAY_WIDTH,
    };
  },
  get dayLiningFirst() {
    return {
      backgroundColor: `${CustomConfig.Colors.primaryColor}33`,
      position: 'absolute',
      top: 0,
      bottom: 0,
      width: DAY_HEIGHT * 0.75,
      right: 0,
    };
  },
  get dayLiningLast() {
    return {
      backgroundColor: `${CustomConfig.Colors.primaryColor}33`,
      position: 'absolute',
      top: 0,
      bottom: 0,
      width: DAY_HEIGHT * 0.75,
      left: 0,
    };
  },
  txtDayNormal: {
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoRegular,
    fontSize: baseStyles.labelFontSize,
  },
  txtDay: {
    textAlign: 'center',
    fontSize: baseStyles.labelFontSize,
  },
  get txtDayActive() {
    return {
      color: baseStyles.white,
      backgroundColor: CustomConfig.Colors.primaryColor,
      width: DAY_HEIGHT,
      lineHeight: DAY_HEIGHT,
      borderRadius: DAY_HEIGHT / 2,
      fontWeight: 'bold',
    };
  },
  txtDayDisabled: {
    color: baseStyles.platinumColor,
  },
  currentDate: {
    width: DAY_HEIGHT,
    lineHeight: DAY_HEIGHT,
    borderRadius: DAY_HEIGHT / 2,
    borderWidth: baseStyles.borderWidth,
    borderColor: baseStyles.lightGrey,
  },
});
