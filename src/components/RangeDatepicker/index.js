import DateRangePickerField from './DateRangePickerField';
import DateRangePicker from './DateRangePicker';

export { DateRangePicker };
export default DateRangePickerField;
