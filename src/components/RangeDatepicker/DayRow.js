import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { baseStyles } from '../../config/baseStyles';

import Day from './Day';

export default class DayRow extends React.Component {
  shouldComponentUpdate(nextProps) {
    const { days } = this.props;

    if (JSON.stringify(nextProps.days) === JSON.stringify(days)) return false;

    return true;
  }

  render() {
    const { days, onSelectDate, isSameDate } = this.props;

    return (
      <View style={styles.container}>
        {days.map((day, i) => {
          return <Day key={i.toString()} onSelectDate={onSelectDate} day={day} isSameDate={isSameDate} />;
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 2,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    flex: 1,
  },
});
