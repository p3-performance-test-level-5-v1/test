/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-deprecated */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';
import moment from 'moment';
import Month from './Month';

export default class DateRangePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: props.startDate && moment(props.startDate, 'YYYYMMDD'),
      untilDate: props.untilDate && moment(props.untilDate, 'YYYYMMDD'),
      availableDates: props.availableDates || null,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ availableDates: nextProps.availableDates });
  }

  onSelectDate = (date) => {
    let startDate = null;
    let untilDate = null;
    const { onSelect } = this.props;
    const { startDate: startDateState, untilDate: untilDateState } = this.state;

    if (startDateState && !untilDateState) {
      if (date.format('YYYYMMDD') < startDateState.format('YYYYMMDD') || this.isInvalidRange(date)) {
        startDate = date;
      } else if (date.format('YYYYMMDD') > startDateState.format('YYYYMMDD')) {
        startDate = startDateState;
        untilDate = date;
      } else {
        startDate = null;
        untilDate = null;
      }
    } else if (!this.isInvalidRange(date)) {
      startDate = date;
    } else {
      startDate = null;
      untilDate = null;
    }

    this.setState({ startDate, untilDate });
    onSelect(startDate, untilDate);
  };

  onSelectUntilDate = (date) => {
    const { onSelect, startDate: startDateProp } = this.props;
    const { startDate: startDateState } = this.state;

    const startDate = startDateProp ? moment(startDateProp, 'YYYYMMDD') : startDateState;

    this.setState({
      startDate,
      untilDate: date,
    });
    onSelect(startDate, date);
  };

  isInvalidRange = (date) => {
    const { startDate, untilDate, availableDates } = this.state;

    if (availableDates && availableDates.length > 0) {
      // select endDate condition
      if (startDate && !untilDate) {
        for (
          let i = startDate.format('YYYYMMDD');
          i <= date.format('YYYYMMDD');
          i = moment(i, 'YYYYMMDD').add(1, 'days').format('YYYYMMDD')
        ) {
          if (availableDates.indexOf(i) === -1 && startDate.format('YYYYMMDD') !== i) return true;
        }
      }
      // select startDate condition
      else if (availableDates.indexOf(date.format('YYYYMMDD')) === -1) return true;
    }

    return false;
  };

  getMonthStack() {
    const res = [];
    const { maxMonth, initialMonth, isHistorical } = this.props;
    let initMonth = moment();
    if (initialMonth && initialMonth !== '') initMonth = moment(initialMonth, 'YYYYMM');

    for (let i = 0; i < maxMonth; i++) {
      res.push(
        !isHistorical
          ? initMonth.clone().add(i, 'month').format('YYYYMM')
          : initMonth.clone().subtract(i, 'month').format('YYYYMM')
      );
    }

    return res;
  }

  handleConfirmDate = () => {
    const { startDate, untilDate } = this.state;
    const { onConfirm } = this.props;
    if (onConfirm) {
      onConfirm(startDate, untilDate);
    }
  };

  handleRenderRow = (month) => {
    const { ignoreMinDate, minDate, maxDate, isFixedStartDate } = this.props;
    let { availableDates } = this.state;
    const { startDate, untilDate } = this.state;

    if (availableDates && availableDates.length > 0) {
      availableDates = availableDates.filter(function (d) {
        if (d.indexOf(month) >= 0) return true;
      });
    }

    const isSameDate = startDate && untilDate && startDate.format('YYYYMMDD') === untilDate.format('YYYYMMDD');

    return (
      <Month
        onSelectDate={isFixedStartDate ? this.onSelectUntilDate : this.onSelectDate}
        startDate={startDate}
        untilDate={untilDate}
        availableDates={availableDates}
        minDate={minDate ? moment(minDate, 'YYYYMMDD') : minDate}
        maxDate={maxDate ? moment(maxDate, 'YYYYMMDD') : maxDate}
        ignoreMinDate={ignoreMinDate}
        month={month}
        isSameDate={isSameDate}
      />
    );
  };

  render() {
    const {
      infoText = 'Select Date Range',
      dayHeaderStyle,
      infoStyle,
      dayHeadings,
      showButton,
      buttonContainerStyle,
      onCancel,
    } = this.props;
    const { startDate, untilDate } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={infoStyle}>{infoText}</Text>

          <View style={baseStyles.selectedDayContainer}>
            <Text style={styles.selectedDay}>
              {`${startDate ? moment(startDate).format('MMM DD') : ''} ${untilDate ? '-' : ''} ${
                untilDate ? moment(untilDate).format('MMM DD') : ''
              }`}
            </Text>
          </View>
        </View>

        <View style={dayHeaderStyle}>
          {dayHeadings.map((day, i) => {
            return (
              <Text style={styles.dayHeading} key={i.toString()}>
                {day}
              </Text>
            );
          })}
        </View>
        <FlatList
          data={this.getMonthStack()}
          renderItem={({ item, index }) => {
            return this.handleRenderRow(item, index);
          }}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.timeContainer}
        />

        {showButton ? (
          <View style={[styles.buttonWrapper, buttonContainerStyle]}>
            <TouchableOpacity style={styles.btnCancel} onPress={onCancel}>
              <Text style={styles.txtCancel}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleConfirmDate}>
              <Text style={styles.txtOk}>OK</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: baseStyles.bgColor,
    zIndex: 1000,
    alignSelf: 'center',
    width: '100%',
    flex: 1,
  },
  get headerContainer() {
    return {
      paddingHorizontal: baseStyles.padding25,
      paddingVertical: baseStyles.padding15,
      backgroundColor: CustomConfig.Colors.primaryColor,
    }
  },
  dayHeader: {
    flexDirection: 'row',
    padding: baseStyles.padding10,
    justifyContent: 'space-between',
  },
  dayHeading: {
    width: `${100 / 7}%`,
    textAlign: 'center',
    fontSize: baseStyles.labelFontSize,
    fontFamily: baseStyles.latoBold,
    color: baseStyles.darkGrayTintColor,
  },
  selectedDayContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingBottom: baseStyles.padding5,
    alignItems: 'center',
  },
  selectedDay: {
    fontSize: 32,
    color: baseStyles.white,
    fontWeight: '600',
    fontFamily: baseStyles.latoRegular,
  },
  timeContainer: {
    paddingHorizontal: baseStyles.padding10,
  },
  buttonWrapper: {
    paddingVertical: baseStyles.padding20,
    paddingHorizontal: baseStyles.padding25,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  infoText: {
    color: baseStyles.white,
    fontSize: baseStyles.labelFontSize,
    marginBottom: baseStyles.margin30,
    lineHeight: 18,
    fontFamily: baseStyles.latoRegular,
  },
  btnCancel: {
    marginRight: baseStyles.margin30,
  },
  get txtCancel() {
    return {
      fontWeight: 'normal',
      color: CustomConfig.Colors.primaryColor,
      fontFamily: baseStyles.latoRegular,
    }
  },
  get txtOk() {
    return {
      color: CustomConfig.Colors.primaryColor,
      fontFamily: baseStyles.latoBold,
    }
  },
});

DateRangePicker.defaultProps = {
  initialMonth: '',
  dayHeadings: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
  maxMonth: 12,
  buttonContainerStyle: {},
  ignoreMinDate: false,
  isHistorical: false,
  onSelect: () => null,
  onConfirm: () => null,
  onCancel: () => null,
  startDate: '',
  untilDate: '',
  minDate: '',
  maxDate: '',
  infoText: 'Select Date Range',
  infoStyle: styles.infoText,
  showButton: true,
  availableDates: null,
  dayHeaderStyle: styles.dayHeader,
  isFixedStartDate: false,
};

DateRangePicker.propTypes = {
  initialMonth: PropTypes.string,
  dayHeadings: PropTypes.arrayOf(PropTypes.string),
  availableDates: PropTypes.arrayOf(PropTypes.string),
  maxMonth: PropTypes.number,
  buttonContainerStyle: PropTypes.object,
  startDate: PropTypes.string,
  untilDate: PropTypes.string,
  minDate: PropTypes.string,
  maxDate: PropTypes.string,
  ignoreMinDate: PropTypes.bool,
  isHistorical: PropTypes.bool,
  isFixedStartDate: PropTypes.bool,
  onSelect: PropTypes.func,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
  infoText: PropTypes.string,
  infoStyle: PropTypes.object,
  showButton: PropTypes.bool,
  dayHeaderStyle: PropTypes.object,
};
