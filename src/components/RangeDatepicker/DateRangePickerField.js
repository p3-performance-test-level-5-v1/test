/* eslint-disable react/jsx-props-no-spreading */
import React, { useRef, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Popover, { PopoverPlacement, Rect } from 'react-native-popover-view';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { baseStyles } from '../../config/baseStyles';
import DateRangePicker from './DateRangePicker';

const DateRangePickerField = (props) => {
  const [show, setShow] = useState(false);

  const showPicker = () => {
    setShow(true);
  };

  const hidePicker = () => {
    setShow(false);
  };

  const onSubmit = (startDate, untilDate) => {
    const { onConfirm } = props;
    hidePicker();
    if (onConfirm) {
      onConfirm(startDate, untilDate);
    }
  };

  const buttonRef = useRef();

  const {
    value,
    pickerDisable,
    labelText,
    baseColor,
    tintColor,
    placeHolderText,
    customPlaceholderTexColor,
    inputContainerStyle,
    inputTextStyle,
    containerStyle,
    calendarIconName = 'calendar',
    calendarIconColor = baseStyles.darkCharcoalBlack,
    onConfirm,
    ...rest
  } = props;

  return (
    <View style={[styles.containerStyle, containerStyle]}>
      <TouchableOpacity ref={buttonRef} onPress={showPicker}>
        <TextField
          label={labelText}
          lineWidth={1}
          baseColor={baseColor || baseStyles.inputLabelColor}
          tintColor={tintColor || baseStyles.inputLabelColor}
          labelHeight={baseStyles.primaryLabelHeight}
          style={[styles.dateTimeStyle, inputTextStyle]}
          editable={false}
          renderAccessory={() => <MaterialCommunityIcons name={calendarIconName} size={22} color={calendarIconColor} />}
          inputContainerStyle={[styles.pickerContainerStyle, inputContainerStyle]}
          placeholder={placeHolderText}
          placeholderTextColor={customPlaceholderTexColor}
          disabledLineType="solid"
          disabled={pickerDisable}
          value={value}
        />
      </TouchableOpacity>
      <Popover
        from={buttonRef}
        placement={PopoverPlacement.BOTTOM}
        displayArea={new Rect(160, 0, 400, 2000)}
        isVisible={show}
        onRequestClose={hidePicker}
        arrowStyle={styles.arrowStyle}
        backgroundStyle={styles.popoverOutsite}
        popoverStyle={[styles.popover]}
        animationConfig={{ duration: 200 }}
      >
        <View style={styles.rangeDatePickerContainer}>
          <DateRangePicker {...rest} onCancel={hidePicker} onConfirm={onSubmit} />
        </View>
      </Popover>
    </View>
  );
};

export default DateRangePickerField;

const styles = StyleSheet.create({
  containerStyle: {
    paddingTop: baseStyles.padding10,
  },
  dateTimeStyle: {
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize16,
    fontFamily: baseStyles.latoRegular,
  },
  pickerContainerStyle: {
    height: baseStyles.buttonSize30,
    justifyContent: 'center',
    paddingTop: 0,
    paddingBottom: baseStyles.padding5,
  },
  rangeDatePickerContainer: {
    height: 550,
    shadowColor: baseStyles.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: baseStyles.bgColor,
    width: 335,
  },
  popover: {
    borderRadius: 0,
    paddingHorizontal: baseStyles.padding20,
    paddingTop: 0,
    paddingBottom: baseStyles.padding20,
    backgroundColor: baseStyles.bgColor,
  },
  popoverOutsite: {
    backgroundColor: 'transparent',
  },
  arrowStyle: {
    backgroundColor: 'transparent',
    height: 0,
  },
});
