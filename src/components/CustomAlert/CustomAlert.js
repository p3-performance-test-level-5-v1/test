import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import styles from './CustomAlert.style';
import { baseStyles } from '../../config/baseStyles';
import CustomConfig from '../../utils/CustomConfig';

class CustomAlert extends React.Component {

    async handleModal() {
        const {
            customAlertParam : {
                primaryButtonCallback
            },
            showCustomAlert
        } = this.props;

        await showCustomAlert(false, {});
        if (primaryButtonCallback) {
            primaryButtonCallback();
        }
    }

    async handleSecondaryClick () {
        const {
            customAlertParam: {
                cancelButtonCallback
            },
            showCustomAlert
        } = this.props;
        await showCustomAlert(false, {});
        if (cancelButtonCallback) {
            cancelButtonCallback();
        }
    }

    renderContent(title = '', description = '') {

        if (title === '') {
            return (
                <View>
                    <Text style={styles.descriptionTextStyle} >
                        {description}
                    </Text>
                </View>
            )

        }
        return (
            <View>
                <Text style={styles.title}>
                    {title}
                </Text>
                <Text style={styles.descriptionTextStyle}>
                    {description}
                </Text>
            </View>
        );
    }

    renderActionButtons(customAlertParam = {}) {
        const {
            primaryButtonText,
            cancelButtonText,
            cancelButtonCallback
        } = customAlertParam;
        return (
            <View style={styles.actionButton}>
                {(cancelButtonText || cancelButtonCallback) &&
                <TouchableOpacity
                    onPress={() => {this.handleSecondaryClick()}}
                    testID="CustomAlert.secondaryClick"
                >
                    <View style={[styles.btnContainer]}>
                        <Text 
                            style={{ 
                                ...styles.buttonStyle, 
                                color: baseStyles.textGreyColor 
                            }}
                        >
                            {cancelButtonText && cancelButtonText!== '' ? cancelButtonText : 'CANCEL'}
                        </Text>
                    </View>
                </TouchableOpacity>
                }
                <TouchableOpacity
                    onPress={() => this.handleModal()}
                    style={{ marginLeft: baseStyles.margin30 }}
                    testID="CustomAlert.primaryClick"
                >
                    <View style={[styles.btnContainer]}>
                        <Text 
                            style={{ 
                                ...styles.buttonStyle, 
                                color: CustomConfig.Colors.btnPrimaryBgColor 
                            }}
                        >
                            {primaryButtonText && primaryButtonText!== '' ? primaryButtonText : 'OK'}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const {
            isVisible,
            customAlertParam : {
                title,
                desc,
            },
            customAlertParam
        } = this.props;

        return (
            <React.Fragment>
                {isVisible &&
                <View style={styles.modalBackground}>
                    <View style={styles.modalViewStyle}>
                        <View style={styles.contentWrapperStyle}>
                            {this.renderContent(title, desc)}
                            {this.renderActionButtons(customAlertParam)}
                        </View>
                    </View>
                </View>
                }
            </React.Fragment>
        )
    }
}

export default CustomAlert;