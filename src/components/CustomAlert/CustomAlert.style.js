import { StyleSheet, Dimensions } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
const {
    height: deviceHeight,
    width: deviceWidth
} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    modalBackground: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.8)',
        justifyContent:"center",
        alignItems:"center"
    },
    contentWrapperStyle: {
        width: deviceWidth * 0.7,
        backgroundColor: '#FFFFFF',
        padding: baseStyles.padding25
    },
    modalViewStyle: {
        backgroundColor: '#FFFFFF',
    },
    actionButton: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: baseStyles.margin10
    },
    descriptionTextStyle : {
        marginVertical: baseStyles.margin20,
        color: baseStyles.dimGray,
        fontFamily: baseStyles.latoRegular,
        fontSize: baseStyles.buttonFontSize,
    },
    buttonStyle: {
        fontFamily: baseStyles.latoBold,
        fontSize: baseStyles.buttonFontSize
    },
    title: {
        fontSize: baseStyles.labelFontSize20,
        color: baseStyles.black,
        fontFamily: baseStyles.latoBold
    },
    btnContainer: {
        maxWidth: 100,
        minWidth: 50,
        maxHeight: 50,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default styles;