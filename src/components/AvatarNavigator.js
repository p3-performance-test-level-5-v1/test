import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text } from 'react-native';
import CachedImage from './CachedImage';
import { AVATAR_COLORS } from '../config/config';

const AvatarNavigator = props => {
  const {
    imageUrl,
    avatharText,
    customHeight,
    customWidth,
    customFontSize,
  } = props;
  const borderRadius = customHeight >= customWidth ? customHeight : customWidth;

  const renderAvatharIcon = () => {
    if (imageUrl) {
      return (
        <CachedImage
          style={[
            styles.backgroundImageStyle,
            customHeight && { height: customHeight },
            customWidth && { width: customWidth },
            { borderRadius: borderRadius ? (borderRadius/2) : 30 }
          ]}
          source={imageUrl}
        />
      );
    } 

    const profileText = avatharText.trim().split('');
    const firstLetter = profileText[0] ? profileText[0].toUpperCase() : '';
    const charCodes = firstLetter && parseInt(firstLetter.charCodeAt(0), 10);
    const avatharBkColor = charCodes ? AVATAR_COLORS[charCodes % AVATAR_COLORS.length] : '#00AA55';
    return (
      <View
        style={[
          styles.backgroundImageStyle,
          {
            borderRadius: borderRadius ? (borderRadius/2) : 30,
            backgroundColor: avatharBkColor,
          },
          customHeight && { height: customHeight },
          customWidth && { width: customWidth },
        ]}
      >
        <View
          style={[
            styles.avatharTextContainer,
            customHeight && { height: customHeight },
            customWidth && { width: customWidth },
          ]}
        >
          <Text
            style={[
              styles.avatharText,
              customFontSize && { fontSize: customFontSize },
            ]}
          >
            {firstLetter}
          </Text>
        </View>
      </View>
    );
  };
  return (
    <View
      style={[
        styles.avatarDefault,
        customHeight && { height: customHeight },
        customWidth && { width: customWidth },
        (borderRadius && { borderRadius: borderRadius/2 }),
      ]}
    >
      {renderAvatharIcon()}
    </View>
  );
};

export default AvatarNavigator;

AvatarNavigator.propTypes = {
  avatharText: PropTypes.string.isRequired,
  imageUrl: PropTypes.string,
  customHeight: PropTypes.number,
  customWidth: PropTypes.number,
  customFontSize: PropTypes.number,
};

AvatarNavigator.defaultProps = {
  customHeight: 0,
  customWidth: 0,
};

const backgroundStyle = {
  height: '100%',
  width: '100%',
};

const styles = StyleSheet.create({
  backgroundImageStyle: {
    ...backgroundStyle,
    resizeMode: 'cover',
  },
  avatharTextContainer: {
    ...backgroundStyle,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatharText: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
  avatarDefault: {
    height: 60,
    width: 60,
    borderRadius: 30,
    backgroundColor: '#c1c1c1',
  },
});
