import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet
} from 'react-native';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { baseStyles } from '../config/baseStyles';
import CustomConfig from '../utils/CustomConfig';
import { buttonCommonStyle } from '../config/buttonStyle';

export default ButtonTextBox = ({
    navigatePath = () => {},
    mainText = '',
    secondaryText = ''
}) => (
    <TouchableOpacity 
    onPress={navigatePath}
    style={[
        styles.roleBtn,
        styles.primaryFontColor
    ]}>
        <View style={styles.rowStyle}>
            <Text>
            <Text 
                style={styles.roleTxt}>
                    {mainText}
                    {"\n"}
            </Text>
            <Text 
                style={styles.roleTxt}>
                    {secondaryText}
            </Text>
            </Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    roleBtn: {
        height: scaledHeight(148),
		width: scaledWidth(320),
		alignItems: 'center',
		justifyContent: 'space-evenly',
		borderWidth: 1,
		flexDirection: 'row',
        ...buttonCommonStyle.butttonStyle,
        borderRadius: 10,
        marginBottom: 0,
        marginTop: baseStyles.margin20
    },
    get primaryFontColor() {
		return {
			backgroundColor: CustomConfig.Colors.btnPrimaryBgColor,
			borderColor: CustomConfig.Colors.btnPrimaryBgColor
		}
	},
    rowStyle: {
        flexDirection: 'row',
		alignItems: 'center'
    },
    roleTxt: {
        color: baseStyles.white,
		textAlign: 'center',
		fontSize: scaledFont(32),
		fontFamily: baseStyles.latoBold,
    }
  });