import React, { Component } from 'react';
import {
	Text,
	View,
	Image,
	TouchableOpacity,
	Dimensions,
	StyleSheet
} from 'react-native';
import {
	scaledWidth,
	scaledHeight,
	scaledFont
} from '../utils/Scale';
import Feather from 'react-native-vector-icons/Feather';
import { headerConfig, layoutConfig } from '../config/config';
import {
	removeItemValue
} from '../utils';
import _ from 'lodash';
import { connect } from 'react-redux';
import { showSignOutConfirmation } from "../redux/actions";
import { t } from '../utils/LocalizationUtils.js';

const { height: deviceHeight } = Dimensions.get('window');

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}
	onLogOutAction = async () => {
		const { showConfirmation, signOutConfirmation } = this.props;
		signOutConfirmation(showConfirmation);
	};
	moveToBack = () => {
		this.props.navigation.goBack();
	};
	render() {
		const { backButton, schoolConfig, showLogOut } = this.props;
		const school = _.get(schoolConfig, 'data.data[0].school.code', 'TCC');

		return <View
		style={[
			this.props.style,
			styles.header
		]}>
			{
				backButton ? <TouchableOpacity
				onPress={this.moveToBack}
				style={{
					flexDirection: 'row',
					alignItems: 'center'
				}}>
					<Feather
						name='arrow-left'
						size={scaledFont(25)}
						color={headerConfig[school].color}
					/>
					{/* <Text
					style={{
						marginLeft: scaledWidth(5),
						color: headerConfig[school].color,
						fontSize: scaledFont(15)
					}}>{t('common.kioskBackBtnTxt')}</Text> */}
				</TouchableOpacity> : <View/>
			}
			{
				showLogOut ? <TouchableOpacity
				onPress={this.onLogOutAction}
				style={styles.signOutImage}>
					<Text
					style={[
						styles.logOutTxt,
						{
							color: headerConfig[school].color,
						}
					]}>{t('common.kioskLogoutText')}</Text>
					<Image
						style={styles.logOutImg}
						resizeMethod='resize'
						resizeMode='contain'
						source={headerConfig[school].image}
					/>
				</TouchableOpacity> : <View/>
			}
		</View>
	}
}

const mapStateToProps = state => ({
	schoolConfig: state.schoolConfig,
	showConfirmation: _.get(state, 'showConfirmation.data', false),
});

const mapDispatchToProps = {
	signOutConfirmation: showSignOutConfirmation
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: scaledWidth(25),
		justifyContent: 'space-between',
		paddingTop: scaledHeight(layoutConfig.statusBar.height)
	},
	logOutTxt: {
		marginRight: scaledWidth(5),
		fontSize: scaledFont(15)
	},
	logOutImg: {
		width: scaledWidth(14),
		height: scaledHeight(20)
	},
	signOutImage: {
		height: '60%',
		alignItems: 'center',
		flexDirection: 'row'
	}
});
