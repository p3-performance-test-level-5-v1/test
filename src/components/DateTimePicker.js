import React, { Component } from 'react';
import { View, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import DatePicker from 'react-native-datepicker';
import { baseStyles } from '../config/baseStyles';
import moment from 'moment';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import CustomConfig from '../utils/CustomConfig';
import { scaledHeight } from '../utils/Scale';

export default class DateTimePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
  }

  renderPasswordAccessory() {
    const { renderIcon, iconName, iconColor, iconSize } = this.props;

    if (renderIcon) {
      return renderIcon();
    }

    return <MaterialCommunityIcons name={iconName} size={iconSize} color={iconColor} />;
  }

  onDateTimePickerChange = (pickerDate) => {
    const { onDateTimeChange, pickerFomat, minDate, maxDate, currentDate } = this.props;
    let date = pickerDate;

    if (minDate && moment(date, 'HH:mm:ss').format('HH:mm') === moment(minDate, 'HH:mm:ss').format('HH:mm')) {
      const minSeconds = moment(minDate, 'HH:mm:ss').get('seconds');
      const dateSeconds = moment(date, 'HH:mm:ss').get('seconds');
      if (dateSeconds <= minSeconds) {
        date = moment(date, 'HH:mm:ss').set({ seconds: minSeconds });
      }
    }
    if (maxDate && moment(date, 'HH:mm:ss').format('HH:mm') === moment(maxDate, 'HH:mm:ss').format('HH:mm')) {
      const maxSeconds = moment(maxDate, 'HH:mm:ss').get('seconds');
      const dateSeconds = moment(date, 'HH:mm:ss').get('seconds');
      if (dateSeconds >= maxSeconds) {
        date = moment(date, 'HH:mm:ss').set({ seconds: maxSeconds });
      }
    }

    let minOrMaxDate = '';
    let check = true;
    const todaysDate = moment().format('YYYY-MM-DD');
    const isToday = currentDate ? moment(currentDate).isSame(todaysDate, 'day') : false;
    const selected = moment(date, pickerFomat);
    if (isToday && !maxDate) {
      const nextDate = moment().format('HH:mm:ss');
      const checkDate = selected.isSameOrBefore(moment(nextDate, 'HH:mm:ss'));
      if (!checkDate) {
        minOrMaxDate = moment(nextDate, pickerFomat);
        date = minOrMaxDate;
      }
    }
    if (minDate && maxDate) {
      const lastDate = moment(minDate, pickerFomat);
      const nextDate = moment(maxDate, pickerFomat);
      if (selected.isSameOrAfter(lastDate) && selected.isSameOrBefore(nextDate)) {
        check = true;
      } else if (!selected.isSameOrAfter(lastDate) && !selected.isSameOrBefore(nextDate)) {
        minOrMaxDate = lastDate;
        check = false;
      } else if (!selected.isSameOrAfter(lastDate)) {
        minOrMaxDate = lastDate;
        check = false;
      } else if (!selected.isSameOrBefore(nextDate)) {
        minOrMaxDate = nextDate;
        check = false;
      }
    } else if (minDate) {
      minOrMaxDate = moment(minDate, pickerFomat);
      check = selected.isSameOrAfter(minOrMaxDate);
    } else if (maxDate) {
      minOrMaxDate = moment(maxDate, pickerFomat);
      check = selected.isSameOrBefore(minOrMaxDate);
    }
    const value = check ? date : minOrMaxDate.format(pickerFomat);
    onDateTimeChange(value);
  };

  componentDidMount = () => {
    this.setState({
      show: false,
    });
  };

  render() {
    const {
      dateTime,
      onDateTimeChange,
      labelText,
      pickerMode,
      pickerFomat,
      minDate,
      maxDate,
      pickerDisable,
      showIcon,
      baseColor,
      tintColor,
      placeHolderText,
      customPlaceholderTexColor,
      formattedDateTimeValue,
      customField,
      cancelBtnText,
      confirmBtnText,
      minInterval,
    } = this.props;
    const { show } = this.state;

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={() => this.dateTimepicker.onPressDate()}>
          <View>
            {customField ? (
              customField
            ) : (
              <TextField
                label={labelText}
                baseColor={baseColor || baseStyles.inputLabelColor}
                tintColor={tintColor || baseStyles.inputLabelColor}
                labelHeight={baseStyles.primaryLabelHeight}
                lineWidth={1}
                style={styles.dateTimeStyle}
                editable={false}
                renderAccessory={() => !showIcon && this.renderPasswordAccessory()}
                inputContainerStyle={styles.pickerContainerStyle}
                value={formattedDateTimeValue ? formattedDateTimeValue : dateTime}
                disabledLineType="solid"
                disabled={pickerDisable}
                placeholder={placeHolderText && !dateTime ? placeHolderText : ''}
                placeholderTextColor={customPlaceholderTexColor}
              />
            )}
          </View>
        </TouchableWithoutFeedback>
        <DatePicker
          showIcon={show}
          hideText
          style={styles.datePicker}
          date={dateTime}
          mode={pickerMode}
          androidMode={pickerMode === 'time' ? 'spinner' : 'default'}
          confirmBtnText={confirmBtnText ? confirmBtnText : 'Select'}
          cancelBtnText={cancelBtnText ? cancelBtnText : 'Cancel'}
          minDate={minDate}
          maxDate={maxDate}
          is24Hour={false}
          ref={(c) => (this.dateTimepicker = c)}
          format={pickerFomat}
          onDateChange={(date) => this.onDateTimePickerChange(date)}
          disabled={pickerDisable}
          customStyles={pickerCustomStyle}
          minuteInterval={minInterval}
        />
      </View>
    );
  }
}

DateTimePicker.defaultProps = {
  iconName: 'calendar-range',
  iconColor: baseStyles.lightGrey,
  iconSize: 20,
};

const pickerCustomStyle = {
  get datePicker() {
    return {
      color: CustomConfig.Colors.primaryColor,
    }
  },
  get btnTextConfirm() {
    return {
      color: CustomConfig.Colors.primaryColor,
    }
  },
};

const styles = StyleSheet.create({
  datePicker: {
    width: 0,
    height: 0,
  },
  dateTimeStyle: {
    color: baseStyles.textColorBlack,
    fontSize: baseStyles.textInputFont16,
    fontFamily: baseStyles.latoRegular,
  },
  dropArrow: {
    width: 15,
    height: 10,
  },
  pickerContainerStyle: {
    paddingTop: baseStyles.padding5,
    height: scaledHeight(35),
  },
  container: {
    width: '100%',
  },
});
