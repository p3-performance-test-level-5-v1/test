import React, { Component } from 'react';
import { TouchableOpacity, View, StyleSheet, Modal, ScrollView, Text } from 'react-native';
import { baseStyles } from '../config/baseStyles';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class SNDropdown extends Component {
  constructor(args) {
    super(args);
    this.state = {
      showOptions: false,
      selectedOption: null,
    };
    this.dropdownRef = React.createRef();
  }

  toggleOptionsView() {
    const { disabled } = this.props;

    if (disabled) {
      return;
    }

    this.dropdownRef.current.measure((x, y, width, height, pageX, pageY) => {
      this.setState(
        {
          offsetX: pageX,
          offsetY: pageY + height,
          dWidth: width,
          dHeight: height,
        },
        () => {
          this.setState({
            showOptions: !this.state.showOptions,
          });
        }
      );
    });
  }

  handleChange(selectedOption) {
    this.props.handleChange(selectedOption);
    this.setState({
      showOptions: false,
    });
  }

  renderModal() {
    const { showOptions } = this.state;
    const { options, disabled, itemStyle, customOptionContainerStyle } = this.props;

    if (disabled) {
      return;
    }

    return (
      <Modal animationType="none" transparent={true} visible={!showOptions ? false : true} onRequestClose={() => {}}>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ showOptions: false })}>
          <View
            style={[
              styles.optionContainer,
              {
                top: this.state.offsetY,
                left: this.state.offsetX,
                width: this.state.dWidth,
              },
              customOptionContainerStyle,
            ]}
          >
            <ScrollView>
              {options && options.length
                ? options.map((option, idx) => (
                    <TouchableOpacity key={idx} style={styles.option} onPress={this.handleChange.bind(this, option)}>
                      <Text
                        style={{
                          color: baseStyles.darkShadeGray,
                          ...itemStyle,
                        }}
                      >
                        {option.name}
                      </Text>
                    </TouchableOpacity>
                  ))
                : null}
            </ScrollView>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

  render() {
    const {
      selectedOption,
      placeHolderStyle,
      selectedOptionStyle,
      touchableContainerStyle,
      dropArrowColor,
    } = this.props;
    const selectedItemStyle = selectedOption ? styles.dropdownSelect : styles.dropdownPlaceHolder;

    const touchableContainer = touchableContainerStyle ? touchableContainerStyle : styles.touchableItemStyle;

    return (
      <View>
        <View style={styles.container}>
          {!this.props.placeHolderasText && this.props.placeholder && (
            <Text
              style={{
                ...styles.placeHolderStyle,
                ...placeHolderStyle,
              }}
            >
              {this.props.placeholder}
            </Text>
          )}
          <TouchableOpacity
            ref={this.dropdownRef}
            onPress={this.toggleOptionsView.bind(this)}
            style={touchableContainer}
          >
            <Text
              numberOfLines={1}
              style={{
                ...selectedItemStyle,
                ...selectedOptionStyle,
              }}
            >
              {selectedOption
                ? selectedOption.name
                : this.props.placeHolderasText && this.props.placeholder
                ? this.props.placeholder
                : ''}
            </Text>
            <MaterialIcons size={25} name={'keyboard-arrow-down'} color={dropArrowColor} />
          </TouchableOpacity>
        </View>
        {this.renderModal()}
      </View>
    );
  }
}

SNDropdown.defaultProps = {
  itemStyle: {},
  placeHolderStyle: {},
  selectedOptionStyle: {},
  dropArrowColor: baseStyles.dropdownColor,
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    zIndex: 100,
  },
  dropdownSelect: {
    flex: 1,
    color: baseStyles.darkShadeGray,
    fontSize: baseStyles.labelFontSize,
    paddingVertical: baseStyles.padding8,
  },
  dropdownPlaceHolder: {
    flex: 1,
    color: `${baseStyles.darkShadeGray}95`,
    fontSize: baseStyles.labelFontSize,
    paddingVertical: baseStyles.padding8,
  },
  dropArrow: {
    width: 11,
    height: 7,
    resizeMode: 'contain',
    position: 'absolute',
    right: 10,
    marginVertical: baseStyles.margin15,
  },
  optionContainer: {
    position: 'absolute',
    backgroundColor: baseStyles.bgColor,
    borderColor: baseStyles.lightGrey,
    borderWidth: 1,
    maxHeight: 200,
    paddingTop: 10,
    paddingBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.84,

    elevation: 5,
  },
  option: {
    padding: 10,
  },
  placeHolderStyle: {
    color: `${baseStyles.darkShadeGray}95`,
    fontSize: baseStyles.labelSecondaryFontSize,
    fontFamily: baseStyles.latoRegular
  },
  touchableItemStyle: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: baseStyles.lightGrey,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default SNDropdown;
