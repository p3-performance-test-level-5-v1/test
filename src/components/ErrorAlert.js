import React, { Component } from 'react';
import {
	View,
	TouchableOpacity,
	StyleSheet,
	Text,
	Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { baseStyles } from '../config/baseStyles';
import { get } from 'lodash';
import {
	updateErrorMessage
} from '../redux/actions';
import { t } from '../utils/LocalizationUtils.js';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

class ErrorAlert extends Component {
	constructor(props) {
		super(props);
		this.timer = null;
	}
	componentWillReceiveProps(nextProps) {
		const message = get(nextProps, 'errorMessage.data.data.message', '')
		if (message) {
			this.timer = setTimeout(() => {
				nextProps.updateErrorMessage();
			}, 10000);
		}
	}
	render() {
		const { errorMessage, updateErrorMessage } = this.props;
		const message = get(errorMessage, 'data.data.message', '');
		const action = get(errorMessage, 'data.data.action');
		if (!message) {
			return null;
		}
		return (
			<View style={styles.container}>
				<View style={styles.errorView}>
					<MaterialIcons
						color='white'
						name='error-outline' 
						size={50}
					/>
					<Text 
						style={styles.errorMessage}
					>{message}</Text>
					<TouchableOpacity 
						onPress={() => {
							action();
							if (this.timer) {
								clearTimeout(this.timer);
							}
							updateErrorMessage();
						}}
					>
						<Text 
							style={styles.retryBtnTextStyle}
						>{t('common.kioskRetryBtnTxt')}</Text>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	errorMessage: state.errorMessage
});

const mapDispatchToProps = {
	updateErrorMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorAlert);

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		height: deviceHeight,
		width: deviceWidth,
		justifyContent: 'flex-end'
	},
	errorView: {
		minHeight: deviceHeight * 0.1,
		margin: baseStyles.margin15,
		padding: baseStyles.padding15,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#9d1e14',
		justifyContent: 'space-between',
		borderRadius: baseStyles.defaultBorderRadius
	},
	errorMessage: {
		color: 'white',
		fontSize: baseStyles.labelFontSize24,
		fontFamily: baseStyles.latoBold,
		flex: 1, 
		marginHorizontal: baseStyles.margin20
	},
	retryBtnTextStyle: {
		color: 'white',
		fontSize: baseStyles.labelFontSize22,
		fontFamily: baseStyles.latoBold,
		textTransform: 'uppercase'
	}
});