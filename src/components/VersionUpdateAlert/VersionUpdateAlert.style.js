import { StyleSheet, Dimensions } from 'react-native';
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        position: 'relative',
        zIndex: 10
    },
    alertBox: {
        backgroundColor: '#505050',
        borderRadius: 8,
        position: 'absolute',
        bottom: 50,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        textAlign: 'center',
        alignItems: 'center',
        marginRight: 4,
    },
    text: {
        color: '#E4E4E4',
        fontSize: 14,
        lineHeight: 16.8,
        fontFamily: 'Lato',
        fontWeight: '400',
        marginLeft: 4,
    }
})
export default styles;