import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './VersionUpdateAlert.style';
import { t } from '../../utils/LocalizationUtils.js';
import {
    View,
    Text,
} from 'react-native';


const VersionUpdateAlert = () => {
    return <View style={styles.container}>
        <View style={styles.alertBox}>
            <AntDesign
                name="exclamationcircle"
                size={20}
                color='#e4e4e4'
                style={styles.icon}
            />
            <Text style={styles.text}>
                {t('common.updateAlert')}
            </Text>
        </View>
    </View>
}
export default VersionUpdateAlert;