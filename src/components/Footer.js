import React, { Component } from 'react';
import { 
	Text, 
	View, 
	Image,
	TouchableOpacity,
	Dimensions,
	StyleSheet 
} from 'react-native';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import CustomConfig from '../utils/CustomConfig';
const { height: deviceHeight } = Dimensions.get('window');

export default class Footer extends Component {
	constructor(props) {
		super(props);
		this.state = {}
	}

	render() {
		return <View 
		style={[
			this.props.style,
			styles.footer
		]}>
			
		</View>
	}
}

const styles = StyleSheet.create({
	footer: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingRight: scaledWidth(25)
	}
});