import React, { Component } from 'react';
import FastImage from 'react-native-fast-image';
const RESIZE_MODE = {
    contain: FastImage.resizeMode.contain,
    cover: FastImage.resizeMode.cover,
    stretch: FastImage.resizeMode.stretch,
    center: FastImage.resizeMode.center
};

const CachedImage = props => {
    const resizeMode = props.resizeMode ? props.resizeMode : 'cover';
    return (
        <FastImage
            style={props.style}
            source={{
                uri: props.source,
                priority: FastImage.priority.normal,
                cache: FastImage.cacheControl.immutable,
            }}
            resizeMode={RESIZE_MODE[resizeMode]} 
            onLoadStart={props.onLoadStart && props.onLoadStart.bind(this)}
            onLoadEnd={props.onLoadEnd && props.onLoadEnd.bind(this)}
        >{props.children}</FastImage>
    );
}

export default CachedImage;