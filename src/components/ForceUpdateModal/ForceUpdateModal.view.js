import React from 'react';
import { Linking, Platform, AppState, AsyncStorage } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import { get, isEmpty } from 'lodash';
import semver from 'semver';
import ModalCard from '../ModalCard/ModalCard.view';
import Constant from '../../constants/Constant';
import { checkMobileVersion } from '../../redux/actions/MobileVersionAction';
import { setValueToRedux } from '../../redux/actions';
import { t } from '../../utils/LocalizationUtils.js';
import { retrieveAsyncStorage, storeAsyncStorage } from '../../utils';
import Sentry from '../../sentry';
import VersionUpdateAlert from '../VersionUpdateAlert/VersionUpdateAlert';
class ForceUpdateModal extends React.Component {
  state = {
    showAlert: false,
    appState: AppState.currentState,
  };

  async componentDidMount() {
    AppState.addEventListener('change', e => {
      this.handleAppStateChange(e);
    });
    this.fetchMobileVersion();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', e => {
      this.handleAppStateChange(e);
    });
  }

  handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      this.fetchMobileVersion();
    }
    this.setState({ appState: nextAppState });
  };

  async fetchMobileVersion() {
    try {
      const {
        checkMobileVersion,
        setValueToRedux
      } = this.props;
      const bundleIdentifier = await DeviceInfo.getBundleId();
      const reqData = {
        platform: Platform.OS,
        bundleIdentifier
      }

      const localAppVersion = await DeviceInfo.getVersion();
      const response = await checkMobileVersion(reqData);
      if (response.success) {
        const latestVersion = semver.valid(semver.coerce(get(response, 'data.getLatestMobileVersion.version', Constant.DEFAULT_APP_VERSION)));
        const minSupportVersion = semver.valid(semver.coerce(get(response, 'data.getLatestMobileVersion.minSupportVersion', Constant.DEFAULT_APP_VERSION)));

        // format to semver version if possible. If not, value will be null.
        const localVersion = semver.valid(semver.coerce(localAppVersion));

        // Clean the localstorage when updating the app
        const appVersion = await retrieveAsyncStorage('appVersion');
        if (isEmpty(appVersion)) {
          await storeAsyncStorage('appVersion', localVersion);
        }
        if (appVersion && localVersion && semver.lt(appVersion, localVersion)) {
          const asyncStorageKeys = await AsyncStorage.getAllKeys();
          if (asyncStorageKeys.length > 0) {
            const selectedKeys = asyncStorageKeys.filter(key => key !== 'accessToken');
            if (selectedKeys && selectedKeys.length > 0) {
              try {
                await AsyncStorage.multiRemove(selectedKeys);
              } catch (err) {
                Sentry.log(get(err));
              }
            }
          }
          await storeAsyncStorage('appVersion', localVersion);
        }

        if (localVersion && latestVersion && semver.lt(localVersion, latestVersion)) {
          if (localVersion && minSupportVersion && semver.lt(localVersion, minSupportVersion)) {
            // Local version have to be the same with latest or minSupportVersion
            this.setState({showAlert: true})
          }
        }
      }
      // set local app version
      await setValueToRedux('localAppVersion', localAppVersion);
    } catch (err) {
      console.log(err) // TODO: push to logger?
    }
  }

  onConfirm() {
    const { downloadURL } = this.props.mobileVersion;
    if (downloadURL) {
      Linking.canOpenURL(downloadURL).then(supported => {
        supported && Linking.openURL(downloadURL);
      });
    }
  }

  render() {
    const { showAlert } = this.state;
    return showAlert ? <VersionUpdateAlert /> : null;
  }
}

const mapStateToProps = state => ({
  mobileVersion: get(state, 'mobileVersion.data', {})
});

const mapDispatchToProps = ({
  checkMobileVersion,
  setValueToRedux
});

export default connect(mapStateToProps, mapDispatchToProps)(ForceUpdateModal);
