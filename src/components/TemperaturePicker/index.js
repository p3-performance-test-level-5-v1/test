import React, { useState, useCallback, useEffect } from 'react';
import { View, FlatList, TouchableOpacity, Dimensions, TouchableWithoutFeedback, Text } from 'react-native';
import PropTypes from 'prop-types';

import { baseStyles } from '../../config/baseStyles';
import { styles } from './TemperaturePicker.styles';
import { temperatureDigitalKeys } from './temperatureDigitalKeys';
import { get } from 'lodash';

const { width: deviceWidth } = Dimensions.get('window');

const TemperaturePicker = ({
  handleTemperature,
  marginBetween,
  showKeyboard = false,
  goBack,
  temperature,
  setTemperatureMarked,
  horizontalPadding = 200,
}) => {
  const [tempValue, setTempValue] = useState({
    tempVal: '',
    decimalVal: '',
  });

  const [isMarked, setIsMarked] = useState(false);

  const callBackFunction = useCallback(() => {
    const tempVal = tempValue.tempVal + tempValue.decimalVal / 10;
    if (tempValue.decimalVal === '' && tempValue.tempVal === '') {
      handleTemperature('');
    } else {
      if (tempValue.decimalVal !== '' && tempValue.tempVal && isMarked) {
        setTemperatureMarked(tempVal);
      }
      handleTemperature(tempVal);
    }
  }, [tempValue]);

  const handleTempChange = useCallback(
    (textPos, value) => {
      if (textPos === 'temp') setTempValue({ ...tempValue, tempVal: value });
      if (textPos === 'decimal') {
        setTempValue({ ...tempValue, decimalVal: value });
      
        setIsMarked(true);
      }
    },
    [tempValue]
  );

  useEffect(() => {
    callBackFunction();
  }, [tempValue]);

  useEffect(() => {
    onComponentWillMount();
  }, []);

  const onComponentWillMount = () => {
    if (!temperature) return;
    const tempValues = temperature.toString().split('.');
    const tempVal = get(tempValues, '[0]');
    const decimalVal = get(tempValues, '[1]');
    setTempValue({
      tempVal: tempVal ? parseInt(tempVal) : '',
      decimalVal: decimalVal ? parseInt(decimalVal) : '',
    });
  };

  const handleCancel = () => {
    setTempValue({ tempVal: '', decimalVal: '' });
    goBack();
  };

  const _renderDigitalKey = (item, index) => {
    const keyWidth =
      index === 0 || index % 3 === 0
        ? (deviceWidth - horizontalPadding * 2) * 0.5
        : (deviceWidth - horizontalPadding * 2) * 0.25;
    const keyBackground =
      item.keyValue === tempValue.tempVal || item.keyValue === tempValue.decimalVal
        ? baseStyles.grayishBlue
        : `${baseStyles.lightGrey}90`;
    const textColor =
      item.keyValue === tempValue.tempVal || item.keyValue === tempValue.decimalVal
        ? baseStyles.white
        : baseStyles.grayishBlue;

    if (item.keyName === 'Cancel') {
      if (goBack) {
        return (
          <TouchableOpacity
            style={[styles.keyStyle, { width: '100%', backgroundColor: keyBackground }]}
            onPress={() => handleCancel()}
          >
            <Text style={[styles.buttonText, { color: textColor }]}>{item.keyName}</Text>
          </TouchableOpacity>
        );
      } else return null;
    } else {
      return (
        <TouchableOpacity
          style={[styles.keyStyle, { width: keyWidth, backgroundColor: keyBackground }]}
          onPress={() => handleTempChange(item.keyType, item.keyValue)}
        >
          <Text style={[styles.buttonText, { color: textColor }]}>{item.keyName}</Text>
        </TouchableOpacity>
      );
    }
  };

  return (
    <View>
      {showKeyboard ? (
        <View
          style={{
            marginTop: marginBetween,
          }}
        >
          <FlatList
            numColumns={3}
            extraData={tempValue}
            data={temperatureDigitalKeys}
            keyExtractor={(item) => `digitalKey-${item.id}`}
            renderItem={({ item, index }) => _renderDigitalKey(item, index)}
            bounces={false}
          />
        </View>
      ) : null}
    </View>
  );
};

TemperaturePicker.propsTypes = {
  handleTemperature: PropTypes.func.isRequired,
  marginBetween: PropTypes.number,
  showKeyboard: PropTypes.bool,
  goBack: PropTypes.func,
  temperature: PropTypes.number,
  setTemperatureMarked: PropTypes.func,
};

TemperaturePicker.defaultProps = {
  setTemperatureMarked: () => null,
};

export default TemperaturePicker;
