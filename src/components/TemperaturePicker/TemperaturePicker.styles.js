import { Dimensions, StyleSheet } from 'react-native';
import { baseStyles } from '../../config/baseStyles';
import { scaledFont } from '../../utils/Scale';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  keyStyle: {
    backgroundColor: `${baseStyles.lightGrey}90`,
    height: deviceHeight * 0.07,
    width: deviceWidth / 3,
    borderColor: baseStyles.lightGrey,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: baseStyles.buttonFontSize,
    fontFamily: baseStyles.latoBold,
  },
});
