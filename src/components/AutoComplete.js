import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Modal,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  TextInput
} from 'react-native';
import _ from 'lodash';
import { baseStyles } from '../config/baseStyles';
import { TextField } from 'react-native-material-textfield';
import Feather from 'react-native-vector-icons/Feather';

class AutoComplete extends Component {

  constructor(args) {
    super(args);
    this.state = {
      showOptions: false,
      selectedOption: {},
      width: 100,
      queryText: ''
    }
  }

  toggleOptionsView() {
    this.queryFieldRef.blur();
    this.refs.dropdown.measure((x, y, width, height, pageX, pageY) => {
      this.setState({
        offsetX: pageX,
        offsetY: pageY
      }, () => {
        const { showOptions } = this.state;
        this.setState({ showOptions: !showOptions })
        this.props.keyboardShowHide(true);
      });
    });
  }

  handleChange(selectedOption) {
    const { handleChange } = this.props;
    this.setState({ queryText: '' });
    handleChange(selectedOption);
    this.handleClose();
  }

  handleClose() {
    this.props.keyboardShowHide(false);
    this.setState({ showOptions: false });
  }

  renderDropdownMenu() {
    const { showOptions, queryText } = this.state;
    const { options, topOffset, textInputStyle } = this.props;
    const filterOption = options.filter(opt => (opt.name.toLowerCase().indexOf(queryText.toLowerCase()) >= 0 || opt.value.toLowerCase().indexOf(queryText.toLowerCase()) >= 0));

    return (
      <Modal 
        animationType="none" 
        transparent={true} 
        visible={showOptions}
        onRequestClose={() => this.handleClose()}
      >
        <TouchableWithoutFeedback onPress={() => this.handleClose()}>
          <View style={{ flex: 1 }}>
            <View style={[
              styles.optionContainer,
              {
                top: this.state.offsetY - topOffset,
                left: this.state.offsetX,
              }
            ]}>
              <TextInput 
                value={queryText}
                autoCorrect={false}
                autoFocus={true}
                autoCapitalize="none" 
                style={{
                  ...styles.inputText, 
                  ...styles.modalInputText,
                  ...textInputStyle,
                }}
                placeholder={this.props.placeholder}
                onChangeText={queryText => this.setState({ queryText })}
              />
              <View style={[styles.dropdownShadow, { borderWidth: filterOption.length > 0 ? 2 : 0 }]}>
                <ScrollView
                  keyboardShouldPersistTaps='handled'
                  contentContainerStyle={{ 
                    width: this.state.width,
                    backgroundColor: baseStyles.bgColor,
                    padding: filterOption.length > 0 ? baseStyles.padding15 : 0
                  }}
                >
                  {
                    filterOption.map((option, index) => (
                      <TouchableOpacity 
                        onPress={() => this.handleChange(option)}
                        key={`option_${index}`}
                        style={{ padding: baseStyles.padding10 }}
                      >
                        <Text style={styles.optionItem}>{`${option.value} ${option.name || ''}`}</Text>
                      </TouchableOpacity>
                    ))
                  }
                </ScrollView>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }

  onLayout = (event) => {
    const { width } = event.nativeEvent.layout;
    this.setState({ width: width });
  }

  renderPasswordAccessory() {
    return (
      <Feather
        size={baseStyles.labelFontSize22}
        name='chevron-down'
        color={baseStyles.inputLabelColor}
      />
    );
  }

  render() {
    const { selectedOption, textInputStyle, lineWidth } = this.props;
    const { showOptions } = this.state;

    return (
      <View
        style={this.props.style}
        onLayout={(event) => this.onLayout(event)}
      >
        <View style={{ height: 50 }}>
            <View ref="dropdown">
              <TextField
                label=''
                autoFocus={false} 
                value={selectedOption}
                editable={!showOptions}
                lineWidth={lineWidth||2}
                ref={ref => this.queryFieldRef = ref}
                labelHeight={baseStyles.inputLabelHeight}
                tintColor={baseStyles.inputLabelColor}
                placeholder={this.props.placeholder}
                inputContainerStyle={styles.placeInput} 
                onFocus={this.toggleOptionsView.bind(this)} 
                style={{ ...styles.inputText, ...textInputStyle }}
                renderAccessory={this.renderPasswordAccessory.bind(this)}
              />
            </View>
        </View>
        {this.renderDropdownMenu()}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  optionContainer: {
    position: 'absolute',
    backgroundColor: baseStyles.bgColor,
    maxHeight: 200
  },
  dropdownShadow: {
    borderColor: baseStyles.descriptionColor,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  placeInput: {
    paddingBottom: 0,
    justifyContent: 'center',
    paddingTop: baseStyles.padding10,
    height: baseStyles.inputPlaceHeight,
  },
  inputText: {
    fontSize: baseStyles.textInputFont,
    color: baseStyles.titleColor,
    fontFamily: baseStyles.latoSemibold
  },
  modalInputText: {
    borderBottomWidth: 2,
    paddingTop: baseStyles.padding10,
    height: baseStyles.inputPlaceHeight,
    borderBottomColor: baseStyles.inputLabelColor
  },
  optionItem: {
    fontSize: baseStyles.textInputFont,
    fontFamily: baseStyles.latoSemibold,
    color: baseStyles.labelColor
  }
});

export default AutoComplete;