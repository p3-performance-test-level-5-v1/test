import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

import { t } from '../utils/LocalizationUtils.js';
import { baseStyles } from '../config/baseStyles';

const TemperatureItem = ({ data }) => {
  return (
    <View style={styles.heathItem}>
      <Text style={styles.time}>{moment(data.time, 'YYYY-MM-DD HH:mm:ss').format('hh:mm A')}</Text>
      <View style={styles.tempContainer}>
        <Text style={styles.temp}>{`${Number(data.value).toFixed(1)}°C`}</Text>
        <View style={styles.circle}>
          <Icon name="check-bold" size={13} color={baseStyles.white} />
        </View>
      </View>
    </View>
  );
};

export default TemperatureItem;

export const EmptyRecords = () => (
  <View>
    <Text style={styles.time}>{t('common.noRecords')}</Text>
    <Text style={styles.temp}>-</Text>
  </View>
);

const styles = StyleSheet.create({
  heathItem: {
    flexBasis: '40%',
  },
  time: {
    color: baseStyles.darkGrayTintColor,
    fontSize: 14,
    lineHeight: 20,
  },
  tempContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  temp: {
    marginRight: 12,
    fontSize: baseStyles.labelFontSize,
    color: baseStyles.darkShadeGray,
    fontFamily: baseStyles.latoBold,
  },
  circle: {
    width: 16,
    height: 16,
    borderRadius: 8,
    backgroundColor: baseStyles.parrotGreen,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
