import React, { Component } from 'react';
import {
	View,
	Text,
	Modal,
	Dimensions,
	TouchableOpacity,
	StyleSheet
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { RNCamera } from 'react-native-camera';
import { 
	scaledWidth, 
	scaledHeight, 
	scaledFont 
} from '../utils/Scale';
import { t } from '../utils/LocalizationUtils.js';

const {
	height: deviceHeight,
	width: deviceWidth
} = Dimensions.get('window');

export default class KioskCamera extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showCaptureButton: true
		};
	}
	takePicture = async ()=> {
		if (this.camera) {
			const options = { quality: 1, base64: true };
			const data = await this.camera.takePictureAsync(options);
			this.props.uploadPicture(data);
			this.setState({
				showCaptureButton: false
			})
		}
	}
	render() {
		const { showCaptureButton } = this.state;
		return <Modal 
		animationType="slide"
		transparent={false}
		visible={this.props.cameraModal}
		onRequestClose={() => this.props.closeCamera()}>
			<View 
			style={styles.container}>
		        <RNCamera
					ref={ref => {
						this.camera = ref;
					}}
					style={styles.preview}
					type={RNCamera.Constants.Type.front}
					permissionDialogTitle={'Permission to use camera'}
					permissionDialogMessage={'We need your permission to use your camera phone'}
		        >
		        	{({ camera, status, recordAudioPermissionStatus }) => {
						if (status !== 'READY') return <View/>;
						return <View 
						style={styles.cameraContainer}>
							<View 
					        style={styles.cameraHeader}>
								<TouchableOpacity 
								onPress={() => this.props.closeCamera()}
								style={styles.backButton}>
									<Feather 
										name='arrow-left' 
										size={scaledFont(20)} 
										color='#fff'
									/>
									<Text 
									style={{
										color: '#fff',
										fontSize: scaledFont(20)
									}}>{t('common.kioskBackBtnTxt')}</Text>
								</TouchableOpacity>
					        </View>
							<View style={styles.cameraView}>
								<TouchableOpacity 
								onPress={this.takePicture.bind(this)} 
								disabled={!showCaptureButton}
								style={styles.capture}>
									<View 
									style={styles.captureButton}/>
								</TouchableOpacity>
							</View>
						</View>;
					}}
		        </RNCamera>
			</View>
		</Modal>;
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'transparent',
	},
	preview: {
		flex: 1,
		justifyContent: 'center'
	},
	capture: {
		flex: 0,
		borderRadius: scaledFont(50)/2,
		borderColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center',
		height: scaledFont(50),
		width: scaledFont(50),
		borderWidth: 3,
		marginBottom: scaledHeight(30)
	},
	cameraContainer: {
		flex: 1,
		justifyContent: 'space-between',
		backgroundColor: 'transparent'
	},
	cameraHeader: { 
    	flex: 0.15,
    	justifyContent: 'center',
    	paddingHorizontal: scaledWidth(20)
    },
    backButton: {
		flexDirection: 'row',
		height: scaledHeight(45),
		justifyContent: 'space-around',
		alignItems: 'center',
		width: scaledWidth(70)
	},
	cameraView: { 
    	flex: 0.15, 
    	justifyContent: 'center',
    	alignItems: 'center' 
    },
    captureButton: {
		borderRadius: scaledFont(35)/2,
		backgroundColor: '#fff',
		height: scaledFont(35),
		width: scaledFont(35),
	}
});