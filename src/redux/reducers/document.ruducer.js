/**
 * 
 * Example Data Redux Reducer complete State tree
 * 
 * 
 * {
 *   listAllCentre: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findAllCentreForSchool: {
 *             data: [
 *               {
 *                 ID: 1,
 *                 label: 'The Caterpillar\'s Cove @ Jurong East',
 *                 fkSchool: 3
 *               },
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   checkInOutToken: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getCheckInOutToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.IjVmNTBhNDc5LGNpb3QsMSI.LI6qwJid4mjqhvYf-Kkf9dNJeksa3HhGiFs2eNuxyEY'
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   centreLogin: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           centreLogin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiYXV0aG9yaXphdGlvbl90b2tlbiIsImlkIjo5MTcsImVtYWlsIjoicHJpbmNpcGFsX3RjY19jMkBleGFtcGxlLmNvbSIsImV4cGlyZV9hdCI6IjIwMjAtMTAtMDNUMTU6NTM6MzUuMjkyMDEzNjErMDg6MDAiLCJpc3N1ZWRfYXQiOiIyMDIwLTA5LTAzVDE1OjUzOjM1LjI5MjAxNTIyOCswODowMCJ9.Wq64bngg17MofJKaKY6W4AD1NiHzuZhR2IxdDxtTIsw'
 *         },
 *         error: null
 *       },
 *       centreId: 1
 *     },
 *     centreId: 1
 *   },
 *   findParentByIC: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findParentByIC: {
 *             data: [
 *               {
 *                 ID: 630,
 *                 firstname: '',
 *                 lastname: 'Maverick Tok Wee Tao(Zhuo Weitao)',
 *                 childRelations: {
 *                   data: [
 *                     {
 *                       child: {
 *                         ID: 76,
 *                         firstname: '',
 *                         lastname: 'Sharmaine Faith Tok Le-En',
 *                         imageKey: 'upload/child/76/baby8-20200902-1599048581995998036-2097.jpeg',
 *                         dateOfBirth: '2019-01-02 00:00:00',
 *                         birthCertificate: 'T1900021J',
 *                         currentLevel: {
 *                           fkCentre: 2,
 *                           fkLevel: 17
 *                         }
 *                       }
 *                     }
 *                   ]
 *                 }
 *               }
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   childCheckIn: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           childCheckIn: true
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   allConfigByCategory: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findAllConfigByCategory: {
 *             data: [
 *               {
 *                 ID: 3190,
 *                 label: 'meet_principal',
 *                 description: 'Meet the principal'
 *               },
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   allStaff: {},
 *   driverLogin: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           busDriverLogin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwicGhvbmVfbnVtYmVyIjoiOTg3ODk4NzgiLCJ0eXBlIjoiYnVzX2RyaXZlcl90b2tlbiJ9.1onB32Prrsi06XNmpEMClgO0KA0zt8V0NFFim4iXbUQ'
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   getSignInOutQRCode: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getCheckInOutToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.IjVmNTBhYzIyLGNpb3QsMSI.zLbUhJaZow1THq_ASFt7uyITEpD7ThRtUZsVSZO4BM8'
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   authorisedPasscode: {},
 *   mobileVersion: {
 *     data: {
 *       minSupportVersion: '0.15.0',
 *       version: '1.0.0',
 *       downloadURL: 'https: *apps.apple.com/app/the-caterpillars-cove-kiosk/id1483028236'
 *     }
 *   },
 *   getChildrenStates: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getChildrenStates: {
 *             childStates: [
 *               {
 *                 childId: 161,
 *                 checkInTime: null,
 *                 checkOutTime: null
 *               },
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   schoolConfig: {
 *     data: {
 *       data: [
 *         {
 *           ID: 82,
 *           key: 'accept_unborn',
 *           value: '1',
 *           active: true,
 *           school: {
 *             name: 'The Caterpillar\'s Cove',
 *             ID: 3,
 *             code: 'TCC'
 *           }
 *         },
 *       ],
 *       error: null
 *     }
 *   },
 *   errorMessage: {
 *     data: {
 *       data: {
 *         message: ''
 *       },
 *       error: null
 *     }
 *   },
 *   userDetails: {
 *     data: {
 *       data: null,
 *       error: {
 *         error: 'firebase.analytics().setUserId(*) \'id\' expected a string value.'
 *       }
 *     }
 *   },
 *   showConfirmation: {
 *     data: false
 *   },
 *   listAllBusEntries: {
 *     data: {
 *       success: true,
 *       data: {
 *         listAllBusEntries: {
 *           totalCount: 1,
 *           data: [
 *             {
 *               ID: 2,
 *               label: 'Testing',
 *               direction: 'arrival',
 *               yearTime: '2020-01-01 19:29:00',
 *               fkBusDriver: 3,
 *               fkBusAttendant: 4,
 *               status: true,
 *               bus: {
 *                 ID: 2,
 *                 plateNumber: 'abv567878',
 *                 active: true
 *               },
 *               busUserByFkBusAttendant: {
 *                 ID: 4,
 *                 firstname: 'testing attandant',
 *                 lastname: null,
 *                 isAttendant: true,
 *                 passcode: '549393',
 *                 mobilePhoneCountryCode: '+65',
 *                 mobilePhone: '98789878'
 *               },
 *               busUserByFkBusDriver: {
 *                 ID: 3,
 *                 firstname: 'testing driver',
 *                 lastname: null,
 *                 isAttendant: false,
 *                 passcode: null,
 *                 mobilePhoneCountryCode: '+65',
 *                 mobilePhone: '98789876'
 *               }
 *             }
 *           ]
 *         }
 *       },
 *       error: null
 *     }
 *   },
 *   listAllBusChildren: {
 *     data: {
 *       success: true,
 *       data: {
 *         listAllBusChildren: {
 *           totalCount: 2,
 *           data: [
 *             {
 *               ID: 2,
 *               locationText: 'bangalore',
 *               remarks: 'hhjgjh',
 *               serviceStartDate: '2020-09-09 13:37:00',
 *               serviceEndDate: '2020-09-30 13:37:00',
 *               child: {
 *                 ID: 61,
 *                 firstname: '',
 *                 lastname: 'Duchamp Augustin',
 *                 imageKey: null,
 *                 dateOfBirth: '2015-10-06 00:00:00',
 *                 birthCertificate: 'G3302535T',
 *                 currentLevel: {
 *                   fkCentre: 1,
 *                   fkLevel: 20
 *                 }
 *               }
 *             }
 *           ]
 *         }
 *       },
 *       error: null
 *     }
 *   },
 *   childCheckInOut: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           childCheckInOutV2: {
 *             successes: [
 *               161,
 *               76
 *             ],
 *             failures: []
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   },
 *   customAlert: {
 *     isVisible: false,
 *     customAlertParam: {
 *       title: '',
 *       desc: '',
 *       primaryButtonText: '',
 *       cancelButtonText: ''
 *     }
 *   },
 *   commonVariableValues: {
 *     localAppVersion: '0.15.1'
 *   },
 *   _persist: {
 *     version: -1,
 *     rehydrated: true
 *   }
 * }
 * 
 */