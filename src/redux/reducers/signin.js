import {
    GET_SIGNIN_QRCODE,
    GET_SCHOOL_CONFIG
} from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   getSignInOutQRCode: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getCheckInOutToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.IjVmNGY4NzhlLGNpb3QsMiI.-9CddbYjyH1HI1-9wfzq6w4HB0MQm-dS6OfAwW2GDJ8'
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const getSignInOutQRCode = (state = {}, action) => {
    if (action.type === GET_SIGNIN_QRCODE) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};



/**
 * 
 * Example Data
 * 
 * 
 * {
 *   schoolConfig: {
 *     data: {
 *       data: [
 *         {
 *           ID: 82,
 *           key: 'accept_unborn',
 *           value: '1',
 *           active: true,
 *           school: {
 *             name: 'The Caterpillar\'s Cove',
 *             ID: 3,
 *             code: 'TCC'
 *           }
 *         },
 *       ],
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const schoolConfig = (state = {}, action) => {
    if (action.type === GET_SCHOOL_CONFIG) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};