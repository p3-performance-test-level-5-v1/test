import {
    FIND_PARENT_BY_IC,
    CHILD_CHECK_IN,
    CHILD_CHECK_OUT,
    AUTHORISE_BY_PASSCODE,
    GET_CHILDREN_STATES,
    CHILD_CHECK_IN_CHECK_OUT_V2,
    FIND_ALL_CHILD_BY_PARENT,
    PARENT_OR_GUARDIAN_SIGN_IN,
    PARENT_GUARDIAN_AUTH_TOKEN,
    UPDATE_LOADER_STATUS,
} from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   findParentByIC: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findParentByIC: {
 *             data: [
 *               {
 *                 ID: 630,
 *                 firstname: '',
 *                 lastname: 'Maverick Tok Wee Tao(Zhuo Weitao)',
 *                 childRelations: {
 *                   data: [
 *                     {
 *                       child: {
 *                         ID: 76,
 *                         firstname: '',
 *                         lastname: 'Sharmaine Faith Tok Le-En',
 *                         imageKey: 'upload/child/76/baby8-20200902-1599048581995998036-2097.jpeg',
 *                         dateOfBirth: '2019-01-02 00:00:00',
 *                         birthCertificate: 'T1900021J',
 *                         currentLevel: {
 *                           fkCentre: 2,
 *                           fkLevel: 17
 *                         }
 *                       }
 *                     }
 *                   ]
 *                 }
 *               }
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 *  
 */

export const findParentByIC = (state = {}, action) => {
    if (action.type === FIND_PARENT_BY_IC) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   childCheckIn: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           childCheckIn: true
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const childCheckIn = (state = {}, action) => {
    if (action.type === CHILD_CHECK_IN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const childCheckOut = (state = {}, action) => {
    if (action.type === CHILD_CHECK_OUT) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const authorisedPasscode = (state = {}, action) => {
    if (action.type === AUTHORISE_BY_PASSCODE) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   getChildrenStates: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getChildrenStates: {
 *             childStates: [
 *               {
 *                 childId: 161,
 *                 checkInTime: null,
 *                 checkOutTime: null
 *               },
 *               {
 *                 childId: 76,
 *                 checkInTime: null,
 *                 checkOutTime: null
 *               }
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const getChildrenStates = (state = {}, action) => {
    if (action.type === GET_CHILDREN_STATES) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};



/**
 * 
 * Example Data
 * 
 * 
 * {
 *   childCheckInOut: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           childCheckInOutV2: {
 *             successes: [
 *               161,
 *               76
 *             ],
 *             failures: []
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const childCheckInOut = (state = {}, action) => {
    if (action.type === CHILD_CHECK_IN_CHECK_OUT_V2) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const findAllChildByParent = (state = {}, action) => {
    if (action.type === FIND_ALL_CHILD_BY_PARENT) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const isGuardianSignIn = (state = {}, action) => {
    if (action.type === PARENT_OR_GUARDIAN_SIGN_IN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const authTokenParentOrGuardian = (state = {}, action) => {
    if (action.type === PARENT_GUARDIAN_AUTH_TOKEN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const isLoading = (state = {}, action) => {
    if (action.type === UPDATE_LOADER_STATUS) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};