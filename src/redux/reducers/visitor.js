import {
    FIND_ALL_CONFIG_BY_CATEGORY,
    INSERT_VISITOR_LOG,
    FIND_ALL_STAFF,
    GET_VISITOR_LOGS,
    FETCH_SELF_ASSESSMENT_FORM,
} from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   allConfigByCategory: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findAllConfigByCategory: {
 *             data: [
 *               {
 *                 ID: 3190,
 *                 label: 'meet_principal',
 *                 description: 'Meet the principal'
 *               },
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const allConfigByCategory = (state = {}, action) => {
    if (action.type === FIND_ALL_CONFIG_BY_CATEGORY) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const allStaff = (state = {}, action) => {
    if (action.type === FIND_ALL_STAFF) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const insertVisitorLog = (state = {}, action) => {
    if (action.type === INSERT_VISITOR_LOG) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const visitorLogs = (state = {}, action) => {
    if (action.type === GET_VISITOR_LOGS) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const selfAssessments = (state = {}, action) => {
    if (action.type === FETCH_SELF_ASSESSMENT_FORM) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};