import {
    BUS_DRIVER_LOGIN,
    LIST_ALL_BUS_ENTRIES,
    LIST_ALL_BUS_CHILDREN,
    BUS_ENTRY_BY_PASSCODE
} from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   driverLogin: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           busDriverLogin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwicGhvbmVfbnVtYmVyIjoiOTg3ODk4NzgiLCJ0eXBlIjoiYnVzX2RyaXZlcl90b2tlbiJ9.1onB32Prrsi06XNmpEMClgO0KA0zt8V0NFFim4iXbUQ'
 *         },
 *         error: null,
 *         loading: false
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const driverLogin = (state = {}, action) => {
    if (action.type === BUS_DRIVER_LOGIN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

/**
 * 
 * Example Data
 * 
 * 
 * {
 *   busEntry: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           busDriverLogin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwicGhvbmVfbnVtYmVyIjoiOTg3ODk4NzgiLCJ0eXBlIjoiYnVzX2RyaXZlcl90b2tlbiJ9.1onB32Prrsi06XNmpEMClgO0KA0zt8V0NFFim4iXbUQ'
 *         },
 *         error: null,
 *         loading: false
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */
export const busEntry = (state = {}, action) => {
    if (action.type === BUS_ENTRY_BY_PASSCODE) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   listAllBusEntries: {
 *     data: {
 *       success: true,
 *       data: {
 *         listAllBusEntries: {
 *           totalCount: 1,
 *           data: [
 *             {
 *               ID: 2,
 *               label: 'Testing',
 *               direction: 'arrival',
 *               yearTime: '2020-01-01 19:29:00',
 *               fkBusDriver: 3,
 *               fkBusAttendant: 4,
 *               status: true,
 *               bus: {
 *                 ID: 2,
 *                 plateNumber: 'abv567878',
 *                 active: true
 *               },
 *               busUserByFkBusAttendant: {
 *                 ID: 4,
 *                 firstname: 'testing attandant',
 *                 lastname: null,
 *                 isAttendant: true,
 *                 passcode: '549393',
 *                 mobilePhoneCountryCode: '+65',
 *                 mobilePhone: '98789878'
 *               },
 *               busUserByFkBusDriver: {
 *                 ID: 3,
 *                 firstname: 'testing driver',
 *                 lastname: null,
 *                 isAttendant: false,
 *                 passcode: null,
 *                 mobilePhoneCountryCode: '+65',
 *                 mobilePhone: '98789876'
 *               }
 *             }
 *           ]
 *         }
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const listAllBusEntries = (state = {}, action) => {
    if (action.type === LIST_ALL_BUS_ENTRIES) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};



/**
 * 
 * Example Data
 * 
 * 
 * {
 *   listAllBusChildren: {
 *     data: {
 *       success: true,
 *       data: {
 *         listAllBusChildren: {
 *           totalCount: 1,
 *           data: [
 *             {
 *               ID: 2,
 *               locationText: 'bangalore',
 *               remarks: 'hhjgjh',
 *               serviceStartDate: '2020-09-09 13:37:00',
 *               serviceEndDate: '2020-09-30 13:37:00',
 *               child: {
 *                 ID: 61,
 *                 firstname: '',
 *                 lastname: 'Duchamp Augustin',
 *                 imageKey: null,
 *                 dateOfBirth: '2015-10-06 00:00:00',
 *                 birthCertificate: 'G3302535T',
 *                 currentLevel: {
 *                   fkCentre: 1,
 *                   fkLevel: 20
 *                 }
 *               }
 *             }
 *           ]
 *         }
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const listAllBusChildren = (state = {}, action) => {
    if (action.type === LIST_ALL_BUS_CHILDREN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};