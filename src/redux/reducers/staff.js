import {
    GET_STAFF_LIST,
    GET_STAFF_HEALTH_RECORDS,
    GET_STAFF_MISSING_DATES_RANGE,
    GET_STAFF_MISSING_RECORDS_REASON,
    GET_STAFF_CICO_RECORDS,
    LOGIN_BY_PIN
} from '../actions/actionTypes';

export const fetchStaffList = (state = {}, action) => {
    if (action.type === GET_STAFF_LIST) {
        return Object.assign({}, state, {
            data: action.value.data
        });
    }
    return state;
};


export const getStaffHealthRecord = (state = {}, action) => {
    if (action.type === GET_STAFF_HEALTH_RECORDS) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const getStaffMissingDateRange = (state = {}, action) => {
    if (action.type === GET_STAFF_MISSING_DATES_RANGE) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const getMissingRecordReason = (state = {}, action) => {
    if (action.type === GET_STAFF_MISSING_RECORDS_REASON) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const getStaffCheckInOutRecords = (state = {}, action) => {
    if (action.type === GET_STAFF_CICO_RECORDS) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const loginByPin = (state = {}, action) => {
    if (action.type === LOGIN_BY_PIN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};
