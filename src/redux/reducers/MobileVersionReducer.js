import { get } from 'lodash';
import {
  CHECK_MOBILE_VERSION,
} from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   mobileVersion: {
 *     data: {
 *       minSupportVersion: '0.15.0',
 *       version: '1.0.0',
 *       downloadURL: 'https: *apps.apple.com/app/the-caterpillars-cove-kiosk/id1483028236'
 *     }
 *   }
 * }
 * 
 */

export const mobileVersion = (state = {}, action) => {
  if (action.type === CHECK_MOBILE_VERSION) {
    return Object.assign({}, state, {
      data: get(action, 'value.data.data.getLatestMobileVersion', {})
    });
  }
  return state;
};