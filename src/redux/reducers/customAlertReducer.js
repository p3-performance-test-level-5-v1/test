import { SHOW_CUSTOM_ALERT } from '../actions/actionTypes';

const INITIAL_STATE = {
  isVisible: false,
  customAlertParam: {
    title: '',
    desc: '',
    primaryButtonText: '',
    cancelButtonText: '',
    primaryButtonCallback: () => {},
    cancelButtonCallback: () => {},
  },
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   customAlert: {
 *     isVisible: false,
 *     customAlertParam: {
 *       title: '',
 *       desc: '',
 *       primaryButtonText: '',
 *       cancelButtonText: ''
 *     }
 *   }
 * }
 * 
 */

export const customAlert = (state = INITIAL_STATE, action) => {
  if (action.type === SHOW_CUSTOM_ALERT) {
    return {
      isVisible: action.payload.isVisible,
      customAlertParam: {
        ...action.payload.customAlertParam,
      },
    };
  }
  return state;
};
