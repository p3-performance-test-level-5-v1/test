import { combineReducers } from 'redux';
import {
    listAllCentre,
    centreLogin,
    checkInOutToken,
    errorMessage,
    userDetails,
    showConfirmation,
    getCentreHoliday
} from './centre';
import { 
    getSignInOutQRCode,
    schoolConfig 
} from './signin';
import {
	findParentByIC,
	childCheckIn,
    authorisedPasscode,
    getChildrenStates,
    childCheckInOut,
    findAllChildByParent,
    isGuardianSignIn,
    authTokenParentOrGuardian,
    isLoading,
} from './children';
import {
	allConfigByCategory,
    allStaff,
    visitorLogs,
    selfAssessments,
} from './visitor';
import {
    driverLogin,
    listAllBusEntries,
    listAllBusChildren,
    busEntry
} from './busDriver';
import { 
  mobileVersion
} from './MobileVersionReducer';

import { 
  customAlert
} from './customAlertReducer';
import {
    commonVariableValues
} from './commonVariableReducer';
import {
    fetchStaffList,
    getStaffHealthRecord,
    getStaffMissingDateRange,
    getMissingRecordReason,
    getStaffCheckInOutRecords,
    loginByPin,
} from './staff';

export default combineReducers({
    listAllCentre,
    checkInOutToken,
    centreLogin,
    findParentByIC,
	childCheckIn,
    allConfigByCategory,
    allStaff,
    driverLogin,
    getSignInOutQRCode,
    authorisedPasscode,
    mobileVersion,
    getChildrenStates,
    schoolConfig,
    errorMessage,
    userDetails,
    showConfirmation,
    listAllBusEntries,
    listAllBusChildren,
    childCheckInOut,
    customAlert,
    commonVariableValues,
    visitorLogs,
    selfAssessments,
    getStaffHealthRecord,
    getStaffMissingDateRange,
    getMissingRecordReason,
    getStaffCheckInOutRecords,
    fetchStaffList,
    loginByPin,
    findAllChildByParent,
    getCentreHoliday,
    isGuardianSignIn,
    authTokenParentOrGuardian,
    isLoading,
    busEntry
});
