import {
    GET_ALL_CENTRES, 
    GET_CHECK_IN_OUT_TOKEN,
    CENTRE_LOGIN,
    SHOW_ERROR_MESSAGE,
    FETCH_USER_DETAILS,
    SIGN_OUT_CONFIRMATION,
    GET_CENTRE_HOLIDAYS_OF_YEAR
} from '../actions/actionTypes';
import { get } from 'lodash';

/**
 * 
 * Example Data
 * 
 * 
 * {
 *   listAllCentre: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           findAllCentreForSchool: {
 *             data: [
 *               {
 *                 ID: 1,
 *                 label: 'The Caterpillar\'s Cove @ Jurong East',
 *                 fkSchool: 3
 *               },
 *               {
 *                 ID: 2,
 *                 label: 'The Caterpillar\'s Cove @ Ngee Ann Polytechnic',
 *                 fkSchool: 3
 *               }
 *             ]
 *           }
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 */

export const listAllCentre = (state = {}, action) => {
    if (action.type === GET_ALL_CENTRES) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   centreLogin: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           centreLogin: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiYXV0aG9yaXphdGlvbl90b2tlbiIsImlkIjo5MTYsImVtYWlsIjoicHJpbmNpcGFsX3RjY19jMUBleGFtcGxlLmNvbSIsImV4cGlyZV9hdCI6IjIwMjAtMTAtMDJUMTg6NTk6MTMuMjI4MTM2MjAxKzA4OjAwIiwiaXNzdWVkX2F0IjoiMjAyMC0wOS0wMlQxODo1OToxMy4yMjgxMzcwODIrMDg6MDAifQ.Vyp0oO_wPgZ_4PtTnFExglnumVlXWuhpMW8QUn96yoM'
 *         },
 *         error: null
 *       },
 *       centreId: 2
 *     },
 *     centreId: 2
 *   }
 * }
 * 
 */

export const centreLogin = (state = {}, action) => {
    const data = get(action, 'value.data', {});
    if ((action.type === CENTRE_LOGIN) && data.centreId) {
        return Object.assign({}, state, { data, centreId: data.centreId });
    }
    return state;
};


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   checkInOutToken: {
 *     data: {
 *       data: {
 *         success: true,
 *         data: {
 *           getCheckInOutToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.IjVmNTA5NzgyLGNpb3QsMiI.ZjqTylV-v9zaqgbcZh4ZPP-O8Ay3sV4yLVW2Zgn4leQ'
 *         },
 *         error: null
 *       },
 *       error: null
 *     }
 *   }
 * }
 * 
 * 
 */


export const checkInOutToken = (state = {}, action) => {
    if (action.type === GET_CHECK_IN_OUT_TOKEN) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const errorMessage = (state = {}, action) => {
    if (action.type === SHOW_ERROR_MESSAGE) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
};

export const userDetails = (state = {}, action) => {
    if (action.type === FETCH_USER_DETAILS) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
}


/**
 * 
 * Example Data
 * 
 * 
 * {
 *   showConfirmation: {
 *     data: false
 *   }
 * }
 * 
 */

export const showConfirmation = (state = {}, action) => {
    if (action.type === SIGN_OUT_CONFIRMATION) {
        return Object.assign({}, state, {
            data: action.value
        });
    }
    return state;
}

export const getCentreHoliday = (state = {}, action) => {
    if (action.type === GET_CENTRE_HOLIDAYS_OF_YEAR) {
        return Object.assign({}, state, {
            data: action.value.data
        });
    }
    return state;
}
