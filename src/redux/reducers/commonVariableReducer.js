import { SET_VARIABLE_TO_REDUX } from '../actions/actionTypes';


/**
 * 
 * Example Data
 * 
 * {
 *   commonVariableValues: {
 *     localAppVersion: '0.18.0'
 *   }
 * }
 * 
 */

export const commonVariableValues = (state = {}, action) => {
  if (action.type === SET_VARIABLE_TO_REDUX) {
    return { ...state, ...action.value };
  }
  return state;
};
