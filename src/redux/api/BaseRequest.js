import axios from 'axios';
import axiosRetry from 'axios-retry';
import { checkIfJson, firebaseLogEvent, handleResponse } from '../../utils';
import { BASE_URL } from '../config';
import { getUserAgent, storeAsyncStorage, retrieveAsyncStorage } from '../../utils/index';
import Constant from '../../constants/Constant';
import _ from 'lodash';
import { autoLogin } from '../actions/Centre'
import { store } from '../../store/store';
import Base64 from 'base-64';

const ADDITIONAL_HEADERS = {};

// This singleton can be used as a store for api client.
export const ApolloStore = Object.freeze({
    setAdditionalHeader: async (key, value) => ADDITIONAL_HEADERS[key] = await value,
    getAdditionalHeaders: () => ADDITIONAL_HEADERS
});


export const skRequest = async (config, isAuth = false, retryCount = 0) => {
    const headers = {
        ...ApolloStore.getAdditionalHeaders()
    };
    let userToken = config.userToken || null;

    if (userToken) {
        headers.Authorization = `Bearer ${userToken}`;
    } else if (config.accessToken) {
        headers.Authorization = `Bearer ${config.accessToken}`;
    }

    const refreshToken = async (logCred) => {
        try {
            const data = await store.dispatch(autoLogin(logCred.userMail, Base64.decode(logCred.userPass)));
            if (data) {
                const accessToken = _.get(data, 'data.centreLogin');
                if (accessToken) {
                    await storeAsyncStorage('accessToken', accessToken);
                }
                return data;
            }
        }
        catch (err) {
            firebaseLogEvent(false, logCred.userMail, _.get(err, 'message'))
        }
    }

    const userAgent = await getUserAgent();
    headers['user-agent'] = userAgent;

    if (config.method === 'POST') {
        headers['content-type'] = 'application/json';
        config.data = JSON.stringify(config.data);
        config.timeout = 10000;
    }

    if (!config.url) {
        config.url = BASE_URL;
    }

    if (!config.method) {
        config.method = 'GET';
    }

    config.headers = headers;

    axiosRetry(axios, {
        retries: Constant.RETRY_ATTEMPT,
        retryCondition: e => {
            return axiosRetry.isNetworkOrIdempotentRequestError(e)
                || !e.response
                || e.message.toLowerCase() === Constant.NETWORK_ERROR_TEXT
        },
        retryDelay: retryCount => {
            return retryCount * Constant.RETRY_INITIAL_DELAY;
        }
    });

    try {
        const resp = await axios(config);
        const isTokenExpired = _.get(resp, 'data.errors[0].message') === 'unauthorized' ||
            _.get(resp, 'data.errors[0].message') === 'no role found' ||
            _.get(resp, 'data.errors[0].message') === 'invalid or expired token';

        if (isTokenExpired && isAuth) {
            const userData = await retrieveAsyncStorage('loginCred');
            const logCred = checkIfJson(userData);
            const result = await refreshToken(logCred.userMail);
            if (_.get(result, 'success')) {
                firebaseLogEvent(true, logCred.userMail);
            }
            else {
                firebaseLogEvent(false, logCred.userMail, _.get(resp, 'data.errors[0].message'))
            }
            return handleResponse(result);
        }
        return handleResponse(resp);
    } catch (ex) {
        if (retryCount < 3) {
            return skRequest(config, isAuth, retryCount = retryCount + 1)
        }
        return handleResponse({
            data: {
                errors: [ex.message]
            }
        });
    }
};