import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';

import { skRequest } from './BaseRequest';
import {
    MutationBuilder,
} from '../queryBuilder';

const _mQueryBuilder = new MutationBuilder();
_mQueryBuilder.setNewMutation('getStaffCheckinUploadURL', {
    Url: 'string',
    Key: 'string',
});

_mQueryBuilder.setNewMutation('staffCheckIn', {});
_mQueryBuilder.setNewMutation('staffCheckOut', {});

export const getStaffCheckinUploadURL = async (reqData, userToken) => {
    const query = _mQueryBuilder.getMutationAsString('getStaffCheckinUploadURL', reqData);
    try {
        const data = await skRequest({
            data: {
                query
            },
            userToken: userToken,
            method: 'POST'
        });
        if (data.success) {
            return data;
        } else {
            return null;
        }
    } catch (ex) {
        return null;
    }
};

// Do CheckIn or CheckOut, based on input parameter
export const staffCheckIn = async (IDUser, imageKey, checkinToken, userToken, isCheckInOrCheckOut = true, currentDatetime) => {
    const queryType = isCheckInOrCheckOut ? 'staffCheckIn' : 'staffCheckOut';
    let query = null;

    if (isCheckInOrCheckOut) {
      query = _mQueryBuilder.getMutationAsString(queryType, {
        IDUser: IDUser,
        imageKey: imageKey,
        token: checkinToken,
        date: currentDatetime
      });
    } else {
      query = _mQueryBuilder.getMutationAsString(queryType, {
        IDUser: IDUser,
        token: checkinToken,
      });
    }

    
    try {
        const data = await skRequest({
            data: {
                query
            },
            userToken: userToken,
            method: 'POST'
        });
        return data;
    } catch (ex) {
        return null;
    }
};

export async function s3FileUploadBlob(s3Url, fileRef) {
    let result = null;

    await RNFetchBlob.fetch('PUT', s3Url, {
        'Content-Type' : "image/jpeg",
    }, RNFetchBlob.wrap(fileRef.replace('file://', '')))
    .then((res) => {
        result = res;
    })
    .catch((err) => {
        result = err;
    })

    return result;
};