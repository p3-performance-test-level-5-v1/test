import { skRequest } from '../api/BaseRequest';
import { QueryBuilder, MutationBuilder } from '../queryBuilder';
import {
	BUS_DRIVER_LOGIN,
	LIST_ALL_BUS_ENTRIES,
	LIST_ALL_BUS_CHILDREN,
	BUS_ENTRY_BY_PASSCODE
} from './actionTypes';
import _ from 'lodash';
import Sentry from '../../sentry';
import {
	retrieveAsyncStorage,
	handleErrorDataFromApi,
	errorResponse
} from '../../utils';
import { updateErrorMessage } from '../actions';
import moment from 'moment';
import { TIME_FORMAT } from '../../constants/Constant';
const _mQueryBuilder = new QueryBuilder();
const _mutationBuilder = new MutationBuilder();

_mQueryBuilder.setNewQuery('getBusEntryByPassCode', {
	ID: 'string',
	label: 'string',
	fkBusAttendant: 'number',
	direction: 'string',
	bus: {
		ID: 'number',
		plateNumber: 'string',
		active: 'Boolean'
	}
});

_mQueryBuilder.setNewQuery('listAllBusEntries', {
	totalCount: 'number',
	data: {
		ID: 'number',
		label: 'string',
		direction: 'BusEntryDirection',
		yearTime: 'Datetime',
		fkBusDriver: 'number',
		fkBusAttendant: 'number',
		status: 'Boolean',
		bus: {
			ID: 'number',
			plateNumber: 'string',
			active: 'Boolean'
		},
		busUserByFkBusAttendant: {
			ID: 'number',
			firstname: 'string',
			lastname: 'number',
			isAttendant: 'Boolean',
			passcode: 'string',
			mobilePhoneCountryCode: 'string',
			mobilePhone: 'string'
		},
		busUserByFkBusDriver: {
			ID: 'number',
			firstname: 'string',
			lastname: 'number',
			isAttendant: 'Boolean',
			passcode: 'string',
			mobilePhoneCountryCode: 'string',
			mobilePhone: 'string'
		}
	}
});

const setListAllBusChildrenQuery = (
	date = moment().format(TIME_FORMAT.DATE)
) => {
	_mQueryBuilder.setNewQuery('listAllBusChildren', {
		totalCount: 'number',
		data: {
			ID: 'number',
			locationText: 'string',
			remarks: 'string',
			serviceStartDate: 'Datetime',
			serviceEndDate: 'Datetime',
			child: {
				ID: 'number',
				firstname: 'string',
				lastname: 'string',
				imageKey: 'string',
				dateOfBirth: 'Datetime',
				birthCertificate: 'string',
				currentLevel: {
					fkCentre: 'number',
					fkLevel: 'number'
				},
				[`checkInOuts(filter: {date: "${date}"})`]: {
					data: {
						ID: 'number',
						date: 'string',
						time: 'string',
						type: 'string',
						status: 'string'
					}
				}
			}
		}
	});
};

_mQueryBuilder.setNewQuery('busDriverLogin', {});

_mutationBuilder.setNewMutation('busDriverAddChildCheckInCheckOut', {});

const dispatchBusLogin = (
	dispatch,
	{ data = {}, error = null, loading = false } = {}
) => {
	dispatch({
		type: BUS_DRIVER_LOGIN,
		value: {
			data,
			error,
			loading
		}
	});
};

export const busLogin = ({
	reqData,
	accessToken = '',
	onSignSuccessCallback = () => {}
}) => async (dispatch) => {
	const query = _mQueryBuilder.getQueryAsString('busDriverLogin', reqData);
	const params = {
		data: {
			query
		},
		userToken: accessToken,
		method: 'POST'
	};
	dispatchBusLogin(dispatch, { loading: true });
	try {
		const data = await skRequest(params);
		if (data.success) {
			dispatchBusLogin(dispatch, { data, loading: false });
		} else {
			dispatchBusLogin(dispatch, {
				data: null,
				error: data.error,
				loading: false
			});
			Sentry.log(_.get(data, 'error[0].message'), params);
		}

		onSignSuccessCallback(data);
		return data;
	} catch (ex) {
		dispatchBusLogin(dispatch, null, { error: ex.message });
		Sentry.log(ex.message);
	}
};

const dispatchBusEntry = (dispatch, data = {}) => {
	dispatch({
		type: BUS_ENTRY_BY_PASSCODE,
		value: data
	});
};

export const busEntryByPassCode = (reqData) => async (dispatch) => {
	try {
		const accessToken = await retrieveAsyncStorage('accessToken');
		const query = _mQueryBuilder.getQueryAsString(
			'getBusEntryByPassCode',
			reqData
		);
		const params = {
			data: {
				query
			},
			userToken: accessToken,
			method: 'POST'
		};
		const data = await skRequest(params);
		dispatchBusEntry(dispatch, data);

		if (!data.success) {
			Sentry.log(_.get(data, 'error[0].message'), params);
		}
		return data;
	} catch (ex) {
		Sentry.log(ex.message);
	}
};

const dispatchListAllBusEntries = (dispatch, data = {}) => {
	dispatch({
		type: LIST_ALL_BUS_ENTRIES,
		value: data
	});
};

export const listAllBusEntries = (reqData, accessToken) => async (dispatch) => {
	const query = _mQueryBuilder.getQueryAsString('listAllBusEntries', reqData);
	const params = {
		data: {
			query
		},
		userToken: accessToken,
		method: 'POST'
	};
	try {
		const data = await skRequest(params);

		dispatchListAllBusEntries(dispatch, data);
		if (!data.success) {
			Sentry.log(_.get(data, 'error[0].message'), params);
		}
		return data;
	} catch (ex) {
		Sentry.log(ex.message);
	}
};

const dispatchListAllBusChildren = (dispatch, data = {}) => {
	dispatch({
		type: LIST_ALL_BUS_CHILDREN,
		value: data
	});
};

export const listAllBusChildren = (reqData, accessToken) => async (
	dispatch
) => {
	dispatchListAllBusChildren(dispatch, { loading: true });
	setListAllBusChildrenQuery(); // init query
	const query = _mQueryBuilder.getQueryAsString(
		'listAllBusChildren',
		reqData
	);
	const params = {
		data: {
			query
		},
		userToken: accessToken,
		method: 'POST'
	};
	try {
		const data = await skRequest(params);
		dispatchListAllBusChildren(dispatch, { ...data, loading: false });
		if (!data.success) {
			Sentry.log(_.get(data, 'error[0].message'), params);
		}
		return data;
	} catch (ex) {
		dispatchListAllBusChildren(dispatch, { loading: false });
		Sentry.log(ex.message);
	}
};

export const busAddChildCheckInOut = (
	reqData,
	successCallback = () => {}
) => async (dispatch) => {
	try {
		const accessToken = await retrieveAsyncStorage('accessToken');
		const query = _mutationBuilder.getMutationAsString(
			'busDriverAddChildCheckInCheckOut',
			reqData
		);
		const params = {
			data: {
				query
			},
			userToken: accessToken,
			method: 'POST'
		};

		const data = await skRequest(params);
		const message = handleErrorDataFromApi(data, errorResponse);
		if (message) dispatch(updateErrorMessage(message));
		if (!data.success) {
			Sentry.log(_.get(data, 'error[0].message'), params);
		} else {
			successCallback();
		}
		return data;
	} catch (ex) {
		Sentry.log(ex.message);
	}
};
