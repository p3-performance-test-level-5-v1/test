import { skRequest } from '../api/BaseRequest';
import Base64 from 'base-64';
import {
    QueryBuilder,
    MutationBuilder
} from '../queryBuilder';
import {
    GET_ALL_CENTRES,
    GET_CHECK_IN_OUT_TOKEN,
    SHOW_ERROR_MESSAGE,
    CENTRE_LOGIN,
    FETCH_USER_DETAILS,
    SIGN_OUT_CONFIRMATION,
    GET_CENTRE_HOLIDAYS_OF_YEAR
} from './actionTypes';
import {
    storeAsyncStorage,
    retrieveAsyncStorage,
    clearAll
} from '../../utils';
import _ from 'lodash';
import analytics from '@react-native-firebase/analytics';
import Sentry from '../../sentry';
import moment from 'moment';

const _mQueryBuilder = new QueryBuilder();
_mQueryBuilder.setNewQuery('findAllCentreForSchool', {
    data: {
        ID: 'number',
        label: 'string',
        fkSchool: 'number'
    }
});
_mQueryBuilder.setNewQuery('centreLogin', {});
_mQueryBuilder.setNewQuery('getCheckInOutToken', {});
_mQueryBuilder.setNewQuery('me', {
    ID: 'number',
    firstname: 'string',
    lastname: 'string',
    email: 'string',
    userAccessControls: {
        data: {
            school: {
                ID: 'number',
                name: 'string'
            }
        }
    }
});

_mQueryBuilder.setNewQuery('getCentreHolidaysOfYear', {
    data: {
        from: "string",
        to: "string"
    }
});

const dispatchCentre = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_ALL_CENTRES,
        value: {
            data,
            error
        }
    });
};

const dispatchCentreLogin = (dispatch, data = {}, error = null) => {
    dispatch({
        type: CENTRE_LOGIN,
        value: {
            data,
            error
        }
    });
};

const dispatchGetCheckInOutToken = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_CHECK_IN_OUT_TOKEN,
        value: {
            data,
            error
        }
    });
};

const dispatchShowErrorMessage = (dispatch, data = {}, error = null) => {
    dispatch({
        type: SHOW_ERROR_MESSAGE,
        value: {
            data,
            error
        }
    });
};

const dispatchUserDetails = (dispatch, data = {}, error = null) => {
    dispatch({
        type: FETCH_USER_DETAILS,
        value: {
            data,
            error
        }
    });
}

const dispatchGetCentreHolidaysOfYear = (dispatch, data = [], error = null) => {
    dispatch({
        type: GET_CENTRE_HOLIDAYS_OF_YEAR,
        value: {
            data,
            error
        }
    });
}

export const listAllCentre = reqData => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('findAllCentreForSchool', reqData);
    const params = {
        data: {
            query
        },
        method: 'POST'
    };
    dispatchCentre(dispatch);
    try {
        const data = await skRequest(params);
        if (data.success) {
            dispatchCentre(dispatch, data);
        } else {
            dispatchCentre(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchCentre(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const centreLogin = (reqData, navigation = {}) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('centreLogin', reqData);
    const params = {
        data: {
            query
        },
        method: 'POST'
    };
    dispatchCentreLogin(dispatch);
    try {
        const data = await skRequest(params);

        if (data.success) {
            const newData = Object.assign({}, { data: data }, { centreId: reqData.IDCentre });
            const loginCred = { userMail: reqData.email, userPass: Base64.encode(reqData.password) }
            const accessToken = _.get(data, 'data.centreLogin', '');
            await storeAsyncStorage('centreId', reqData.IDCentre);
            await storeAsyncStorage('accessToken', accessToken);
            await storeAsyncStorage('loginCred', JSON.stringify(loginCred));
            await dispatch(fetchUserDetails(navigation));
            dispatchCentreLogin(dispatch, newData);
        } else {
            Sentry.log(_.get(data, 'error[0].message'), params);
        }

        return data;
    } catch (ex) {
        dispatchCentreLogin(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const autoLogin = (email, password) => async (dispatch) => {
    try {
        const centreId = await retrieveAsyncStorage('centreId');
        const reqData = {
            IDCentre: parseInt(centreId, 10),
            email: email,
            password: password
        }
        const data = await dispatch(centreLogin(reqData));
        return data
    } catch (ex) {
        dispatchCentreLogin(dispatch, null, {error: ex.message});
        Sentry.log(ex.message);
    }
}

export const getCheckInOutToken = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('getCheckInOutToken', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchGetCheckInOutToken(dispatch);
    try {
        const data = await skRequest(params, true);

        if (data.success) {
            dispatchGetCheckInOutToken(dispatch, data);
        } else {
            const { error } = data;
            dispatchGetCheckInOutToken(dispatch, null, error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchGetCheckInOutToken(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const redirectByAuthStatus = navigation => async () => {
  const accessToken = await retrieveAsyncStorage('accessToken');
  if (accessToken) {
    navigation.navigate('Home');
  } else {
    navigation.navigate('SchoolLogin');
  }
};

export const fetchUserDetails = (navigation = {}) => async dispatch => {
    const query = _mQueryBuilder.getQueryAsString('me');
    dispatchUserDetails(dispatch)
    try {
        const accessToken = await retrieveAsyncStorage('accessToken');
        const params = {
            data: {
                query
            },
            userToken: accessToken,
            method: 'POST'
        }
        const data = await skRequest(params, true);
        if (data.success) {
            dispatchUserDetails(dispatch, data)
            const schoolID = _.get(data, 'data.me.userAccessControls.data[0].school.ID','');
            await storeAsyncStorage('schoolId', schoolID);
            const user_id = await _.get(data, 'data.me.ID', '');
            analytics().setUserId(user_id);
        }
        return data;
    } catch (ex) {
        dispatchUserDetails(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
        return ex;
    }
};

export const signOut =  async (navigation) => {
    await clearAll();
    navigation.navigate('SchoolLogin');
};

export const updateErrorMessage = (message = '', retryAction = null) => dispatch => {
    const isTimeOut = message.indexOf('timeout of');
    const isNetworkError = message.indexOf('Network Error');
    if (isTimeOut >= 0 || isNetworkError >= 0 || !message) {
        const payload = { message, action: retryAction };
        return dispatchShowErrorMessage(dispatch, payload);
    } else if (message) {
        alert(message);
    }
}

const dispatchShowSignOutConfirmation = (dispatch, data = {}) => {
    dispatch({
        type: SIGN_OUT_CONFIRMATION,
        value: data
    });
}

export const showSignOutConfirmation = showConfirmation => dispatch => {
    return dispatchShowSignOutConfirmation(dispatch, !showConfirmation);
}

export const getCentreHolidaysOfYear = () => async (dispatch) => {
    try {

    
      const fkSchool = await retrieveAsyncStorage('schoolId');
      const accessToken = await retrieveAsyncStorage('accessToken');
      const centreId = await retrieveAsyncStorage('centreId');
  
      const reqParams = {
        year: moment().year(),
        IDCentres: [Number(centreId)],
        filter: {
          fkSchool: Number(fkSchool),
        },
      };
  
      const query = _mQueryBuilder.getQueryAsString('getCentreHolidaysOfYear', reqParams);
      const result = await skRequest({
        data: {
          query,
        },
        userToken: accessToken,
        method: 'POST',
      });

      const data = _.get(result, 'data.getCentreHolidaysOfYear.data', [])
  
      dispatchGetCentreHolidaysOfYear(dispatch, data);
  
      return result;
    } catch (ex) {
      Sentry.log(ex.message);
    }
  };
  
