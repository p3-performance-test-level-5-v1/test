import { NavigationActions } from "react-navigation";
import { skRequest } from '../api/BaseRequest';
import {
    QueryBuilder,
} from '../queryBuilder';
import {
    GET_SIGNIN_QRCODE,
    GET_SCHOOL_CONFIG
} from './actionTypes';
import _ from 'lodash';
import SentryHandler from '../../sentry';
import * as Sentry from '@sentry/react-native';
import { get } from 'lodash'
 
const _mQueryBuilder = new QueryBuilder();
_mQueryBuilder.setNewQuery('getCheckInOutToken', {});
_mQueryBuilder.setNewQuery('findAllSchoolConfig', {
    data: {
        ID: 'number',
        key: 'string',
        value: 'string',
        active: 'Boolean',
        school: {
            name: 'string',
            ID: 'number',
            code: 'string'
        }
    }
});

const dispatchFetchCheckInCheckOutToken = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_SIGNIN_QRCODE,
        value: {
            data,
            error
        }
    });
};

const dispatchGetSchoolConfig = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_SCHOOL_CONFIG,
        value: {
            data,
            error
        }
    });
};

export const fetchCheckInCheckOutToken = (reqData, userToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('getCheckInOutToken', reqData);
    try {
        const params = {
            data: {
                query
            },
            userToken: userToken,
            method: 'POST'
        }
        const data = await skRequest(params, true);
        if (data.success) {
            dispatchFetchCheckInCheckOutToken(dispatch, data);
        } else {
            const err = _.get(data, 'error[0]') || _.get(data, 'error');
            SentryHandler.log(err, Sentry.Severity.Error);
        }
        return data;
    } catch (ex) {
        dispatchFetchCheckInCheckOutToken(dispatch, null, { error: ex.message });
        SentryHandler.log(ex.message);
    }
};

export const getSchoolConfig = (config) =>  async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('findAllSchoolConfig',config);
    try {
        const data = await skRequest({
            data: {
                query
            },
            method: 'POST'
        });
        const result = get(data, 'data.findAllSchoolConfig.data', {})
        dispatchGetSchoolConfig(dispatch,result);
        return data;
    } catch(e) {
        SentryHandler.log(e.message);
        return e;
    }
}