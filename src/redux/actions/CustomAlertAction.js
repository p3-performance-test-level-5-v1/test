import { SHOW_CUSTOM_ALERT } from './actionTypes';

const dispatchCustomAlertAction = (dispatch, payload) => {
  dispatch({
    type: SHOW_CUSTOM_ALERT,
    payload,
  });
};

export const showCustomAlert = (
  isVisible,
  customAlertParam
) => async dispatch => {
  const payload = {
    isVisible,
    customAlertParam,
  };

  // hide alerts if the dispatch is to open a new alert.
  if (isVisible) {
    dispatchCustomAlertAction(dispatch, {
      isVisible: false,
      customAlertParam: {},
    });
  }

  dispatchCustomAlertAction(dispatch, payload);
};
