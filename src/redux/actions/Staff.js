import { skRequest } from '../api/BaseRequest';
import { QueryBuilder, MutationBuilder } from '../queryBuilder';
import {
  GET_STAFF_HEALTH_RECORDS,
  GET_STAFF_LIST,
  ADD_STAFF_HEALTH_RECORD,
  GET_STAFF_MISSING_DATES_RANGE,
  GET_STAFF_MISSING_RECORDS_REASON,
  GET_STAFF_CICO_RECORDS,
  ADD_STAFF_MISSING_RECORDS,
  LOGIN_BY_PIN,
  ADD_STAFF_CHECK_IN_CHECK_OUT,
  CHECK_STAFF_HAS_MISSING_RECORD
} from './actionTypes';
import { retrieveAsyncStorage } from '../../utils';
import _, { get } from 'lodash';
import Sentry from '../../sentry';

const mutationBuilder = new MutationBuilder();
const _mQueryBuilder = new QueryBuilder();

_mQueryBuilder.setNewQuery('loginByPin', {});

_mQueryBuilder.setNewQuery('findAllStaff', {
  data: {
    ID: 'number',
    staffID: 'number',
    userByFkUser: {
      ID: 'string',
      lastname: 'string',
      firstname: 'string',
      email: 'string',
      imageKey: 'string',
    },
  },
});

_mQueryBuilder.setNewQuery('getStaffHealthRecords', {
  Staff: {
    ID: 'number',
    fkUser: 'number',
    fkSchool: 'number',
  },
  HealthCheckRecords: {
    ID: 'number',
    value: 'number',
    time: 'string',
    date: 'string',
  },
});

_mQueryBuilder.setNewQuery('getStaffMissingDatesRange', {
  from: 'string',
  to: 'string',
});

_mQueryBuilder.setNewQuery('findAllConfigByCategory', {
  data: {
    ID: 'number',
    label: 'string',
    description: 'string',
  },
});

_mQueryBuilder.setNewQuery('getStaffCheckInOutRecords', {
  data: {
    time: 'string',
    date: 'date',
    type: 'string',
  },
});

_mQueryBuilder.setNewQuery('hasMissingRecord', {});

mutationBuilder.setNewMutation('staffCheckInOutV3', {});

mutationBuilder.setNewMutation('addStaffHealthRecordsAt', {
  Staff: {
    ID: 'number',
  },
});

mutationBuilder.setNewMutation('createMissingCheckInOutForStaff', {
  ID: 'number',
});

mutationBuilder.setNewMutation('addStaffCheckInCheckOut', {});

const dispatchFetchStaffList = (dispatch, data = [], error = null) => {
  dispatch({
    type: GET_STAFF_LIST,
    value: {
      data,
      error,
    },
  });
};

const dispatchGetStaffMissingDatesRange = (dispatch, data = {}) => {
  dispatch({
    type: GET_STAFF_MISSING_DATES_RANGE,
    value: data,
  });
};

const dispatchGetStaffMissingRecordsReason = (dispatch, data = {}) => {
  dispatch({
    type: GET_STAFF_MISSING_RECORDS_REASON,
    value: data,
  });
};

const dispatchGetStaffCICORecords = (dispatch, data = {}) => {
  dispatch({
    type: GET_STAFF_CICO_RECORDS,
    value: data,
  });
};

const dispatchAddStaffMissingRecords = (dispatch, data = {}) => {
  dispatch({
    type: ADD_STAFF_MISSING_RECORDS,
    value: data,
  });
};

const dispatchLoginByPin = (dispatch, data = {}) => {
  dispatch({
    type: LOGIN_BY_PIN,
    value: data,
  });
};

const dispatchAddStaffCheckInCheckOut = (dispatch, data = {}) => {
  dispatch({
    type: ADD_STAFF_CHECK_IN_CHECK_OUT,
    value: data,
  });
};

const dispatchCheckStaffHasMissingRecord = (dispatch, data = {}) => {
  dispatch({
    type: CHECK_STAFF_HAS_MISSING_RECORD,
    value: data,
  });
};

export const findAllStaff = (userToken, centreId, centreStaff = false) => async (dispatch) => {
  const query = _mQueryBuilder.getQueryAsString('findAllStaff', { centre: centreId, centreStaffOnly: centreStaff });
  try {
    const data = await skRequest({
      data: {
        query,
      },
      userToken: userToken,
      method: 'POST',
    });
    if (data.success) {
      dispatchFetchStaffList(dispatch, get(data, 'data.findAllStaff.data', []));
    } else {
      Sentry.log(_.get(data, 'error[0].message'), query);
    }
    return data;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

export const staffCheckInOutV3 = (reqData, accessToken) => async (dispatch) => {
  const query = mutationBuilder.getMutationAsString('staffCheckInOutV3', reqData);
  try {
    const data = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    return data;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

const dispatchGetStaffHealthRecords = (dispatch, data = {}) => {
  dispatch({
    type: GET_STAFF_HEALTH_RECORDS,
    value: data,
  });
};

export const getStaffHealthRecords = (reqData) => async (dispatch) => {
  try {
    const query = _mQueryBuilder.getQueryAsString('getStaffHealthRecords', reqData);
    const accessToken = await retrieveAsyncStorage('accessToken');
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    const data = get(result, 'data.getStaffHealthRecords[0].HealthCheckRecords', []);

    dispatchGetStaffHealthRecords(dispatch, data);
    return data;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

const dispatchAddStaffHealthRecord = (dispatch, data = {}) => {
  dispatch({
    type: ADD_STAFF_HEALTH_RECORD,
    value: data,
  });
};

export const addStaffHealthRecord = (reqData) => async (dispatch) => {
  try {
    const query = mutationBuilder.getMutationAsString('addStaffHealthRecordsAt', reqData);
    const accessToken = await retrieveAsyncStorage('accessToken');
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    dispatchAddStaffHealthRecord(dispatch, result);

    return result;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

export const getStaffMissingDatesRange = (reqParams) => async (dispatch) => {
  try {
    const query = _mQueryBuilder.getQueryAsString('getStaffMissingDatesRange', reqParams);
    const accessToken = await retrieveAsyncStorage('accessToken');
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    const data = get(result, 'data.getStaffMissingDatesRange', {});

    dispatchGetStaffMissingDatesRange(dispatch, data);
    return data;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

export const getStaffMissingRecordsReason = () => async (dispatch) => {
  try {
    const fkSchool = await retrieveAsyncStorage('schoolId');
    const accessToken = await retrieveAsyncStorage('accessToken');

    const reqParams = {
      searchCategory: 'missing_attendance_reason',
      filter: { fkSchool },
    };

    const query = _mQueryBuilder.getQueryAsString('findAllConfigByCategory', reqParams);
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    const data = {
      missingRecordReasons: get(result, 'data.findAllConfigByCategory.data', []).map((item) => ({
        ...item,
        name: item?.description,
      })),
    };

    dispatchGetStaffMissingRecordsReason(dispatch, data);
    return data;
  } catch (e) {
    Sentry.log(e.message);
  }
};

export const getStaffCICORecords = (reqParams) => async (dispatch) => {
  try {
    const accessToken = await retrieveAsyncStorage('accessToken');
    const params = {
      ...reqParams,
      pagination: { perPage: 100000, page: 1 },
    };

    const query = _mQueryBuilder.getQueryAsString('getStaffCheckInOutRecords', params);
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    const data = get(result, 'data.getStaffCheckInOutRecords.data', []);

    dispatchGetStaffCICORecords(dispatch, data);
    return data;
  } catch (e) {
    Sentry.log(e.message);
  }
};

export const addStaffMissingRecords = (reqData) => async (dispatch) => {
  try {
    const query = mutationBuilder.getMutationAsString('createMissingCheckInOutForStaff', reqData);
    const accessToken = await retrieveAsyncStorage('accessToken');
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });
    dispatchAddStaffMissingRecords(dispatch, result);

    return result;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

export const addStaffCheckInCheckOut = (reqData, accessToken) => async (dispatch) => {
  try {
    const query = mutationBuilder.getMutationAsString('addStaffCheckInCheckOut', reqData);
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });

    dispatchAddStaffCheckInCheckOut(dispatch, result);

    return result;
  } catch (ex) {
    Sentry.log(ex.message);
  }
};

export const loginByPin = (reqParams) => async (dispatch) => {
  try {
    const query = _mQueryBuilder.getQueryAsString('loginByPin', reqParams);
    const result = await skRequest({
      data: {
        query,
      },
      // userToken: accessToken,
      method: 'POST',
    });

    const token = get(result, 'data.loginByPin', '');

    dispatchLoginByPin(dispatch, token);
    return result;
  } catch (error) {
    Sentry.log(error.message);
  }
};

export const checkStaffHasMissingRecord = (reqParams) => async (dispatch) => {
  try {
    let centreID = await retrieveAsyncStorage('centreId');
    centreID = centreID ? parseInt(centreID) : 0;
    const accessToken = await retrieveAsyncStorage('accessToken');
    const query = _mQueryBuilder.getQueryAsString('hasMissingRecord', {
      centreID,
      ...reqParams
    });
    const result = await skRequest({
      data: {
        query,
      },
      userToken: accessToken,
      method: 'POST',
    });
    
    return result;
  } catch (e) {
    Sentry.log(e.message);
  }
};
