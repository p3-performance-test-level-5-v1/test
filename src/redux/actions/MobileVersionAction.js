import { skRequest } from '../api/BaseRequest';
import {
  QueryBuilder,
} from '../queryBuilder';
import {
  CHECK_MOBILE_VERSION,
} from './actionTypes';
import _ from 'lodash';
import Sentry from '../../sentry';

const _mQueryBuilder = new QueryBuilder();
_mQueryBuilder.setNewQuery('getLatestMobileVersion', {
  minSupportVersion: 'string',
  version: 'string',
  downloadURL: 'string'
});

const dispatchCheckMobileVersion = (dispatch, data = {}, error = null) => {
  dispatch({
    type: CHECK_MOBILE_VERSION,
    value: {
      data,
      error
    }
  });
};

export const checkMobileVersion = (reqData, userToken='') => async (dispatch) => {
  const query = _mQueryBuilder.getQueryAsString('getLatestMobileVersion', reqData);
  const params = {
    data: {
      query
    },
    userToken,
    method: 'POST'
  };
  try {
    const data = await skRequest(params);
    if (data.success) {
      dispatchCheckMobileVersion(dispatch, data);
    } else {
      dispatchCheckMobileVersion(dispatch, null, data.error);
      Sentry.log(_.get(data, 'error[0].message'), params);
    }
    return data;
  } catch (ex) {
    dispatchCheckMobileVersion(dispatch, null, { error: ex.message });
    Sentry.log(ex.message);
  }
};