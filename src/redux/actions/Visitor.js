import { skRequest } from '../api/BaseRequest';
import {
    QueryBuilder,
    MutationBuilder
} from '../queryBuilder';
import {
    FIND_ALL_CONFIG_BY_CATEGORY,
    INSERT_VISITOR_LOG,
    GET_VISITOR_LOGS,
    FETCH_SELF_ASSESSMENT_FORM,
} from './actionTypes';
import Sentry from '../../sentry';
const _mQueryBuilder = new QueryBuilder();
const mutationBuilder = new MutationBuilder();

_mQueryBuilder.setNewQuery('findAllConfigByCategory', {
    data: {
        ID: 'number',
        label: 'string',
        description: 'string'
    }
});

mutationBuilder.setNewMutation('insertVisitorLog', {
    ID: 'number'
});

const dispatchAllConfigByCategory = (dispatch, data = {}, error = null) => {
    dispatch({
        type: FIND_ALL_CONFIG_BY_CATEGORY,
        value: {
            data,
            error
        }
    });
};

const dispatchInsertVisitorLog = (dispatch, data = {}, error = null) => {
    dispatch({
        type: INSERT_VISITOR_LOG,
        value: {
            data,
            error
        }
    });
};

export const findAllConfigByCategory = reqData => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('findAllConfigByCategory', reqData);
    dispatchAllConfigByCategory(dispatch);
    try {
        const data = await skRequest({
            data: {
                query
            },
            method: 'POST'
        });
        if (data.success) {
            dispatchAllConfigByCategory(dispatch, data);
        } else {
            dispatchAllConfigByCategory(dispatch, null, data.error);
        }
        return data;
    } catch (ex) {
        dispatchAllConfigByCategory(dispatch, null, { error: ex.message });
    }
};

export const insertVisitorLog = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('insertVisitorLog', reqData);
    dispatchInsertVisitorLog(dispatch);
    try {
        const data = await skRequest({
            data: {
                query
            },
            userToken: accessToken,
            method: 'POST'
        });

        if (data.success) {
            dispatchInsertVisitorLog(dispatch, data);
        } else {
            dispatchInsertVisitorLog(dispatch, null, data.error);
        }
        return data;
    } catch (ex) {
        dispatchInsertVisitorLog(dispatch, null, { error: ex.message });
    }
};

_mQueryBuilder.setNewQuery('getVisitorLog', {
    totalCount: 'number',
    data: {
        ID: 'string',
        name: 'string',
        nric: 'string',
        countryCode: 'string',
        phone: 'string',
        temperature: 'string',
        checkInTime: 'Datetime',
        checkOutTime: 'Datetime',
        createdAt: 'Datetime',
        code: {
            ID: 'string',
            label: 'string',
            description: 'string',
        },
        user: {
            ID: 'string',
            firstname: 'string',
            lastname: 'string',
        }
    }
});

const dispatchGetVisitorLog = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_VISITOR_LOGS,
        value: {
            data,
            error
        }
    });
};

export const findVisitorLog = (reqData, accessToken) => async (dispatch) => {
    try {
        const batchReqData = [
            { getVisitorLog: [{filter: { nric: reqData.nric }}] },
            { getVisitorLog: [{filter: { countryCode: '+65', phone: reqData.nric }}] }
        ];
        const batchQuery = _mQueryBuilder.getBulkQueryAsStringFromArray(batchReqData);
        const data = await skRequest({
            data: { query: batchQuery },
            userToken: accessToken,
            method: 'POST'
        });
        if (data.success) {
            dispatchGetVisitorLog(dispatch, data);
        } else {
            dispatchGetVisitorLog(dispatch, null, data.error);
        }
        return data;
    } catch (ex) {
        dispatchGetVisitorLog(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

mutationBuilder.setNewMutation('checkoutVisitor', {});

export const checkoutVisitor = (reqData, accessToken) => async () => {
    const query = mutationBuilder.getMutationAsString('checkoutVisitor', reqData);
    try {
        const data = await skRequest({
            data: {
                query
            },
            userToken: accessToken,
            method: 'POST'
        });

        return data;
    } catch (ex) {
        Sentry.log(ex.message);
    }
};

_mQueryBuilder.setNewQuery('fetchSelfAssessmentForm', {
    totalCount: 'number',
    data: {
        ID: 'number',
        label: 'string',
        description: 'string'
    }
});

const dispatchAllSelfAssessment = (dispatch, data = {}, error = null) => {
    dispatch({
        type: FETCH_SELF_ASSESSMENT_FORM,
        value: {
            data,
            error
        }
    });
};

export const fetchSelfAssessmentForm = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('fetchSelfAssessmentForm', reqData);
    try {
        const data = await skRequest({
            data: {
                query
            },
            method: 'POST',
            userToken: accessToken,
        });
        if (data.success) {
            dispatchAllSelfAssessment(dispatch, data);
        } else {
            dispatchAllSelfAssessment(dispatch, null, data.error);
        }
        return data;
    } catch (ex) {
        dispatchAllSelfAssessment(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

mutationBuilder.setNewMutation('saveAssessmentOfVisitor', {});

export const saveAssessmentOfVisitor = (reqData, accessToken) => async () => {
    const query = mutationBuilder.getMutationAsString('saveAssessmentOfVisitor', reqData);
    try {
        const data = await skRequest({
            data: {
                query
            },
            userToken: accessToken,
            method: 'POST'
        });

        return data;
    } catch (ex) {
        Sentry.log(ex.message);
    }
};