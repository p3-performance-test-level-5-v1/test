import { skRequest } from '../api/BaseRequest';
import {
    QueryBuilder,
    MutationBuilder
} from '../queryBuilder';
import {
    FIND_PARENT_BY_IC,
    CHILD_CHECK_IN,
    CHILD_CHECK_OUT,
    AUTHORISE_BY_PASSCODE,
    GET_CHILDREN_STATES,
    CHILD_CHECK_IN_CHECK_OUT_V2,
    FIND_ALL_CHILD_BY_PARENT,
    PARENT_OR_GUARDIAN_SIGN_IN,
    PARENT_GUARDIAN_AUTH_TOKEN,
    UPDATE_LOADER_STATUS,
} from './actionTypes';
import _ from 'lodash';
import Sentry from '../../sentry';
import { childrenByCentreAndWithdrawn } from '../../utils';

const _mQueryBuilder = new QueryBuilder();
const mutationBuilder = new MutationBuilder();

_mQueryBuilder.setNewQuery('findParentByIC', {
    data: {
        ID: 'string',
        firstname: 'string',
        lastname: 'string',
        childRelations: {
            data: {
                child: {
                    ID: 'number',
                    firstname: 'string',
                    lastname: 'string',
                    imageKey: 'string',
                    dateOfBirth: 'Datetime',
                    birthCertificate: 'string',
                    currentLevel: {
                        fkCentre: 'number',
                        fkLevel: 'number',
                    }
                }
            }
        }
    }
});

mutationBuilder.setNewMutation('childCheckIn', {});

mutationBuilder.setNewMutation('childCheckOut', {});

_mQueryBuilder.setNewQuery('authorisedPersonalByPasscode', {
    ID: 'number',
    fkUser: 'string',
    validFrom: 'timestamp',
    validTo: 'timestamp',
    childAuthorisedPersonals: {
        data: {
            child: {
                ID: 'number',
                firstname: 'string',
                lastname: 'string',
                imageKey: 'string',
                currentLevel: {
                    fkChild: 'number',
                    fkCentre: 'number',
                    fkLevel: 'number',
                }
            }
        }
    }
});

_mQueryBuilder.setNewQuery('getChildrenStates', {
    childStates: {
      childId: 'number',
      checkInTime: 'Datetime',
      checkOutTime: 'Datetime'
    }
});

mutationBuilder.setNewMutation('childCheckInOutV2', {
    successes: '[number]',
    failures: {
        childId: 'number',
        errCode: 'string',
        errMessage: 'string'
    }
});

_mQueryBuilder.setNewQuery('authoriseParent', {});

_mQueryBuilder.setNewQuery('findAllChildrenByParent', {
    data: {
        ID: 'string',
        firstname: 'string',
        lastname: 'string',
        imageKey: 'string',
        birthCertificate: 'string',
        currentLevel: {
            ID: 'number',
            fkCentre: 'string',
        },
        guardianChildRelations: {
            data: {
                ID: 'number',
                fkRelation: 'number',
                guardian: {
                    ID: 'number',
                    firstname: 'string',
                    lastname: 'string',
                    identificationNo: 'string'
                }
            }
        },
        childRelations: {
            data: {
                ID: 'number',
                fkRelation: 'number',
                isGuardian: 'Boolean',
                parent: {
                    ID: 'number',
                    firstname: 'string',
                    lastname: 'string',
                    identificationNo: 'string',
                }
            }
        },
        classAttendances: {
            data: {
                ID: 'number',
                time: 'Datetime',
                date: 'Datetime',
                remarks: 'string'
            }
        },
        childClassAt: {
            class: {
                ID: 'number',
                label: 'string'
            }
        },
        childLevelAt: {
            level: {
                ID: 'number',
                label: 'string',
            },
            centre: {
                ID: 'number',
                label: 'string',
            }
        },
        checkInOuts: {
            data: {
                ID: 'number',
                type: 'CheckInOutType',
                date: 'Datetime',
                time: 'Datetime',
                remarks: 'string',
                status: 'CheckInOutStatus',
                source: 'CheckInOutSource',
                createdBy: 'number',
            }
        }
    }
});

mutationBuilder.setNewMutation('addChildCheckInCheckOut', {});

const dispatchFindParentByIC = (dispatch, data = {}, error = null) => {
    dispatch({
        type: FIND_PARENT_BY_IC,
        value: {
            data,
            error
        }
    });
};

const dispatchChildCheckIn = (dispatch, data = {}, error = null) => {
    dispatch({
        type: CHILD_CHECK_IN,
        value: {
            data,
            error
        }
    });
};

const dispatchChildCheckOut = (dispatch, data = {}, error = null) => {
    dispatch({
        type: CHILD_CHECK_OUT,
        value: {
            data,
            error
        }
    });
};

const dispatchAuthoriseByPasscode = (dispatch, data = {}, error = null) => {
    dispatch({
        type: AUTHORISE_BY_PASSCODE,
        value: {
            data,
            error
        }
    });
};

const dispatchGetChildrenStates = (dispatch, data = {}, error = null) => {
    dispatch({
        type: GET_CHILDREN_STATES,
        value: {
            data,
            error
        }
    });
};

export const findParentByIC = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('findParentByIC', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchFindParentByIC(dispatch);
    try {
        const data = await skRequest(params);
        
        if (data.success) {
            dispatchFindParentByIC(dispatch, data);
        } else {
            dispatchFindParentByIC(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchFindParentByIC(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const childCheckIn = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('childCheckIn', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchChildCheckIn(dispatch);
    try {
        const data = await skRequest(params);

        if (data.success) {
            dispatchChildCheckIn(dispatch, data);
        } else {
            dispatchChildCheckIn(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchChildCheckIn(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const childCheckOut = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('childCheckOut', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchChildCheckOut(dispatch);
    try {
        const data = await skRequest(params);

        if (data.success) {
            dispatchChildCheckOut(dispatch, data);
        } else {
            dispatchChildCheckOut(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchChildCheckOut(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const authorisedPersonalByPasscode = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('authorisedPersonalByPasscode', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchAuthoriseByPasscode(dispatch);
    try {
        const data = await skRequest(params);
        
        if (data.success) {
            dispatchAuthoriseByPasscode(dispatch, data);
        } else {
            dispatchAuthoriseByPasscode(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchAuthoriseByPasscode(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const getChildrenStates = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('getChildrenStates', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchGetChildrenStates(dispatch);
    try {
        const data = await skRequest(params);
        
        if (data.success) {
            dispatchGetChildrenStates(dispatch, data);
        } else {
            dispatchGetChildrenStates(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchGetChildrenStates(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

const dispatchChildCheckInOutV2 = (dispatch, data = {}, error = null) => {
    dispatch({
        type: CHILD_CHECK_IN_CHECK_OUT_V2,
        value: {
            data,
            error
        }
    });
};

export const childCheckInOutV2 = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('childCheckInOutV2', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    dispatchChildCheckInOutV2(dispatch);
    try {
        const data = await skRequest(params);

        if (data.success) {
            dispatchChildCheckInOutV2(dispatch, data);
        } else {
            dispatchChildCheckInOutV2(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchChildCheckInOutV2(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const authoriseParent = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('authoriseParent', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    try {
        dispatchParentOrGuardianAuthToken(dispatch);
        const data = await skRequest(params);
        if (data.success) {
            const token = _.get(data, 'data.authoriseParent', '');
            dispatchParentOrGuardianAuthToken(dispatch, { token });
        } else {
            dispatchParentOrGuardianAuthToken(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchParentOrGuardianAuthToken(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

const dispatchFindAllChildByParent = (dispatch, data = {}, error = null) => {
    dispatch({
        type: FIND_ALL_CHILD_BY_PARENT,
        value: {
            data,
            error
        }
    });
};

export const findAllChildrenByParent = (respFilter = {}, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('findAllChildrenByParent', {}, false, respFilter);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    try {
        dispatchFindAllChildByParent(dispatch);
        const data = await skRequest(params);
        const childLists = await childrenByCentreAndWithdrawn(_.get(data, 'data.findAllChildrenByParent.data', []));
        if (data.success) {
            dispatchFindAllChildByParent(dispatch, { data: childLists });
        } else {
            dispatchFindAllChildByParent(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return childLists;
    } catch (ex) {
        dispatchFindAllChildByParent(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

export const addChildCheckInCheckOut = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('addChildCheckInCheckOut', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    try {
        const data = await skRequest(params);
        if (!data.success) {
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        Sentry.log(ex.message);
    }
};

const dispatchParentOrGuardianAuthToken = (dispatch, data = {}, error = null) => {
    dispatch({
        type: PARENT_GUARDIAN_AUTH_TOKEN,
        value: {
            data,
            error
        }
    });
};

export const dispatchLoaderStatus = (dispatch, status = false) => {
    dispatch({
        type: UPDATE_LOADER_STATUS,
        value: status
    });
};

_mQueryBuilder.setNewQuery('authoriseGuardian', {});

export const authoriseGuardian = (reqData, accessToken) => async (dispatch) => {
    const query = _mQueryBuilder.getQueryAsString('authoriseGuardian', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    try {
        dispatchParentOrGuardianAuthToken(dispatch);
        const data = await skRequest(params);
        if (data.success) {
            const token = _.get(data, 'data.authoriseGuardian', '');
            dispatchParentOrGuardianAuthToken(dispatch, { token });
        } else {
            dispatchParentOrGuardianAuthToken(dispatch, null, data.error);
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        dispatchParentOrGuardianAuthToken(dispatch, null, { error: ex.message });
        Sentry.log(ex.message);
    }
};

const dispatchParentOrGuardianSignIn = (dispatch, status = false) => {
    dispatch({
        type: PARENT_OR_GUARDIAN_SIGN_IN,
        value: status
    });
};

export const parentOrGuardianSignIn = (isGuardian = false) => async (dispatch) => {
    dispatchParentOrGuardianSignIn(dispatch, isGuardian);
}

mutationBuilder.setNewMutation('guardianAddChildCheckInCheckOut', {});

export const guardianAddChildCheckInCheckOut = (reqData, accessToken) => async (dispatch) => {
    const query = mutationBuilder.getMutationAsString('guardianAddChildCheckInCheckOut', reqData);
    const params = {
        data: {
            query
        },
        userToken: accessToken,
        method: 'POST'
    };
    try {
        const data = await skRequest(params);
        if (!data.success) {
            Sentry.log(_.get(data, 'error[0].message'), params);
        }
        return data;
    } catch (ex) {
        Sentry.log(ex.message);
    }
};
