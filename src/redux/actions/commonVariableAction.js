import { SET_VARIABLE_TO_REDUX } from './actionTypes';
import Sentry from '../../sentry';
const dispatchVariableValue = (dispatch, data = {}) => {
  dispatch({
    type: SET_VARIABLE_TO_REDUX,
    key: 'setVariable',
    value: data,
  });
};

export const setValueToRedux = (variableName, value) => async dispatch => {
  try {
    const result = {};
    result[variableName] = value;
    dispatchVariableValue(dispatch, result);
  } catch (err) {
    Sentry.log(err.message);
  }
};
