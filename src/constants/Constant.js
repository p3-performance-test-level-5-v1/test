export default {
  IOS: 'ios',
  DEFAULT_APP_VERSION: '0.0.0',
  KEYBOARD_OFFSET: 50,
  TCC: 'TCC',
  MFS: 'MFS',
  LSH: 'LSH',
  CHECK_IN: 'check in',
  CHECK_OUT: 'check out',
  CHECK_IN_CAPS: 'Check-In',
  CHECK_OUT_CAPS: 'Check-Out',
  CHECKED_IN: 'checked in',
  CHECKED_OUT: 'checked out',
  RETRY_ATTEMPT: 3,
  RETRY_INITIAL_DELAY: 300,
  NETWORK_ERROR_TEXT: 'network error',
  SHOULD_FIND_USER_BY_PHONE: 'should_find_user_by_phone',
  TYPE_CHECK_IN: 'check_in',
  TYPE_CHECK_OUT: 'check_out',
  TYPE_ADD_TEMPERATURE: 'add_temperature',
  NON_PARENT_RELATIONSHIP: 'non_parent_relationship',
  RELATION_CHILD: 'Relation_Child',
  VERIFIED: 'verified',
  PENDING: 'pending',
  DECLINED: 'declined',
  KIOSK: 'kiosk',
  INCORRECT_PASSWORD_ERROR: 'incorrect password',
  STAFF_SEARCH_REGEX: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@/,
  PRESENT: 'present',
  CHECKIN: 'check_in',
  CHECKOUT: 'check_out',
  ARRIVAL: 'arrival',
  CHARACTER_LIMIT: 50,
}

// used to check app env from config.
export const APP_ENVIRONMENTS = {
  dev: 'development',
  stg: 'staging',
  prd: 'production',
  local: 'local',
};

export const TIME_FORMAT = {
  DATA_TRANFERS: 'YYYY-MM-DD HH:mm:ss',
  DATE_RANGE_PICKER: 'YYYYMMDD',
  TIME_PICKER: 'hh:mm A',
  DATE: 'YYYY-MM-DD'
};
