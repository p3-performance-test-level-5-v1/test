import Home from "../screens/Home";
import ChooseRole from "../screens/ChooseRole";
import SchoolLogin from "../screens/SchoolLogin";
import ChildSignIn from "../screens/ChildSignIn";
import StaffSignIn from "../screens/StaffSignIn";
import BusAttendant from "../screens/BusAttendant";
import BusSignIn from "../screens/Bus/BusSignIn";
import ChildAttendance from "../screens/ChildAttendance";
import SignedInSuccess from "../screens/SignedInSuccess";
import VisitorSignIn from "../screens/VisitorSignIn";
import VisitorSignedIn from "../screens/VisitorSignedIn";
import StaffSignInSuccess from "../screens/StaffSignInSuccess";
import BusAttendantSuccess from "../screens/BusAttendantSuccess";
import AuthValidator from "../screens/AuthValidator";
import CheckInCheckOut from "../screens/CheckInCheckOut";
import AuthorisedPerson from "../screens/AuthorisedPerson";
import BusRouteList from "../screens/BusRouteList";
import Visitor from '../screens/Visitor/Visitor';
import VisitorCheckOut from '../screens/VisitorCheckOut/VisitorCheckOut';
import VisitorCheckIn from '../screens/VisitorCheckIn/VisitorCheckIn';
import CheckInOutSuccess from '../screens/Visitor/CheckInOutSuccess';
import SelfAssessment from '../screens/SelfAssessment/SelfAssessment';
import StaffCheckIn from '../screens/StaffCheckIn/StaffCheckIn';
import MissingRecord from '../screens/MissingRecord/MissingRecord';
import ParentOrGuardian from '../screens/ParentOrGuardian/ParentOrGuardian';
import Guardian from '../screens/Guardian/GuardianView';
import ChildMissingRecord from '../screens/ChildMissingRecord/ChildMissingRecord';
import ChildCheckInCheckOut from '../screens/ChildCheckInOut/ChildCheckInOut';
import ChildCheckInOutSuccess from '../screens/ChildCheckInOut/ChildCheckInOutSuccess';
import ChildCheckInOut from '../screens/ChildCheckInOut';
import BusCheckInCheckOut from '../screens/Bus/BusCheckInCheckOut';
import BusCheckInOutSuccess from '../screens/Bus/BusCheckInOutSuccess';
import StaffCheckOut from '../screens/StaffCheckOut/StaffCheckOut';

const Routes = {
	SchoolLogin: {
		name: "SchoolLogin",
		description: "School Login",
		screen: SchoolLogin,
		path: "snKiosk/schoolLogin",
		navigationOptions: {
			header: null
		}
	},
	AuthValidatorRoute: {
	    name: 'AuthValidator',
	    description: 'Auth Validator checks for token',
	    screen: AuthValidator,
	    path: 'snKiosk/authValidator',
	    navigationOptions: {
			header: null
	    }
	},
	Home: {
		name: "Home",
		description: "User Home",
		screen: Home,
		path: "snKiosk/home",
		navigationOptions: {
			header: null,
			gestureEnabled: false,
		}
	},
	ChooseRole: {
		name: "ChooseRole",
		description: "User ChooseRole",
		screen: ChooseRole,
		path: "snKiosk/chooseRole",
		navigationOptions: {
			header: null
		}
	},
	ChildSignIn: {
		name: "ChildSignIn",
		description: "Child Sign In",
		screen: ChildSignIn,
		path: "snKiosk/childSignIn",
		navigationOptions: {
			header: null
		}
	},
	StaffSignIn: {
		name: "StaffSignIn",
		description: "Staff Sign In",
		screen: StaffSignIn,
		path: "snKiosk/staffSignIn",
		navigationOptions: {
			header: null
		}
	},
	BusCheckInCheckOut: {
		name: "BusCheckInCheckOut",
		description: "Bus BusCheckInCheckOut",
		screen: BusCheckInCheckOut,
		path: "snKiosk/busCheckinCheckout",
		navigationOptions: {
			header: null
		}
	},
	BusSignIn: {
		name: "BusSignIn",
		description: "School Bus Sign In",
		screen: BusSignIn,
		path: "snKiosk/busSignIn",
		navigationOptions: {
			header: null,
			gestureEnabled: false,
		}
	},
	BusCheckInOutSuccess:{
		name: "BusCheckInOutSuccess",
		description: "Bus Check In - Out Success",
		screen: BusCheckInOutSuccess,
		path: "snKiosk/busCheckInOutSuccess",
		navigationOptions: {
			header: null,
			gestureEnabled: false,
		}
	},
	ChildAttendance: {
		name: "ChildAttendance",
		description: "Child Attendance",
		screen: ChildAttendance,
		path: "snKiosk/childAttendance",
		navigationOptions: {
			header: null
		}
	},
	SignedInSuccess: {
		name: "SignedInSuccess",
		description: "Signed In Success",
		screen: SignedInSuccess,
		path: "snKiosk/signedInSuccess",
		navigationOptions: {
			header: null
		}
	},
	VisitorSignIn: {
		name: "VisitorSignIn",
		description: "Visitor Sign In",
		screen: VisitorSignIn,
		path: "snKiosk/visitorSignIn",
		navigationOptions: {
			header: null
		}
	},
	VisitorSignedIn: {
		name: "VisitorSignedIn",
		description: "Visitor Signed In",
		screen: VisitorSignedIn,
		path: "snKiosk/visitorSignedIn",
		navigationOptions: {
			header: null
		}
	},
	StaffSignInSuccess: {
		name: "StaffSignInSuccess",
		description: "Staff Sign In Success",
		screen: StaffSignInSuccess,
		path: "snKiosk/staffSignInSuccess",
		navigationOptions: {
			header: null
		}
	},
	BusAttendantSuccess: {
		name: "BusAttendantSuccess",
		description: "Bus Attendant Success",
		screen: BusAttendantSuccess,
		path: "snKiosk/busAttendantSuccess",
		navigationOptions: {
			header: null
		}
	},
	CheckInCheckOut: {
		name: "CheckInCheckOut",
		description: "Checkin Checkout Choice",
		screen: CheckInCheckOut,
		path: "snKiosk/checkInCheckOut",
		navigationOptions: {
			header: null
		}
	},
	AuthorisedPerson: {
		name: "AuthorisedPerson",
		description: "Authorised Person Checkin/Checkout",
		screen: AuthorisedPerson,
		path: "snKiosk/authorisedPerson",
		navigationOptions: {
			header: null
		}
	},
	Visitor: {
		name: "Visitor",
		description: "Visitor CheckIn CheckOut",
		screen: Visitor,
		path: "snKiosk/visitorCheckInCheckOut",
		navigationOptions: {
			header: null
		}
	},
	VisitorCheckOut: {
		name: "VisitorCheckOut",
		description: "Visitor CheckOut",
		screen: VisitorCheckOut,
		path: "snKiosk/visitorCheckOut",
		navigationOptions: {
			header: null
		}
	},
	VisitorCheckIn: {
		name: "VisitorCheckIn",
		description: "Visitor CheckIn",
		screen: VisitorCheckIn,
		path: "snKiosk/visitorCheckIn",
		navigationOptions: {
			header: null
		}
	},
	CheckInOutSuccess: {
		name: "CheckInOutSuccess",
		description: "Check In Check Out Success",
		screen: CheckInOutSuccess,
		path: "snKiosk/checkInOutSuccess",
		navigationOptions: {
			header: null
		}
	},
	// SelfAssessment: {
	// 	name: "SelfAssessment",
	// 	description: "Self Assessment",
	// 	screen: SelfAssessment,
	// 	path: "snKiosk/selfAssessment",
	// 	navigationOptions: {
	// 		header: null
	// 	}
	// },
	StaffCheckIn: {
		name: "StaffCheckIn",
		description: "Staff CheckIn",
		screen: StaffCheckIn,
		path: "snKiosk/staffCheckIn",
		navigationOptions: {
			header: null
		}
	},
	MissingRecord: {
		name: "MissingRecord",
		description: "Missing Record",
		screen: MissingRecord,
		path: "snKiosk/MissingRecord",
		navigationOptions: {
			header: null
		}
	},
	Guardian: {
		name: "Guardian",
		description: "Guardian CheckIn/CheckOut",
		screen: Guardian,
		path: "snKiosk/guardian",
		navigationOptions: {
			header: null
		}
	},
	ParentOrGuardian: {
		name: "ParentOrGuardian",
		description: "Parent/Guardian CheckIn/CheckOut",
		screen: ParentOrGuardian,
		path: "snKiosk/parentOrGuardian",
		navigationOptions: {
			header: null
		}
	},
	ChildMissingRecord: {
		name: "ChildMissingRecord",
		description: "Child Missing Record",
		screen: ChildMissingRecord,
		path: "snKiosk/childMissingRecord",
		navigationOptions: {
			header: null
		}
	},
	ChildCheckInOut: {
		name: "ChildCheckInOut",
		description: "Child CheckIn/CheckOut Option",
		screen: ChildCheckInOut,
		path: "snKiosk/childCheckInOut",
		navigationOptions: {
			header: null
		}
	},
	ChildCheckInCheckOut: {
		name: "ChildCheckInCheckOut",
		description: "Child CheckIn/CheckOut",
		screen: ChildCheckInCheckOut,
		path: "snKiosk/childCheckInCheckOut",
		navigationOptions: {
			header: null
		}
	},
	ChildCheckInOutSuccess: {
		name: "ChildCheckInOutSuccess",
		description: "Child CheckIn CheckOut Success",
		screen: ChildCheckInOutSuccess,
		path: "snKiosk/childCheckInOutSuccess",
		navigationOptions: {
			header: null
		}
	},
	StaffCheckOut: {
		name: "StaffCheckOut",
		description: "Staff CheckOut",
		screen: StaffCheckOut,
		path: "snKiosk/staffCheckOut",
		navigationOptions: {
			header: null
		}
	},
};
export default Routes;
