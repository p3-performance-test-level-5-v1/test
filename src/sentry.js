import * as Sentry from '@sentry/react-native';
import { SENTRY_ENDPOINT, APP_NAME, SCHOOL, SENTRY_ENV } from './redux/config/index';
import { getTrackingStatus, requestTrackingPermission } from 'react-native-tracking-transparency';
import { get, isEmpty } from 'lodash';

let trackingStatus = null;


function ifTrackingAvailable() {
  if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
    return true;
  }

  return false;
}

export default class SentryHandler {
  static async  init() {
    trackingStatus = await requestTrackingPermission();
    trackingStatus = await getTrackingStatus();

    if (!ifTrackingAvailable()) {
      return;
    }

    Sentry.init({
      dsn: SENTRY_ENDPOINT,
      enableAutoSessionTracking: true,
      environment: SENTRY_ENV,
    });

    Sentry.setExtras({
      APP_NAME,
      SCHOOL,
    });

    Sentry.setTags({
      "environment": SENTRY_ENV,
      "react": true,
      "APP_NAME": APP_NAME,
      "schoolId": SCHOOL,
    });
  }

  // currentScreen used to define owner
  static setAdditionalTags ({ currentScreen }) {
    if (!ifTrackingAvailable()) {
      return;
    }

    Sentry.setTags({
      "currentScreen": currentScreen
    });
  }

  static log (message, extra = {}, level = Sentry.Severity.Log) {
    if (!ifTrackingAvailable()) {
      return;
    }

    let tempMessage = message;

    try {
      tempMessage = JSON.stringify(tempMessage);
    } catch(e) {

    }

    Sentry.configureScope((scope) => {
      !isEmpty(extra) && scope.setExtras(extra);
      Sentry.captureMessage(tempMessage, level);
    })
  }
}
