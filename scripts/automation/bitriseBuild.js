const request = require('request');
const https = require('https');
const fs = require('fs');
const { trigger_token, access_token } = require('../config/config');
const appBaseURL = 'https://app.bitrise.io';
const apiBaseURL = 'https://api.bitrise.io/v0.1';
const appId = '25977d7ffa7a0bda';

const args = process.argv.slice(2);
const branch = args[0];
const appSchool = args[1];
const appEnv = args[2];
const workflow_id = 'iOS-All-Env-Adhoc';

const dataString = `{"hook_info":{"type":"bitrise","build_trigger_token":"${trigger_token}"},"build_params":{"branch":"${branch}","workflow_id":"${workflow_id}","environments":[{"mapped_to":"CUSTOM_SCHOOL","value":"${appSchool}","is_expand":true},{"mapped_to":"CUSTOM_API_ENV","value":"${appEnv}","is_expand":true}]},"triggered_by":"curl"}`;

const headers = {
  accept: 'application/json',
  Authorization: access_token,
};

const triggerBuild = () => {
  return new Promise((resolve, reject) => {
    request.post(
      {
        url: `${appBaseURL}/app/${appId}/build/start.json`,
        headers: {
          'Content-Type': 'application/json',
        },
        body: dataString,
      },
      (err, resp, body) => {
        if (err) {
          console.log('err: ', err);
          return reject(null);
        }
        const result = JSON.parse(body);
        resolve(result);
      }
    );
  });
};

const buildStatus = build_slug => {
  return new Promise((resolve, reject) => {
    request.get(
      {
        url: `${apiBaseURL}/apps/${appId}/builds/${build_slug}`,
        headers,
      },
      (err, resp, body) => {
        if (err) {
          return reject(null);
        }
        const result = JSON.parse(body);
        resolve(result.data);
      }
    );
  });
};

const getAtrifatcs = build_slug => {
  return new Promise((resolve, reject) => {
    request.get(
      {
        url: `${apiBaseURL}/apps/${appId}/builds/${build_slug}/artifacts`,
        headers,
      },
      (err, resp, body) => {
        if (err) {
          return reject(null);
        }
        const result = JSON.parse(body);
        const data = result.data;
        const artifact = data.find(item => item.artifact_type === 'ios-ipa');
        if (artifact) resolve(artifact.slug);
      }
    );
  });
};

const getFileUrl = (build_slug, artifacts_slug) => {
  return new Promise((resolve, reject) => {
    request.get(
      {
        url: `${apiBaseURL}/apps/${appId}/builds/${build_slug}/artifacts/${artifacts_slug}`,
        headers,
      },
      (err, resp, body) => {
        if (err) {
          return reject(null);
        }
        const result = JSON.parse(body);
        const data = result.data;
        if (data) resolve(data.expiring_download_url);
      }
    );
  });
};

const downLoadfile = fileUrl => {
  https.get(fileUrl, res => {
    const path = `${__dirname}/kiosk.ipa`;
    const filePath = fs.createWriteStream(path);
    res.pipe(filePath);
    filePath.on('finish', () => {
      filePath.close();
      console.log('Download Completed');
      process.exit(0);
    });
  });
};

const checkBuildStatus = async (build_slug, status, build_url) => {
  console.log('current build status: ', status);
  if (status === 'in-progress' || status === 'on-hold' || status === 'ok') {
    setTimeout(() => {
      buildStatus(build_slug).then(res => {
        if (res) {
          checkBuildStatus(build_slug, res.status_text);
        }
      });
    }, 30000);
  } else {
    if (status === 'success') {
      const artifacts_slug = await getAtrifatcs(build_slug);
      if (artifacts_slug) {
        const fileUrl = await getFileUrl(build_slug, artifacts_slug);
        if (fileUrl) {
          downLoadfile(fileUrl);
        }
      }
    } else {
      console.log(
        '\nBitrise build has failed. Please check the details here: \n\n'
      );
      console.log(build_url);
      process.exit(1);
    }
  }
};

const bitriseFunctions = async () => {
  const buildData = await triggerBuild();
  if (buildData) {
    const { build_slug, status, build_url } = buildData;
    const bStatus = status === 'ok' ? 'in-progress' : status;
    checkBuildStatus(build_slug, bStatus, build_url);
  } else {
    console.log('failed');
    process.exit(1);
  }
};

bitriseFunctions();
// checkBuildStatus('e245098a-3b26-4d16-894b-c67d9cd6d33f', 'in-progress');
