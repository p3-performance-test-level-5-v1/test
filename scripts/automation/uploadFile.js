const request = require('request');
const https = require('https');
const fs = require('fs');
const updateSheet = require('./googleSheet');
const { browserstack_username, browserstack_access_key } = require('../config/config');

const args = process.argv.slice(2);
const appSchool = args[0] || 'Adhoc';
const appEnv = args[1] || 'All-Env';

const browserstack_url = "https://api-cloud.browserstack.com/app-automate/upload";

const uploadFile = () => {
  const curlCommand =
    `curl -u "${browserstack_username}:${browserstack_access_key}" -X POST "${browserstack_url}" -F "file=@kiosk.ipa"`;
  var exec = require('child_process').exec;
  var child = exec(curlCommand);
  child.stdout.on('data', res => {
    if (res) {
      const result = JSON.parse(res);
      const { app_url } = result;
      if (!app_url) {
        console.log('upload failed...');
        process.exit(1);
      }
      const appDetails = ['ios', 'kiosk', appSchool, appEnv, app_url];
      console.log('\nupload completed...\n');
      console.log('\napp_url: ', app_url);
      console.log('\n');
      updateSheet(appDetails);
    }
  });
  child.stderr.on('data', data => {
    if (data) {
      console.log('data: ', data);
    }
  });
  child.on('close', code => {
    // console.log('code: ', code);
  });
};
uploadFile();
