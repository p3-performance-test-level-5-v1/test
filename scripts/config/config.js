module.exports = {
    trigger_token: process.env.bitrise_trigger_token || undefined,
    access_token: process.env.bitrise_access_token || undefined,
    browserstack_username: process.env.browserstack_username || undefined,
    browserstack_access_key: process.env.browserstack_access_key || undefined
};