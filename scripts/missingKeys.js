var fs = require("fs"),
  readline = require("readline");
const axios = require("axios").default;

const url =
  "https://skoolnet-stg.s3.ap-southeast-1.amazonaws.com/localisation/en.json";
const en_json = "../src/translations/en/index.js";
var paths = [
  "../src/components",
  "../src/constants",
  "../src/navigations",
  "../src/screens",
  "../src/redux",
  "../src/utils",
];
var files = [];
const otherFiles = ["../src/App.js"];

var getFiles = function (path, files) {
  fs.readdirSync(path).forEach(function (file) {
    var subpath = path + "/" + file;
    if (fs.lstatSync(subpath).isDirectory()) {
      getFiles(subpath, files);
    } else {
      if (!file.includes("style.")) {
        files.push(path + "/" + file);
      }
    }
  });
};
for (var i = 0; i < paths.length; i++) {
  getFiles(paths[i], files);
}

function promiseHandler(f) {
  return new Promise(function (resolve, reject) {
    const keys = [];
    var rd = readline.createInterface({
      input: fs.createReadStream(f),
    });
    rd.on("line", function (line) {
      var regex = /[t][(]'(\w+.\w+)'[)]/;
      var matched = regex.exec(line);
      if (matched) {
        const key = matched[1];
        if (key.includes(".")) {
          keys.push(key);
        }
      }
    });
    rd.on("close", function () {
      resolve(keys);
    });
  });
}

function getKeys(files) {
  const promises = [];
  const newFiles = files.concat(otherFiles);
  newFiles.forEach((f) => {
    promises.push(promiseHandler(f));
  });
  return Promise.all(promises).then(function (list) {
    return list.reduce(function (agg, item) {
      return agg.concat(item);
    }, []);
  });
}

function objHasKeys(obj, keys) {
  var next = keys.shift();
  return obj[next] && (!keys.length || objHasKeys(obj[next], keys));
}

function keyHandler(k) {
  return new Promise(function (resolve, reject) {
    resolve(k);
  });
}

function readFile() {
  return new Promise((resolve, reject) => {
    fs.readFile(en_json, (err, data) => {
      if (err) throw err;
      resolve(data);
    });
  });
}

function s3Search(keys) {
  const promises = [];
  return axios
    .get(url)
    .then((res) => {
      if (res && res.data) {
        const json = res.data;
        keys.forEach((i) => {
          const key = i.split(".");
          const isKeyExist = objHasKeys(json, key);
          if (!isKeyExist) {
            promises.push(keyHandler(i));
          }
        });
      }
      return Promise.all(promises);
    })
    .catch((e) => console.error(e));
}

function localSearch(keys) {
  const promises = [];
  return readFile().then((data) => {
    const enJson = JSON.parse(data);
    if (enJson) {
      keys.forEach((i) => {
        const key = i.split(".");
        const isKeyExist = objHasKeys(enJson, key);
        if (!isKeyExist) {
          promises.push(keyHandler(i));
        }
      });
    }
    return Promise.all(promises);
  });
}
getKeys(files)
  .then((results) => {
    const uniqueKeys = results.filter(function (elem, pos) {
      return results.indexOf(elem) == pos;
    });
    return new Promise((resolve, reject) => {
      localSearch(uniqueKeys).then((local) => {
        if (local && local.length > 0) {
          console.log(
            `\n------- Compare localization keys with local json file --------`
          );
          console.log("\nkeys not exist:\n");
          local.map((i) => console.log(`"${i}"`));
          resolve(true);
        } else {
          resolve(false);
        }
      });
    }).then((status) => {
      return new Promise((resolve, reject) => {
        s3Search(uniqueKeys).then((s3) => {
          if (s3 && s3.length > 0) {
            console.log(
              `\n------- Compare localization keys with S3 file --------`
            );
            console.log(`\ns3 file url : ${url}`);
            console.log("\nkeys not exist:\n");
            s3.map((i) => console.log(`"${i}"`));
            resolve(status || true);
          } else {
            resolve(status || false);
          }
        });
      });
    });
  })
  .then((status) => {
    if (status) {
      process.exit(1);
    } else {
      process.exit(0);
    }
  })
  .catch((e) => console.log(e));
